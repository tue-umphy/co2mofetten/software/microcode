# Contributing to this Repository

## Code Style

The code in this repository **must** follow a strict code style.
This style is defined in the `.clang-format` configuration file which is
interpreted by the
[`clang-format`](https://clang.llvm.org/docs/ClangFormat.html) program.

It is not necessary to worry so much about the style during coding, but
conformity to the code style is asserted in the [CI
Pipelines](https://gitlab.com/tue-umphy/co2mofetten/microcode/pipelines).
[Merge
Requests](https://gitlab.com/tue-umphy/co2mofetten/microcode/merge_requests)
with failing jobs will not be merged.

To format a sketch according to the code style, the following command can be
used:

```bash
clang-format --style=file -i path/to/sketch.ino
```

To format all C/C++ source code files at once, run from the repository root:

```bash
git ls-files | xargs -L1 file -i | perl -ne 'print if s|^(.*):\s+text/x-c.*|$1|g' | xargs -r clang-format -i
```

On macOS one apparently has to use `file -I` instead of `file -i` in the code
above.

If that doesn't work, the following should also do:

```bash
git ls-files '*.cpp' '*.hpp' '*.h' '*.ino' | xargs -r clang-format -i
```
