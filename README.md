# Microcontroller Code

This directory contains the firmware for our microcontroller-based sensor stations.

## How To Use

This repository is a [PlatformIO](https://platformio.org) project and can be built and flashed as usual. There are
[instructions on installing PlatformIO in the Wiki](https://gitlab.com/tue-umphy/co2mofetten/co2mofetten/wikis/platformio).

The usual drill is:

```bash
# Just compile firmware (to check)
pio run -e d1
# Compile & Flash firmware to a ESP8266 (D1 Mini)
pio run -e d1 -t upload
# Upload configuration files to an ESP8266
pio run -e d1 -t uploadfs
# Connect to serial interface
pio device monitor
```

## Setup

You can check how this firmware is embedded into the sensor network [in this graph](https://gitlab.com/tue-umphy/co2mofetten/co2mofetten-ops/-/wikis/Network).

From the point of view of the µController, an exemplary setup for an ESP8622 is the following:

```mermaid

graph LR

  central((Central Raspberry Pi))

  uc((µController))
  
  central -.-|WLAN| uc
  
  uc -->|I2C| rtc(Real-Time Clock)
  uc -->|SPI| sd(SD-Card)
  uc -->|I2C| co2sensor(CO2-Sensor)
  uc -->|...| extra(...)

```

## Firmware

The firmware is built using the [Arduino](https://arduino.cc) framework which is available for many microcontrollers. [PlatformIO](https://platformio.org) automatically handles installation of the correct version of the framework. This firmware was written to work with the [Arduino Mega 2560](https://store.arduino.cc/arduino-mega-2560-rev3) and the [ESP8266 (D1 Mini variant)](https://www.wemos.cc/en/latest/d1/d1_mini.html).


The firmware is structured as follows:

- It's **not** a [Real-Time Operating System (RTOS)](https://en.wikipedia.org/wiki/Real-time_operating_system), so really **only one task can be performed at a time**.
- A **scheduler** manages execution of repeated tasks. For example, there are tasks for handling the connected components regularly:

   ```mermaid
   sequenceDiagram
       loop continuously
         alt if is due
           µController-->Component: handle
         end
       end
   ```
   
- This scheduler design-pattern has a couple of drawbacks:
   - Slow tasks are delaying other tasks.
   - It is only possible to define a **minimum interval** between executions of a specific. The exact interval depends on the duration of the tasks executed in between.
   - If a tasks freezes (e.g. gets caught up in an endless loop - that can happen with bad libraries), the whole µController stalls and doesn't continue.
- However, this design was easy to implement and is flexible enough for this use case.




