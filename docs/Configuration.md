# Software Configuration

## PlatformIO

This project uses [PlatformIO](https://platformio.org) to build and flash the
microcontroller code.

You can find instructions for installing and using PlatformIO to build and
flash the code in this project [here](docs/PlatformIO.md).

