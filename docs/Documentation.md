# Doxygen Documentation

[![documentation](https://img.shields.io/badge/documentation-on%20GitLab-green.svg)](https://gitlab.com/tue-umphy/co2mofetten/co2mofetten/-/jobs/artifacts/master/file/doxygen-html/index.html?job=doxygen)

This [doxygen](https://doxygen.nl) documentation can be created by running
`doxygen` from this folder. The HTML files then reside unter the `html` folder.
Open the `index.html` file to view the pages.

> ## Prerequisites
>
> To install the prerequisites, run
>
> ```bash
> sudo apt-get update && sudo apt-get install -y doxygen graphviz
> ```

An online version can also be found
[here](https://gitlab.com/tue-umphy/co2mofetten/co2mofetten/-/jobs/artifacts/master/file/doxygen-html/index.html?job=doxygen)
