# Hardware Setup

All of the below hardware is optional. If support for a specific hardware
component is compiled-in via the Compilation Flags (see the Usage of
[PlatformIO](docs/PlatformIO.md) and @ref ConfigurationFlags), but a component
wasn't found or does not work, the code should skip it and continue to run.

> Note: The shields are listed in order from bottom to top

## [Wireless SD Shield](https://store.arduino.cc/arduino-wireless-sd-shield)

> Note: For the SD card to work, the ICSP header needs to be connected (which
> is the case if the Wireless SD Shield is the first shield on the Arduino).

> Note: Basically, this shield is just a convenience to easily connect an XBee
> module **and** and SD-card to the Arduino. Other ways of connection should
> also be fine but might need some tuning.

- XBee support compiled in if #WITH_XBEE is true
- XBee plugged in (+antenna)
- SD card formatted with FAT32 and plugged in
- pins 0 and 1 bent away (RX,TX)
- ”Serial Select”-switch set to ”Micro”
- connect XBee serial to Serial1:
  - connect Pin 0 to Pin 19
  - connect Pin 1 to Pin 18

## [Adafruit Ultimate GPS+Logging Shield](https://www.adafruit.com/product/1272)

> Note: This shield also has an SD-card slot. To use this slot instead of the
> Wireless SD Shield slot one has to connect the SPI pins correctly and set
> #SD_CHIPSELECT_PIN to `10`.

- support compiled in if #WITH_GPS is true
- (+antenna)
- Serial connection to Serial2:
  - bend away Pins 7 and 8
  - connect Pin 7 to Pin 16
  - connect Pin 8 to Pin 17
- set the Switch to "Soft. Serial" on the Shield

## [Adafruit TCA9548A I²C Multiplexer](https://learn.adafruit.com/adafruit-tca9548a-1-to-8-i2c-multiplexer-breakout/overview)

- only used if #WITH_I2C_MULTIPLEXER is true
- connected via I²C
- is used to realize connecting multiple sensor packets at different
  #SENSOR_HEIGHTS

## Bosch BME280 environmental I²C sensor

Either [the Adafruit version](https://www.adafruit.com/product/2652) or [the GY
version](https://www.amazon.de/GY-bme280-Precision-barometrischer-Temperatur-Luftfeuchtigkeit/dp/B079BWWSRP)
connected via I²C to provide pressure, temperature and humidity readings.

- support compiled in if #WITH_BME280 is true
- connected via I²C either directly to the Arduino or behind the Multiplexer

## [Sensirion SCD30 CO2 sensor](https://www.sensirion.com/en/environmental-sensors/carbon-dioxide-sensors-co2/)

- support compiled in if #WITH_SCD30 is true
- connected via I²C either directly to the Arduino or behind the Multiplexer

## [INA219 Current Sensor](https://www.adafruit.com/product/904)

- support compiled in if #WITH_ENERGYLOGGER is true
- connected via I²C directly to the Arduino
