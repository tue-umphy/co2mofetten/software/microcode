# µC Code {#mainpage}

This is the documentation for the microcontroller code used in the
[CO2-Mofetten Project](https://fit.uni-tuebingen.de/Activity/Details?id=5564).

The code resides [on
GitLab.com](https://gitlab.com/tue-umphy/co2mofetten/co2mofetten).

Have a look at the [Hardware](docs/Hardware.md) and Software
[Configuration](docs/Configuration.md) setup.
