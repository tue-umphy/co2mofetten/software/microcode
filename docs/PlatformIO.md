# PlatformIO

## Installation

First, make sure you have `pip` for Python 3 installed:

```bash
sudo apt-get -qq update && sudo apt-get -y install python3-pip
python3 -m pip install -U pip
```

To install PlatformIO, run

```bash
python3 -m pip install --user -U platformio
```

If something fails with setuptools

```bash
sudo apt-get -y install python3-setuptools
```

### Udev rules

For platformIO to work properly, you also need to [install some udev
rules](https://docs.platformio.org/en/latest/faq.html#platformio-udev-rules):

```bash
sudo apt-get update -qq && sudo apt-get install -y curl
curl -fsSL https://raw.githubusercontent.com/platformio/platformio-core/master/scripts/99-platformio-udev.rules | sudo tee /etc/udev/rules.d/99-platformio-udev.rules
```

### Invocation

You can use PlatformIO via the `pio` command:

```bash
pio --version
```

If it says `pio: Command not found` or something alike,
you need to set up your `PATH` accordingly. Run the following
to add the path to your shell configuration file (here `~/.bashrc`):

```bash
echo 'export PATH="$HOME/.local/bin:$PATH"' >> ~/.bashrc
```

Reopen your terminal or run `source ~/.bashrc` to let this take effect.

### Permissions

To be able to talk to serial devices, the user has to be in the `dialout`
group. Run the following to make sure this is the case:

```bash
groups | grep -q dialout && echo "Already in dialout group" \
  || (sudo usermod -aG dialout $USER;echo "Now log in again or reboot")
```

## Usage

For this project, you will need two PlatformIO commands: `pio run` for
compilation and flashing and `pio device monitor` for connecting and
monitoring.

### Compilation

To compile or check the code, open a terminal in the project directory
containing the `platformio.ini` file and the `src` folder and run:

```bash
pio run
```

This will install all needed libraries and compile the code for all defined
environments. The environments are defined in the `platformio.ini` file and are
basically groups of settings to simplify compilation and flashing. To compile
the code only for a specific environment, run

```bash
pio run -e ENVIRONMENT # e.g. pio run -e mega-station-debug
```

To flash the code to a connected microcontroller, the `-t upload` option has to
be specified:

```bash
pio run -e ENVIRONMENT -t upload
```

If more than one serial port is connected, the port to flash can be selected
with the `--upload-port PORT` option:

```bash
pio run -e ENVIRONMENT -t upload --upload-port PORT # e.g. /dev/ttyACM0
```

#### Setting Flags

The code can be configured by setting **compiler flags** (see
@ref ConfigurationFlags) via the `PLATFORMIO_BUILD_FLAGS` environment variable. An
environment variable can be set as an extra command via

```bash
export PLATFORMIO_BUILD_FLAGS='... bla bla bla ...'
```

... or by prepending it to a command to execute:

```bash
PLATFORMIO_BUILD_FLAGS='... bla bla bla ...' pio run
```

The compilation flags modifying the code are described in the @ref
ConfigurationFlags and throughout this documentation. To set a compilation flag
to a specific value, the `PLATFORMIO_BUILD_FLAGS` need to contain the string
`-D<FLAGNAME>=<FLAGVALUE>`. For example, to set the #SERIAL_BAUDRATE to 115200
and to disable the BME280 sensor via #WITH_BME280, set the
`PLATFORMIO_BUILD_FLAGS` like this:

```bash
PLATFORMIO_BUILD_FLAGS='-DSERIAL_BAUDRATE=115200 -DWITH_BME280=false' pio run
```
