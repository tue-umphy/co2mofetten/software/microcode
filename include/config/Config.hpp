#pragma once

/**
  @file Config.hpp
  @brief Configuration functions
*/

#include "../utils/LogLevels.hpp"
#include "Defaults.hpp"

#include <ArduinoJson.h>

#if !NATIVE
#include <StreamUtils.h>
#endif // #if ! NATIVE

#if ESP8266
#include <FS.h>
#endif // #if ESP8266

#if ARDUINO_ARCH_AVR
#include <ArduinoSTL.h>
#endif // #if ARDUINO_ARCH_AVR

#if !defined(CONFIG_JSON_FILENAME) || DOXYGEN_RUN
/**
 * @ingroup WiFiFlags
 * @brief LittleFS filename of hardware layout JSON file
 */
#define CONFIG_JSON_FILENAME "/config.json"
#endif // #ifndef CONFIG_JSON_FILENAME

#ifndef LOGGING_DEBUG_RAM_INITIAL
#define LOGGING_DEBUG_RAM_INITIAL false
#endif // #ifndef LOGGING_DEBUG_RAM_INITIAL

#if ESP8266
#include <LittleFS.h>
#endif // #if ESP8266

/**
  @brief Configuration functions
*/
namespace Config {

using namespace Logging;

#ifdef BUILD_FLAGS
const PROGMEM char buildFlagsJson[] = BUILD_FLAGS;
#endif // #ifdef BUILD_FLAGS

struct Configuration
{
  Configuration()
    : runtimeLogLevelMin(LOGLEVEL_MIN)
    , runtimeLogLevelMax(LOGLEVEL_INITIAL)
    , runtimeChannels(LOGGING_CHANNELS_INITIAL)
    , runtimeDebugRam(LOGGING_DEBUG_RAM_INITIAL)
#if WITH_WIFI
    , wifiKeepConnecting(true)
#endif // #if WITH_WIFI
#if WITH_SERIAL_DATA_STREAM
    , runtimeSerialDataStream(SERIAL_DATA_STREAM_INITIAL)
#endif // #if WITH_SERIAL_DATA_STREAM
  {}

  //! Minimum runtime loglevel to show
  int runtimeLogLevelMin;
  //! Maximum runtime loglevel to show
  int runtimeLogLevelMax;
  //! Bitmask for channels to show at runtime
  uint64_t runtimeChannels;
  //! Whether to debug ram during runtime
  bool runtimeDebugRam;
#if WITH_WIFI
  //! whether to keep connecting to WiFi
  bool wifiKeepConnecting;
#endif // #if WITH_WIFI
#if WITH_SERIAL_DATA_STREAM
  bool runtimeSerialDataStream;
#endif // #if WITH_SERIAL_DATA_STREAM
  //! name of the station
  std::string stationName;

  const char* getStationName(void)
  {
    return this->stationName.empty() ? STRINGIFY_MACRO(DEFAULT_STATION_NAME)
                                     : this->stationName.c_str();
  }

  bool toJson(JsonObject json)
  {
    if (not json["name"].set(this->getStationName()))
      return false;
    auto logging = json.createNestedObject("logging");
    auto loggingLevel = logging.createNestedArray("level");
    if (not loggingLevel[0].set(this->runtimeLogLevelMin))
      return false;
    if (not loggingLevel[1].set(this->runtimeLogLevelMax))
      return false;
#if ARDUINOJSON_USE_LONG_LONG
    if (not logging["channels"].set(this->runtimeChannels))
      return false;
#else  // #if ARDUINOJSON_USE_LONG_LONG
    auto loggingChannel = logging.createNestedArray("channels");
    if (not loggingChannel[0].set(
          (uint32_t)(this->runtimeChannels >> (sizeof(uint32_t) * 8)) &
          ~(uint32_t)0))
      return false;
    if (not loggingChannel[1].set(
          (uint32_t)(this->runtimeChannels & ~(uint32_t)0)))
      return false;
#endif // #if ARDUINOJSON_USE_LONG_LONG
    if (not logging["debug-ram"].set(this->runtimeDebugRam))
      return false;
#if WITH_SERIAL_DATA_STREAM
    if (not json["serial-data-stream"].set(this->runtimeSerialDataStream))
      return false;
#endif // #if WITH_SERIAL_DATA_STREAM
#if WITH_WIFI
    auto wifi = json.createNestedObject("wifi");
    if (not wifi["keep-connecting"].set(this->wifiKeepConnecting))
      return false;
#endif // #if WITH_WIFI
    return true;
  }

#if ESP8266
  bool readFromFs(Stream& file)
  {
    const size_t capacity = ESP.getMaxFreeBlockSize() -
                            sizeof(DynamicJsonDocument) -
                            1000; // also reserve some stack space
    DynamicJsonDocument doc(capacity);
    DeserializationError err = deserializeJson(doc, file);
    doc.shrinkToFit();
    if (err)
      return false;
    if (doc["name"].is<char*>())
      this->stationName = doc["name"].as<char*>();
    // Logging
    auto loggingDebugRam = doc["logging"]["debug-ram"];
    if (loggingDebugRam.is<bool>())
      this->runtimeDebugRam = loggingDebugRam;
    auto loggingChannels = doc["logging"]["channels"];
    if (loggingChannels.is<JsonArray>())
      this->runtimeChannels = (uint64_t)loggingChannels[0].as<uint32_t>()
                                << (sizeof(uint32_t) * 8) |
                              loggingChannels[1].as<uint32_t>();
#if ARDUINOJSON_USE_LONG_LONG
    else if (loggingChannels.is<uint64_t>())
      this->runtimeChannels = loggingChannels;
#endif // #if ARDUINOJSON_USE_LONG_LONG
    else
      return false;
    auto loggingLevel = doc["logging"]["level"];
    if (loggingLevel.is<JsonArray>()) {
      this->runtimeLogLevelMin = loggingLevel[0];
      this->runtimeLogLevelMax = loggingLevel[1];
    } else if (loggingLevel.is<int>())
      this->runtimeLogLevelMax = loggingLevel;
#if WITH_SERIAL_DATA_STREAM
    auto serialDataStream = doc["serial-data-stream"];
    if (serialDataStream.is<bool>())
      this->runtimeSerialDataStream = serialDataStream.as<bool>();
#endif // #if WITH_SERIAL_DATA_STREAM
#if WITH_WIFI
    this->wifiKeepConnecting = doc["wifi"]["keep-connecting"] | true;
#endif // #if WITH_WIFI
    return true;
  }

  bool readFromFs(void)
  {
    if (LittleFS.begin()) {
      File file = LittleFS.open(CONFIG_JSON_FILENAME, "r");
      if (not file)
        return false;
      ReadBufferingStream bufferedFile(file, 64);
      const bool success = this->readFromFs(bufferedFile);
      file.close();
      return success;
    }
    return false;
  };

  bool writeToFs(void)
  {
    const size_t capacity = ESP.getMaxFreeBlockSize() -
                            sizeof(DynamicJsonDocument) -
                            1000; // also reserve some stack space
    DynamicJsonDocument doc(capacity);
    if (not this->toJson(doc.to<JsonObject>()))
      return false;
    doc.shrinkToFit();
    if (LittleFS.begin()) {
      File file = LittleFS.open(CONFIG_JSON_FILENAME, "w");
      if (not file)
        return false;
      WriteBufferingStream bufferedFile(file, 64);
      const bool success = serializeJson(doc, bufferedFile);
      bufferedFile.flush();
      file.close();
      return success;
    }
    return false;
  }
#endif // #if ESP8266

  bool read(void)
  {
#if ESP8266
    if (this->readFromFs())
      return true;
#endif // #if ESP8266
    return false;
  }
};

Configuration config;
} // namespace Config
