#pragma once

/**
 @file Defaults.h
 @brief Default Compiler Flag Configuration
*/

/**
 *  @defgroup ConfigurationFlags Configuration Flags
 *  @{
 */

#if !defined(NATIVE) || DOXYGEN_RUN
/**
  @brief Whether code is compiled on a native platform, not a µController
*/
#define NATIVE false
#endif // #ifndef NATIVE

#if !defined(WITH_SERIAL) || DOXYGEN_RUN
/**
  @brief Whether to use Serial at all
*/
#define WITH_SERIAL true
#endif // #ifndef WITH_SERIAL

#if !defined(SERIAL_BAUDRATE) || DOXYGEN_RUN
/**
  @brief Serial baudrate
  @note only useful if #WITH_SERIAL is true
*/
#define SERIAL_BAUDRATE 9600
#endif // #ifndef SERIAL_BAUDRATE

#if !defined(MEASUREMENT_INTERVAL_MS) || DOXYGEN_RUN
/**
  @brief Amount of milliseconds between measurements
*/
#define MEASUREMENT_INTERVAL_MS 10e3
#endif // #ifndef MEASUREMENT_INTERVAL_MS

#if !defined(BLINK_WHEN_IDLE) || DOXYGEN_RUN
/**
  @brief Whether to pulse the #IDLE_BLINK_PIN
  @note
    - The blinking frequency can be changed with #IDLE_BLINK_INTERVAL_MS
*/
#define BLINK_WHEN_IDLE 0
#endif // #ifndef BLINK_WHEN_IDLE

#if !defined(IDLE_BLINK_INTERVAL_MS) || DOXYGEN_RUN
/**
  @brief Blinking period in milliseconds when #BLINK_WHEN_IDLE is enabled
*/
#define IDLE_BLINK_INTERVAL_MS 500
#endif // #ifndef IDLE_BLINK_INTERVAL_MS

#if !defined(IDLE_BLINK_PIN) || DOXYGEN_RUN
/**
  @brief Blinking pin when #BLINK_WHEN_IDLE is enabled
*/
#define IDLE_BLINK_PIN LED_BUILTIN
#endif // #ifndef IDLE_BLINK_PIN

#if !defined(WITH_SERIAL_DATA_STREAM) || DOXYGEN_RUN
/**
  @brief Whether to continuously stream measurement data on a serial port
  @note The serial port can be selected with #SERIAL_DATA_STREAM_PORT
*/
#define WITH_SERIAL_DATA_STREAM true
#endif // #ifndef WITH_SERIAL_DATA_STREAM

#if !defined(SERIAL_DATA_STREAM_INITIAL) || DOXYGEN_RUN
/**
  @brief Initial runtime state for #WITH_SERIAL_DATA_STREAM
*/
#define SERIAL_DATA_STREAM_INITIAL false
#endif // #ifndef SERIAL_DATA_STREAM_INITIAL

#if !defined(SERIAL_DATA_STREAM_PORT) || DOXYGEN_RUN
/**
  @brief The serial port for streaming measurement data continuously
  @note The baudrate can be set via #SERIAL_DATA_STREAM_BAUDRATE
*/
#define SERIAL_DATA_STREAM_PORT Serial
#endif // #ifndef SERIAL_DATA_STREAM_PORT

#if !defined(SERIAL_DATA_STREAM_BAUDRATE) || DOXYGEN_RUN
/**
  @brief The baudrate for #SERIAL_DATA_STREAM_PORT
*/
#define SERIAL_DATA_STREAM_BAUDRATE 115200
#endif // #ifndef SERIAL_DATA_STREAM_BAUDRATE

#if !defined(DEFAULT_STATION_NAME) || DOXYGEN_RUN
/**
  @brief The human-readable default name of this station
  @details Will be stringified with #STRINGIFY_MACRO, so quotes are not
  necessary. Should only contain [a-zA-Z0-9] as it might be used in MQTT
  topics, as file- or hostname, etc...
*/
#define DEFAULT_STATION_NAME test
#endif // #ifndef DEFAULT_STATION_NAME

#define STRINGIFY(x) #x
#define STRINGIFY_MACRO(x) STRINGIFY(x)

#if !defined(WITH_OTA) || DOXYGEN_RUN
/**
  @brief Whether OTA should be used
*/
#define WITH_OTA true
#endif // #ifndef WITH_OTA

#if !defined(WITH_MQTT) || DOXYGEN_RUN
/**
 * @ingroup MQTTFlags
 * @brief Whether to enable MQTT functionality
 */
#if ESP8266
#define WITH_MQTT true
#else // #if ESP8266
#define WITH_MQTT false
#endif // #if ESP8266
#endif // #ifndef WITH_MQTT

#if !defined(WITH_WIFI) || DOXYGEN_RUN
/**
 * @ingroup WiFiFlags
 * @brief Whether to enable WiFi functionality
 */
#if ESP8266
#define WITH_WIFI true
#else // #if ESP8266
#define WITH_WIFI false
#endif // #if ESP8266
#endif // #ifndef WITH_WIFI

#if !defined(WITH_UNITCONVERSION) || DOXYGEN_RUN
/**
  @brief Whether to enable unit conversion
*/
#define WITH_UNITCONVERSION false
#endif // #ifndef WITH_UNITCONVERSION

#ifndef LOGPRINT
#define LOGPRINT Serial
#endif // #ifndef LOGPRINT

#ifndef LOGLEVEL_MAX
//! Maximum compiled-in logging level, can contain #Level constants
#define LOGLEVEL_MAX Debug
#endif // #ifndef LOGLEVEL_MAX

#ifndef LOGLEVEL_MIN
//! Minimum compiled-in logging level, can contain #Level constants
#define LOGLEVEL_MIN 0
#endif // #ifndef LOGLEVEL

#ifndef LOGLEVEL_INITIAL
//! Initially shown minimum logging level, can contain #Level constants
#define LOGLEVEL_INITIAL LOGLEVEL_MAX
#endif // #ifndef LOGLEVEL_INITIAL

#ifndef LOGGING_CHANNELS
//! Bitmask for enabled channels, can contain #Channel constants
#define LOGGING_CHANNELS ~(uint64_t)0
#endif // #ifndef LOGGING_CHANNELS

#ifndef LOGGING_CHANNELS_INITIAL
//! Initially enabled logging channels, can contain #Channel constants
#define LOGGING_CHANNELS_INITIAL ~(uint64_t)0
#endif // #ifndef LOGGING_CHANNELS_INITIAL

#ifndef LOGGING_DEBUG_RAM
//! Whether to show the amount of available RAM on each LOG() call
#define LOGGING_DEBUG_RAM false
#endif // #ifndef LOGGING_DEBUG_RAM

#include "Native.hpp"

/**
 *  }@
 */
