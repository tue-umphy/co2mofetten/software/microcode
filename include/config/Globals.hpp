#pragma once

/**
  @file Globals.hpp
  @brief Global variables
*/

#include "Defaults.hpp"
#include "utils/Logging.hpp"
#include "utils/Utils.hpp"

#ifndef PAUSED_INITIAL
//! Whether to pause the sketch initially
#define PAUSED_INITIAL false
#endif // #ifndef PAUSED_INITIAL

/**
  @brief Global variables
*/
namespace Globals {

//! whether the application is currently paused
bool paused = PAUSED_INITIAL;
//! whether the application is was previously paused
bool previouslyPaused = PAUSED_INITIAL;

// global pressure variable in hPa
float pressurehPa = 0.0F / 0.0F;

} // namespace Globals
