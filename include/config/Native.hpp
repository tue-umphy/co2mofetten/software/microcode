#pragma once

/**
  @file Native.hpp
  @brief Fixing code for native platforms
*/

#ifndef NATIVE
//! Whether the software is compiled on a native platform
#define NATIVE false
#endif // #ifndef NATIVE

#if NATIVE || DOXYGEN_RUN
//! macro to enable its argument only if #NATIVE is true
#define WHEN_NATIVE(x) x
#define WHEN_NATIVE_ELSE(ifcase, elsecase) ifcase
#define UNLESS_NATIVE(x)
#define UNLESS_NATIVE_ELSE(ifcase, elsecase) elsecase
#else // #if NATIVE
//! macro to enable its argument only if #NATIVE is true
#define WHEN_NATIVE(x)
#define WHEN_NATIVE_ELSE(ifcase, elsecase) elsecase
#define UNLESS_NATIVE(x) x
#define UNLESS_NATIVE_ELSE(ifcase, elsecase) ifcase
#endif // #if NATIVE

#if NATIVE
//! Ignore PROGMEM and F string definitions
#define PSTR(x) x
#define F(x) x
#endif // #if NATIVE

#if NATIVE
#include <cstddef>
#include <cstdint>
#include <cstdio>

//! Just call snprintf internally instead of snprintf_P
template<typename... Args>
int
snprintf_P(char* s, size_t n, const char* fmt, Args... args)
{
  return snprintf(s, n, fmt, args...);
}
#endif // #if NATIVE
