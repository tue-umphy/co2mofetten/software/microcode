#pragma once

/**
  @file Cli.hpp
  @brief Command-line Interface via the Serial interface
*/

/**
  @defgroup CliFlags Command-Line Interface Flags
  @ingroup ConfigurationFlags
*/

#if !defined(WITH_CLI) || DOXYGEN_RUN
/**
  @ingroup CliFlags
  @brief Whether to provide a CLI
*/
#define WITH_CLI true
#endif // #ifndef WITH_CLI

#if !defined(WITH_SERIAL_CLI) || DOXYGEN_RUN
/**
  @ingroup CliFlags
  @brief Whether to provide a CLI on the Serial interface
*/
#define WITH_SERIAL_CLI true
#endif // #ifndef WITH_SERIAL_CLI

#include "../config/Config.hpp"
#include "../config/Defaults.hpp"
#include "../config/Globals.hpp"
#include "../utils/Concatenator.hpp"
#include "../utils/Logging.hpp"
#include "../utils/MultiStream.hpp"
#include "../utils/TimeUtils.hpp"
#include "../utils/UnitConversion.hpp"
#include "../utils/Utils.hpp"

#include "EEPROM.hpp"
#include "Hardware.hpp"
#include "Mqtt.hpp"
#include "Multiplexer.hpp"
#include "Scheduler.hpp"
#include "WatchDog.hpp"
#include "WiFi.hpp"

#if WITH_CLI

#if ESP8266
#include <FS.h>
#include <LittleFS.h>
#include <StreamUtils.h>
#endif // #if ESP8266

#if ARDUINO_ARCH_AVR
#include <EEPROM.h>
#endif // #if ARDUINO_ARCH_AVR

#include <ArduinoJson.h>
#include <Shell.h>
#include <StreamUtils.h>
#include <Time.h>
#include <TimeLib.h>
#include <limits.h>

/**
  @brief Command-line Interface via the Serial interface
*/

namespace Cli {

Scheduler::Scheduler<1
#if WITH_MQTT_CLI
                     + 1
#endif
                     >
  scheduler;

MultiStream<0
#if WITH_SERIAL_CLI
            + 1
#endif // #if WITH_SERIAL_CLI
#if WITH_MQTT_CLI
            + 1
#endif // #if WITH_MQTT_CLI
            >
  cliStream;

Concatenator cat(cliStream);

uint8_t currentHeightIndex = 0;

int
serialReader(char* data)
{
  if (cliStream.available()) {
    *data = cliStream.read();
    return 1;
  }
  return 0;
}

void
serialWriter(char data)
{
  cliStream.write(data);
}

void
printSketchInfo(Print& output)
{
  output.println();
#if defined(__DATE__) || defined(__TIME__) ||                                 \
  defined(COMPILATION_TIMESTAMP_UTC)
  output.print(F("Sketch compiled at:"));
#if defined(__DATE__)
  output.print(" ");
  output.print(F(__DATE__));
#endif // #if defined(__DATE__)
#if defined(__TIME__)
  output.print(" ");
  output.print(F(__TIME__));
  output.print(F(" local time"));
#endif // #if defined(__TIME__)
#if defined(COMPILATION_TIMESTAMP_UTC)
  output.print(", unix UTC timestamp ");
  output.print(COMPILATION_TIMESTAMP_UTC);
#endif // #if defined(COMPILATION_TIMESTAMP_UTC)
  output.println();
  output.flush();
#endif // #if defined(__DATE__) || defined(__TIME__) ||
       // defined(COMPILATION_TIMESTAMP_UTC)
#ifdef SHELL_USER
  output.print(F("Sketch compiled by: "));
  output.println(F(SHELL_USER));
  output.flush();
#endif // #ifdef USER
#ifdef SHELL_HOST
  output.print(F("Sketch compiled on: "));
  output.println(F(SHELL_HOST));
  output.flush();
#endif // #ifdef USER
#ifdef GIT_REV
  output.print(F("Sketch git revision: "));
  output.print(F(GIT_REV));
  output.println();
  output.flush();
#endif // #if GIT_REV
#ifdef BUILD_FLAGS
  output.println("Build flags: ");
  StaticJsonDocument<1000> doc;
  deserializeJson(doc, (const __FlashStringHelper*)Config::buildFlagsJson);
  serializeJsonPretty(doc, output);
  output.println();
  output.flush();
#endif // #if BUILD_FLAGS
}

int
cliInfoFunc(int argc, char** argv)
{
  printSketchInfo(cliStream);
  return 0;
}

int
cliPauseFunc(int argc, char** argv)
{
  Globals::paused = true;
  cliStream.println("sketch paused");
#if WITH_WATCHDOG
  cliStream.println("watchdog disabled");
  WatchDog.disable();
#endif // #if WITH_WATCHDOG
  return 0;
}

int
cliResumeFunc(int argc, char** argv)
{
  Globals::paused = false;
  cliStream.println("sketch resumed");
  return 0;
}

int
systemUnixTimeFunc(int argc, char** argv)
{
  if (argc == 1) {
    uint32_t ms = 0;
#if TIMELIB_ENABLE_MILLIS
    const auto timeNow = now(ms);
#else  // #if TIMELIB_ENABLE_MILLIS
    const auto timeNow = now();
#endif // #if TIMELIB_ENABLE_MILLIS
    cliStream.println(timeNow);
    LOG(Logging::Hardware + 2, Debug + 1, {
      Timestamp timestamp(timeNow, ms);
      char str[16] = { 0 };
      cat("Timestamp(",
          timeNow,
          ',',
          ms,
          ").toString()",
          " = ",
          timestamp.toString(str),
          ", ",
          "epochSeconds()",
          " = ",
          timestamp.epochSeconds(),
          ", ",
          "epochMilliseconds()",
          " = ",
          timestamp.epochMilliseconds(),
          ", ",
          "epochMillisecondsFraction()",
          " = ",
          timestamp.epochMillisecondsFraction(),
          "\n");
    });
    return 0;
  } else if (argc == 2) {
    const unsigned long givenTime = strtoul(argv[1], NULL, 0);
    if (givenTime == 0 or givenTime == ULONG_MAX) {
      cliStream.print(F("Error: invalid UNIX-timestamp "));
      cliStream.println(argv[1]);
      return 1;
    } else {
      setTime(givenTime);
      cliStream.print(F("adopted time "));
      cliStream.print(givenTime);
      cliStream.print(" (");
      TimeUtils::printDateTime(cliStream, givenTime);
      cliStream.print(")");
      cliStream.println();
      return 0;
    }
  } else {
    cliStream.println(F("Usage: ... unixtime [UNIXTIMESTAMP]"));
    return 1;
  }
}

int
systemTimeFunc(int argc, char** argv)
{
  if (argc == 1) {
    TimeUtils::printDateTime(cliStream);
    return 0;
  } else {
    cliStream.println(F("Usage: ... time"));
    return 1;
  }
}

void
cliSystemHelp(Print& output)
{
  output.print(
    F("Usage: system [time | unixtime [UNIXTIMESTAMP] | heap | stack]"));
  output.println();
}

int
cliSystemFunc(int argc, char** argv)
{
  if (argc <= 1) {
    cliSystemHelp(cliStream);
    return 10;
  } else {
    if (strcmp(argv[1], "unixtime") == 0) {
      return systemUnixTimeFunc(argc - 1, argv + 1);
    } else if (strcmp(argv[1], "time") == 0) {
      return systemTimeFunc(argc - 1, argv + 1);
    } else if (strcmp(argv[1], "stack") == 0) {
      cliStream.print("approx. stack size: ");
      cliStream.print(Utils::stackSize());
      cliStream.println(" bytes");
      return 0;
    } else if (strcmp(argv[1], "heap") == 0) {
#if ARDUINO_ARCH_AVR
      {
        cliStream.print(F("__malloc_heap_start = "));
        cliStream.println((uintptr_t)__malloc_heap_start);
        cliStream.print(F("__malloc_heap_end = "));
        cliStream.println((uintptr_t)__malloc_heap_end);
        cliStream.print(F("heap total size: "));
        auto maxHeap = (ptrdiff_t)(__malloc_heap_start - __malloc_heap_end);
        cliStream.println(maxHeap);
        cliStream.print(F("largest possible malloc(): "));
        auto freeHeap = Utils::largestMalloc();
        cliStream.print(freeHeap);
        cliStream.println(" bytes");
        cliStream.print(F("fragmentation: "));
        cliStream.print(Utils::heapFragmentation(freeHeap, maxHeap) * 100.0);
        cliStream.println("%");
      }
#endif // #if ARDUINO_ARCH_AVR
#if ESP8266
      {
        const unsigned long freeHeap = ESP.getFreeHeap();
        cliStream.print(F("ESP.getFreeHeap() = "));
        cliStream.print(freeHeap);
        cliStream.println(" bytes");
        const unsigned long maxFreeBlock = ESP.getMaxFreeBlockSize();
        cliStream.print(F("ESP.getMaxFreeBlockSize() = "));
        cliStream.print(maxFreeBlock);
        cliStream.println(" bytes");
        cliStream.print(F("fragmentation: "));
        cliStream.print(Utils::heapFragmentation(maxFreeBlock, freeHeap) *
                        100.0);
        cliStream.println("%");
      }
#endif // #if ESP8266
      return 0;
    }
    cliSystemHelp(cliStream);
    return 11;
  }
}

bool
printJson(JsonVariant json)
{
  serializeJson(json, cliStream);
  cliStream.println();
  return true;
}

int
cliHelpFunc(int argc, char** argv)
{
  cliStream.println(F("sketchinfo     - print sketch info"));
  cliStream.println(F("log            - query or set runtime log level"));
  cliStream.println(F("pause          - pause sketch"));
  cliStream.println(F("resume         - resume sketch operation"));
  cliStream.println(F("system         - system functions"));
  cliStream.println(F("convert        - convert units"));
  cliStream.println(F("config         - view, read or save config"));
  cliStream.println(F("hardware       - access hardware"));
#if WITH_WIFI
  cliStream.println(F("wifi           - WiFi functionality"));
#endif //#if WITH_WIFI
  cliStream.println(F("help           - print this help"));
  return 0;
}

#if WITH_SERIAL_DATA_STREAM
void
outputHelpFunc(Print& output)
{
  cliStream.print(F("Usage: output [on|off]"));
}

int
outputFunc(int argc, char** argv)
{
  if (argc == 1) {
    cliStream.println(Config::config.runtimeSerialDataStream ? "on" : "off");
    return 0;
  } else if (argc == 2) {
    if (strcmp(argv[1], "on") == 0) {
      Config::config.runtimeSerialDataStream = true;
      cliStream.println("on");
      return 0;
    }
    if (strcmp(argv[1], "off") == 0) {
      Config::config.runtimeSerialDataStream = false;
      cliStream.println("off");
      return 0;
    }
  }
  outputHelpFunc(cliStream);
  return 1;
}
#endif // #if WITH_SERIAL_DATA_STREAM

#if WITH_WIFI
void
wifiHelpFunc(Print& output)
{
  output.println(F("wifi [info | pause | resume | ping [HOST [SECONDS]] | "
                   "connect [SSID [PASSWORD]] | disconnect]"));
}

int
wifiFunc(int argc, char** argv)
{
  if (not argc) {
    wifiHelpFunc(cliStream);
    return 1;
  }
  if (strncmp_P(argv[1], PSTR("info"), 4) == 0) {
    cliStream.print(F("SSID: "));
    cliStream.println(WiFi.SSID());
    cliStream.print(F("RSSI: "));
    cliStream.println(WiFi.RSSI());
    cliStream.print(F("gateway IP: "));
    cliStream.println(WiFi.gatewayIP());
    cliStream.print(F("local IP: "));
    cliStream.println(WiFi.localIP());
    WiFi.printDiag(cliStream);
    return 0;
  }
  if (strncmp(argv[1], "pause", 5) == 0) {
    cliStream.print("Pausing");
    cliStream.println(" WiFi reconnecting attempts");
    Config::config.wifiKeepConnecting = false;
    return 0;
  }
  if (strncmp(argv[1], "resume", 6) == 0) {
    cliStream.print("Resuming");
    cliStream.println(" WiFi reconnecting attempts");
    Config::config.wifiKeepConnecting = true;
    return 0;
  }
  if (strncmp_P(argv[1], PSTR("disconnect"), 10) == 0) {
#if WITH_MQTT
    cliStream.println(F("Disconnecting MQTT client"));
    MQTT::disconnect();
#endif // #if WITH_MQTT
    cliStream.println(F("Disconnecting from WiFi"));
    WiFi.disconnect();
    return 0;
  }
  if (strcmp(argv[1], "connect") == 0) {
    cliStream.print(F("Attempting to connect to SSID '"));
    cliStream.print(argv[2]);
    cliStream.print("'");
    if (argc > 3) {
      cliStream.print(" with password '");
      cliStream.print(argv[3]);
      cliStream.print("'");
      WiFi.begin(argv[2], argv[3]);
    } else
      WiFi.begin(argv[2]);
    cliStream.println("...");
    wl_status_t lastStatus = WiFi.status();
    unsigned long time_before = millis();
    while (true) { // wait for connection
      yield();
      if (millis() - time_before > WIFI_CONNECT_TIMEOUT_MS) {
        cliStream.print(F("Stop waiting for connection on '"));
        cliStream.print(argv[2]);
        cliStream.println(F("', takes too long..."));
        break;
      }
      const wl_status_t status = WiFi.status();
      if (status != lastStatus) {
        cliStream.print(F("WiFi.Status() = "));
        cliStream.println(status);
        lastStatus = status;
      }
      if (status == WL_CONNECTED or status == WL_CONNECT_FAILED)
        break; // stop waiting for connection
    }
    if (WiFi.status() == WL_CONNECTED)
      cliStream.print(F("Connected to network "));
    else
      cliStream.print(F("Couldn't connect to WiFi network "));
    cliStream.println(argv[2]);
    return 0;
  }
  if (argc > 2) {
    if (strcmp(argv[1], "ping") == 0) {
      const size_t seconds = argc > 3 ? strtoul(argv[3], NULL, 0) : 1;
      const bool hostUp = Ping.ping(argv[2], seconds);
      cliStream.print(F("Host "));
      cliStream.print(argv[2]);
      cliStream.print(F(" is "));
      cliStream.println(hostUp ? "up" : "down");
      return 0;
    }
  }
  wifiHelpFunc(cliStream);
  return 1;
}

#if WITH_MQTT
void
mqttHelpFunc(Print& output)
{
  output.println(F("mqtt [status | pause | resume | "
                   "reconnect | publish TOPIC MESSAGE]"));
}

int
mqttFunc(int argc, char** argv)
{
  if (not argc) {
    mqttHelpFunc(cliStream);
    return 1;
  }
  if (argc == 2) {
    if (strncmp(argv[1], "status", 6) == 0) {
      cliStream.print(F("connected: "));
      cliStream.println(MQTT::client.connected() ? "yes" : "no");
      return 0;
    }
    if (strcmp(argv[1], "pause") == 0) {
      MQTT::paused = true;
      return 0;
    }
    if (strcmp(argv[1], "resume") == 0) {
      MQTT::paused = false;
      return 0;
    }
    if (strncmp(argv[1], "reconnect", 9) == 0) {
      cliStream.print(F("reconnection "));
      cliStream.println(MQTT::reconnect() ? "successful" : "failed");
      return 0;
      mqttHelpFunc(cliStream);
      return 1;
    }
  }
  if (argc == 4) {
    if (strncmp(argv[1], "publish", 7) == 0) {
      const bool success = MQTT::publish(argv[2], argv[3]);
      cliStream.print(F("publishment "));
      cliStream.print(success ? "successful" : "failed");
      if (not success) {
        char errorMsg[100];
        cliStream.print(F("Error: "));
        cliStream.println(MQTT::errorMessage(errorMsg));
        return 1;
      }
      return 0;
    } else {
      mqttHelpFunc(cliStream);
      return 1;
    }
  }
  mqttHelpFunc(cliStream);
  return 1;
}
#endif // #if WITH_MQTT

#endif // #if WITH_WIFI

#if ESP8266

void
fsHelpFunc(Print& output)
{
  output.println(F("fs [format | ls | info]"));
  output.println(F("fs cat FILEPATH"));
}

int
fsFunc(int argc, char** argv)
{
  if (argc > 1) {
    if (strncmp(argv[1], "format", 7) == 0) {
      const bool formatWorked = LittleFS.format();
      cliStream.println(formatWorked ? F("Successfully formatted ")
                                     : F("ERROR: Couldn't format "));
      cliStream.println(F("LittleFS filesystem"));
      return 0;
    }
    const bool fileSystemOk = LittleFS.begin();
    if (not fileSystemOk) {
      cliStream.println("ERROR: couldn't mount LittleFS filesystem");
      return 1;
    }
    if (strncmp(argv[1], "info", 5) == 0) {
      FSInfo fsInfo;
      const bool infoSuccessful = LittleFS.info(fsInfo);
      if (not infoSuccessful) {
        cliStream.println(F("ERROR: Couldn't retrieve LittleFS info"));
        return 1;
      }
      cliStream.print(fsInfo.usedBytes);
      cliStream.print("/");
      cliStream.print(fsInfo.totalBytes);
      cliStream.print(F(" bytes used"));
      return 0;
    }
    if (strncmp(argv[1], "ls", 3) == 0) {
      Dir dir = LittleFS.openDir("/");
      while (dir.next()) {
        cliStream.println(dir.fileName());
      }
      return 0;
    }
    if (strncmp(argv[1], "cat", 4) == 0) {
      for (size_t i = 2; i < (size_t)argc; i++) {
        char* fileName = argv[i];
        const bool fileExists = LittleFS.exists(fileName);
        if (not fileExists) {
          cliStream.print(F("ERROR: file '"));
          cliStream.print(fileName);
          cliStream.print(F("' doesn't exist in LittleFS"));
          continue;
        }
        File file = LittleFS.open(fileName, "r");
        if (not file) {
          cliStream.print(F("ERROR: Couldn't open file '"));
          cliStream.println(fileName);
          continue;
        }
        ReadBufferingStream bufferedFile(file, 64);
        while (bufferedFile.available() > 0) {
          cliStream.print((char)bufferedFile.read());
        }
        file.close();
        cliStream.println();
      }
      return 0;
    }
  }
  fsHelpFunc(cliStream);
  return 1;
}

#endif // #if ESP8266

void
hardwareHelpFunc(Print& output)
{
  output.println(F("hardware component COMPONENT-ID-GLOB-PATTERN [ ... | "
                   "reset [ time | status ] ]"));
#if WITH_SD
  output.println(F("hw comp SDCARD ls | [cat|head|tail FILENAME]"));
#endif // #if WITH_SD
#if WITH_DS3231
  output.println(F("hw comp RTC set [ unix-timestamp ]"));
#endif // #if WITH_DS3231
#if WITH_LP8
  output.println(
    F("hw comp LP8 calibrate [zero|0|background|400] [filtered] [reset]"));
#endif // #if WITH_LP8
}

int
componentFunc(Hardware::Component& component,
              Hardware::Component::TraverseContext& context,
              int argc,
              char** argv)
{
  LOG(Logging::Hardware,
      Verbose,
      Logging::cat(__PRETTY_FUNCTION__, " for ", component.name.c_str()));
  if (argc > 1) {
    if (strncmp(argv[1], "measure", 4) == 0) {
      if (argc > 2) {
        for (size_t i = 2; i < (const size_t)argc; i++) {
          cat(F("Telling "),
              component.name.c_str(),
              F(" to measure "),
              argv[i],
              "\n");
          component.measure({ argv[i] });
        }
      } else {
        cat(F("Measuring everything "), component.name.c_str(), " can", "\n");
        component.measure();
      }
      return 0;
    }
    if (strcmp(argv[1], "reset") == 0) {
      auto text = F("Silently resetting ");
      if (argc > 2) {
        if (strcmp(argv[2], "status") == 0) {
          cat(text, F("status "), "of ", component.name.c_str(), "\n");
          return 0;
        }
        if (strcmp(argv[2], "time") == 0) {
          cat(text, F("time "), "of ", component.name.c_str(), "\n");
          component.resetTime();
        }
      } else {
        cat(text, F("status and time "), "of ", component.name.c_str(), "\n");
        component.error.setSilently("?");
        component.resetTime();
        return 0;
      }
    }
#if WITH_DS3231
    if (component.is<Hardware::I2C::DS3231>()) {
      auto& rtc = static_cast<Hardware::I2C::DS3231&>(component);
      if (strcmp(argv[1], "set") == 0) {
        if (argc > 2) {
          const unsigned long givenTime = strtoul(argv[2], NULL, 0);
          if (givenTime == 0 or givenTime == ULONG_MAX) {
            cat(F("Error: invalid UNIX-timestamp "), argv[1], "\n");
            return 1;
          } else {
            if (rtc.adjust(givenTime)) {
              cliStream.print(F("adopted time "));
              cliStream.print(givenTime);
              cliStream.print(" (");
              TimeUtils::printDateTime(cliStream, givenTime);
              cliStream.print(")");
              cliStream.println();
              return 0;
            } else {
              cat(F("Error setting "), rtc.name.c_str(), F(" time"), "\n");
            }
          }
        } else {
          const bool success = rtc.adjust(now());
          auto text = F(" time to system time");
          if (success)
            cat(F("Set "), rtc.name.c_str(), text, "\n");
          else
            cat(F("Error setting "), rtc.name.c_str(), text, "\n");
          return success ? 0 : 1;
        }
      }
    }
#endif // #if WITH_DS3231
#if WITH_LP8
    if (component.is<Hardware::SenseAirLP8>()) {
      auto& lp8 = static_cast<Hardware::SenseAirLP8&>(component);
      if (strBeginsWith("calibrate", argv[1])) {
        if (argc > 2) {
          bool reset = false, filtered = false;
          for (size_t i = 3; i < (size_t)argc; i++) {
            if (strBeginsWith("reset", argv[i]))
              reset = true;
            if (strBeginsWith("filtered", argv[i]))
              filtered = true;
          }
          bool success = false, doneSomething = false, zero = false;
          if (strBeginsWith("zero", argv[2]) or *argv[2] == '0') {
            doneSomething = zero = true;
            success = lp8.calibrateZero(filtered, reset);
          }
          if (strBeginsWith("background", argv[2]) or
              strcmp(argv[2], "400") == 0) {
            doneSomething = true;
            success = lp8.calibrateBackground(filtered, reset);
          }
          if (doneSomething) {
            cat(success ? F("Calibrated ") : F("Couldn't calibrate "),
                lp8.name.c_str(),
                filtered ? F(" filtered ") : F(" noisy "),
                F("CO2 concentration to "),
                zero ? 0 : 400,
                "ppm",
                F(" and "),
                reset ? F("reset ") : F("kept "),
                F("old filter data"),
                "\n");
            return success ? 0 : 1;
          }
        }
      }
    }
#endif // #if WITH_LP8
#if WITH_GSS
    if (component.is<Hardware::GSS>()) {
      auto& gss = static_cast<Hardware::GSS&>(component);
      if (strBeginsWith("calibrate", argv[1])) {
        if (argc > 2) {
          const auto co2ppm = strtoul(argv[2], NULL, 10);
          const bool success = gss.calibrate(co2ppm);
          cat(success ? F("Calibrated ") : F("Couldn't calibrate "),
              gss.name.c_str(),
              F(" to "),
              co2ppm,
              ' ',
              "ppm",
              "\n");
          return success ? 0 : 1;
          cat(F("Strange CO2 value "), argv[2], "\n");
        }
      }
    }
#endif // #if WITH_GSS
#if WITH_CM1106
    if (component.is<Hardware::I2C::Cm1106>()) {
      auto& cubic = static_cast<Hardware::I2C::Cm1106&>(component);
      if (strBeginsWith("calibrate", argv[1])) {
        if (argc > 2) {
          const auto co2ppm = strtoul(argv[2], NULL, 10);
          if (Utils::co2ppmOkay(co2ppm)) {
            const bool success =
              cubic.sensor.success(cubic.sensor.zeroSetting(co2ppm));
            cat(success ? F("Calibrated ") : F("Couldn't calibrate "),
                cubic.name.c_str(),
                F(" to "),
                co2ppm,
                ' ',
                "ppm",
                "\n");
            return success ? 0 : 1;
          }
          cat(F("Strange CO2 value "), argv[2], "\n");
        }
      }
    }
#endif // #if WITH_CM1106
#if WITH_SCD30
    if (component.is<Hardware::I2C::Scd30>()) {
      auto& scd30 = static_cast<Hardware::I2C::Scd30&>(component);
      if (strBeginsWith("calibrate", argv[1])) {
        if (argc > 2) {
          const auto co2ppm = strtoul(argv[2], NULL, 10);
          if (argc > 3) {
            const auto pressurehPa = strtoul(argv[3], NULL, 10);
            if (Utils::pressurehPaOkay(pressurehPa)) {
              const bool success =
                scd30.sensor.setAmbientPressure(pressurehPa);
              cat(success ? F("Set pressure of ")
                          : F("Couldn't set pressure of "),
                  scd30.name.c_str(),
                  F(" to "),
                  pressurehPa,
                  ' ',
                  "hPa",
                  F(" only for calibration"),
                  "\n");
            }
            cat(F("Strange pressure value "), argv[2], "\n");
          }
          if (Utils::co2ppmOkay(co2ppm)) {
            const bool success =
              scd30.sensor.setForcedRecalibrationFactor(co2ppm);
            cat(success ? F("Calibrated ") : F("Couldn't calibrate "),
                scd30.name.c_str(),
                F(" to "),
                co2ppm,
                ' ',
                "ppm",
                "\n");
            return success ? 0 : 1;
          }
          cat(F("Strange CO2 value "), argv[2], "\n");
          return 1;
        }
        cat(F("Usage hw comp SCDNAME calibrate PPM [hPA]"));
        return 1;
      }
    }
#endif // #if WITH_SCD30
#if WITH_STC31
    if (component.is<Hardware::I2C::Stc31>()) {
      auto& stc31 = static_cast<Hardware::I2C::Stc31&>(component);
      if (strBeginsWith("calibrate", argv[1])) {
        if (argc > 2) {
          const auto co2ppm = strtoul(argv[2], NULL, 10);
          if (argc > 3) {
            const auto pressurehPa = strtoul(argv[3], NULL, 10);
            if (Utils::pressurehPaOkay(pressurehPa)) {
              const bool error = stc31.sensor.setPressure(pressurehPa);
              cat(not error ? F("Set pressure of ")
                            : F("Couldn't set pressure of "),
                  stc31.name.c_str(),
                  F(" to "),
                  pressurehPa,
                  ' ',
                  "hPa",
                  F(" only for calibration"),
                  "\n");
            } else {
              cat(F("Strange pressure value "), argv[3], "\n");
              return 1;
            }
          }
          if (Utils::co2ppmOkay(co2ppm)) {
            const auto calibrationGasTicks =
              Hardware::I2C::Stc31::gasTicksFromVolumeFraction((float)co2ppm *
                                                               1e-6);
            const auto error =
              stc31.sensor.forcedRecalibration(calibrationGasTicks);
            cat(not error ? F("Calibrated ") : F("Couldn't calibrate "),
                stc31.name.c_str(),
                F(" to "),
                co2ppm,
                ' ',
                "ppm",
                "\n");
            return not error ? 0 : 1;
          } else {
            cat(F("Strange CO2 value "), argv[2], "\n");
            return 1;
          }
        }
        cat(F("Usage hw comp STCNAME calibrate PPM [hPA]"));
        return 1;
      }
    }
#endif // #if WITH_STC31
#if WITH_SD
    if (component.is<Hardware::SPI::SDCard>()) {
      auto& sdCard = static_cast<Hardware::SPI::SDCard&>(component);
      if (strcmp(argv[1], "ls") == 0) {
        if (not sdCard.init()) {
          cat(F("Error initializing SD card"), "\n");
          return 1;
        }
        sdCard.filesystem.ls(&cliStream, LS_R | LS_SIZE);
        return 0;
      }
      if (strcmp(argv[1], "cat") == 0 or strcmp(argv[1], "head") == 0 or
          strcmp(argv[1], "tail") == 0) {
        if (argc <= 2) {
          cat("Usage: ", F("cat|head|tail FILENAME"));
          return 0;
        }
        if (not sdCard.init()) {
          cat(F("Error initializing SD card"), "\n");
          return 1;
        }
        const char* fileName = argv[2];
        auto file = sdCard.filesystem.open(fileName, SDFAT(O_RDONLY));
        if (not file.isOpen()) {
          cat(F("Couldn't open file "), fileName, "\n");
          return 1;
        }
        if (strcmp(argv[1], "head") == 0) {
          size_t newlines = 0;
          ReadBufferingStream bufferedFile{ file, 64 };
          while (bufferedFile.available() and newlines < 10) {
            char c = bufferedFile.read();
            if (c == '\n')
              newlines++;
            cliStream.print(c);
          }
        } else if (strcmp(argv[1], "tail") == 0) {
          size_t newlines = 0;
          file.seekEnd();
          while (newlines < 10) {
            auto pos = file.curPosition();
            LOG(Storage, Debug + 10, Cli::cat(F("curPosition() = "), pos));
            if (pos <= 2)
              break;
            file.seek(pos - 2);
            LOG(
              Storage, Debug + 10, Cli::cat(F("curPosition() after = "), pos));
            char c = file.read();
            LOG(Storage, Debug + 10, Cli::cat(F("c = "), c));
            if (c == '\n')
              newlines++;
          }
          LOG(Storage,
              Verbose,
              Cli::cat(F("File contents from "), file.curPosition(), " on:"));
          ReadBufferingStream bufferedFile{ file, 64 };
          while (bufferedFile.available())
            cliStream.print((char)bufferedFile.read());
        } else {
          ReadBufferingStream bufferedFile{ file, 64 };
          while (bufferedFile.available())
            cliStream.print((char)bufferedFile.read());
        }
        return 0;
      }
    }
#endif // #if WITH_SD
#if WITH_RELAY
    if (component.is<Hardware::Relay>()) {
      auto& relay = static_cast<Hardware::Relay&>(component);
      auto usage = [&relay]() {
        cat(F("Usage: hw comp RELAYNAME switch [on|off] port [port [port "
              "[port ...]]]"),
            '\n',
            '\n');
        cat(F("Available ports: "), 1, ' ', '-', ' ', relay.pins.size(), "\n");
      };
      if (strncmp(argv[1], "switch", 2) == 0) {
        if (argc < 4) {
          usage();
          return 1;
        }
        int p = -1;
        if (strcmp(argv[2], "on") == 0)
          p = 1;
        else if (strcmp(argv[2], "off") == 0)
          p = 0;
        if (p < 0) {
          usage();
          return 1;
        }
        const bool pinOn = p;
        for (size_t i = 3; i < (size_t)argc; i++) {
          const auto pinIndex = strtoul(argv[i], nullptr, 10);
          if (not(1 <= pinIndex and pinIndex <= relay.pins.size())) {
            usage();
            return 1;
          }
          const auto pin = relay.pins[pinIndex - 1];
          digitalWrite(pin, relay.pinState(pinOn));
          cat(F("Switched "),
              relay.name.c_str(),
              F(" port "),
              pinIndex,
              " ",
              '(',
              "pin",
              ' ',
              pin,
              ')',
              ' ',
              pinOn ? "on" : "off",
              '\n');
        }
        return 0;
      }
      return 0;
    }
#endif // #if WITH_RELAY
    // TODO: Multiplexer
    //     if (strcmp(argv[3], "select") == 0) {
    //       if (not component.is<Hardware::I2C::TCA9548A>()) {
    //         cliStream.print("ERROR: ");
    //         cliStream.print("Component '");
    //         cliStream.print(component.name.c_str());
    //         cliStream.print("' is of type ");
    //         cliStream.print(component.getClassString());
    //         cliStream.print(" and not a multiplexer");
    //         cliStream.println();
    //         return 1;
    //       }
    //       Hardware::I2C::TCA9548A& multiplexer =
    //         static_cast<Hardware::I2C::TCA9548A&>(component);
    //       if (argc != 5) {
    //         hardwareHelpFunc(cliStream);
    //         return 1;
    //       }
    //       const auto port = strtoul(argv[4], nullptr, 10);
    //       if (not(0 <= port and port <= 7)) {
    //         hardwareHelpFunc(cliStream);
    //         return 1;
    //       }
    //       if (not multiplexer.selectPorts({ (uint8_t)port })) {
    //         cliStream.println("error");
    //         return 1;
    //       }
    //       cliStream.print(F("  Now "));
    //       cliStream.println(multiplexer.toString(tmp));
    //       return 0;
    //     }
    //   }
    //   return 0;
    // }
  } else {
    char tmp[128] = { 0 };
    cat(component.toString(tmp), "\n");
    return 0;
  }
  cat(F("Don't know how to '"));
  for (int i = 1; i < argc; i++) {
    cat(argv[i]);
    if (i + 1 < argc)
      cat(' ');
  }
  cat(F("' with "), component.name.c_str(), "\n");
  return 1;
}

int
cliHardwareFunc(int argc, char** argv)
{
  if (not Hardware::component) {
    cat(F("No hardware components were parsed!"), "\n");
    return 1;
  }
  if (argc > 1) {
    if (strncmp(argv[1], "component", 4) == 0) {
      if (argc > 2) {
        struct Container
        {
          int argc;
          char** argv;
          const char* namePattern;
          size_t nFound;
          size_t failures;
          int returnCode;
        } container{ argc - 2, argv + 2, argv[2], 0, 0, 0 };
        Hardware::component->traverse(
          [](Hardware::Component& component,
             Hardware::Component::TraverseContext& context,
             void* extra) {
            Container& container = *reinterpret_cast<Container*>(extra);
            if (linuxGlobMatch(container.namePattern,
                               component.name.c_str())) {
              container.nFound++;
              const int ret = componentFunc(
                component, context, container.argc, container.argv);
              if (ret) {
                container.failures++;
                container.returnCode = ret;
                LOG(Logging::Hardware, Warning, Cli::cat("Return code ", ret));
              }
            }
          },
          &container);
        if (not container.nFound) {
          cat(F("No components matching "), container.namePattern, "\n");
          return 1;
        }
        cat(F("Processed "), container.nFound, F(" components. "));
        if (container.returnCode) {
          cat(F("There were "),
              container.failures,
              F(" errors"),
              F(", the last error code was "),
              container.returnCode,
              "\n");
          return container.returnCode;
        }
        cat("\n");
        return 0;
      } else {
        Hardware::Component::TraverseContext context;
        context.hardwareAccess = false;
        Hardware::component->traverse(
          context, Hardware::Component::print, &cliStream);
        return 0;
      }
    }
  }
  hardwareHelpFunc(cliStream);
  return 1;
}

#if WITH_UNITCONVERSION
void
unitHelpFunc(Print& output)
{
  output.println(F("convert NUMBER UNIT to UNIT"));
}

int
cliUnitFunc(int argc, char** argv)
{
  if (argc == 5) {
    if (strcmp(argv[3], "to") != 0) {
      unitHelpFunc(cliStream);
      return 1;
    }
    UnitConversion::ConverterCollection<5> converter;
    converter.add("Pa", "bar", [](const double x) { return x * 0.00001; });
    converter.add("bar", "psi", [](const double x) { return x * 14.50377; });
    const double value = strtod(argv[1], nullptr);
    const char* givenUnit = argv[2];
    const char* targetUnit = argv[4];
    double newValue = value;
    const unsigned long microsBefore = micros();
    const bool converted = converter.convert(newValue, givenUnit, targetUnit);
    const unsigned long microsAfter = micros();
    cliStream.print("Computation time: ");
    cliStream.print(microsAfter - microsBefore);
    cliStream.println("us");
    if (converted) {
      cliStream.print(value, 6);
      cliStream.print(" ");
      cliStream.print(givenUnit);
      cliStream.print(" = ");
      cliStream.print(newValue, 6);
      cliStream.print(" ");
      cliStream.print(targetUnit);
      cliStream.println();
      return 0;
    } else {
      cliStream.print(F("Don't know how to convert "));
      cliStream.print(value, 6);
      cliStream.print(" ");
      cliStream.print(givenUnit);
      cliStream.print(" to ");
      cliStream.print(targetUnit);
      cliStream.println();
      return 1;
    }
  }
  unitHelpFunc(cliStream);
  return 1;
}
#endif // #if WITH_UNITCONVERSION

#if ARDUINO_ARCH_AVR
void
eepromHelpFunc(Print& output = cliStream)
{
  output.println(
    F("eeprom [ readjson [ STARTADDRESS ] | writejson [ STARTADDRESS ] ]"));
}

int
eepromFunc(int argc, char** argv)
{
  if (argc > 1) {
    size_t startAddress = 0;
    if (argc > 2) {
      const auto number = strtoul(argv[2], nullptr, 10);
      if (number >= EEPROM_SIZE) {
        eepromHelpFunc();
        return 1;
      }
      startAddress = number;
    }
    EepromStream eepromStream(startAddress, EEPROM_SIZE);
    if (strcmp(argv[1], "readjson") == 0) {
      const size_t capacity = Utils::largestMalloc() -
                              sizeof(DynamicJsonDocument) -
                              sizeof(DeserializationError) - 1000;
      DynamicJsonDocument doc(capacity);
      DeserializationError err = deserializeJson(doc, eepromStream);
      doc.shrinkToFit();
      if (err) {
        cliStream.print(F("Could not read JSON from EEPROM: "));
        cliStream.println(err.c_str());
        return 1;
      }
      serializeJsonPretty(doc, cliStream);
      cliStream.println();
      return 0;
    } else if (strcmp(argv[1], "writejson") == 0) {
      const size_t capacity =
        Utils::largestMalloc() - sizeof(DynamicJsonDocument) -
        sizeof(ReadLoggingStream) - sizeof(DeserializationError) - 1000;
      DynamicJsonDocument doc(capacity);
      // ReadLoggingStream cliStreamEcho(cliStream, cliStream);
      cliStream.setTimeout(10000);
      cliStream.println(F("Now paste/write JSON to save to EEPROM!"));
      DeserializationError err = deserializeJson(doc, cliStream);
      doc.shrinkToFit();
      cliStream.println();
      if (err) {
        cliStream.print(F("Could not read your JSON: "));
        cliStream.println(err.c_str());
        return 1;
      }
      cliStream.print(F("Writing "));
      serializeJson(doc, cliStream);
      cliStream.println(F(" to EEPROM"));
      const size_t nBytesWritten = serializeJson(doc, eepromStream);
      cliStream.print(F("Wrote "));
      cliStream.print(nBytesWritten);
      cliStream.println(F(" bytes to EEPROM"));
      return 0;
    }
  }
  eepromHelpFunc();
  return 1;
}
#endif // #if ARDUINO_ARCH_AVR

int
resetFunc(int argc, char** argv)
{
#if WITH_WATCHDOG
  cliStream.println(F(""));
  cliStream.flush();
  WatchDog.device_reset();
  delay(10000);
#endif // #if WITH_WATCHDOG
#if ESP8266
  ESP.restart();
  delay(10000);
#endif
  cliStream.println(
    F("OH MY GOD YOU SHOULD NEVER SEE THIS! Device didn't reset!"));
  return 1;
}

void
loggingHelp(Print& output)
{
  output.println(F("log test"));
  output.println(F("log level [[MIN] MAX]"));
  output.println(F("log channel [show|hide NAME|NUMBER|all|none]"));
#if LOGGING_DEBUG_RAM
  output.println(F("log ram [enable|disable]"));
#endif // #if LOGGING_DEBUG_RAM
}

template<typename T>
void
printBitMaskValues(const T bitMask, Print& output)
{
  if (not bitMask) {
    output.print("none");
    return;
  }
  if (not(~bitMask)) {
    output.print("all");
    return;
  }
  bool any = false;
  T bitMaskCopy = bitMask;
  for (size_t i = 0; i < sizeof(T) * 8; i++) {
    if (bitMaskCopy & 1) {
      if (any)
        output.print(',');
      output.print(i);
      any = true;
    }
    bitMaskCopy >>= 1;
  }
  output.print(' ');
  output.print('[');
  output.print("0b");
  size_t i = sizeof(T) * 8;
  while (i)
    output.print(bitMask & ((T)1 << --i) ? '1' : '0');
  output.print(']');
}

int
loggingFunc(int argc, char** argv)
{
  if (argc > 1) {
    if (strncmp(argv[1], "test", 4) == 0) {
      for (size_t channel = 0; channel < sizeof(Logging::channels) * 8;
           channel++) {
        for (auto level = Logging::logLevelMin; level < Logging::logLevelMax;
             level++) {
          LOG(channel,
              level,
              cat("Message",
                  " in ",
                  "channel",
                  ' ',
                  channel,
                  " at ",
                  "level",
                  ' ',
                  level));
#if ESP8266
          yield();
#endif // #if ESP8266
        }
      }
      return 0;
    }
#if LOGGING_DEBUG_RAM
    if (strncmp(argv[1], "ram", 3) == 0) {
      if (argc > 2) {
        if (strncmp(argv[2], "enable", 6) == 0)
          Config::config.runtimeDebugRam = true;
        else if (strncmp(argv[2], "disable", 6) == 0)
          Config::config.runtimeDebugRam = false;
        else {
          loggingHelp(cliStream);
          return 1;
        }
      }
      cat("ram",
          F(" logging "),
          Config::config.runtimeDebugRam ? "enable" : "disable",
          'd',
          '\n');
      return 0;
    }
#endif // #if LOGGING_DEBUG_RAM
    if (strncmp(argv[1], "level", 5) == 0) {
      if (argc == 2) {
        char levelStr[16] = { 0 };
        cat("loglevels ",
            Logging::logLevelMin,
            " (",
            Logging::levelToString(Logging::logLevelMin, levelStr),
            ")",
            " to ");
        cat(Logging::logLevelMax,
            " (",
            Logging::levelToString(Logging::logLevelMax, levelStr),
            ")",
            F(" are compiled in, "));
        cat(
          "loglevels ",
          Config::config.runtimeLogLevelMin,
          " (",
          Logging::levelToString(Config::config.runtimeLogLevelMin, levelStr),
          ")",
          " to ");
        cat(
          Config::config.runtimeLogLevelMax,
          " (",
          Logging::levelToString(Config::config.runtimeLogLevelMax, levelStr),
          ")",
          F(" are currently shown."),
          "\n");
        return 0;
      } else if (argc == 3) {
        int level = 0;
        if (not Logging::logLevel(argv[2], level))
          level = atoi(argv[2]);
        Config::config.runtimeLogLevelMax = level;
        return 0;
      } else if (argc == 4) {
        int level = 0;
        if (not Logging::logLevel(argv[2], level))
          level = atoi(argv[2]);
        Config::config.runtimeLogLevelMin = level;
        if (not Logging::logLevel(argv[3], level))
          level = atoi(argv[3]);
        Config::config.runtimeLogLevelMax = level;
        return 0;
      }
    }
    if (strncmp(argv[1], "channel", 7) == 0) {
      if (argc == 2) {
        cat("compiled-in ", "channels: ");
        printBitMaskValues(Logging::channels, cliStream);
        cat("\n");
        cat(' ', ' ', ' ', ' ', ' ', ' ', "shown ", "channels: ");
        printBitMaskValues(Config::config.runtimeChannels, cliStream);
        cat("\n");
        return 0;
      }
      if (argc >= 4) {
        if (strcmp(argv[2], "show") == 0) {
          if (strcmp(argv[3], "all") == 0) {
            Config::config.runtimeChannels = ~(uint64_t)0;
            return 0;
          }
          if (strcmp(argv[3], "none") == 0) {
            Config::config.runtimeChannels = 0;
            return 0;
          }
          for (size_t n = 3; n < (size_t)argc; n++) {
            uint64_t bitMask = 0;
            if ((bitMask = Logging::channelBitMask(argv[n]))) {
              Config::config.runtimeChannels |= bitMask;
              continue;
            }
            auto c = strtoul(argv[n], NULL, 10);
            if (0 <= c and
                c <= sizeof(Config::config.runtimeChannels) * 8 - 1) {
              Config::config.runtimeChannels |= (uint64_t)1 << c;
              continue;
            }
            cat("Strange channel ", argv[n], "\n");
          }
          return 0;
        }
        if (strcmp(argv[2], "hide") == 0) {
          if (strcmp(argv[3], "all") == 0) {
            Config::config.runtimeChannels = 0;
            return 0;
          }
          if (strcmp(argv[3], "none") == 0) {
            Config::config.runtimeChannels = ~(uint64_t)0;
            return 0;
          }
          for (size_t n = 3; n < (size_t)argc; n++) {
            uint64_t bitMask = 0;
            if ((bitMask = Logging::channelBitMask(argv[n]))) {
              Config::config.runtimeChannels &= ~bitMask;
              continue;
            }
            auto c = strtoul(argv[n], NULL, 10);
            if (0 <= c and
                c <= sizeof(Config::config.runtimeChannels) * 8 - 1) {
              Config::config.runtimeChannels &= ~((uint64_t)1 << c);
              continue;
            }
            cat("Strange channel ", argv[n], "\n");
          }
          return 0;
        }
      }
    }
  }
  loggingHelp(cliStream);
  return 1;
}

void
configHelpFunc(Print& output)
{
  output.print(F("config json"));
#if ESP8266
  output.println(F("|read|save"));
#endif // #if ESP8266
}

int
configFunc(int argc, char** argv)
{
  if (argc > 1) {
    if (strcmp(argv[1], "json") == 0) {
      const size_t capacity =
#if ESP8266
        ESP.getMaxFreeBlockSize()
#else                                         // #if ESP8266
        Utils::largestMalloc()
#endif                                        // #if ESP8266
        - sizeof(DynamicJsonDocument) - 1000; // also reserve some stack space
      DynamicJsonDocument doc(capacity);
      if (not Config::config.toJson(doc.to<JsonObject>())) {
        cat(F("Couldn't serialize config to JSON"), "\n");
        return 1;
      }
      doc.shrinkToFit();
      serializeJsonPretty(doc, cliStream);
      cliStream.println();
      return 0;
    }
#if ESP8266
    if (strcmp(argv[1], "read") == 0) {
      const bool success = Config::config.readFromFs();
      cat(success ? F("Successfully read") : F("Error reading"),
          F(" JSON config file"),
          "\n");
      return 0;
    }
    if (strcmp(argv[1], "save") == 0) {
      const bool success = Config::config.writeToFs();
      cat(success ? F("Successfully wrote") : F("Error writing"),
          F(" JSON config file"),
          "\n");
      return 0;
    }
#endif // #if ESP8266
  }
  configHelpFunc(cliStream);
  return 1;
}

void
process()
{
  do {
    shell_task();
  } while (cliStream.available());
}

void
setupCli(void)
{
  scheduler.add(process);
  cliStream.add(Serial);
#if WITH_MQTT && WITH_MQTT_CLI
  cliStream.add(MQTT::cliStream);
#endif // #if WITH_MQTT_CLI
  shell_init(serialReader, serialWriter, 0);
  shell_register(loggingFunc, PSTR("log"));
  shell_register(configFunc, PSTR("config"));
  shell_register(cliInfoFunc, PSTR("sketchinfo"));
  shell_register(cliPauseFunc, PSTR("pause"));
  shell_register(cliResumeFunc, PSTR("resume"));
  shell_register(cliSystemFunc, PSTR("system"));
  shell_register(cliHelpFunc, PSTR("help"));
  shell_register(cliHardwareFunc, PSTR("hardware"));
  shell_register(cliHardwareFunc, PSTR("hw"));
  shell_register(resetFunc, PSTR("reset"));
  shell_register(resetFunc, PSTR("reboot"));
#if WITH_UNITCONVERSION
  shell_register(cliUnitFunc, PSTR("convert"));
#endif // #if WITH_UNITCONVERSION
#if WITH_SERIAL_DATA_STREAM
  shell_register(outputFunc, PSTR("output"));
#endif // #if SERIAL_DATA_STREAM
#if WITH_WIFI
  shell_register(wifiFunc, PSTR("wifi"));
#if WITH_MQTT
  shell_register(mqttFunc, PSTR("mqtt"));
#endif // #if WITH_MQTT
#endif // #if WITH_WIFI
#if ESP8266
  shell_register(fsFunc, PSTR("fs"));
#endif // #if ESP8266
#if ARDUINO_ARCH_AVR
  shell_register(eepromFunc, PSTR("eeprom"));
#endif // #if ARDUINO_ARCH_AVR
}
} // namespace Cli

#endif // #if WITH_CLI
