#pragma once

#if ARDUINO_ARCH_AVR

#include <EEPROM.h>
#include <StreamUtils.h>

#if ARDUINO_AVR_MEGA2560
#define EEPROM_SIZE 4096
#else // #if ARDUINO_AVR_MEGA2560
#if defined(ARDUINO_AVR_UNO) || defined(ARDUINO_AVR_NANO)
#define EEPROM_SIZE 1024
#else // #if defined(ARDUINO_AVR_UNO) || defined(ARDUINO_AVR_NANO)
#define EEPROM_SIZE 512
#endif // #if defined(ARDUINO_AVR_UNO) || defined(ARDUINO_AVR_NANO)
#endif // #if ARDUINO_AVR_MEGA2560

#endif // #if ARDUINO_ARCH_AVR
