#pragma once

/**
  @file Hardware.hpp
  @brief Hardware Functionality
*/

#if !defined(HARDWARE_JSON_FILENAME) || DOXYGEN_RUN
/**
 * @ingroup WiFiFlags
 * @brief LittleFS filename of hardware layout JSON file
 */
#define HARDWARE_JSON_FILENAME "/hardware.json"
#endif // #ifndef HARDWARE_JSON_FILENAME

#if !defined(DEFAULT_HARDWARE_JSON) || DOXYGEN_RUN
/**
 * @ingroup WiFiFlags
 * @brief Default JSON-encoded definition of hardware layout like this:
 *   ```.py
 *     {
 *     ...
 *     }
 *   ```
 *   This value is used if no #HARDWARE_JSON_FILENAME file can be found in
 *   LittleFS.
 */
#define DEFAULT_HARDWARE_JSON "{}"
#endif // #ifndef DEFAULT_HARDWARE_JSON

#if !defined(PARSE_HARDWARE_CONFIG) || DOXYGEN_RUN
//! Whether to parse the hardware config. Might be useful to disable if parsing
// causes resets or freezes.
#define PARSE_HARDWARE_CONFIG true
#endif // #ifndef PARSE_HARDWARE_CONFIG || DOXYGEN_RUN

#if !defined(HARDWARE_INTERVAL_MS) || DOXYGEN_RUN
//! Interval for Hardware component traversal
#define HARDWARE_INTERVAL_MS 1000
#endif // #if !defined(HARDWARE_INTERVAL_MS) || DOXYGEN_RUN

#include "utils/Utils.hpp"

#include "EEPROM.hpp"
#include "Mqtt.hpp"
#include "Multiplexer.hpp"
#include "Scheduler.hpp"
#include "utils/StringUtils.hpp"

#if !NATIVE
#include <SPI.h>
#include <Wire.h>
#endif // #if !NATIVE

#include <ArduinoJson.h>

#if ARDUINO_ARCH_AVR
#include <ArduinoSTL.h>
#endif // #if ARDUINO_ARCH_AVR

#include <algorithm>

#if ESP8266
#include "WiFi.hpp"
#include <FS.h>
#include <StreamUtils.h>
#endif // #if ESP8266

#include "hardware/Hardware.hpp"

/**
  @brief Bundled access to sensors
*/

namespace Hardware {

Component* component = nullptr;

Scheduler::Scheduler<2> scheduler;

#if ESP8266
template<typename jsonDocumentType>
bool
parseHardwareConfigFromFs(jsonDocumentType& doc)
{
  if (LittleFS.begin()) {
    File file = LittleFS.open(HARDWARE_JSON_FILENAME, "r");
    if (not file) {
      LOG(System,
          Debug,
          cat("No '", HARDWARE_JSON_FILENAME, "' file on LittleFS"));
      return false;
    }
    ReadBufferingStream bufferedFile(file, 64);
    DeserializationError err = deserializeJson(doc, bufferedFile);
    file.close();
    if (err) {
      LOG(System,
          Error,
          cat("ERROR: Can't parse ",
              HARDWARE_JSON_FILENAME,
              "' file on LittleFS as JSON: ",
              err.c_str()));
      return false;
    }
    return true;
  }
  return false;
}
#endif // #if ESP8266

#if ARDUINO_ARCH_AVR
template<typename jsonDocumentType>
bool
parseHardwareConfigFromEEPROM(jsonDocumentType& doc)
{
  EepromStream eepromStream(0, EEPROM_SIZE);
  DeserializationError err = deserializeJson(doc, eepromStream);
  if (err) {
    LOG(System,
        Error,
        cat("ERROR: Can't parse hardware layout JSON from EEPROM: ",
            err.c_str()));
    return false;
  }
  return true;
}
#endif // #if ARDUINO_ARCH_AVR

template<typename jsonDocumentType>
bool
parseDefaultHardwareConfig(jsonDocumentType& doc)
{
  DeserializationError err = deserializeJson(doc, DEFAULT_HARDWARE_JSON);
  if (err) {
    LOG(Logging::Hardware,
        Warning,
        cat("WARNING: Couldn't deserialize default hardware JSON definition (",
            err.c_str(),
            "):\n",
            DEFAULT_HARDWARE_JSON));
    return false;
  }
  return true;
}

template<typename jsonDocumentType>
bool
parseHardwareConfig(jsonDocumentType& doc)
{
  bool success = false;
#if ESP8266
  if (not success)
    success = parseHardwareConfigFromFs(doc); // try LittleFS
#endif                                        // #if ESP8266
#if ARDUINO_ARCH_AVR
  if (not success)
    success = parseHardwareConfigFromEEPROM(doc); // try EEPROM
#endif                                            // #if ARDUINO_ARCH_AVR
  if (not success) // otherwise fall back to COMPILER FLAG
    success = parseDefaultHardwareConfig(doc);
  return success;
}

Component*
parseHardwareConfig()
{
  // As temporary JsonDocument we'd better allocate one on the stack than on
  // the heap, because we'll later on add ever-living objects on the heap so
  // deallocating a JsonDocument on the heap afterwards would lead to huge
  // fragmentation. BUT, the stack size seems to be limited on ESP (not on
  // Arduino however), so we need to allocate on the heap. A little
  // fragmentation is unavoidable, I guess...
  // If this becomes a critical problem, we could use a StaticJsonDocument with
  // a staggered capacity based on Utils::stackSize().
  const size_t capacity =
    std::min((size_t)8192,
#if ESP8266
             ESP.getMaxFreeBlockSize()
#else  // #if ESP8266
             Utils::largestMalloc()
#endif // #if ESP8266
               - sizeof(DynamicJsonDocument) -
               1000); // also reserve some stack space for
                      // parseHardwareConfig() and subfunctions
  DynamicJsonDocument doc(capacity);
  if (not parseHardwareConfig(doc))
    return nullptr;
  doc.shrinkToFit();
  LOG(Logging::Hardware,
      Info,
      cat(F("Shrunk the DynamicJsonDocument from "),
          capacity,
          " to ",
          doc.capacity(),
          F(" bytes")));
  JsonObjectConst layout = doc.as<JsonObject>();
  if (layout.isNull())
    return nullptr;
  return Component::from(layout);
}

void
performMeasurements(Component& component,
                    Component::TraverseContext& context,
                    void* extra)
{
  if (Globals::paused)
    return;
  if (context.abort)
    return;
  if (not component.due())
    return;
  component.measure();
}

#if WITH_SERIAL_DATA_STREAM
void
initSerialDataStream(void)
{
  if (not Hardware::component)
    return;
  Hardware::Component::TraverseContext context;
  context.hardwareAccess = false;
  struct Container
  {
    Print& output;
  } container{ SERIAL_DATA_STREAM_PORT };
  SERIAL_DATA_STREAM_PORT.begin(SERIAL_DATA_STREAM_BAUDRATE);
  container.output.println();
  container.output.print(F("time unix [utc]"));
  Hardware::component->traverse(
    context,
    [](Hardware::Component& component,
       Hardware::Component::TraverseContext& context,
       void* extra) {
      Container& container = *reinterpret_cast<Container*>(extra);
      for (auto m : component.measurements) {
        auto& measurement = *m;
        container.output.print(','); // TODO: Make this configurable
        if (component.name.size())
          container.output.print(component.name.c_str());
#if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
        container.output.print(' ');
        container.output.print('(');
        container.output.print(component.getClassString());
        container.output.print(')');
#endif // #if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
        container.output.print(' ');
        container.output.print(measurement.quantity);
        if (measurement.unit) {
          container.output.print(' ');
          container.output.print('[');
          container.output.print(measurement.unit);
          container.output.print(']');
        }
      }
    },
    &container);
  container.output.println();
}
#endif // #if WITH_SERIAL_DATA_STREAM

void
resetComponentStatus(void)
{
  LOG(
    Logging::Hardware, Info, cat("Silently resetting all components' status"));
  Hardware::Component::TraverseContext context;
  context.hardwareAccess = false;
  Hardware::component->traverse(
    context,
    [](Hardware::Component& component,
       Hardware::Component::TraverseContext& context,
       void* extra) { component.error.setSilently("?"); });
}

void
errorChangeCallback(Component::Error& error,
                    const Component::Error& previousError,
                    void* extra)
{

  LOG(Logging::Hardware, Info, char tmp[100] = { 0 };
      cat(error.component().toString(tmp),
          F(": status changed from '"),
          previousError.c_str(),
          "' to '",
          error.c_str(),
          "'"));
#if ESP8266
  yield();
#endif // #if ESP8266
#if WITH_SD
  {
    auto timeBefore = micros();
    Hardware::Component::TraverseContext context;
    context.hardwareAccess =
      false; // TODO: This is a little hack that prevents nested I2C
             // multiplexer madness. It would be better if we only considered
             // SPI components from the very beginning...
    Hardware::component->traverse(
      context,
      [](Component& component,
         Component::TraverseContext& context,
         void* extra) {
        if (not component.is<SPI::SDCard>())
          return;
        auto& sdCard = static_cast<SPI::SDCard&>(component);
        if (not extra)
          return;
        const auto& error =
          *reinterpret_cast<Hardware::Component::Error*>(extra);
        sdCard.store(error);
      },
      &const_cast<Hardware::Component::Error&>(error));
    auto duration = micros() - timeBefore;
    LOG(Logging::Hardware,
        Debug,
        cat("Storing to SD-card(s) took ", duration, " microseconds"));
  }
#endif // #if WITH_SD
#if WITH_MQTT
  {
    char topic[MQTT_MAX_PACKET_SIZE] = { 0 };
    error.component().toMqttTopic(topic, Utils::arraySize(topic));
    sprintfcat(topic, "/%s", "status");
    MQTT::publish(topic, error.c_str(), true);
  }
#endif // #if WITH_MQTT
#if WITH_DS3231
  {
    if (error.component().is<I2C::DS3231>()) {
      if (error == "lost-power") {
        auto& rtc = static_cast<I2C::DS3231&>(error.component());
        if (timeStatus() == timeSet) {
          rtc.adjust(now());
        }
      }
    }
  }
#endif // #if WITH_DS3231
#if WITH_WIFI
  {
    // If we connected, reset all components' status
    if (error.component().is<WiFiNetwork>()) {
      if (previousError == "disconnected" and error == "ok") {
        LOG(Logging::Hardware, Info, cat(F("Freshly connected to WiFi")));
        resetComponentStatus();
      }
    }
  }
#endif // #if WITH_WIFI
  LOG(Logging::Hardware, Debug + 30, cat(F("End of "), __PRETTY_FUNCTION__));
}

void
attachCallbackToErrorChange(Component& component,
                            Component::TraverseContext& context,
                            void* extra)
{
  if (extra)
    component.error.connect(
      reinterpret_cast<Component::Error::changedCallback_t>(extra));
}

void
handleMeasurement(const Component::Measurement& measurement,
                  const Component::Measurement& previousMeasurement,
                  void* extra)
{
  {
    char tmp1[20] = { 0 };
    char tmp2[20] = { 0 };
    char tmp3[20] = { 0 };
    LOG(Logging::Hardware, Verbose, {
      const auto tdiffMs =
        (unsigned long)(measurement.time.epochMilliseconds() -
                        previousMeasurement.time.epochMilliseconds());
      cat(F("Handling "),
          measurement.component().name.c_str(),
          " ",
          measurement.quantity,
          F(" measurement "),
          measurement.toString(tmp1),
          " ",
          measurement.unit,
          F(" from time "),
          measurement.time.toString(tmp2),
          F(" (was "),
          previousMeasurement.toString(tmp3),
          " ",
          previousMeasurement.unit,
          " ",
          tdiffMs,
          F("ms before"),
          " [",
          1000.0F / tdiffMs,
          "Hz",
          "]",
          ")");
    });
  }
  if (measurement == "time") {
    if ((uintptr_t)measurement.unit == (uintptr_t) "unix") {
      const auto epochSeconds = measurement.as<uint32_t>();
      LOG(
        System, Info, cat("Setting system time to UNIX time ", epochSeconds));
      setTime(epochSeconds);
      return;
    }
  }
  if (measurement == "pressure") {
    auto pressure = measurement.as<float>();
    if (Utils::pressurehPaOkay(pressure)) {
      LOG(Sensor,
          Verbose,
          cat(F("Using "),
              measurement.component().name.c_str(),
              ' ',
              measurement.quantity,
              ' ',
              pressure,
              ' ',
              measurement.unit ? measurement.unit : "",
              ' ',
              F("as global pressure")));
      Globals::pressurehPa = pressure;
    }
  }
#if WITH_SERIAL_DATA_STREAM
  if (Config::config.runtimeSerialDataStream and
      measurement.component().store) {
    Hardware::Component::TraverseContext context;
    context.hardwareAccess = false;
    struct Container
    {
      Print& output;
      const Hardware::Component::Measurement& measurement;
    } container{ SERIAL_DATA_STREAM_PORT, measurement };
    {
      char tmp[16] = { 0 };
      container.output.print(measurement.time.toString(tmp));
    }
    Hardware::component->traverse(
      context,
      [](Hardware::Component& component,
         Hardware::Component::TraverseContext& context,
         void* extra) {
        Container& container = *reinterpret_cast<Container*>(extra);
        for (auto measurement : component.measurements) {
          container.output.print(','); // TODO: Make this configurable
          if ((uintptr_t)&container.measurement != (uintptr_t)measurement)
            continue;
          char tmp[16] = { 0 };
          container.output.print(measurement->toString(tmp));
        }
      },
      &container);
    container.output.println();
  }
#endif // #if WITH_SERIAL_DATA_STREAM
#if ESP8266
  yield();
#endif // #if ESP8266
#if WITH_SD
  if (measurement.component().store) {
    auto timeBefore = micros();
    Hardware::Component::TraverseContext context;
    context.hardwareAccess =
      false; // TODO: This is a little hack that prevents nested I2C
             // multiplexer madness. It would be better if we only considered
             // SPI components from the very beginning...
    Hardware::component->traverse(
      context,
      [](Component& component,
         Component::TraverseContext& context,
         void* extra) {
        if (not component.is<SPI::SDCard>())
          return;
        auto& sdCard = static_cast<SPI::SDCard&>(component);
        if (not extra)
          return;
        const auto& measurement =
          *reinterpret_cast<Hardware::Component::Measurement*>(extra);
#if ESP8266
        yield();
#endif // #if ESP8266
        sdCard.store(measurement);
      },
      &const_cast<Hardware::Component::Measurement&>(measurement));
    auto duration = micros() - timeBefore;
    LOG(Logging::Hardware,
        Debug,
        cat(F("Storing to SD-card(s) took "), duration, " microseconds"));
  }
#endif // #if WITH_SD
#if ESP8266
  yield();
#endif // #if ESP8266
#if WITH_MQTT
  if (measurement.component().store) {
    char topic[MQTT_MAX_PACKET_SIZE] = { 0 };
    measurement.component().toMqttTopic(topic, Utils::arraySize(topic));
    sprintfcat(topic, "/%s", measurement.quantity);
    char msg[32] = { 0 };
    measurement.toString(msg);
    if (measurement.unit)
      sprintfcat(msg, " %s", measurement.unit);
    MQTT::publish(topic, msg);
  }
#endif // #if WITH_MQTT
  LOG(Logging::Hardware, Debug + 30, cat(F("End of "), __PRETTY_FUNCTION__));
}

void
attachCallbackToMeasurements(Component& component,
                             Component::TraverseContext& context,
                             void* extra)
{
  if (not extra)
    return;
  auto callback = reinterpret_cast<Component::Measurement::callback_t>(extra);
  for (Component::Measurement* measurement : component.measurements) {
    if (not measurement)
      continue;
    LOG(Network,
        Verbose,
        cat("Subscribing to ",
            component.name.c_str(),
            " measurement ",
            measurement->quantity));
    measurement->connect(callback);
  }
}

float
getGlobalPressure(void)
{
  LOG(Logging::Hardware,
      Verbose,
      cat(F("Global pressure is "), Globals::pressurehPa));
  return Globals::pressurehPa;
}

void
attachPressureCallback(Component& component,
                       Component::TraverseContext& context,
                       void* extra)
{
  if (not extra)
    return;
#if WITH_SCD30
  if (component.is<I2C::Scd30>()) {
    I2C::Scd30& scd = static_cast<I2C::Scd30&>(component);
    auto callback = reinterpret_cast<I2C::Scd30::pressureCallback_t>(extra);
    scd.setPressureCallback(callback);
    LOG(Sensor,
        Info,
        cat(F("Attached global pressure to "), component.name.c_str()));
  }
#endif // #if WITH_SCD30
#if WITH_LP8
  if (component.is<SenseAirLP8>()) {
    SenseAirLP8& lp8 = static_cast<SenseAirLP8&>(component);
    auto callback = reinterpret_cast<SenseAirLP8::pressureCallback_t>(extra);
    lp8.setPressureCallback(callback);
    LOG(Sensor,
        Info,
        cat(F("Attached global pressure to "), component.name.c_str()));
  }
#endif // #if WITH_LP8
#if WITH_GSS
  if (component.is<GSS>()) {
    GSS& gss = static_cast<GSS&>(component);
    auto callback = reinterpret_cast<GSS::pressureCallback_t>(extra);
    gss.setPressureCallback(callback);
    LOG(Sensor,
        Info,
        cat(F("Attached global pressure to "), component.name.c_str()));
  }
#endif // #if WITH_GSS
#if WITH_STC31
  if (component.is<I2C::Stc31>()) {
    I2C::Stc31& stc31 = static_cast<I2C::Stc31&>(component);
    auto callback = reinterpret_cast<I2C::Stc31::pressureCallback_t>(extra);
    stc31.setPressureCallback(callback);
    LOG(Sensor,
        Info,
        cat(F("Attached global pressure to "), component.name.c_str()));
  }
#endif // #if WITH_STC31
}

void
storeBuildFlags(void)
{
  Hardware::Component::TraverseContext context;
  context.hardwareAccess =
    false; // TODO: This is a little hack that prevents nested I2C
           // multiplexer madness. It would be better if we only considered
           // SPI components from the very beginning...
  const size_t capacity = std::min((size_t)8192,
#if ESP8266
                                   ESP.getMaxFreeBlockSize()
#else  // #if ESP8266
                                   Utils::largestMalloc()
#endif // #if ESP8266
                                     - sizeof(DynamicJsonDocument) -
                                     1000); // also reserve some stack space
  DynamicJsonDocument doc(capacity);
  auto err =
    deserializeJson(doc, (const __FlashStringHelper*)Config::buildFlagsJson);
  doc.shrinkToFit();
  if (err) {
    LOG(System, Error, cat(F("Can't parse build flags: "), err.c_str()));
    return;
  }
#if WITH_SD
  JsonVariantConst json = doc.as<JsonVariant>();
  LOG(Storage, Info, cat("Storing build flags to SD card(s)"));
  Hardware::component->traverse(
    context,
    [](
      Component& component, Component::TraverseContext& context, void* extra) {
      if (not component.is<SPI::SDCard>())
        return;
      auto& sdCard = static_cast<SPI::SDCard&>(component);
      if (not extra)
        return;
      auto& json = *reinterpret_cast<JsonVariantConst*>(extra);
      sdCard.store("/", "build-flags.csv", json);
    },
    &json);
#endif // #if WITH_SD
}

timeStatus_t previousTimeStatus = timeNotSet;
void
timeSetCallback(void)
{
  timeStatus_t currentTimeStatus = timeStatus();
  if (previousTimeStatus == timeNotSet and currentTimeStatus == timeSet) {
    LOG(System, Info, cat("System time was freshly set."));
    resetComponentStatus();
    storeBuildFlags();
    scheduler.remove(timeSetCallback); // no need to reschedule this
  }
  previousTimeStatus = currentTimeStatus;
}

void
setup()
{
#if ESP8266
  const float heapFragmentationBefore = Utils::heapFragmentation();
#endif // #if ESP8266
#if PARSE_HARDWARE_CONFIG
  component = parseHardwareConfig();
#endif // #if PARSE_HARDWARE_CONFIG
#if ESP8266
  LOG(System,
      Debug,
      cat("parseHardwareConfig() changed heap fragmentation from ",
          heapFragmentationBefore * 100.0,
          "% to ",
          Utils::heapFragmentation() * 100.0,
          "%"));
#endif // #if ESP8266
  if (component) {
    {
      Hardware::Component::TraverseContext context;
      context.hardwareAccess = false;
      // connect to all status changes
      component->traverse(
        context, attachCallbackToErrorChange, (void*)errorChangeCallback);
    }
    {
      Hardware::Component::TraverseContext context;
      context.hardwareAccess = false;
      // connect to all measurements
      component->traverse(
        context, attachCallbackToMeasurements, (void*)handleMeasurement);
    }
    {
      Hardware::Component::TraverseContext context;
      context.hardwareAccess = false;
      // connect pressure to interested sensors
      component->traverse(
        context, attachPressureCallback, (void*)getGlobalPressure);
    }
#if ARDUINO_ARCH_AVR
// no idea why but on the Arduino Mega this line freezes it..
#else  // #if ARDUINO_ARCH_AVR
    LOG(Logging::Hardware, Verbose, {
      Hardware::Component::TraverseContext context;
      context.hardwareAccess = false;
      // just print the structure
      component->traverse(context, Component::print, &Serial);
    });
#endif // #if ARDUINO_ARCH_AVR
    // attach looping over the structure
    scheduler.repeat(HARDWARE_INTERVAL_MS, []() {
      LOG(Logging::Hardware,
          Debug + 30,
          cat("Begin ", "traversing through components"));
      component->traverse(performMeasurements);
      LOG(Logging::Hardware,
          Debug + 30,
          cat("Done ", "traversing through components"));
    });
    scheduler.repeat(1000, timeSetCallback);
#if WITH_SERIAL_DATA_STREAM
    if (Config::config.runtimeSerialDataStream)
      initSerialDataStream();
#endif // #if WITH_SERIAL_DATA_STREAM
  } else {
    LOG(Logging::Hardware, Warning, cat(F("No hardware components defined.")));
  }
}

} // namespace Hardware
