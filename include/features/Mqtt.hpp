#pragma once

#if defined(ESP8266) || DOXYGEN_RUN || NATIVE

/**
 * @defgroup MQTTFlags MQTT Configuration flags
 * @ingroup ConfigurationFlags
 */

/**
  @file Mqtt.hpp
  @brief MQTT functionality
*/

#if !defined(WITH_MQTT_CLI) || DOXYGEN_RUN
/**
 * @ingroup MQTTFlags
 * @brief Whether to provide the Cli functionality also via MQTT
 */
#define WITH_MQTT_CLI true
#endif // #ifndef WITH_MQTT_CLI

#if !defined(MQTT_SUBSCRIBE_TIME) || DOXYGEN_RUN
/**
 * @ingroup MQTTFlags
 * @brief Whether to subscribe to #MQTT_TIME_TOPICS to retrieve the time
 */
#define MQTT_SUBSCRIBE_TIME true
#endif // #ifndef MQTT_SUBSCRIBE_TIME

#if !defined(MQTT_TIME_TOPICS) || DOXYGEN_RUN
/**
 * @ingroup MQTTFlags
 * @brief Topic(pattern)s to subscribe to containing the time if
 *  #MQTT_SUBSCRIBE_TIME is true
 */
#define MQTT_TIME_TOPICS "time/unix"
#endif // #ifndef MQTT_TIME_TOPICS

#if !defined(MQTT_SUBSCRIBE_PRESSURE) || DOXYGEN_RUN
/**
 * @ingroup MQTTFlags
 * @brief Whether to subscribe to #MQTT_PRESSURE_TOPICS to retrieve the
 *   pressure
 */
#define MQTT_SUBSCRIBE_PRESSURE true
#endif // #ifndef MQTT_SUBSCRIBE_PRESSURE

#if !defined(MQTT_PRESSURE_TOPICS) || DOXYGEN_RUN
/**
 * @ingroup MQTTFlags
 * @brief Topic(pattern)s to subscribe to containing the pressure if
 *  #MQTT_SUBSCRIBE_PRESSURE is true
 */
#define MQTT_PRESSURE_TOPICS "+/+/pressure"
#endif // #ifndef MQTT_PRESSURE_TOPICS

#if !defined(MAX_TOPIC_COPY_SIZE) || DOXYGEN_RUN
/**
  @brief Size for the writable topic copy buffer
*/
#define MAX_TOPIC_COPY_SIZE 100
#endif // #ifndef MAX_TOPIC_COPY_SIZE

#if !defined(WITH_MQTT_GLOBAL_PRESSURE_MEASUREMENT) || DOXYGEN_RUN
/**
  @brief Whether to provide a global Measurement for pressure via MQTT
*/
#define WITH_MQTT_GLOBAL_PRESSURE_MEASUREMENT true
#endif // #ifndef WITH_MQTT_GLOBAL_PRESSURE_MEASUREMENT

#if WITH_MQTT

#if ARDUINO
#include "../utils/StreamGateway.hpp"
#include "Scheduler.hpp"
#endif // #if ARDUINO

#include "../config/Config.hpp"
#include "../config/Globals.hpp"
#include "../utils/StringUtils.hpp"
#include "../utils/Utils.hpp"

#if WITH_MQTT_GLOBAL_PRESSURE_MEASUREMENT
#include "../hardware/GenericComponent.hpp"
#endif // #if WITH_MQTT_GLOBAL_PRESSURE_MEASUREMENT

#include <cctype>

#if ARDUINO
#include <PubSubClient.h>
#include <Time.h>
#include <TimeLib.h>
#endif // #if ARDUINO

//! MQTT functionality
namespace MQTT {

#if WITH_MQTT_GLOBAL_PRESSURE_MEASUREMENT
Hardware::GenericComponent mqttComponent;
Hardware::Component::Measurement pressureMeasurement(mqttComponent,
                                                     "pressure",
                                                     "hPa");
#endif // #if WITH_MQTT_GLOBAL_PRESSURE_MEASUREMENT

bool wasConnectedOnce = false;

/**
 * @brief Determine if a topic matches a subscription pattern
 * @details This is a very simple and fast matching implementation. It does not
 *   work reliably for invalid topics or patterns.
 * @param topic the topic to check. This string will be modified.
 * @param pattern the subscription pattern to check against. This string will
 *   be modified
 * @returns whether the topic matches the pattern
 */
bool
topicMatchesPattern(char* topic, char* pattern)
{
  char *topicToken = nullptr, *patternToken = nullptr, *topicTmp = nullptr,
       *patternTmp = nullptr;
  size_t topicTokenCount = 0, patternTokenCount = 0;
  while (true) {
    patternTokenCount++;
    patternToken =
      strtok_r(patternToken ? nullptr : pattern, "/", &patternTmp);
    if (not patternToken)
      break;
    LOG(Network + 2,
        Debug + 10,
        cat(F("pattern token Nr. "), patternTokenCount, ": ", patternToken));
    topicTokenCount++;
    topicToken = strtok_r(topicToken ? nullptr : topic, "/", &topicTmp);
    if (not topicToken) {
      LOG(Network + 2,
          Debug + 10,
          cat(F("expecting '"),
              patternToken,
              F("', but topic has no tokens left")));
      return false;
    }
    LOG(Network + 2,
        Debug + 10,
        cat("topic token Nr. ", topicTokenCount, ": ", topicToken));
    if (strncmp(patternToken, "+", 2) == 0) {
      LOG(Network + 2,
          Debug + 10,
          cat(F("topic token matches single-element wildcard")));
      continue;
    } else if (strncmp(patternToken, "#", 2) == 0) {
      LOG(Network + 2,
          Debug + 10,
          cat(F("rest of topic matches multi-element wildcard")));
      return true;
    } else {
      const bool tokenMatches = strcmp(topicToken, patternToken) == 0;
      LOG(Network + 2,
          Debug + 10,
          cat("topic and pattern token ",
              tokenMatches ? "" : "do not ",
              "match"));
      if (tokenMatches)
        continue;
      else
        return false;
    }
  }
  LOG(Network + 2, Debug + 10, cat(F("reached end of pattern")));
  return true;
}

template<size_t topicSize, size_t patternSize>
bool
topicMatchesPattern(const char (&topic)[topicSize],
                    const char (&pattern)[patternSize])
{
  char writableTopic[topicSize];
  strncpy(writableTopic, topic, topicSize);
  char writablePattern[patternSize];
  strncpy(writablePattern, pattern, patternSize);
  return topicMatchesPattern(topic, pattern);
}

//! Convenience overload with copy-buffer size of #MAX_TOPIC_COPY_SIZE
bool
topicMatchesPattern(const char* topic, const char* pattern)
{
  char writableTopic[MAX_TOPIC_COPY_SIZE];
  strncpy(writableTopic, topic, MAX_TOPIC_COPY_SIZE);
  char writablePattern[MAX_TOPIC_COPY_SIZE];
  strncpy(writablePattern, pattern, MAX_TOPIC_COPY_SIZE);
  return topicMatchesPattern(writableTopic, writablePattern);
}

#if ARDUINO

//! whether to cease trying to reconnect
bool paused = false;

#if MQTT_SUBSCRIBE_TIME
const char* timeTopics[] = { MQTT_TIME_TOPICS };
#endif // #if MQTT_SUBSCRIBE_TIME

#if MQTT_SUBSCRIBE_PRESSURE
const char* pressureTopics[] = { MQTT_PRESSURE_TOPICS };
#endif // #if MQTT_SUBSCRIBE_PRESSURE

Scheduler::Scheduler<2
#if WITH_MQTT_CLI
                     + 1
#endif // #if WITH_MQTT_CLI
                     >
  scheduler;

//! Whether we have subscribed to the topics we're interested in
bool subscribed = false;

#if WITH_MQTT_CLI
StreamGateway<> cliStream;

char cliInTopic[100];
char cliOutTopic[100];
#endif // #if WITH_MQTT_CLI

char connectedTopic[100];
bool connectedPublished = false;

void
callback(const char* topic, const byte* payload, unsigned int payloadLength)
{
  if (paused)
    return;
  LOG(
    Network + 2,
    Debug,
    cat(F("MQTT: Recieved message in topic '"), topic, "': ");
    for (size_t i = 0; i < payloadLength; i++) {
      const uint8_t value = payload[i];
      cat(not(0x20 <= value and value <= 0x7E) ? (char)0x20 : (char)value);
    };
    cat(' ', '[');
    for (size_t i = 0; i < payloadLength; i++) {
      if (i)
        cat(' ');
      const uint8_t value = payload[i];
      char tmp[4] = { 0 };
      cat(sprintf2(tmp, "%0X", value));
    };
    cat(']'););
#if WITH_MQTT_CLI
  if (topicMatchesPattern(topic, cliInTopic)) {
    cliStream.queue(payload, payloadLength);
    return;
  }
#endif // #if WITH_MQTT_CLI
#if MQTT_SUBSCRIBE_TIME
  for (size_t i = 0; i < Utils::arraySize(timeTopics); i++) {
    if (topicMatchesPattern(topic, timeTopics[i])) {
      char* rest;
      char payloadAsChar[MQTT_MAX_PACKET_SIZE] = { 0 };
      // copy the payload into a definitely zero-terminated buffer
      memcpy(payloadAsChar, payload, payloadLength);
      const unsigned long unixTime = strtoul(payloadAsChar, &rest, 10);
      if (not unixTime or unixTime == ULONG_MAX) {
        LOG(Network + 2,
            Warning,
            cat("Couldn't interpret payload as ascii unix timestamp"));
        return;
      }
      LOG(Network + 2, Info, cat("Recieved unix timestamp ", unixTime));
      if (strlen(rest) > 0) {
        LOG(Network + 2,
            Verbose,
            cat("Ignoring ",
                strlen(rest),
                " remaining bytes of timestamp payload: ",
                rest));
      }
      if (unixTime < 1583700248) {
        LOG(Network + 2,
            Verbose,
            cat("Ignoring very outdated unix timestamp ", unixTime));
        return;
      }
      LOG(System, Info, cat("Setting time to ", unixTime));
      setTime(unixTime);
      return;
    }
  }
#endif // #if MQTT_SUBSCRIBE_TIME
#if MQTT_SUBSCRIBE_PRESSURE
  for (size_t i = 0; i < Utils::arraySize(pressureTopics); i++) {
    if (topicMatchesPattern(topic, pressureTopics[i])) {
      char* unit;
      char payloadAsChar[MQTT_MAX_PACKET_SIZE] = { 0 };
      // copy the payload into a definitely zero-terminated buffer
      memcpy(payloadAsChar, payload, payloadLength);
      const float pressure = strtod(payloadAsChar, &unit);
      if (not pressure) {
        LOG(Network + 2,
            Error,
            cat("ERROR: ",
                "Couldn't interpret '",
                payloadAsChar,
                "' as pressure"));
        return;
      }
      while (isspace(*unit) and strlen(unit)) // skip whitespace
        unit++;
      if (strlen(unit)) { // make sure pressure is in hPa
        if (strncasecmp(unit, "hPa", 4) == 0) {
          // no need to convert anything
        } else {
          LOG(Network + 2,
              Error,
              cat("ERROR: ",
                  F("Don't know how to convert pressure "),
                  pressure,
                  " from ",
                  "unit ",
                  unit,
                  " to ",
                  "hPa",
                  '.'));
          return;
        }
      } else { // without unit, try to determine it
        if (Utils::pressurehPaOkay(pressure)) {
          LOG(Network + 2,
              Verbose,
              cat("Pressure ", pressure, F(" looks like unit "), "hPa"));
        } else {
          LOG(Network + 2,
              Error,
              cat("ERROR: ",
                  F("Don't know what unit pressure "),
                  pressure,
                  " has"));
          return;
        }
      }
      if (Utils::pressurehPaOkay(pressure)) {
        LOG(System,
            Info,
            cat(F("Remembering "), "pressure", ' ', pressure, " hPa"));
        Globals::pressurehPa = pressure;
#if WITH_MQTT_GLOBAL_PRESSURE_MEASUREMENT
        pressureMeasurement = pressure;
#endif // #if WITH_MQTT_GLOBAL_PRESSURE_MEASUREMENT
      } else {
        LOG(Network + 1,
            Error,
            cat("ERROR: ",
                F("Ignoring strange pressure value "),
                pressure,
                ' ',
                "hPa"));
      }
      return;
    }
  }
#endif // #if MQTT_SUBSCRIBE_PRESSURE
  LOG(Network + 2,
      Warning,
      cat("WARNING: ", F("Don't know what to do with this message...")));
}

PubSubClient client;

char*
errorMessage(char* msg, const size_t msgLength)
{
  const int mqttState = client.state();
  switch (mqttState) {
    case MQTT_CONNECTION_TIMEOUT:
      strncpy_P(msg, PSTR("connection timeout"), msgLength);
      break;
    case MQTT_CONNECTION_LOST:
      strncpy_P(msg, PSTR("connection lost"), msgLength);
      break;
    case MQTT_CONNECT_FAILED:
      strncpy_P(msg, PSTR("connection failed"), msgLength);
      break;
    case MQTT_DISCONNECTED:
      strncpy_P(msg, PSTR("disconnected"), msgLength);
      break;
    case MQTT_CONNECTED:
      strncpy_P(msg, PSTR("connected"), msgLength);
      break;
    case MQTT_CONNECT_BAD_PROTOCOL:
      strncpy_P(msg, PSTR("bad protocol"), msgLength);
      break;
    case MQTT_CONNECT_BAD_CLIENT_ID:
      strncpy_P(msg, PSTR("bad client id"), msgLength);
      break;
    case MQTT_CONNECT_UNAVAILABLE:
      strncpy_P(msg, PSTR("connection unavailable"), msgLength);
      break;
    case MQTT_CONNECT_BAD_CREDENTIALS:
      strncpy_P(msg, PSTR("bad credentials"), msgLength);
      break;
    case MQTT_CONNECT_UNAUTHORIZED:
      strncpy_P(msg, PSTR("unauthorized"), msgLength);
      break;
    default:
      snprintf_P(msg, msgLength, PSTR("unknown error code: %d"), mqttState);
      break;
  };
  return msg;
}

template<size_t MSG_LENGTH>
char*
errorMessage(char (&msg)[MSG_LENGTH])
{
  return errorMessage(msg, MSG_LENGTH);
}

bool
subscribe(const char* topic, int qos = 0)
{
  const bool success = client.subscribe(topic);
  if (success) {
    LOG(Network + 2, Info, cat("Subscribed to topic ", topic));
  } else {
    LOG(Network + 2, Error, char buf[100];
        cat("Couldn't subscribe to topic '", topic, "': ", errorMessage(buf)));
  }
  return success;
}
bool
publish(const char* topic,
        const uint8_t* payload,
        const size_t payloadLength,
        const bool retain = false)
{
  const bool published = client.publish(topic, payload, payloadLength, retain);
  LOG(
    Network + 2,
    Debug + 20,
    cat("MQTT: ",
        published ? F("published ") : F("WARNING: Could not publish "),
        payloadLength,
        " bytes to topic ",
        topic,
        ": ");
    for (size_t i = 0; i < payloadLength; i++) {
      const uint8_t value = payload[i];
      cat(not(0x20 <= value and value <= 0x7E) ? (char)0x20 : (char)value);
    };
    cat(' ', '[');
    for (size_t i = 0; i < payloadLength; i++) {
      if (i)
        cat(' ');
      const uint8_t value = payload[i];
      if (value <= 0xF)
        cat(0);
      char tmp[4] = { 0 };
      cat(sprintf2(tmp, "%0X", value));
    };
    cat(']', ' ');
    if (not published) {
      char buf[100];
      cat(' ', '(', errorMessage(buf), ')');
    };);
  return published;
}

//! Convenience overload of publish() to use a payload of known size
template<size_t PAYLOAD_SIZE>
bool
publish(const char* topic,
        const uint8_t (&payload)[PAYLOAD_SIZE],
        const bool retain = false)
{
  return publish(topic, payload, PAYLOAD_SIZE, retain);
}

//! Convenience overload of publish() to use a C-string pointer as payload
bool
publish(const char* topic, const char* payload, const bool retain = false)
{
  return publish(topic, (const uint8_t*)payload, strlen(payload), retain);
}

//! Convenience overload of publish() to use a C-string as payload
template<size_t PAYLOAD_SIZE>
bool
publish(const char* topic,
        const char (&payload)[PAYLOAD_SIZE],
        const bool retain = false)
{
  const size_t payloadLength = strlen(payload);
  return publish(
    topic, (const uint8_t*)payload, min(PAYLOAD_SIZE, payloadLength), retain);
}

bool
reconnect()
{
  subscribed = false;
  connectedPublished = false;
  return client.connect(Config::config.getStationName(),
                        connectedTopic, // will topic
                        0,              // QoS
                        true,           // retain will message
                        "no (forced)"   // will message
  );
}

template<typename AddressType>
bool
connect(AddressType address, const int port = 1883)
{
  client.setServer(address, port);
  return reconnect();
}

//! Disconnect MQTT #client and set #subscribed to false
void
disconnect()
{
  publish(connectedTopic, "no (freely)");
  client.disconnect();
  subscribed = false;
}

void
setup()
{
  snprintf_P(connectedTopic,
             Utils::arraySize(connectedTopic),
             PSTR("station/%s/connected"),
             Config::config.getStationName());
  client.setCallback(callback);
  scheduler.add([]() {
    if (client.connected())
      wasConnectedOnce = true;
    client.loop();
  });
#if WITH_MQTT_CLI
  cliStream.setWriteFunc([](const uint8_t* payload, const size_t payloadSize) {
    MQTT::publish(MQTT::cliOutTopic, payload, payloadSize);
  });
  scheduler.repeat(1e3, []() { cliStream.flush(); });
  sprintf_P(
    cliInTopic, PSTR("station/%s/cli/in"), Config::config.getStationName());
  sprintf_P(
    cliOutTopic, PSTR("station/%s/cli/out"), Config::config.getStationName());
#endif // #if WITH_MQTT_CLI
  // make sure we're subscribed to the topics we're interested in
  scheduler.repeat(1e3, []() {
    if (paused)
      return;
    if (not client.connected()) {
      subscribed = false;
      connectedPublished = false;
      reconnect();
      return;
    }
    if (not connectedPublished)
      connectedPublished = publish(connectedTopic, "yes", true);
    if (subscribed)
      return;
    bool success = true;
#if WITH_MQTT_CLI
    success &= subscribe(cliInTopic);
#endif // #if WITH_MQTT_CLI
#if MQTT_SUBSCRIBE_TIME
    for (size_t i = 0; i < Utils::arraySize(timeTopics); i++) {
      const char* topic = timeTopics[i];
      if (strlen(topic))
        success &= subscribe(topic);
    }
#endif // #if MQTT_SUBSCRIBE_TIME
#if MQTT_SUBSCRIBE_PRESSURE
    for (size_t i = 0; i < Utils::arraySize(pressureTopics); i++) {
      const char* topic = pressureTopics[i];
      if (strlen(topic))
        success &= subscribe(topic);
    }
#endif // #if MQTT_SUBSCRIBE_PRESSURE
    subscribed = success;
  });
}

#endif // #if ARDUINO

} // namespace MQTT
#endif // #if WITH_MQTT

#endif // #if ESP8266
