#pragma once

/**
  @file Multiplexer.hpp
  @brief I²C Multiplexer functionality
*/

/**
 * @defgroup MultiplexerFlags I²C Multiplexer Flags
 * @ingroup ConfigurationFlags
 */

#if !defined(WITH_I2C_MULTIPLEXER) || DOXYGEN_RUN
/**
 * @ingroup MultiplexerFlags
 * @brief Whether to enable support for a TCA9548A I²C multiplexer
 */
#define WITH_I2C_MULTIPLEXER true
#endif // #if WITH_I2C_MULTIPLEXER

#include "config/Defaults.hpp"

#if WITH_I2C_MULTIPLEXER

#include <Wire.h>

#include "../config/Globals.hpp"
#include "../utils/Utils.hpp"

/**
  @brief I²C Multiplexer functionality
*/
namespace Multiplexer {

/**
 * @brief   Multiplexer Class to integrate functions
 */
class TCA9548A
{
public:
  const size_t maxPortSlots = 7;

  /**
   * @brief   Error Responses for the Multiplexer Class
   */
  enum class Status
  {
    Unknown,
    Ok,
    I2CProtocolError,
    I2CWiringError,
    SensorIndexOutOfBounds,
    Error
  };

  TCA9548A()
    : address(0x70)
    , i2c(Wire)
    , portMask(0)
  {}

  //! Set the I²C address
  void setAddress(const uint8_t address) { this->address = address; }
  uint8_t getAddress(void) { return this->address; }

  /**
   * @brief   Selecting a port from the multiplexer
   * \param[in]     i        Multiplexer Port
   * \param[out]    Status   Error Status
   */
  Status selectPort(const uint8_t i)
  {
    if (i > maxPortSlots)
      return Status::SensorIndexOutOfBounds;
    uint8_t mask = 1 << i;
    this->i2c.beginTransmission(address);
    this->i2c.write(mask);
    const uint8_t errorCode = this->i2c.endTransmission();
    this->portMask = 0;
    if (errorCode == 0) {
      this->portMask = mask;
      return Status::Ok;
    } else if (1 <= errorCode && errorCode <= 4) {
      return Status::I2CProtocolError;
    } else if (4 < errorCode) {
      return Status::I2CWiringError;
    }
    return Status::Error;
  }

  /**
   * @brief   Printing the status from the various multiplexer ports
   * \param[in]     output   Selecting the output port (e.g. Serial)
   */
  void printPortRegistry(Print& output,
                         const size_t rowNameLength = 6,
                         const size_t colWidth = 6)
  {
    char rowNameFormat[8], rowName[rowNameLength + 1];
    sprintf(rowNameFormat, "%%%ds", rowNameLength);
    char colIntFormat[6], colStringFormat[6], colValue[colWidth + 1];
    sprintf(colIntFormat, "%%%dd", colWidth);
    sprintf(colStringFormat, "%%%ds", colWidth);
    sprintf(rowName, rowNameFormat, "port:");
    output.print(rowName);
    for (size_t i = 0; i <= maxPortSlots; i++) {
      sprintf(colValue, colIntFormat, i);
      output.print(colValue);
    }
    output.println();
    sprintf(rowName, rowNameFormat, "open:");
    output.print(rowName);
    for (size_t i = 0; i <= maxPortSlots; i++) {
      sprintf(colValue, colStringFormat, ((1 << i) & portMask) ? "yes" : "no");
      output.print(colValue);
    }
    output.println();
    output.flush();
  }

  /**
   * @brief   Selecting a port from the multiplexer
   *
   * \details The uint8_t gives the information about the selected ports
   *  as a binary to the multiplexer
   *
   * \param[in]     i        Multiplexer Ports
   * \param[out]    Status   Error Status
   */
  Status selectVariousPorts(const uint8_t mask)
  {
    this->i2c.beginTransmission(address);
    this->i2c.write(mask);
    const uint8_t errorCode = this->i2c.endTransmission();
    this->portMask = 0;
    if (errorCode == 0) {
      this->portMask = mask;
      return Status::Ok;
    } else if (1 <= errorCode && errorCode <= 4) {
      return Status::I2CProtocolError;
    } else if (4 < errorCode) {
      return Status::I2CWiringError;
    }
    return Status::Error;
  }

  /**
   * @brief   Checking if the Multiplexer is available
   * \param[out]    boolean   Availability of the multiplexer
   */
  bool isAvailable()
  {
    this->i2c.beginTransmission(address);
    if (this->i2c.endTransmission() == 0) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * @brief   Disabling the multiplexer, so you can verify if a sensor is
   * connected via multiplexer
   * \param[out]    Status   Error Status
   */
  Status disable() { return selectVariousPorts(0); }

  /**
   * @brief   Checks which ports on the multiplexer are active
   * \param[out]    portMask   Active ports as an uint8_t (need to read this as
   * a binary)
   */
  uint8_t querySelectedPorts()
  {
    this->i2c.requestFrom(address, (uint8_t)1);
    if (this->i2c.available() <= 1) {
      this->portMask = this->i2c.read();
    }
    return portMask;
  }
  /**
   * @brief   Returns the selected ports on the multiplexer
   */
  uint8_t getSelectedPorts() { return portMask; }

  /**
   * @brief   Transforms the StatusMessages into readable Strings
   * \param[in]     status        Error Status
   * \param[in]     str           empty String as placeholder
   * \param[out]    boolean       checks if the conversion was succesful
   */
  template<typename TYPE>
  static char* statusToString(const TYPE status, char* str)
  {
    switch (status) {
      case (TYPE)Status::Ok:
        strcpy(str, "Ok");
        break;
      case (TYPE)Status::I2CProtocolError:
        strcpy(str, "protError");
        break;
      case (TYPE)Status::I2CWiringError:
        strcpy(str, "wiringError");
        break;
      case (TYPE)Status::SensorIndexOutOfBounds:
        strcpy(str, "outOfBounds");
        break;
      case (TYPE)Status::Error:
        strcpy(str, "Error");
        break;
      case (TYPE)Status::Unknown:
        strcpy(str, "unknown");
        break;
      default:
        strcpy(str, "?");
        break;
    }
    return str;
  }

protected:
  uint8_t address;
  TwoWire& i2c;
  uint8_t portMask;
};

bool newStatus = false;
TCA9548A::Status actualStatus = TCA9548A::Status::Unknown;
TCA9548A::Status lastStatus = TCA9548A::Status::Unknown;

/**
 * @brief   Object for the TCA9548A Class
 */
TCA9548A multiplexer;

}

#endif //#if WITH_I2C_MULTIPLEXER
