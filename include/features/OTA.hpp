#pragma once

#if ESP8266

/**
  @file OTA.hpp
  @brief OTA functionality
*/

/**
 * @defgroup OTAFlags OTA Flags
 * @ingroup ConfigurationFlags
 */

#if !defined(WITH_OTA) || DOXYGEN_RUN
/**
 * @ingroup OTAFlags
 * @brief Whether to enable support for OTA uploading
 */
#define WITH_OTA true
#endif // #if WITH_OTA

#include "config/Config.hpp"
#include "config/Defaults.hpp"

#if WITH_OTA

#include <ArduinoOTA.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>

/**
  @brief OTA functionality
*/
namespace OTA {

void
initOTA()
{
  // Port defaults to 8266
  // ArduinoOTA.setPort(8266);

  // Hostname defaults to esp8266-[ChipID]
  ArduinoOTA.setHostname(Config::config.getStationName());

  // No authentication by default
  // ArduinoOTA.setPassword("admin");

  // Password can be set with it's md5 value as well
  // MD5(admin) = 21232f297a57a5a743894a0e4a801fc3
  // ArduinoOTA.setPasswordHash("21232f297a57a5a743894a0e4a801fc3");

  ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH)
      type = "sketch";
    else { // U_LittleFS
      type = "filesystem";
      LittleFS.end();
    }
    Serial.println("Start updating " + type);
  });
  ArduinoOTA.onEnd([]() { Serial.println("\nEnd"); });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR)
      Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR)
      Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR)
      Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR)
      Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR)
      Serial.println("End Failed");
  });
  ArduinoOTA.begin();
}

void
handleOTA()
{
  ArduinoOTA.handle();
}

}

#endif //#if WITH_OTA

#endif // #if ESP8266
