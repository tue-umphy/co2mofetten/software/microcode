#pragma once

/**
  @file Scheduler.hpp
  @brief Task Scheduling
*/

#include "../utils/Logging.hpp"

/**
  @namespace Scheduler
  @brief Task Scheduling
*/
namespace Scheduler {

//! simple callback type without arguments nor return value
typedef void (*voidCallback_t)(void);
//! interval in milliseconds
typedef unsigned long msInterval_t;
//! timestamp in milliseconds
typedef unsigned long msTime_t;

enum class Type : uint16_t
{
  None = 0,        //!< no type
  OneShot = 0x1,   //!< single-execution
  Interval = 0x10, //!< scheduled regularly
  Idle = 0x100,    //!< idle
};

/**
 * @brief   Task Scheduler
 */
template<size_t nTaskSlots>
class Scheduler
{
public:
  Scheduler(void)
    : _taskTypes{ Type::None }
    , _taskCallbacks{ nullptr }
    , _taskIntervals{ 0 }
    , _taskExecutionTimes{ 0 }
  {}

  /**
   * @brief determine the current amount of registered tasks
   * @return current number of registered tasks
   */
  size_t countTasks(void)
  {
    size_t n = 0;
    for (size_t i = 0; i < nTaskSlots; i++) {
      if (this->_taskTypes[i] != Type::None)
        n++;
    }
    return n;
  };

  /**
   * @brief Determine the longest scheduling interval
   * @return longest Interval or `0`
   */
  msInterval_t maxInterval(void)
  {
    msInterval_t maximum = 0;
    for (size_t i = 0; i < nTaskSlots; i++) {
      const msInterval_t interval = this->_taskIntervals[i];
      if (this->_taskTypes[i] == Type::Interval and interval > maximum)
        maximum = interval;
    }
    return maximum;
  };

  /**
   *  @brief Queue a function for later execution
   *  @param callback the function to queue
   *  @return whether the function was successfully queued
   */
  bool queue(voidCallback_t callback)
  {
    uint8_t freeSlot;
    if (not this->nextFreeSlot(freeSlot))
      return false;
    this->_taskCallbacks[freeSlot] = callback;
    this->_taskTypes[freeSlot] = Type::OneShot;
    return true;
  }

  /**
   *  @brief Schedule a function for repeated execution
   *  @param callback the function to queue
   *  @param interval minimum interval between executions in milliseconds
   *  @return whether the function was successfully scheduled
   */
  bool repeat(const msInterval_t interval, voidCallback_t callback)
  {
    uint8_t freeSlot;
    if (not this->nextFreeSlot(freeSlot))
      return false;
    this->_taskCallbacks[freeSlot] = callback;
    this->_taskTypes[freeSlot] = Type::Interval;
    this->_taskIntervals[freeSlot] = interval;
    this->_taskExecutionTimes[freeSlot] = 0;
    return true;
  }

  /**
   *  @brief Add a function for continuous execution
   *  @param callback the function to queue
   *  @return whether the function was successfully added
   */
  bool add(voidCallback_t callback)
  {
    uint8_t freeSlot;
    if (not this->nextFreeSlot(freeSlot))
      return false;
    this->_taskCallbacks[freeSlot] = callback;
    this->_taskTypes[freeSlot] = Type::Idle;
    return true;
  }

  /**
   * @brief Unschedule a function
   * @param callback the function to remove
   * @return whether the callback was successfully removed
   */
  bool remove(voidCallback_t callback)
  {
    if (not callback)
      return false;
    for (size_t i = 0; i < nTaskSlots; i++) {
      if (this->_taskCallbacks[i] == callback) {
        this->_taskTypes[i] = Type::None;
        this->_taskCallbacks[i] = nullptr;
        this->_taskExecutionTimes[i] = 0;
        this->_taskIntervals[i] = 0;
        return true;
      }
    }
    return false;
  }

  /**
   * @brief Unschedule a function at a specific index
   * @param callback the function to remove
   * @return whether the callback was successfully removed
   */
  bool remove(const uint8_t index)
  {
    if (index >= nTaskSlots)
      return false;
    this->_taskTypes[index] = Type::None;
    this->_taskCallbacks[index] = nullptr;
    this->_taskExecutionTimes[index] = 0;
    this->_taskIntervals[index] = 0;
    return true;
  }

  /**
   * @brief Execute tasks
   * @param types bitmask of #Type to select. Defaults to all types.
   * @details The registered tasks are executed in order.  Tasks of
   * Type::OneShot tasks are remove()d after execution. Tasks of Type::Interval
   * are only executed if their last execution time is more than their
   * specified interval in the past. Tasks of Type::Idle are always executed.
   * @todo Type::OneShot tasks should be executed first.
   */
  void loop(const uint16_t types = ~(uint16_t)0)
  {
    for (size_t i = 0; i < nTaskSlots; i++) {
      if ((uint16_t)this->_taskTypes[i] & types) {
        this->executeTask(i);
      }
    }
  }

protected:
  /**
   * @brief Execute a task depending on its type
   * @param index the task index
   */
  void executeTask(uint8_t index)
  {
    if (index >= nTaskSlots)
      return;
    switch (this->_taskTypes[index]) {
      case Type::None:
        return;
      case Type::OneShot: {
        voidCallback_t callback = this->_taskCallbacks[index]; // get it
        if (callback) {
          callback();             // call it
          this->remove(callback); // unqueue it
        }
        return;
      }
      case Type::Interval: {
        voidCallback_t callback = this->_taskCallbacks[index]; // get it
        if (not callback)
          return;
        msTime_t lastTime = this->_taskExecutionTimes[index];
        if (((millis() - lastTime) > this->_taskIntervals[index]) or
            not lastTime) {
          callback(); // call it
          this->_taskExecutionTimes[index] = millis();
        }
        return;
      }
      case Type::Idle: {
        voidCallback_t callback = this->_taskCallbacks[index]; // get it
        if (callback)
          callback(); // call it
        return;
      }
    }
  }

  /**
   * @brief determine the next free task slot index
   * @return next free task slot index, `-1` if none is empty
   */
  bool nextFreeSlot(uint8_t& index)
  {
    for (size_t i = 0; i < nTaskSlots; i++) {
      if (this->_taskTypes[i] == Type::None) {
        index = i;
        return true;
      }
    }
    return false;
  };

  Type _taskTypes[nTaskSlots];
  voidCallback_t _taskCallbacks[nTaskSlots];
  msInterval_t _taskIntervals[nTaskSlots];
  msTime_t _taskExecutionTimes[nTaskSlots];
};

}
