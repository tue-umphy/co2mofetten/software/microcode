#pragma once

/**
  @file WatchDog.hpp
  @brief Watchdog Automatic Reset Functionality
*/

/**
  @defgroup WatchDogFlags Command-Line WatchDog Flags
  @ingroup ConfigurationFlags
*/

#if !defined(WITH_WATCHDOG) || DOXYGEN_RUN
/**
  @ingroup WatchDogFlags
  @brief Whether to enable the watchdog
  @details

    When enabled, the watchdog causes an auto-reset if the µC is stuck on a
    task for too long.
*/
#define WITH_WATCHDOG true
#endif // #ifndef WITH_WATCHDOG

#include "../config/Defaults.hpp"

#if WITH_WATCHDOG

#include <LongerWatchDog.h>
/**
  @brief Watchdog object
*/
LongerWatchDog WatchDog;

/**
  @brief Reset the µC after a specified amount of milliseconds
  @note This macro expands to nothing if #WITH_WATCHDOG is false.
*/
#define WATCHDOG_RESET_IF_ENABLED_AFTER_MS(ms)                                \
  WatchDog.device_reset_after_ms(ms)

/**
  @brief Reset the µC after #MEASUREMENT_INTERVAL_MS milliseconds
  @details If #MEASUREMENT_INTERVAL_MS is 0, use a default value.
  @note This macro expands to nothing if #WITH_WATCHDOG is false.
*/
#define WATCHDOG_ENABLE()                                                     \
  WATCHDOG_RESET_IF_ENABLED_AFTER_MS(                                         \
    MEASUREMENT_INTERVAL_MS > 0 ? MEASUREMENT_INTERVAL_MS * 2 : 30e3)

/**
  @brief Reset the µC after a specified amount of milliseconds with a minimum
    of #MEASUREMENT_INTERVAL_MS milliseconds
  @note This macro expands to nothing if #WITH_WATCHDOG is false.
*/
#define WATCHDOG_ENABLE_MIN(ms)                                               \
  WATCHDOG_RESET_IF_ENABLED_AFTER_MS(                                         \
    MEASUREMENT_INTERVAL_MS > 0                                               \
      ? (MEASUREMENT_INTERVAL_MS > ms ? MEASUREMENT_INTERVAL_MS : ms)         \
      : ms)

#else // #if WITH_WATCHDOG

#define WATCHDOG_RESET_IF_ENABLED_AFTER_MS(ms)
#define WATCHDOG_ENABLE()
#define WATCHDOG_ENABLE_MIN(ms)

#endif // #if WITH_WATCHDOG
