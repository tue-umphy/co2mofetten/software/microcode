#pragma once

#if defined(ESP8266) || DOXYGEN_RUN

/**
 * @file WiFi.hpp
 * @brief WiFi networking
 */

/**
 * @defgroup WiFiFlags Configuration Flags
 * @ingroup ConfigurationFlags
 */

#if !defined(WIFI_RECONNECTION_INTERVAL_MS) || DOXYGEN_RUN
/**
 * @ingroup WiFiFlags
 * @brief Minimum interval between WiFi reconnections attemps
 */
#define WIFI_RECONNECTION_INTERVAL_MS 1e3
#endif // #ifndef WIFI_RECONNECTION_INTERVAL_MS

#if !defined(WIFI_CONNECT_TIMEOUT_MS) || DOXYGEN_RUN
/**
 * @ingroup WiFiFlags
 * @brief Maximum time in milliseconds to wait for a specific WiFi connection
 */
#define WIFI_CONNECT_TIMEOUT_MS 30e3
#endif // #ifndef WIFI_CONNECT_TIMEOUT_MS

#if !defined(NETWORKS_JSON_FILENAME) || DOXYGEN_RUN
/**
 * @ingroup WiFiFlags
 * @brief LittleFS filename of networks configuration JSON file
 */
#define NETWORKS_JSON_FILENAME "/networks.json"
#endif // #ifndef NETWORKS_JSON_FILENAME

#include "../config/Defaults.hpp"

#if WITH_WIFI

#include <ArduinoJson.h>
#include <ArduinoSort.h>
#include <ESP8266Ping.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <FS.h>
#include <StreamUtils.h>

#include <Time.h>
#include <TimeLib.h>

#include "../utils/BitUtils.hpp"
#include "../utils/Logging.hpp"
#include "Mqtt.hpp"
#include "Scheduler.hpp"

#if !defined(DEFAULT_NETWORKS_JSON) || DOXYGEN_RUN
/**
 * @ingroup WiFiFlags
 * @brief Default JSON-encoded definition of prioritized WiFi-network
 *   conditions to connect to like this:
 *   ```.py
 *     [
 *      # Standard definition with network name and password
 *      {"ssid": "...", "password":"..."},
 *      # Here the router should also provide a MQTT broker
 *      # ”router” is just a shortcut, IP addresses should also be possible
 *      {"ssid": "...", "password":"...","services":{"router":{"mqtt":true}}},
 *      ...
 *     ]
 *   ```
 *   This value is used if no #NETWORKS_JSON_FILENAME file can be found in
 *   LittleFS.  #WITH_MQTT enabled, the default connects to the first
 *   password-less network where the router provides an MQTT broker, otherwise
 *   password-less networks are tried out.
 */
#if WITH_MQTT
#define DEFAULT_NETWORKS_JSON "[{\"services\":{\"any\":{\"mqtt\":true}}}]"
#else // #if WITH_MQTT
#define DEFAULT_NETWORKS_JSON "[{}]"
#endif // #if WITH_MQTT
#endif // #ifndef DEFAULT_NETWORKS_JSON

/**
 * @brief WiFi Networking Resources
 * @details
 *  This namespace contains functions related to WiFi networking.
 */
namespace WiFiNetwork {

WiFiClient wifiClient;

//! Scheduler for WiFi tasks
Scheduler::Scheduler<3
#if WITH_MQTT
                     + 3
#endif // #if WITH_MQTT
                     >
  scheduler;

bool
strongerRSSI(int a, int b)
{
  return WiFi.RSSI(a) > WiFi.RSSI(b);
}

bool
parseNetworkConfigFromFs(JsonDocument& doc)
{
  if (LittleFS.begin()) {
    File file = LittleFS.open(NETWORKS_JSON_FILENAME, "r");
    if (not file) {
      LOG(Network,
          Info,
          cat("No '", NETWORKS_JSON_FILENAME, "' file on LittleFS"));
      return false;
    }
    ReadBufferingStream bufferedFile(file, 64);
    DeserializationError err = deserializeJson(doc, bufferedFile);
    file.close();
    if (err) {
      LOG(Network,
          Error,
          cat("Can't parse ",
              NETWORKS_JSON_FILENAME,
              "' file on LittleFS as JSON: ",
              err.c_str()));
      return false;
    }
    return true;
  }
  return false;
}

bool
parseDefaultNetworkConfig(JsonDocument& doc)
{
  DeserializationError err = deserializeJson(doc, DEFAULT_NETWORKS_JSON);
  if (err) {
    LOG(Network,
        Warning,
        cat("Couldn't deserialize default JSON networks definition (",
            err.c_str(),
            "):",
            "\n",
            DEFAULT_NETWORKS_JSON));
    return false;
  }
  return true;
}

bool
parseNetworkConfig(JsonDocument& doc)
{
  bool success;
  success = parseNetworkConfigFromFs(doc); // first stry LittleFS
  if (not success) // otherwise fall back to COMPILER FLAG
    success = parseDefaultNetworkConfig(doc);
  if (success) {
    LOG(Network,
        Debug,
        cat(F("Prioritized WiFi networks:\n"), doc.as<JsonVariantConst>()));
  }
  return success;
}

void
doMDNS(void)
{
  if (WiFi.status() == WL_CONNECTED) {
    if (not WiFi.hostname(Config::config.getStationName())) {
      LOG(Network,
          Warning,
          cat(F("Problem setting hostname to "),
              Config::config.getStationName()));
    }
    if (not MDNS.begin(Config::config.getStationName())) {
      LOG(Network,
          Warning,
          cat(F("Problem starting mDNS with name "),
              Config::config.getStationName()));
    }
  }
}

void
ensureConnected()
{
  if (WiFi.status() == WL_CONNECTED)
    return;
  if (WiFi.getMode() != WIFI_STA) {
    LOG(Network, Info, cat(F("Setting WiFi-Mode to "), "STATION"));
    const bool success = WiFi.mode(WIFI_STA);
    if (not success) {
      LOG(
        Network, Error, cat(F("Couldn't set WiFi-Mode to "), "STATION", '!'));
      return;
    }
  }
  const int8_t scanStatus = WiFi.scanComplete();
  switch (scanStatus) {
    case WIFI_SCAN_RUNNING:
      LOG(Network, Info, cat("WiFi network scan still ongoing..."));
      return;
    case WIFI_SCAN_FAILED:
      LOG(Network,
          Info,
          cat("Begin scanning for WiFi networks in the background..."));
      WiFi.scanNetworks(true);
      return;
  }
  if (scanStatus < 0) {
    LOG(Network, Warning, cat(F("Strange scan result code: "), scanStatus));
    WiFi.scanDelete();
    return;
  }
  StaticJsonDocument<1500> networksDoc;
  if (not parseNetworkConfig(networksDoc))
    return;
  const size_t nFoundNetworks = scanStatus;
  int sortedByRSSI[64]; // array holding the network indices sorted by RSSI
  for (size_t i = 0; i < nFoundNetworks; i++)
    sortedByRSSI[i] = i; // initialize the array with ascending indices
  sortArrayReverse(sortedByRSSI, scanStatus, strongerRSSI); // sort by RSSI
  for (size_t _i = 0; _i < nFoundNetworks; _i++) { // loop over networks
    bool networkOk = true;
    const size_t i = sortedByRSSI[_i];
    LOG(
      Network,
      Verbose,
      cat(
        F("Found network '"), WiFi.SSID(i), F("' with RSSI "), WiFi.RSSI(i)));
    // check conditions
    size_t conditionsMet = 0;
    for (JsonObject condition : networksDoc.as<JsonArray>()) {
      if (not condition["ssid"].isNull() and condition["ssid"] != WiFi.SSID(i))
        continue; // to try out next condition
      const char* password = condition["password"];
      LOG(Network,
          Verbose,
          cat(F("Connecting to WiFi network "), '\'', WiFi.SSID(i), '\'');
          if (password) cat(F(" with password "), '\'', password, '\'');
          else cat(F(" without password"));
          cat("..."));
      WiFi.begin(WiFi.SSID(i), password);
      wl_status_t lastStatus = WiFi.status();
      unsigned long time_before = millis();
      while (true) { // wait for connection
        yield();
        if (millis() - time_before > WIFI_CONNECT_TIMEOUT_MS) {
          LOG(Network,
              Warning,
              cat(F("Stop waiting for connection on '"),
                  WiFi.SSID(i),
                  F("', takes too long...")));
          break;
        }
        const wl_status_t status = WiFi.status();
        if (status != lastStatus) {
          LOG(Network, Debug, cat("WiFi.Status() = ", status));
          lastStatus = status;
        }
        if (status == WL_CONNECTED or status == WL_CONNECT_FAILED)
          break; // stop waiting for connection
      }
      if (WiFi.status() == WL_CONNECTED)
        doMDNS();
      else {
        LOG(Network,
            Error,
            cat(F("Couldn't connect to WiFi network "), WiFi.SSID()));
        continue; // to try out next condition
      }
      LOG(
        Network, Info, cat(F("Now connected to WiFi network "), WiFi.SSID()));
      // now check for all machines that are supposed to provides services
      for (JsonPair mkv : condition["services"].as<JsonObject>()) {
        // if (not networkOk)
        //   break; // stop checking for services on machines
        const char* machine = mkv.key().c_str();
        JsonObject machineServices = mkv.value().as<JsonObject>();
        // check all services for this machine
        for (JsonPair skv : machineServices) {
          // if (not networkOk)
          //   break; // stop checking services for this machine
          const char* service = skv.key().c_str();
#if WITH_MQTT
          if (strncmp_P(service, PSTR("mqtt"), 4) == 0) {
            bool mqttWorks = false;
            uint16_t port = skv.value()["port"] | 1883U;
            LOG(Network + 2, Debug, cat(F("MQTT port to check is "), port));
            if (mkv.key() == "any") { // scan IPs!
              const IPAddress routerAddress = WiFi.gatewayIP();
              uint32_t routerAddressInt = 0;
              // routerAddress: lowest-valued byte is the „rightmost” part
              for (size_t i = 0; i < 4; i++)
                routerAddressInt |= routerAddress[i] << ((3 - i) * 8);
              LOG(Network + 2, Debug, cat("Router IP: ", routerAddress));
              LOG(Network + 2, Debug, cat("Router IP: ", routerAddressInt));
              LOG(Network + 2,
                  Info,
                  cat(F("Scanning for MQTT brokers in subnet with mask "),
                      WiFi.subnetMask()));
              // subnetMask is now more intuitive: fixed bits start at high
              // side, not from right side
              const uint32_t subnetMask =
                reverseBits((uint32_t)WiFi.subnetMask());
              // same for localIP: lowest-valued byte is the „rightmost” part
              const uint32_t subnet = routerAddressInt & subnetMask;
              LOG(Network + 2, Debug, cat("Subnet: ", subnet));
              const uint32_t localIP = reverseBytes((uint32_t)WiFi.localIP());
              LOG(Network + 2, Debug + 10, cat("IP ", WiFi.localIP()));
              LOG(
                Network + 2, Debug + 10, cat("IP ", (uint32_t)WiFi.localIP()));
              LOG(Network + 2, Debug + 10, cat("localIP ", localIP));
              LOG(Network + 2, Debug + 10, cat("Subnetmask ", subnetMask));
              const uint32_t nIPsInSubnet = ~subnetMask;
              LOG(Network + 2,
                  Debug + 10,
                  cat("# of IPs in subnet: ", nIPsInSubnet));
              for (size_t count = 0; count < nIPsInSubnet; count++) {
                if (mqttWorks)
                  break; // stop scanning subnet
                const uint32_t nextAddressSuffix =
                  ((routerAddressInt & ~subnetMask) + count) % nIPsInSubnet;
                LOG(Network + 2,
                    Debug + 10,
                    cat("Next suffix: ", nextAddressSuffix));
                const uint32_t machineAddressInt = subnet + nextAddressSuffix;
                const IPAddress machineAddress(
                  reverseBytes(machineAddressInt));
                const bool machineUp = Ping.ping(machineAddress, 1);
                if (not machineUp) {
                  LOG(Network + 2,
                      Verbose,
                      cat(F("Skipping offline machine "), machineAddress));
                  continue; // with scanning the next IP in the subnet
                }
                mqttWorks = MQTT::connect(machineAddress, port);
                LOG(Network + 2,
                    Verbose,
                    cat("Machine '",
                        machineAddress,
                        mqttWorks ? F("' provides ") : F("' doesn't provide "),
                        F("an MQTT broker with options "),
                        skv.value().as<JsonVariantConst>()));
              }
            } else {                       // single machine check
              if (mkv.key() == "router") { // use router
                LOG(Network + 2,
                    Verbose,
                    cat(F("Router address is "), WiFi.gatewayIP()));
                mqttWorks = MQTT::connect(WiFi.gatewayIP(), port);
              } else {
                mqttWorks = MQTT::connect(machine, port);
              }
              LOG(Network + 2,
                  Verbose,
                  cat("Machine '",
                      machine,
                      mqttWorks ? F("' provides ") : F("' doesn't provide "),
                      F("an MQTT broker with options "),
                      skv.value().as<JsonVariantConst>()));
            }
            networkOk = mqttWorks;
            continue; // with checking the next service on this machine
          }
#endif // #if WITH_MQTT
          LOG(Network,
              Warning,
              cat("WARNING: ",
                  F("Don't know how to check for service "),
                  service,
                  F("' with options "),
                  skv.value().as<JsonVariantConst>(),
                  F("' on machine '"),
                  machine));
          networkOk = false; // can't check --> assume it doesn't work
        }
      }
      if (networkOk) {
        LOG(
          Network,
          Info,
          cat(
            "Network '", WiFi.SSID(i), F("' fulfils condition "), condition));
        conditionsMet++;
        break;
      }
    }
    if (networkOk and conditionsMet > 0)
      break;
    else {
      LOG(Network,
          Info,
          cat(F("Disconnecting from network '"),
              WiFi.SSID(i),
              F("' as it doesn't fulfil any set of conditions")));
      WiFi.disconnect();
    }
  }
  WiFi.scanDelete();
}

char*
statusToString(const int status, char* statusString)
{
  switch (status) {
    case WL_CONNECTED:
      strcpy(statusString, "connected");
      break;
    case WL_IDLE_STATUS:
      strcpy(statusString, "idle");
      break;
    case WL_NO_SSID_AVAIL:
      strcpy(statusString, "ssid unavailable");
      break;
    case WL_CONNECT_FAILED:
      strcpy(statusString, "connect failed");
      break;
    case WL_DISCONNECTED:
      strcpy(statusString, "disconnected");
      break;
    default:
      strcpy(statusString, "?");
      break;
  }
  return statusString;
}

void
setup(void)
{
  scheduler.add([]() { MDNS.update(); });
#if WITH_MQTT
  MQTT::setup();
  scheduler.add([]() { MQTT::scheduler.loop(); });
  MQTT::client.setClient(wifiClient);
  scheduler.repeat(1e3, []() {
    if (WiFi.status() == WL_CONNECTED) {
      if (not MQTT::client.connected()) {
        if (not MQTT::reconnect() and MQTT::wasConnectedOnce) {
          LOG(Logging::Network,
              Warning,
              cat(F("MQTT connection can't be reestablished. Seems to be this "
                    "one bug where we have to reconnect to WiFi...")));
          WiFi.disconnect();
          LOG(Logging::Network,
              Warning,
              cat(F("Disconnected from WiFi network")));
        }
      }
    }
  });
#endif // #if WITH_MQTT
  scheduler.repeat(WIFI_RECONNECTION_INTERVAL_MS, []() {
    if (Config::config.wifiKeepConnecting) {
      ensureConnected();
    }
  });
  scheduler.repeat(30e3, doMDNS);
}

} // namespace WiFiNetwork

#endif // #if WITH_WIFI

#endif // #if ESP8266
