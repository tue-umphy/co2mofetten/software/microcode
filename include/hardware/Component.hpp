#pragma once

#if !defined(HARDWARE_COMPONENT_ENABLE_IS) || defined(DOXYGEN_RUN)
/**
 * @ingroup HardwareFlags
 * @brief Whether to add #HARDWARE_COMPONENT_IS_DECLARATIONS to
 * #HARDWARE_COMPONENT and thus enable Hardware::Component::is() in decorated
 * Hardware::Component subclasses
 */
#define HARDWARE_COMPONENT_ENABLE_IS true
#endif // #ifndef HARDWARE_COMPONENT_ENABLE_IS

#define STRINGIFY(x) #x

#ifndef HARDWARE_COMPONENT_ENABLE_CLASS_STRING
/**
 * @ingroup HardwareFlags
 * @brief Whether to add #HARDWARE_COMPONENT_CLASS_STRING_DECLARATIONS to
 * #HARDWARE_COMPONENT and thus enable Hardware::Component::classString and
 * Hardware::Component::getClassString() in decorated Hardware::Component
 * subclasses
 */
#define HARDWARE_COMPONENT_ENABLE_CLASS_STRING true
#endif // #ifndef HARDWARE_COMPONENT_ENABLE_CLASS_STRING

#include "../config/Defaults.hpp"
#include "../utils/Logging.hpp"
#include "../utils/Timestamp.hpp"

#if WITH_MQTT || defined(UNIT_TEST)
#ifndef MQTT_MAX_PACKET_SIZE
#define MQTT_MAX_PACKET_SIZE 128
#endif // #ifndef MQTT_MAX_PACKET_SIZE
#include "../utils/StringUtils.hpp"
#endif // #if WITH_MQTT || defined(UNIT_TEST)

#if ARDUINO_ARCH_AVR
#include <ArduinoSTL.h>
#endif // #if ARDUINO_ARCH_AVR

#include <ArduinoJson.h>

#include <algorithm>
#include <functional>
#include <vector>

#if NATIVE
#include <chrono>
#include <cstdio>
#else // #if NATIVE
#include <Time.h>
#endif // #if NATIVE

#if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
#define HARDWARE_COMPONENT_CLASS_STRING_DECLARATIONS(cls)                     \
public:                                                                       \
  static constexpr const char* classString = STRINGIFY(cls);                  \
  virtual const char* getClassString() { return cls::classString; }           \
                                                                              \
private:
#else // #if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
#define HARDWARE_COMPONENT_CLASS_STRING_DECLARATIONS(cls)
#endif // #if HARDWARE_COMPONENT_ENABLE_CLASS_STRING

#if HARDWARE_COMPONENT_ENABLE_IS
size_t __hardwareComponentClassId = 0;

#define HARDWARE_COMPONENT_IS_DECLARATIONS_BASE(id)                           \
public:                                                                       \
  static const size_t __classId = id;                                         \
  template<typename componentType>                                            \
  bool is()                                                                   \
  {                                                                           \
    return this->_is(componentType::__classId);                               \
  }                                                                           \
                                                                              \
private:                                                                      \
  virtual bool _is(const size_t classId) { return this->__classId == classId; }

#if defined(__COUNTER__) || defined(DOXYGEN_RUN)
/**
 *  @ingroup HardwareFlags
 *  @brief Add Hardware::Component::is() to a Hardware::Component subclass
 *  @details If the compiler provides the `__COUNTER__` macro, classes
 * decorated with #HARDWARE_COMPONENT will be added a robust identifying id.
 * If, however, the compiler does not provide `__COUNTER__`, a **horrible,
 * unsafe, unreliable and hacky** alternative based on the address of the
 * `__FILE__` string constant the and `__LINE__` number will be used as class
 * id.
 */
#define HARDWARE_COMPONENT_IS_DECLARATIONS                                    \
  HARDWARE_COMPONENT_IS_DECLARATIONS_BASE(__COUNTER__)
#else // #ifndef __COUNTER__
#pragma message("No __COUNTER__. Using an unsafe "                            \
                "alternative to enable Hardware::Component::is()")
#define HARDWARE_COMPONENT_IS_DECLARATIONS                                    \
  HARDWARE_COMPONENT_IS_DECLARATIONS_BASE((uintptr_t)__FILE__ -               \
                                          (uintptr_t)__LINE__)
#endif // #ifndef __COUNTER__
#else  // #if HARDWARE_COMPONENT_ENABLE_IS
#define HARDWARE_COMPONENT_IS_DECLARATIONS
#endif // #if HARDWARE_COMPONENT_ENABLE_IS

/**
 *  @ingroup HardwareFlags
 *  @brief Add common functionality to Hardware::Component subclasses
 *  @details Put this at the top of the subclass definition
 */
#define HARDWARE_COMPONENT(cls)                                               \
  HARDWARE_COMPONENT_CLASS_STRING_DECLARATIONS(cls)                           \
  HARDWARE_COMPONENT_IS_DECLARATIONS                                          \
public:                                                                       \
  template<size_t bufSize>                                                    \
  char* toString(char(&buf)[bufSize])                                         \
  {                                                                           \
    return this->toString(buf, bufSize);                                      \
  }                                                                           \
                                                                              \
private:

namespace Hardware {

/**
 * @brief Base class for hardware components
 * @details To enable Hardware::Component::is(), a subclass **MUST** include
 * #HARDWARE_COMPONENT somwhere in its definition.
 */
class Component
{

  HARDWARE_COMPONENT(Component)

public:
  struct TraverseContext
  {
    TraverseContext()
      : level(0)
      , abort(false)
      , hardwareAccess(true)
    {}
    size_t level;
    bool abort;
    bool hardwareAccess;
  };

#if ARDUINO_ARCH_AVR
  // uClibC++ (and thus the port ArduinoSTL which we use) doesn't implement
  // std::function unfortunately, so we stick with oldschool function pointers
  // for Arduinos
  typedef void (*traverseCallback_t)(Component& component,
                                     TraverseContext& context,
                                     void* extra);
#else
  typedef std::function<
    void(Component& component, TraverseContext& context, void* extra)>
    traverseCallback_t;
#endif // #if ARDUINO_ARCH_AVR

  struct Measurement : public Number
  {
    Measurement(Component& component,
                const char* quantity = "?",
                const char* unit = nullptr)
      : Number::Number(FNAN)
      , quantity(quantity)
      , unit(unit)
      , time()
      , _component(component)
      , _callback(nullptr)
      , _callbackExtra(nullptr)
    {}

    typedef void (*callback_t)(const Measurement& measurement,
                               const Measurement& previousMeasurement,
                               void* extra);

    virtual Component& component() const { return this->_component; }

    template<typename T>
    void set(const T newValue)
    {
      const Measurement previousMeasurement = *this;
      this->time.refresh();
      Number::operator=(newValue);
      if (this->_callback)
        this->_callback(*this, previousMeasurement, this->_callbackExtra);
    }

    template<typename T>
    void set(const T newValue, const Timestamp timeStamp)
    {
      const Measurement previousMeasurement = *this;
      this->time = timeStamp;
      Number::operator=(newValue);
      if (this->_callback)
        this->_callback(*this, previousMeasurement, this->_callbackExtra);
    }

    virtual void connect(callback_t callback, void* extra = nullptr)
    {
      this->_callback = callback;
      this->_callbackExtra = extra;
    }

    template<typename T>
    Measurement& operator=(const T newValue)
    {
      this->set(newValue);
      return *this;
    }

    virtual bool operator==(const char* str) const
    {
      return str == this->quantity or strcmp(str, this->quantity) == 0;
    }

  public:
    const char* const quantity;
    const char* const unit;
    Timestamp time;

  private:
    Component& _component;
    callback_t _callback;
    void* _callbackExtra;
  };

  struct Measurements : public std::vector<Measurement*>
  {
    using std::vector<Measurement*>::vector;
    virtual Measurement* operator[](const char* const quantity) const
    {
      for (auto measurement : *this) {
        if (not measurement)
          continue;
        if (measurement->quantity == quantity or
            strcmp(measurement->quantity, quantity) == 0)
          return measurement;
      }
      return nullptr;
    }
  };

  //! Measure
  virtual void measure(Measurement& measurement) {}
  virtual void measure(std::initializer_list<const char*> quantities = {})
  {
    LOG(Logging::Hardware,
        Debug + 20,
        cat(__PRETTY_FUNCTION__,
            this->name.c_str(),
            ' ',
            F(": starting loop over "),
            this->measurements.size(),
            ' ',
            "children"));
    for (auto ptr : this->measurements) {
      LOG(
        Logging::Hardware, Debug + 30, cat(__PRETTY_FUNCTION__, "deref ptr"));
      auto& measurement = *ptr;
      LOG(Logging::Hardware,
          Debug + 30,
          cat(__PRETTY_FUNCTION__, "after ptr deref"));
      if (quantities.size()) {
        LOG(Logging::Hardware,
            Debug + 10,
            cat(__PRETTY_FUNCTION__, F("quantities defined")));
        auto itr =
          std::find_if(quantities.begin(),
                       quantities.end(),
                       [&](const char* q) { return measurement == q; });
        if (itr == quantities.end())
          continue;
      }
      LOG(Logging::Hardware, Debug + 10, cat(measurement.quantity));
      this->measure(measurement);
    }
    LOG(Logging::Hardware, Debug + 20, cat(__PRETTY_FUNCTION__, "after loop"));
  }

  /**
   * @brief determine the upmost parent
   * @returns nullptr if
   */
  virtual Component* getRootParent()
  {
    Component* parent = this->getParent();
    while (parent) {
      Component* parentsParent = parent->getParent();
      if (not parentsParent)
        return parent;
      parent = parentsParent;
    }
    return parent;
  };

  /**
   * @brief Find a measurement of a component by name
   * @returns nullptr on failure
   */
  static Measurement* findMeasurement(
    const char* componentPattern,
    const char* measurementPattern,
    Hardware::Component* rootComponent = nullptr)
  {
    LOG(Logging::Hardware,
        Logging::Debug + 10,
        cat(__PRETTY_FUNCTION__,
            "(\"",
            componentPattern,
            "\",",
            measurementPattern,
            "\",",
            (uintptr_t)rootComponent,
            ")"));
    if (not rootComponent)
      return nullptr;
    TraverseContext context;
    context.hardwareAccess = false;
    struct Container
    {
      const char* componentPattern;
      const char* measurementPattern;
      Measurement* measurement;
    } container{ componentPattern, measurementPattern, nullptr };
    rootComponent->traverse(
      context,
      [](Hardware::Component& component,
         TraverseContext& context,
         void* extra) {
        Container& container = *reinterpret_cast<Container*>(extra);
        if (container.measurement)
          return;
        const bool componentNameMatches =
          linuxGlobMatch(container.componentPattern, component.name.c_str());
        LOG(Logging::Hardware,
            Logging::Debug + 10,
            cat(F("linuxGlobMatch"),
                "(\"",
                container.componentPattern,
                "\",\"",
                component.name.c_str(),
                "\") = ",
                componentNameMatches ? "true" : "false"));
        if (componentNameMatches) {
          for (auto measurement : component.measurements) {
            const bool measurementQuantityMatches = linuxGlobMatch(
              container.measurementPattern, measurement->quantity);
            LOG(Logging::Hardware,
                Logging::Debug + 10,
                cat(F("linuxGlobMatch"),
                    "(\"",
                    container.measurementPattern,
                    "\",\"",
                    measurement->quantity,
                    "\") = ",
                    componentNameMatches ? "true" : "false"));
            if (measurementQuantityMatches) {
              container.measurement = measurement;
              return;
            }
          }
        }
      },
      &container);
    if (not container.measurement) {
      LOG(Logging::Hardware,
          Logging::Error,
          cat(F("Couldn't find "),
              measurementPattern,
              F(" measurement of component named "),
              componentPattern))
      return nullptr;
    }
    LOG(Logging::Hardware,
        Verbose,
        cat(F("Found "),
            container.measurement->component().name.c_str(),
            F("'s "),
            container.measurement->quantity,
            F(" measurement")));
    return container.measurement;
  }

  Component(Component* parent = nullptr,
            std::initializer_list<Measurement*> m = {})
    : _parent(parent)
#ifdef ARDUINO
    , _lastTime(0)
    , _interval(0)
#endif // #ifdef  ARDUINO
    , error(*this)
    , measurements(m)
    , store(true)
  {}
  Component(Component& parent)
    : Component(&parent)
  {}

  template<typename componentType>
  componentType as()
  {
    return static_cast<componentType>(this);
  }

  virtual Component* getParent() { return this->_parent; }
  virtual void setParent(Component* parent) { this->_parent = parent; }
  virtual void setParent(Component& parent) { this->setParent(&parent); }

  struct Error
  {
    Error(Component& component, const char* s = "?")
      : time()
      , _component(component)
      , _string(s)
      , _lastString(nullptr)
      , _changedCallback(nullptr)
      , _changedCallbackExtra(nullptr)
    {}
    typedef void (*changedCallback_t)(const Error& error,
                                      const Error& previousError,
                                      void* extra);
    virtual void setSilently(const char* s) { this->_string = s; }
    virtual Error& operator=(const char* s)
    {
      if ((*this) != s) {
        const Error oldError = *this;
        this->time.refresh();
        this->_string = s;
        if (this->_changedCallback)
          this->_changedCallback(*this, oldError, this->_changedCallbackExtra);
      }
      return *this;
    }
    virtual bool operator==(const char* s) const
    {
      return (s == this->_string or (strcmp(s, this->_string) == 0));
    }
    virtual bool operator!=(const char* s) const
    {
      return (s != this->_string or (strcmp(s, this->_string) != 0));
    }
    virtual const char* c_str() const { return this->_string; }
    virtual bool changed()
    {
      const bool r = (*this) == this->_lastString;
      this->_lastString = this->_string;
      return r;
    }
    virtual void connect(changedCallback_t callback, void* extra = nullptr)
    {
      this->_changedCallback = callback;
      this->_changedCallbackExtra = extra;
    }
    virtual operator bool() const { return (*this) != "ok"; }
    virtual operator const char*() const { return this->_string; }

    virtual Hardware::Component& component(void) const
    {
      return this->_component;
    }

    Timestamp time;

  private:
    Component& _component;
    const char* _string;
    const char* _lastString;
    changedCallback_t _changedCallback;
    void* _changedCallbackExtra;
  };

  virtual void traverse(TraverseContext& context,
                        traverseCallback_t callback,
                        void* callbackExtra = nullptr)
  {
    if (context.abort) {
      LOG(Logging::Hardware, Debug + 10, char tmp[100] = { 0 };
          cat("Skipping ", __PRETTY_FUNCTION__, " for ", this->toString(tmp)));
      return;
    }
    callback(*this, context, callbackExtra);
  }
  virtual void traverse(traverseCallback_t callback,
                        void* callbackExtra = nullptr)
  {
    TraverseContext context;
    this->traverse(context, callback, callbackExtra);
  }

  /**
   *  @brief Fill a cstring with a textual representation of this object
   *  @returns the given `buf`, but filled
   *  @details This method is pure virtual, subclasses must define this.
   */
  virtual char* toString(char* buf, const size_t bufSize) = 0;

#ifdef ARDUINOJSON_VERSION
  typedef Component* (*builderCallback_t)(JsonObjectConst json);
  static std::vector<builderCallback_t>& getBuilders(); // implemented later

  //! Set common properties from a Json object
  virtual void setCommonFrom(JsonObjectConst json)
  {
#if ARDUINO
    this->setInterval(json["interval"] | (unsigned long)0);
#endif // #if ARDUINO
    this->store = json["store"] | true;
  }

  /**
   * @brief Try all builders from getBuilders() to create an object of a
   * given type from a `JsonObject`
   * @returns nullptr on failure
   */
  static Component* from(JsonObjectConst json)
  {
    size_t i = 0;
    for (auto builder : Component::getBuilders()) {
      LOG(Logging::General,
          Logging::Debug + 1,
          cat("Trying ",
#if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
              classString,
              " ",
#endif // #if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
              "builder ",
              i++));
      Component* component = builder(json);
      if (component) {
        component->setCommonFrom(json);
        return component;
      }
    }
    LOG(Logging::Hardware,
        Logging::Error,
        cat(F("JSON "),
            json,
            F(" could't be converted to "),
#if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
            classString,
            " ",
#endif // #if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
            "subclass");)
    return nullptr;
  }

#endif // #ifdef ARDUINOJSON_VERSION

  static void print(Component& component,
                    TraverseContext& context,
                    void* extra = nullptr)
  {
    char repr[256] = { 0 };
#if NATIVE
    printf("%*s`--> (%#X, %#X) %s\n",
           context.level,
           "",
           (uintptr_t)&component,
           (uintptr_t)component.getParent(),
           component.toString(repr));
#else  // #if NATIVE
    Print& output = *reinterpret_cast<Print*>(extra);
    for (size_t i = 0; i < context.level; i++)
      output.print(' ');
    output.print("`--> (0x");
    output.print((uintptr_t)&component, HEX);
    output.print(", parent 0x");
    output.print((uintptr_t)component.getParent(), HEX);
    output.print(") ");
    output.println(component.toString(repr));
#endif // #if NATIVE
  }

  /**
   * @brief Fill a C-string with a reversed path representation of this
   *   Component
   * @details The reversed approach is the most efficient as we can walk up the
   *   tree using getParent() until we reach the top.
   * @returns the filled given C-string pointer
   */
  virtual char* toReversePath(char* path, const size_t pathSize)
  {
    char tmp[100] = { 0 };
    snprintf(
      path, strnfree(path, pathSize), "%s", strrev(this->name.c_str(), tmp));
    Component* parent = this->getParent();
    while (parent) {
      const std::string& name = parent->name;
      if (name.length())
        snprintfcat(path, pathSize, "/%s", strrev(name.c_str(), tmp));
      Component* parentsParent = parent->getParent();
      if (not parentsParent)
        break;
      parent = parentsParent;
    }
    return path;
  }

#if WITH_MQTT || defined(UNIT_TEST)
  virtual char* toMqttTopic(char* topic, const size_t topicSize)
  {
    this->toReversePath(topic, topicSize);
    char tmp[MQTT_MAX_PACKET_SIZE] = { 0 };
    snprintfcat(
      topic, topicSize, "/%s", strrev(Config::config.getStationName(), tmp));
    snprintfcat(topic, topicSize, "/%s", strrev("station", tmp));
    strrev(topic);
    return topic;
  }
#endif // #if WITH_MQTT || defined(UNIT_TEST)

#ifdef ARDUINO
  /**
   * @brief Determines whether this component is due for processing
   * @details Resets the timer if yes
   */
  virtual bool due()
  {
    auto m = millis();
    if ((not this->_interval or not this->_lastTime) or
        (m - this->_lastTime) > this->_interval) {
      this->_lastTime = m;
      return true;
    }
    return false;
  }

  virtual void resetTime(void) { this->_lastTime = 0; }

  virtual void setInterval(unsigned long interval)
  {
    this->_interval = interval;
  }
  virtual unsigned long getInterval(void) { return this->_interval; }
#endif // #ifdef ARDUINO

  template<typename TYPE>
  static TYPE* allocate()
  {
    TYPE* ptr = reinterpret_cast<TYPE*>(malloc(sizeof(TYPE)));
    if (not ptr) {
      LOG(Logging::General,
          0,
          cat("ERROR: ",
              "Not enough RAM"
#if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
              ,
              " to allocate ",
              TYPE::classString
#endif // #if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
              ));
    }
    return ptr;
  }

  std::string name;

protected:
  Component* _parent;
#ifdef ARDUINO
  unsigned long _lastTime;
  unsigned long _interval;
#endif // #ifdef ARDUINO

public:
  Error error;
  Measurements measurements;
  bool store;
};

} // namespace Hardware
