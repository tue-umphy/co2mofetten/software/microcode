#pragma once

#include "Component.hpp"

#if NATIVE
#include <cstdio>
#endif // #if NATIVE

namespace Hardware {

class Components
  : public std::vector<Component*>
  , public Component
{
  HARDWARE_COMPONENT(Components)
public:
  Components(Component* parent = nullptr)
    : std::vector<Component*>::vector()
    , Component::Component(parent)
  {}
  Components(Component& parent)
    : std::vector<Component*>::vector()
    , Component::Component(&parent)
  {}
  Components(const size_t nComponents, Component* parent = nullptr)
    : std::vector<Component*>::vector(nComponents, nullptr)
    , Component::Component(parent)
  {}
  Components(const size_t nComponents, Component& parent)
    : Components(nComponents, &parent)
  {}
  Components(std::initializer_list<Component*> components,
             Component* parent = nullptr)
    : std::vector<Component*>::vector{ components }
    , Component::Component(parent)
  {
    for (Component* component : *this) {
      if (not component)
        continue;
      component->setParent(this);
    }
  }

  //! append a component and set us as this component's parent
  virtual void add(Component& component)
  {
    component.setParent(this);
    this->push_back(&component);
  }

  virtual void traverse(TraverseContext& context,
                        traverseCallback_t callback,
                        void* callbackExtra = nullptr) override
  {
    if (context.abort) {
      LOG(Logging::Hardware,
          Debug,
          cat("Skipping ", __PRETTY_FUNCTION__, " for ", this->name.c_str()));
      return;
    }
    callback(*this->as<Component*>(), context, callbackExtra);
    size_t n = 0;
    for (Component* nestedComponent : *this) {
      if (context.abort) {
        LOG(
          Logging::Hardware, Debug, cat(F("Aborting "), __PRETTY_FUNCTION__));
        break;
      }
      n++;
      if (not nestedComponent)
        continue;
      const size_t levelBefore = context.level;
      Component::TraverseContext nestedContext = context; // copy context
      nestedContext.level++;
      nestedComponent->traverse(nestedContext, callback, callbackExtra);
      context = nestedContext; // apply
      context.level = levelBefore;
    }
  }
  virtual void traverse(traverseCallback_t callback,
                        void* callbackExtra = nullptr) override
  {
    TraverseContext context;
    this->traverse(context, callback, callbackExtra);
  }

  virtual char* toString(char* buf, const size_t bufSize) override
  {
    snprintf_P(buf,
               bufSize,
               PSTR("[%s] Collection of max. %u components"),
               this->name.c_str(),
               this->size());
    return buf;
  }

#ifdef ARDUINOJSON_VERSION
  static Component* from(JsonObjectConst json); // implemented later
#endif                                          // #ifdef ARDUINOJSON_VERSION
};

} // namespace Hardware
