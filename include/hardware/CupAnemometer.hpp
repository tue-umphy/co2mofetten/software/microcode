#pragma once

#ifndef WITH_CUP_ANEMOMETER
//! Whether to enable support for a cup anemometer
#define WITH_CUP_ANEMOMETER false
#endif // #ifndef WITH_CUP_ANEMOMETER

#if WITH_CUP_ANEMOMETER

#include "../utils/Polynomial.hpp"
#include "Component.hpp"

#include <Arduino.h>

namespace Hardware {

class CupAnemometer : public Component
{
  HARDWARE_COMPONENT(CupAnemometer)
  using Component::Component;
  CupAnemometer(const int pin,
                const size_t nVelocityPolynomCoefs = 0,
                const size_t nFlowPolynomCoefs = 0,
                Component* parent = nullptr)
    : Component::Component(
        parent,
        {
          new Hardware::Component::Measurement(*this, "frequency", "Hz"),
          new Hardware::Component::Measurement(*this, "velocity", "m/s"),
          new Hardware::Component::Measurement(*this, "flow-rate", "m^3/s"),
        })
    , pin(pin)
    , velocityPolynom(nVelocityPolynomCoefs)
    , flowPolynom(nFlowPolynomCoefs)
    , highThresholdFraction(0.9F)
    , lowThresholdFraction(0.1F)
    , minTicks(1024)
    , maxTicks(0)
    , _lowThreshold(0)
    , _highThreshold(1024)
  {}

public:
  virtual char* toString(char* buf, const size_t bufSize)
  {
    snprintf_P(buf,
               bufSize,
               PSTR("[%s] cup anemometer (%s)"),
               this->name.c_str(),
               this->error.c_str());
    return buf;
  }

  void updateThresholds()
  {
    this->_lowThreshold = this->minTicks + this->lowThresholdFraction *
                                             (this->maxTicks - this->minTicks);
    this->_highThreshold =
      this->minTicks +
      this->highThresholdFraction * (this->maxTicks - this->minTicks);
  }

  uint16_t readTicks()
  {
    uint16_t ticks = analogRead(this->pin);
    if (ticks < this->minTicks) {
      this->minTicks = ticks;
      this->updateThresholds();
    }
    if (ticks > this->maxTicks) {
      this->maxTicks = ticks;
      this->updateThresholds();
    }
    char buf[81];
    LOG(Logging::Sensor,
        Debug + 8,
        cat(sprintf2(buf,
                     "%u us: analogRead(%u) = %u [%u <= %u <= %u <= %u]",
                     millis(),
                     this->pin,
                     ticks,
                     this->minTicks,
                     this->_lowThreshold,
                     this->_highThreshold,
                     this->maxTicks)));
    return ticks;
  }

  unsigned long readPulseWidth()
  {
    const static unsigned long cutoff = 1000000;
    unsigned long timeBefore = micros();
    unsigned long microsPassed;
    // wait for low signal
    while (true) {
      if ((microsPassed = micros() - timeBefore) > cutoff)
        return 0;
      if (this->readTicks() < this->_lowThreshold)
        break;
    }
    // wait for high signal
    while (true) {
      if ((microsPassed = micros() - timeBefore) > cutoff)
        return 0;
      if (this->readTicks() > this->_highThreshold)
        break;
    }
    timeBefore = micros();
    // wait for low signal
    while (true) {
      if ((microsPassed = micros() - timeBefore) > cutoff)
        return 0;
      if (this->readTicks() < this->_lowThreshold)
        break;
    }
    // wait for high signal
    while (true) {
      if ((microsPassed = micros() - timeBefore) > cutoff)
        return 0;
      if (this->readTicks() > this->_highThreshold)
        break;
    }
    return microsPassed;
  }

  float readFrequency()
  {
    const auto pulseWidth = this->readPulseWidth();
    return pulseWidth > 1000 ? 1e6 / (float)pulseWidth : 0;
  }

  virtual void measure(Hardware::Component::Measurement& measurement) override
  {
    if (measurement == "frequency") {
      const auto frequency = this->readFrequency();
      if (frequency > 300)
        return;
      measurement = frequency;
      return;
    }
    if (measurement == "velocity") {
      const auto frequency = this->measurements["frequency"]->as<float>();
      measurement = this->velocityPolynom(frequency);
      return;
    }
    if (measurement == "flow-rate") {
      const auto frequency = this->measurements["frequency"]->as<float>();
      measurement = this->flowPolynom(frequency);
      return;
    }
    LOG(Logging::Hardware,
        Warning,
        cat(F("unhandled measurement "), measurement.quantity));
  }
  virtual void measure(
    std::initializer_list<const char*> quantities = {}) override
  {
    Hardware::Component::measure(quantities);
  }

#ifdef ARDUINOJSON_VERSION
  static Component* from(JsonObjectConst json)
  {
    LOG(
      Logging::Hardware, Debug + 10, cat(__PRETTY_FUNCTION__, "(", json, ")"));
    JsonVariantConst type = json["type"];
    if (type != "cup-anemometer")
      return nullptr;
    auto ptr = Component::allocate<CupAnemometer>();
    if (not ptr)
      return nullptr;
    auto velocityPolynom = json["velocity-polynom"];
    auto flowPolynom = json["flow-polynom"];
    auto& anemometer = *new (ptr) CupAnemometer(
      json["pin"] | (int)0,
      velocityPolynom.is<JsonArray>() ? velocityPolynom.size() : 2,
      flowPolynom.is<JsonArray>() ? flowPolynom.size() : 2);
    pinMode(anemometer.pin, INPUT);
    anemometer.lowThresholdFraction =
      json["thresholds"]["low"] | anemometer.lowThresholdFraction;
    anemometer.highThresholdFraction =
      json["thresholds"]["high"] | anemometer.highThresholdFraction;
    if (velocityPolynom.is<JsonArray>()) {
      size_t i = 0;
      for (const double coef : velocityPolynom.as<JsonArray>())
        anemometer.velocityPolynom.coefficients[i++] = coef;
    } else {
      anemometer.velocityPolynom.coefficients[0] = 0;
      anemometer.velocityPolynom.coefficients[1] = 1;
    }
    if (flowPolynom.is<JsonArray>()) {
      size_t i = 0;
      for (const double coef : flowPolynom.as<JsonArray>())
        anemometer.flowPolynom.coefficients[i++] = coef;
    } else {
      anemometer.flowPolynom.coefficients[0] = 0;
      anemometer.flowPolynom.coefficients[1] = 1;
    }
    return (&anemometer)->as<Component*>();
  }
#endif // #ifdef ARDUINOJSON_VERSION

  int pin;
  Polynomial velocityPolynom;
  Polynomial flowPolynom;
  float highThresholdFraction;
  float lowThresholdFraction;
  uint16_t minTicks;
  uint16_t maxTicks;

private:
  uint16_t _lowThreshold;
  uint16_t _highThreshold;
};

} // namespace Hardware

#endif // #if WITH_CUP_ANEMOMETER
