#pragma once

#ifndef WITH_EMISSION_CALCULATOR
//! Whether to enable support for a cup calculator
#define WITH_EMISSION_CALCULATOR false
#endif // #ifndef WITH_EMISSION_CALCULATOR

#if WITH_EMISSION_CALCULATOR

#include "Component.hpp"

#include "../features/Mqtt.hpp"

#include <Arduino.h>

namespace Hardware {

class EmissionCalculator : public Component
{
  HARDWARE_COMPONENT(EmissionCalculator)
  using Component::Component;
  EmissionCalculator(Component* parent = nullptr)
    : Component::Component(
        parent,
        { new Hardware::Component::Measurement(*this, "co2-flux", "g/s"),
          new Hardware::Component::Measurement(*this, "co2-flux-full", "g/s"),
          new Hardware::Component::Measurement(*this,
                                               "co2-emission-total",
                                               "kg"),
          new Hardware::Component::Measurement(*this,
                                               "co2-emission-full-total",
                                               "kg"),
          new Hardware::Component::Measurement(*this, "volume-total", "m^3"),
          new Hardware::Component::Measurement(*this, "co2", "ppm"),
          new Hardware::Component::Measurement(*this, "temperature", "*C"),
          new Hardware::Component::Measurement(*this, "pressure", "hPa"),
          new Hardware::Component::Measurement(*this, "velocity", "m/s"),
          new Hardware::Component::Measurement(*this, "flow-rate", "m^3/s") })
    , co2ComponentName()
    , pressureComponentName()
    , temperatureComponentName()
    , velocityComponentName()
    , flowRateComponentName()
    , crossSection(NAN)
    , _componentsSearched(false)
    , _measurementsSubscribed(false)
    , _lastPressure(NAN)
    , _lastTemperatureC(NAN)
    , _lastVelocity(NAN)
    , _lastCo2ppm(NAN)
    , _averagePressure(NAN)
    , _averageTemperature(NAN)
    , _averageVelocity(NAN)
    , _averageCo2(NAN)
    , _averageFlowRate(NAN)
    , _nPressure(0)
    , _nVelocity(0)
    , _nCo2(0)
    , _nFlowRate(0)
    , _meanEmissionKgps(0)
    , _nMeanEmissionKgps(0)
    , _meanFullEmissionKgps(0)
    , _nMeanFullEmissionKgps(0)
    , _totalEmissionKg(0)
    , _totalVolumeM3(0)
    , _totalFullEmissionKg(0)
    , _lastFluxKgps(NAN)
    , _lastFlowRateM3ps(NAN)
    , _lastIntegrationTime(0)
  {}

public:
  virtual char* toString(char* buf, const size_t bufSize)
  {
    snprintf_P(buf,
               bufSize,
               PSTR("[%s] CO2 emission calculator (%s)"),
               this->name.c_str(),
               this->error.c_str());
    return buf;
  }

  //! CO2 mass flux in kg/s
  static double co2Flux(double pressure_Pa,
                        double co2Ratio,
                        double flowRate_m3ps,
                        double temperature_K)
  {
    const double co2_molarmass_kgpmol = 44.01e-3; // kg / mol
    const double gas_constant = 8.3144598;        // J / (kg * K)
    return (flowRate_m3ps * co2Ratio * pressure_Pa * co2_molarmass_kgpmol) /
           (gas_constant * temperature_K);
  }

  bool integrateUntil(const Timestamp timeStamp)
  {
    // determine the flow rate from velocity and cross section or directly use
    // a flow-rate measurement
    double currentFlowRateM3ps = NAN;
    if (Utils::notNan(this->_lastFlowRate)) {
      currentFlowRateM3ps = this->_lastFlowRate;
    } else if (Utils::notNan(this->_lastVelocity) and
               Utils::notNan(this->crossSection)) {
      currentFlowRateM3ps = this->_lastVelocity * this->crossSection;
    }
    const bool allDefined =
      Utils::pressurehPaOkay(this->_lastPressure) and
      Utils::temperatureCelsiusOkay(this->_lastTemperatureC) and
      (Utils::notNan(currentFlowRateM3ps));
    // don't include CO2 here because otherwise the full total flux is not
    // calculated 😒
    if (not allDefined)
      return false;
    const double currentFluxKgps =
      this->co2Flux(this->_lastPressure * 100.0F,
                    this->_lastCo2ppm * 1e-6,
                    currentFlowRateM3ps,
                    this->_lastTemperatureC + 273.15);
    LOG(Logging::Hardware,
        Debug + 10,
        cat(F("co2Flux("),
            this->_lastPressure * 100.0F,
            ", ",
            this->_lastCo2ppm * 1e-6,
            ", ",
            currentFlowRateM3ps,
            ", ",
            this->_lastTemperatureC + 273.15,
            F(") = "),
            currentFluxKgps,
            F(" kg / s = "),
            currentFluxKgps * 1e6,
            F("mg / s")));
    const double currentFullFluxKgps =
      this->co2Flux(this->_lastPressure * 100.0F,
                    1.0F, // as if it was only CO2
                    currentFlowRateM3ps,
                    this->_lastTemperatureC + 273.15);
    LOG(Logging::Hardware,
        Debug + 10,
        cat(F("co2Flux("),
            this->_lastPressure * 100.0F,
            ", ",
            1.0F,
            ", ",
            currentFlowRateM3ps,
            ", ",
            this->_lastTemperatureC + 273.15,
            F(") = "),
            currentFullFluxKgps,
            F(" kg / s = "),
            currentFullFluxKgps * 1e6,
            F("mg / s")));
    if (this->_lastIntegrationTime) {
      const double tdiff =
        (timeStamp.epochMilliseconds() - this->_lastIntegrationTime) * 1e-3;
      LOG(Logging::Hardware, Debug + 11, cat(F("tdiff = "), tdiff));
      const double intervalFluxKgps =
        (this->_lastFluxKgps + currentFluxKgps) / 2;
      const double intervalFullFluxKgps =
        (this->_lastFullFluxKgps + currentFullFluxKgps) / 2;
      const double intervalFlowRateM3ps =
        (this->_lastFlowRateM3ps + currentFlowRateM3ps) / 2;
      this->_meanEmissionKgps =
        this->_nMeanEmissionKgps
          ? (this->_meanEmissionKgps * this->_nMeanEmissionKgps +
             intervalFluxKgps) /
              (this->_nMeanEmissionKgps + 1)
          : intervalFluxKgps;
      this->_nMeanEmissionKgps++;
      this->_meanFullEmissionKgps =
        this->_nMeanFullEmissionKgps
          ? (this->_meanFullEmissionKgps * this->_nMeanFullEmissionKgps +
             intervalFullFluxKgps) /
              (this->_nMeanFullEmissionKgps + 1)
          : intervalFullFluxKgps;
      this->_nMeanFullEmissionKgps++;
      if (tdiff > max((unsigned long)100, this->getInterval() * 10)) {
        LOG(
          Logging::Hardware,
          Warning,
          cat(F("time difference to last integration time is very large. "),
              F("Probably the time was set. Not adding this step to total.")));
        this->_lastIntegrationTime = timeStamp.epochMilliseconds();
        return false;
      }
      this->_totalEmissionKg += intervalFluxKgps * tdiff;
      this->_totalFullEmissionKg += intervalFullFluxKgps * tdiff;
      this->_totalVolumeM3 += intervalFlowRateM3ps * tdiff;
    }
    this->_lastFluxKgps = currentFluxKgps;
    this->_lastFullFluxKgps = currentFullFluxKgps;
    this->_lastFlowRateM3ps = currentFlowRateM3ps;
    this->_lastIntegrationTime = timeStamp.epochMilliseconds();
    return true;
  }

  void handleMeasurement(const Measurement& measurement,
                         const Measurement& previousMeasurement)
  {
    LOG(Logging::Hardware,
        Debug,
        cat(this->name.c_str(),
            F(": handling "),
            measurement.component().name.c_str(),
            F(" measurement "),
            measurement.quantity));
    const auto value = measurement.as<double>();
    if (measurement == "co2") {
      if (not Utils::co2ppmOkay(value)) {
        if (Utils::isNan(value))
          return;
        else {
          double clampedCo2ppm = max(min(value, 1000000.0), 0.0);
          LOG(Logging::Hardware,
              Verbose,
              cat(this->name.c_str(),
                  ": ",
                  F("Assuming strange "),
                  "co2",
                  F(" value "),
                  value,
                  F(" is 0"),
                  "ppm"));
          this->_lastCo2ppm = clampedCo2ppm;
        }
      } else {
        this->_lastCo2ppm = value;
      }
      this->_averageCo2 =
        this->_nCo2 ? (this->_averageCo2 * this->_nCo2 + this->_lastCo2ppm) /
                        (this->_nCo2 + 1)
                    : this->_lastCo2ppm;
      this->_nCo2++;
    } else if (measurement == "velocity") {
      this->_lastVelocity = value;
      this->_averageVelocity =
        this->_nVelocity
          ? (this->_averageVelocity * this->_nVelocity + this->_lastVelocity) /
              (this->_nVelocity + 1)
          : this->_lastVelocity;
      this->_nVelocity++;
    } else if (measurement == "flow-rate") {
      this->_lastFlowRate = value;
      this->_averageFlowRate =
        this->_nFlowRate
          ? (this->_averageFlowRate * this->_nFlowRate + this->_lastFlowRate) /
              (this->_nFlowRate + 1)
          : this->_lastFlowRate;
      this->_nFlowRate++;
    } else if (measurement == "pressure") {
      this->_lastPressure = value;
      this->_averagePressure =
        this->_nPressure
          ? (this->_averagePressure * this->_nPressure + this->_lastPressure) /
              (this->_nPressure + 1)
          : this->_lastPressure;
      this->_nPressure++;
    } else if (measurement == "temperature") {
      this->_lastTemperatureC = value;
      this->_averageTemperature =
        this->_nTemperature
          ? (this->_averageTemperature * this->_nTemperature +
             this->_lastTemperatureC) /
              (this->_nTemperature + 1)
          : this->_lastTemperatureC;
      this->_nTemperature++;
    }
    this->integrateUntil(measurement.time);
  }

  //! Also reset _lastIntegrationTime
  virtual void resetTime(void) override
  {
    Hardware::Component::resetTime();
    this->_lastIntegrationTime = 0;
  }

  bool subscribeToMeasurements()
  {
    if (this->_componentsSearched)
      return this->_measurementsSubscribed;
    this->_componentsSearched = true;
    auto rootParent = this->getRootParent();
    if (not rootParent)
      return false;
    TraverseContext context;
    context.hardwareAccess = false;
    struct Container
    {
      const char* pressureComponentName;
      const char* temperatureComponentName;
      const char* co2ComponentName;
      const char* velocityComponentName;
      const char* flowRateComponentName;
      Measurement* pressureMeasurement;
      Measurement* temperatureMeasurement;
      Measurement* co2Measurement;
      Measurement* velocityMeasurement;
      Measurement* flowRateMeasurement;
      EmissionCalculator* object;
      bool full(void) const
      {
        return pressureMeasurement and temperatureMeasurement and
               co2Measurement and (velocityMeasurement or flowRateMeasurement);
      }
      bool subscribe(void)
      {
        if (not this->pressureMeasurement) {
#if WITH_MQTT_GLOBAL_PRESSURE_MEASUREMENT
          LOG(Logging::Hardware,
              Info,
              cat(this->object->name.c_str(),
                  F(": Using global MQTT pressure measurement")));
          this->pressureMeasurement = &MQTT::pressureMeasurement;
#endif // #if WITH_MQTT_GLOBAL_PRESSURE_MEASUREMENT
        }
        if (not this->full()) {
          LOG(Logging::Hardware,
              Warning,
              cat(this->object->name.c_str(),
                  F(": didn't find all measurements")));
          LOG(Logging::Hardware,
              Warning,
              cat(this->object->name.c_str(),
                  F(": found measurements:  "),
                  this->pressureMeasurement ? "pressure " : "",
                  this->temperatureMeasurement ? "temperature " : "",
                  this->co2ComponentName ? "co2 " : "",
                  this->velocityMeasurement ? "velocity " : "",
                  this->flowRateMeasurement ? "flow-rate" : "",
                  F(" but not "),
                  not this->pressureMeasurement ? "pressure " : "",
                  not this->temperatureMeasurement ? "temperature " : "",
                  not this->co2ComponentName ? "co2 " : "",
                  not this->velocityMeasurement ? "velocity " : "",
                  not this->flowRateMeasurement ? "flow-rate " : ""));
          return false;
        }
        for (auto measurement :
             std::initializer_list<Measurement*>({ pressureMeasurement,
                                                   temperatureMeasurement,
                                                   co2Measurement,
                                                   velocityMeasurement,
                                                   flowRateMeasurement })) {
          if (not measurement)
            continue;
          LOG(Logging::Hardware,
              Verbose,
              cat(this->object->name.c_str(),
                  F(": overwriting subscription to "),
                  measurement->component().name.c_str(),
                  ' ',
                  measurement->quantity,
                  F(" measurement ")));
          measurement->connect(
            [](const Measurement& measurement,
               const Measurement& previousMeasurement,
               void* extra) {
              EmissionCalculator& calculator =
                *reinterpret_cast<EmissionCalculator*>(extra);
              calculator.handleMeasurement(measurement, previousMeasurement);
            },
            this->object);
        }
        return true;
      }
    } container{ this->pressureComponentName.c_str(),
                 this->temperatureComponentName.c_str(),
                 this->co2ComponentName.c_str(),
                 this->velocityComponentName.c_str(),
                 this->flowRateComponentName.c_str(),
                 nullptr,
                 nullptr,
                 nullptr,
                 nullptr,
                 nullptr,
                 this };
    rootParent->traverse(
      context,
      [](Component& component, TraverseContext& context, void* extra) {
        Container& container = *reinterpret_cast<Container*>(extra);
        LOG(Logging::Hardware,
            Debug,
            cat(F("searching in "), component.name.c_str()));
        for (auto measurement : component.measurements) {
          if (container.full())
            return;
          if (component.name == container.pressureComponentName) {
            if (measurement->operator==("pressure"))
              container.pressureMeasurement = measurement;
          }
          if (component.name == container.co2ComponentName) {
            if (measurement->operator==("co2"))
              container.co2Measurement = measurement;
          }
          if (component.name == container.temperatureComponentName) {
            if (measurement->operator==("temperature"))
              container.temperatureMeasurement = measurement;
          }
          if (component.name == container.velocityComponentName) {
            if (measurement->operator==("velocity"))
              container.velocityMeasurement = measurement;
          }
          if (component.name == container.flowRateComponentName) {
            if (measurement->operator==("flow-rate"))
              container.flowRateMeasurement = measurement;
          }
        }
      },
      &container);
    return this->_measurementsSubscribed = container.subscribe();
  }

  virtual void measure(Hardware::Component::Measurement& measurement) override
  {
    if (not this->subscribeToMeasurements())
      return;
    if (measurement == "volume-total") {
      measurement = this->_totalVolumeM3;
      return;
    }
    if (measurement == "co2-flux") {
      measurement = this->_meanEmissionKgps * 1e3; // kg/s --> g/s
      this->_nMeanEmissionKgps = 0;
      return;
    }
    if (measurement == "co2-flux-full") {
      measurement = this->_meanFullEmissionKgps * 1e3; // kg/s --> g/s
      this->_nMeanFullEmissionKgps = 0;
      return;
    }
    if (measurement == "co2-emission-total") {
      measurement = this->_totalEmissionKg;
      return;
    }
    if (measurement == "co2-emission-full-total") {
      measurement = this->_totalFullEmissionKg;
      return;
    }
    if (measurement == "temperature") {
      measurement = this->_averageTemperature;
      this->_nTemperature = 0;
      return;
    }
    if (measurement == "co2") {
      measurement = this->_averageCo2;
      this->_nCo2 = 0;
      return;
    }
    if (measurement == "pressure") {
      measurement = this->_averagePressure;
      this->_nPressure = 0;
      return;
    }
    if (measurement == "velocity" and
        not this->velocityComponentName.empty()) {
      measurement = this->_averageVelocity;
      this->_nVelocity = 0;
      return;
    }
    if (measurement == "flow-rate" and
        not this->flowRateComponentName.empty()) {
      measurement = this->_averageFlowRate;
      this->_nFlowRate = 0;
      return;
    }
    LOG(Logging::Hardware,
        Warning,
        cat(F("unhandled measurement "), measurement.quantity));
  }
  virtual void measure(
    std::initializer_list<const char*> quantities = {}) override
  {
    Hardware::Component::measure(quantities);
  }

#ifdef ARDUINOJSON_VERSION
  static Component* from(JsonObjectConst json)
  {
    LOG(
      Logging::Hardware, Debug + 10, cat(__PRETTY_FUNCTION__, "(", json, ")"));
    JsonVariantConst type = json["type"];
    if (type != "emission")
      return nullptr;
    auto ptr = Component::allocate<EmissionCalculator>();
    if (not ptr)
      return nullptr;
    auto& calculator = *new (ptr) EmissionCalculator;
    if (json["pressure"].is<char*>())
      calculator.pressureComponentName = json["pressure"].as<char*>();
    if (json["temperature"].is<char*>())
      calculator.temperatureComponentName = json["temperature"].as<char*>();
    if (json["co2"].is<char*>())
      calculator.co2ComponentName = json["co2"].as<char*>();
    if (json["velocity"].is<char*>())
      calculator.velocityComponentName = json["velocity"].as<char*>();
    if (json["flow-rate"].is<char*>())
      calculator.flowRateComponentName = json["flow-rate"].as<char*>();
    calculator.crossSection = json["cross-section"] | (double)NAN;
    if (calculator.flowRateComponentName.empty() and
        (Utils::isNan(calculator.crossSection) and
         calculator.velocityComponentName.empty())) {
      LOG(Logging::Hardware,
          Warning,
          cat(
#if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
            calculator.getClassString(),
            ' ',
#endif // #if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
            calculator.name.c_str(),
            F(" has neither cross-section and velocity component name nor "
              "flow-rate component name defined!")));
    }
    return (&calculator)->as<Component*>();
  }
#endif // #ifdef ARDUINOJSON_VERSION

  std::string co2ComponentName;
  std::string pressureComponentName;
  std::string temperatureComponentName;
  std::string velocityComponentName;
  std::string flowRateComponentName;

  double crossSection;

private:
  bool _componentsSearched;
  bool _measurementsSubscribed;
  double _lastPressure;
  double _lastTemperatureC;
  double _lastVelocity;
  double _lastCo2ppm;
  double _lastFlowRate;
  double _averagePressure;
  double _averageTemperature;
  double _averageVelocity;
  double _averageCo2;
  double _averageFlowRate;
  size_t _nPressure;
  size_t _nTemperature;
  size_t _nVelocity;
  size_t _nCo2;
  size_t _nFlowRate;
  double _meanEmissionKgps;
  size_t _nMeanEmissionKgps;
  double _meanFullEmissionKgps;
  size_t _nMeanFullEmissionKgps;
  double _totalEmissionKg;
  double _totalVolumeM3;
  double _totalFullEmissionKg;
  double _lastFluxKgps;
  double _lastFullFluxKgps;
  double _lastFlowRateM3ps;
  uintmax_t _lastIntegrationTime;
};

} // namespace Hardware

#endif // #if WITH_EMISSION_CALCULATOR
