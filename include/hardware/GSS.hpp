#pragma once

#ifndef WITH_GSS
//! Whether to enable support for the GSS CO2 sensor
#define WITH_GSS false
#endif // #ifndef WITH_GSS

#if WITH_GSS

#include "Component.hpp"

#include <Arduino.h>

#if !defined(WITH_SOFTWARESERIAL) || DOXYGEN_RUN
//! Whether to enable SoftwareSerial library usage
#define WITH_SOFTWARESERIAL true
#endif // #ifndef WITH_SOFTWARESERIAL

#if WITH_SOFTWARESERIAL
#include <SoftwareSerial.h>
#endif // #if WITH_SOFTWARESERIAL

namespace Hardware {

class GSS : public Component
{
  HARDWARE_COMPONENT(GSS)
  using Component::Component;
  GSS(Stream& port, Component* parent = nullptr)
    : Component::Component(
        parent,
        {
          new Hardware::Component::Measurement(*this, "co2", "ppm"),
        })
    , pressureComponentId()
    , _stream(&port)
    , _pressureMeasurementSearched()
    , _pressureMeasurement(nullptr)
    , _pressureCallback(nullptr)
    , _lastPressureCode(0)
  {}

public:
  typedef float (*pressureCallback_t)(void);

  void setPressureCallback(pressureCallback_t callback)
  {
    this->_pressureCallback = callback;
  }

  //! Given a pressure value in hPa/mbar, calculate the code to send to the
  //  sensor (formula determined by Kevin Hörmle)
  static uint16_t pressure2Code(const float pressurehPa)
  {
    return -8.192F * pressurehPa + 1.649e4;
  }

  virtual char* toString(char* buf, const size_t bufSize)
  {
    snprintf_P(buf,
               bufSize,
               PSTR("[%s] GSS CO2 sensor (%s)"),
               this->name.c_str(),
               this->error.c_str());
    return buf;
  }

  bool findPressureMeasurement(void)
  {
    if (this->_pressureMeasurementSearched)
      return this->_pressureMeasurement;
    this->_pressureMeasurementSearched = true;
    auto rootParent = this->getRootParent();
    if (not rootParent)
      return false;
    TraverseContext context;
    context.hardwareAccess = false;
    struct Container
    {
      const char* name;
      Measurement* measurement;
    } container{ this->pressureComponentId.c_str(), nullptr };
    rootParent->traverse(
      context,
      [](Component& component, TraverseContext& context, void* extra) {
        Container& container = *reinterpret_cast<Container*>(extra);
        if (container.measurement)
          return;
        if (component.name == container.name) {
          for (auto measurement : component.measurements) {
            if (measurement->operator==("pressure")) {
              container.measurement = measurement;
              return;
            }
          }
        }
      },
      &container);
    if (container.measurement) {
      this->_pressureMeasurement = container.measurement;
      LOG(Logging::Hardware,
          Verbose,
          cat(F("Successfully attached "),
              container.measurement->component().name.c_str(),
              F("'s "),
              container.measurement->quantity,
              F(" measurement to "),
              this->name.c_str()));
      return true;
    } else {
      LOG(
        Logging::Hardware,
        Logging::Error,
        cat(F("Couldn't find pressure measurement for "), this->name.c_str()));
    }
    return false;
  }

  bool calibrate(const uint32_t co2ppm)
  {
    if (not this->_stream)
      return false;
    uint16_t response;
    const bool success =
      co2ppm ? this->sendCommand('X', response, co2ppm / 100, 2000)
             : this->sendCommand('U', response, ~(uint16_t)0, 2000);
    if (success) {
      LOG(
        Logging::Hardware,
        Debug,
        cat(
          this->name.c_str(), F(" now uses "), response, F(" as CO2 offset")));
    }
    return success;
  }

  bool sendCommand(const char cmd,
                   uint16_t& response,
                   const uint16_t argument = ~(uint16_t)0,
                   const size_t timeout = 500)
  {
    if (not this->_stream)
      return false;
    this->_stream->write(cmd);
    if (argument != ~(uint16_t)0) {
      this->_stream->write(' ');
      this->_stream->print(argument, DEC);
    }
    this->_stream->write('\r');
    this->_stream->write('\n');
    this->_stream->flush();
    const auto timeBefore = millis();
    while (true) {
      if (millis() - timeBefore > timeout) {
        LOG(Logging::Hardware,
            Logging::Error,
            cat(this->name.c_str(), F(" didn't answer command")));
        return false;
      }
      if (not this->_stream->available())
        continue;
      char buf[1] = { 0 };
      this->_stream->readBytes(buf, 1);
      if (buf[0] == '?') {
        LOG(Logging::Hardware,
            Logging::Error,
            cat(this->name.c_str(), F(" didn't understand us")));
        return false;
      }
      if (buf[0] == cmd)
        break;
    }
    while (true) {
      if (millis() - timeBefore > timeout) {
        LOG(Logging::Hardware,
            Logging::Error,
            cat(this->name.c_str(), F(" didn't answer value")));
        return false;
      }
      const auto c = this->_stream->peek();
      if (0 <= c and c < 128) {
        if ((char)c == ' ')
          this->_stream->read();
        else
          break;
      }
    }
    const auto c = this->_stream->peek();
    if (0 <= c and c < 128 and (char) c == '?') {
      LOG(Logging::Hardware,
          Logging::Error,
          cat(this->name.c_str(), F(" doesn't know this command")));
      return false;
    }
    const auto answer = this->_stream->parseInt();
    response = answer;
    return true;
  }

  virtual void measure(Hardware::Component::Measurement& measurement) override
  {
    if (measurement == "co2") {
      this->findPressureMeasurement();
      float pressurehPa = FNAN;
      if (this->_pressureMeasurement)
        pressurehPa = this->_pressureMeasurement->as<float>();
      else if (this->_pressureCallback)
        pressurehPa = this->_pressureCallback();
      if (Utils::pressurehPaOkay(pressurehPa)) {
        const auto code = this->pressure2Code(pressurehPa);
        if (code == this->_lastPressureCode) {
          LOG(Logging::Hardware,
              Debug,
              cat(F("Calculated pressure code for "),
                  this->name.c_str(),
                  F(" would still be "),
                  code));
        } else {
          LOG(Logging::Hardware,
              Debug,
              cat(F("Sending pressure code "),
                  code,
                  F(" to "),
                  this->name.c_str()));
          if (this->sendCommand('S', this->_lastPressureCode, code)) {
            LOG(Logging::Hardware,
                Debug,
                cat(this->name.c_str(), F(" confirmed pressure")));
            this->error = "ok";
          } else {
            LOG(Logging::Hardware,
                Logging::Error,
                cat(this->name.c_str(),
                    F(" responded "),
                    this->_lastPressureCode,
                    F(" instead of "),
                    code));
            this->error = "pressure-problem";
          }
        }
      }
      LOG(Logging::Hardware,
          Debug,
          cat(F("Querying CO2 from "), this->name.c_str()));
      uint16_t response;
      if (this->sendCommand('z', response)) {
        const float co2 = response * 100.0F;
        if (Utils::co2ppmOkay(co2)) {
          this->error = "ok";
          measurement = co2;
          return;
        }
      } else
        this->error = "no-co2";
      return;
    }
    LOG(Logging::Hardware,
        Warning,
        cat(F("unhandled measurement "), measurement.quantity));
  }
  virtual void measure(
    std::initializer_list<const char*> quantities = {}) override
  {
    Hardware::Component::measure(quantities);
  }

#ifdef ARDUINOJSON_VERSION
  static Component* from(JsonObjectConst json)
  {
    LOG(
      Logging::Hardware, Debug + 10, cat(__PRETTY_FUNCTION__, "(", json, ")"));
    JsonVariantConst type = json["type"];
    if (type != "gss")
      return nullptr;
    auto ptr = Component::allocate<GSS>();
    if (not ptr)
      return nullptr;
    Stream* stream = nullptr;
    JsonVariantConst port = json["port"];
    const auto baudrate = json["baudrate"] | (uint32_t)9600;
    if (port.is<int>()) {
#if ARDUINO_AVR_MEGA2560
      switch (port.as<int>()) {
        case 1:
          Serial1.begin(baudrate);
          stream = &Serial1;
          break;
        case 2:
          Serial2.begin(baudrate);
          stream = &Serial2;
          break;
        case 3:
          Serial3.begin(baudrate);
          stream = &Serial3;
          break;
      };
#endif // #if ARDUINO_AVR_MEGA2560
    }
#if WITH_SOFTWARESERIAL
    else if (port.is<JsonArray>()) {
      if (port.size() == 2) {
        if (port[0].is<int>() and port[1].is<int>()) {
          SoftwareSerial* ptr =
            reinterpret_cast<SoftwareSerial*>(malloc(sizeof(SoftwareSerial)));
          if (ptr) {
            auto& serialPort =
              *new (ptr) SoftwareSerial(port[0].as<int>(), port[1].as<int>());
            serialPort.begin(baudrate);
            LOG(Logging::Hardware,
                Verbose,
                cat(F("Using SoftwareSerial port with RX pin "),
                    port[0].as<int>(),
                    F(" and TX pin "),
                    port[1].as<int>(),
                    F(" for GSS")));
            stream = static_cast<Stream*>(&serialPort);
          }
        }
      }
    }
#endif // #if WITH_SOFTWARESERIAL
    if (not stream) {
      LOG(Logging::Hardware,
          Warning,
          cat(F("Invalid serial port '"), port, '\''));
      return nullptr;
    }
    auto& gss = *new (ptr) GSS(*stream);
    if (json["pressure"].is<char*>())
      gss.pressureComponentId = json["pressure"].as<char*>();
    return (&gss)->as<Component*>();
  }
#endif // #ifdef ARDUINOJSON_VERSION

  std::string pressureComponentId;

private:
  Stream* _stream;
  bool _pressureMeasurementSearched;
  Measurement* _pressureMeasurement;
  pressureCallback_t _pressureCallback;
  uint16_t _lastPressureCode;
};

} // namespace Hardware

#endif // #if WITH_GSS
