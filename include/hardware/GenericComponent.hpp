#pragma once

#include "Component.hpp"

namespace Hardware {

class GenericComponent : public Component
{
  HARDWARE_COMPONENT(GenericComponent)
  using Component::Component;

public:
  virtual char* toString(char* buf, const size_t bufSize)
  {
    snprintf_P(buf,
               bufSize,
               PSTR("[%s] generic hardware component (%s)"),
               this->name.c_str(),
               this->error.c_str());
    return buf;
  }

#ifdef ARDUINOJSON_VERSION
  static Component* from(JsonObjectConst json)
  {
    LOG(Logging::Hardware, Debug, cat(__PRETTY_FUNCTION__, "(", json, ")"));
    auto ptr = Component::allocate<GenericComponent>();
    if (not ptr)
      return nullptr;
    return new (ptr) GenericComponent;
  }
#endif // #ifdef ARDUINOJSON_VERSION
};

} // namespace Hardware
