#pragma once

#include "../config/Defaults.hpp"
#include "../utils/Logging.hpp"

#include "Component.hpp"
#include "Components.hpp"
#include "CupAnemometer.hpp"
#include "EmissionCalculator.hpp"
#include "GSS.hpp"
#include "GenericComponent.hpp"
#include "Relay.hpp"
#include "SenseAirLP8.hpp"
#include "WiFiNetwork.hpp"

#if ARDUINO
#include "i2c/I2C.hpp"
#include "spi/SPI.hpp"
#endif // #if ARDUINO

namespace Hardware {

#ifdef ARDUINOJSON_VERSION

Component*
Components::from(JsonObjectConst json)
{
  LOG(Logging::Hardware, Debug, cat(__PRETTY_FUNCTION__, "(", json, ")"));
  for (JsonPairConst pair : json) {
    if (not pair.value().is<JsonObject>())
      return nullptr;
  }
  auto ptr = Component::allocate<Components>();
  if (not ptr)
    return nullptr;
  Components& components = *new (ptr) Components(json.size());
  size_t iComponent = 0;
  for (JsonPairConst jsonPair : json) {
    JsonString key = jsonPair.key();
    JsonObjectConst value = jsonPair.value();
    if (false) {
    }
#ifdef TwoWire_h
    else if (value["type"] == "i2c") {
      I2C::Component* component =
        I2C::Component::from(value["bus"].as<JsonObject>());
      if (component) {
        component->name = key.c_str();
        component->setParent(components);
      }
      components[iComponent] = component;
    }
#endif // #ifdef TwoWire_h
#ifdef _SPI_H_INCLUDED
    else if (value["type"] == "spi") {
      SPI::Component* component =
        SPI::Component::from(value["bus"].as<JsonObject>());
      if (component) {
        component->name = key.c_str();
        component->setParent(components);
      }
      components[iComponent] = component;
    }
#endif // #ifdef _SPI_H_INCLUDED
    else {
      auto component = Component::from(value);
      if (component) {
        component->name = key.c_str();
        component->setParent(components);
        components[iComponent] = component;
      } else {
        LOG(Logging::Hardware,
            Warning,
            cat(F("Ignoring unknown component '"), key.c_str()));
        components[iComponent] = nullptr;
      }
    }
    iComponent++;
  }
  return (&components)->as<Component*>();
}

std::vector<Component::builderCallback_t>&
Component::getBuilders()
{
  static std::vector<Component::builderCallback_t> builders
  {
#if WITH_WIFI
    WiFiNetwork::from,
#endif // #if WITH_WIFI
#if WITH_LP8
      SenseAirLP8::from,
#endif // #if WITH_LP8
#if WITH_GSS
      GSS::from,
#endif // #if WITH_GSS
#if WITH_CUP_ANEMOMETER
      CupAnemometer::from,
#endif // #if WITH_CUP_ANEMOMETER
#if WITH_EMISSION_CALCULATOR
      EmissionCalculator::from,
#endif // #if WITH_EMISSION_CALCULATOR
#if WITH_RELAY
      Relay::from,
#endif // #if WITH_RELAY
      Components::from, GenericComponent::from
  };
  return builders;
}

#endif // #ifdef ARDUINOJSON_VERSION

} // namespace Hardware
