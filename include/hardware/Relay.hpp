#pragma once

#ifndef WITH_RELAY
//! Whether to enable support for relay boards
#define WITH_RELAY false
#endif // #ifndef WITH_RELAY

#if WITH_RELAY

#include "Component.hpp"

#include <Arduino.h>

namespace Hardware {

class Relay : public Component
{
  HARDWARE_COMPONENT(Relay)
  using Component::Component;
  Relay(const size_t nPins,
        const bool triggerLow = true,
        const bool triggerIsOn = true,
        const bool triggerInitOn = true,
        Component* parent = nullptr)
    : Component::Component(parent, {})
    , pins(nPins)
    , triggerLow(triggerLow)
    , triggerIsOn(triggerIsOn)
    , triggerInitOn(triggerInitOn)
  {}
  Relay(std::initializer_list<int> pins,
        bool triggerLow = true,
        Component* parent = nullptr)
    : Component::Component(parent, {})
    , pins(pins)
    , triggerLow(triggerLow)
  {}

public:
  virtual char* toString(char* buf, const size_t bufSize)
  {
    snprintf_P(buf,
               bufSize,
               PSTR("[%s] relay board with %d ports (%s)"),
               this->name.c_str(),
               this->pins.size(),
               this->error.c_str());
    return buf;
  }

  virtual void measure(Hardware::Component::Measurement& measurement) override
  {}
  virtual void measure(
    std::initializer_list<const char*> quantities = {}) override
  {
    Hardware::Component::measure(quantities);
  }

  // state = true -> on   state = false -> off
  bool pinState(const bool switchOn)
  {
    return switchOn == (this->triggerLow != this->triggerIsOn);
  }

  void initPins()
  {
    for (const int pin : this->pins) {
      const auto state = this->pinState(this->triggerInitOn);
      LOG(Logging::Hardware,
          Info,
          cat(this->name.c_str(),
              ":",
              " ",
              F("Setting pin "),
              pin,
              F(" as "),
              state ? F("HIGH") : F("LOW"),
              " ",
              F("OUTPUT")));
      pinMode(pin, OUTPUT);
      digitalWrite(pin, state ? HIGH : LOW);
    }
  }

#ifdef ARDUINOJSON_VERSION
  static Component* from(JsonObjectConst json)
  {
    LOG(
      Logging::Hardware, Debug + 10, cat(__PRETTY_FUNCTION__, "(", json, ")"));
    JsonVariantConst type = json["type"];
    if (type != "relay")
      return nullptr;
    auto ptr = Component::allocate<Relay>();
    if (not ptr)
      return nullptr;
    const auto pins = json["pins"];
    auto& relay = *new (ptr) Relay(pins.is<JsonArray>() ? pins.size() : 0,
                                   json["trigger"]["onLow"] | true,
                                   json["trigger"]["isOn"] | true,
                                   json["trigger"]["initOn"] | true);
    size_t i = 0;
    for (const int pin : pins.as<JsonArray>())
      relay.pins[i++] = pin;
    relay.initPins();
    return (&relay)->as<Component*>();
  }
#endif // #ifdef ARDUINOJSON_VERSION

  std::vector<int> pins;
  bool triggerLow;
  bool triggerIsOn;
  bool triggerInitOn;

private:
};

} // namespace Hardware

#endif // #if WITH_RELAY
