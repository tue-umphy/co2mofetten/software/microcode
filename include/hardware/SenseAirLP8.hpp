#pragma once

#ifndef WITH_LP8
//! Whether to enable support for the SenseAir LP8 CO2 sensor
#define WITH_LP8 false
#endif // #ifndef WITH_LP8

#if WITH_LP8

#include "Component.hpp"

#include <Arduino.h>
#include <LP8.h>

namespace Hardware {

class SenseAirLP8 : public Component
{
  HARDWARE_COMPONENT(SenseAirLP8)
  using Component::Component;
  SenseAirLP8(HardwareSerial& port,
              unsigned int vbbPin,
              Component* parent = nullptr)
    : Component::Component(
        parent,
        { new Hardware::Component::Measurement(*this, "temperature", "*C"),
          new Hardware::Component::Measurement(*this, "co2-raw", "ppm"),
          new Hardware::Component::Measurement(*this, "co2-corr", "ppm"),
          new Hardware::Component::Measurement(*this, "co2-filt", "ppm"),
          new Hardware::Component::Measurement(*this,
                                               "co2-filt-corr",
                                               "ppm") })
    , sensor(&port, vbbPin)
    , _pressureCallback(nullptr)
  {}

public:
  LP8<HardwareSerial> sensor;

  typedef float (*pressureCallback_t)(void);

  void setPressureCallback(pressureCallback_t callback)
  {
    this->_pressureCallback = callback;
  }

  virtual char* toString(char* buf, const size_t bufSize)
  {
    snprintf_P(buf,
               bufSize,
               PSTR("[%s] SenseAir LP8 CO2 sensor (%s)"),
               this->name.c_str(),
               this->error.c_str());
    return buf;
  }

  virtual bool calibrateZero(const bool filtered = false,
                             const bool reset = true)
  {
    bool success = this->sensor.calibrate(
      filtered
        ? (reset ? LP8_MODBUS_CONTROL_BYTE::ZERO_CALIB_NOISE_FILTERED_RESET
                 : LP8_MODBUS_CONTROL_BYTE::ZERO_CALIB_NOISE_FILTERED)
      : reset ? LP8_MODBUS_CONTROL_BYTE::ZERO_CALIB_NOISE_FILTERED_RESET
              : LP8_MODBUS_CONTROL_BYTE::ZERO_CALIB_NOISE_FILTERED);
    if (success)
      this->error = "ok";
    else
      this->error = "error"; // TODO: more elaborate error
    return success;
  }

  virtual bool calibrateBackground(const bool filtered = false,
                                   const bool reset = true)
  {
    bool success = this->sensor.calibrate(
      filtered
        ? (reset ? LP8_MODBUS_CONTROL_BYTE::BACK_CALIB_NOISE_FILTERED_RESET
                 : LP8_MODBUS_CONTROL_BYTE::BACK_CALIB_NOISE_FILTERED)
      : reset ? LP8_MODBUS_CONTROL_BYTE::BACK_CALIB_NOISE_FILTERED_RESET
              : LP8_MODBUS_CONTROL_BYTE::BACK_CALIB_NOISE_FILTERED);
    if (success)
      this->error = "ok";
    else
      this->error = "error"; // TODO: more elaborate error
    return success;
  }

  virtual void measure(Hardware::Component::Measurement& measurement) override
  {
    if (this->error)
      this->sensor.begin();
    if (this->error or this->sensor.ready_for_measurement()) {
      const float pressure =
        this->_pressureCallback ? this->_pressureCallback() : FNAN;
      if (Utils::pressurehPaOkay(pressure)
            ? this->sensor.measure(pressure * 10)
            : this->sensor.measure())
        this->error = "ok";
      else
        this->error = "error"; // TODO: more elaborate error
    }
    if (measurement == "co2-raw") {
      measurement = this->sensor.co2_raw_ppm;
      return;
    }
    if (measurement == "co2-corr") {
      measurement = this->sensor.co2_pres_ppm;
      return;
    }
    if (measurement == "co2-filt") {
      measurement = this->sensor.co2_filt_ppm;
      return;
    }
    if (measurement == "co2-filt-corr") {
      measurement = this->sensor.co2_filt_pres_ppm;
      return;
    }
    if (measurement == "temperature") {
      measurement = this->sensor.space_temp_celsius;
      return;
    }
    LOG(Logging::Hardware,
        Warning,
        cat(F("unhandled measurement "), measurement.quantity));
  }
  virtual void measure(
    std::initializer_list<const char*> quantities = {}) override
  {
    Hardware::Component::measure(quantities);
  }

#ifdef ARDUINOJSON_VERSION
  static Component* from(JsonObjectConst json)
  {
    LOG(
      Logging::Hardware, Debug + 10, cat(__PRETTY_FUNCTION__, "(", json, ")"));
    JsonVariantConst type = json["type"];
    if (type != "lp8")
      return nullptr;
    auto ptr = Component::allocate<SenseAirLP8>();
    if (not ptr)
      return nullptr;
    HardwareSerial* serialPort = nullptr;
    JsonVariantConst port = json["port"];
#if ARDUINO_AVR_MEGA2560
    switch (port.as<unsigned int>()) {
      case 1:
        serialPort = &Serial1;
        break;
      case 2:
        serialPort = &Serial2;
        break;
      case 3:
        serialPort = &Serial3;
        break;
    };
#endif // #if ARDUINO_AVR_MEGA2560
    if (not serialPort) {
      LOG(Logging::Hardware,
          Warning,
          cat(F("Invalid serial port '"), port, '\''));
      return nullptr;
    }
    unsigned int vbbPin = json["pin"];
    if (not vbbPin) {
      LOG(Logging::Hardware,
          Warning,
          cat(F("Invalid enabling pin: "), json["pin"]));
      return nullptr;
    }
    auto& lp8 = *new (ptr) SenseAirLP8(*serialPort, vbbPin);
    return (&lp8)->as<Component*>();
  }
#endif // #ifdef ARDUINOJSON_VERSION

private:
  pressureCallback_t _pressureCallback;
};

} // namespace Hardware

#endif // #if WITH_LP8
