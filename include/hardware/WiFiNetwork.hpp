#pragma once

#if !defined(WITH_NTP) || DOXYGEN_RUN
/**
 * @ingroup WiFiFlags
 * @brief Whether to enable NTP time querying
 */
#define WITH_NTP true
#endif // #ifndef WITH_NTP

#if ESP8266
#if WITH_WIFI

#include "../features/WiFi.hpp"
#include "Component.hpp"

#if WITH_NTP
#include <NTPClient.h>
#include <WiFiUdp.h>
#endif // #if WITH_NTP

namespace Hardware {

class WiFiNetwork : public Component
{
  HARDWARE_COMPONENT(WiFiNetwork)

  WiFiNetwork(Component* parent = nullptr)
    : Component::Component(parent, {
#if WITH_NTP
      new Hardware::Component::Measurement(*this, "time", "unix"),
#endif // #if WITH_NTP
        new Hardware::Component::Measurement(*this, "rssi", "dbi")
    })
  {}

public:
  virtual char* toString(char* buf, const size_t bufSize)
  {
    snprintf_P(buf,
               bufSize,
               PSTR("[%s] WiFiNetwork (%s)"),
               this->name.c_str(),
               this->error.c_str());
    return buf;
  }

  virtual void measure(Hardware::Component::Measurement& measurement) override
  {
    if (measurement == "rssi") {
      measurement = WiFi.RSSI();
      return;
    }
#if WITH_NTP
    if (measurement == "time") {
      WiFiUDP ntpUDP;
      const auto gatewayIP = WiFi.gatewayIP();
      NTPClient timeClient(ntpUDP, gatewayIP);
      timeClient.begin();
      unsigned long unixTime = 0;
      const auto timeBefore = millis();
      size_t nFailures = 0;
      while (true) {
        if (millis() - timeBefore > 5000) {
          LOG(Network, Logging::Error, cat(F("Stop waiting for NTP seconds")));
          break;
        }
        if (nFailures > 2) {
          LOG(Network,
              Logging::Error,
              cat(F("Stop querying NTP server on router after "),
                  nFailures,
                  " failures"));
          break;
        }
        if (not timeClient.forceUpdate()) {
          nFailures++;
          LOG(Network,
              Logging::Error,
              cat(F("Couldn't query NTP server on router (failures: "),
                  nFailures,
                  ")"));
          continue;
        }
        const auto tmp = timeClient.getEpochTime();
        if (not unixTime) {
          unixTime = tmp;
          break;
        }
      }
      if (unixTime) {
        if (this->error == "no-ntp")
          this->error = "ok";
        measurement = unixTime;
      } else
        this->error = "no-ntp";
      return;
    }
#endif // #if WITH_NTP
    LOG(Logging::Hardware,
        Warning,
        cat(F("unhandled measurement "), measurement.quantity));
  }
  virtual void measure(
    std::initializer_list<const char*> quantities = {}) override
  {
    if (WiFi.status() != WL_CONNECTED) {
      LOG(Network,
          Verbose,
          cat(F("WiFi not connected. "),
              "Skipping ",
              this->name.c_str(),
              "'s ",
              "measurements"));
      this->error = "disconnected";
      return;
    }
    if (this->error == "disconnected")
      this->error = "ok";
    Hardware::Component::measure(quantities);
  }

#ifdef ARDUINOJSON_VERSION
  static Component* from(JsonObjectConst json)
  {
    LOG(
      Logging::Hardware, Debug + 10, cat(__PRETTY_FUNCTION__, "(", json, ")"));
    JsonVariantConst type = json["type"];
    if (type != "wifi")
      return nullptr;
    auto ptr = Component::allocate<WiFiNetwork>();
    if (not ptr)
      return nullptr;
    auto& wifi = *new (ptr) WiFiNetwork;
    return (&wifi)->as<Component*>();
  }
#endif // #ifdef ARDUINOJSON_VERSION
};

} // namespace Hardware

#endif // #if WITH_WIFI
#endif // #if ESP8266
