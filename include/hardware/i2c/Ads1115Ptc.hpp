#pragma once

#if !defined(WITH_ADS1115PTC) || DOXYGEN_RUN
/**
  @ingroup Ads1115PtcFlags
  @brief Whether to enable support for a thermistor connected to an ADS1115
  @details The thermistor should be connected in series with a resistor of know
    resistance. The two resistors together connect Vcc and GND.
    The ADS1115 has to be connected as follows:

    - A0 --> Vcc
    - A1 --> GND
    - A2 --> between thermistor and fixed resistor
    - A3 --> GND

    This is the case for this board:
    https://gitlab.com/tue-umphy/co2mofetten/hardware/ads1x15-thermistor-board
*/
#define WITH_ADS1115PTC false
#endif // #ifndef WITH_ADS1115PTC

#if WITH_ADS1115PTC

#include "../../utils/Polynomial.hpp"
#include "Component.hpp"

#include <Adafruit_ADS1015.h>

#include <algorithm>

namespace Hardware {
namespace I2C {

class Ads1115Ptc : public I2C::Component
{
  HARDWARE_COMPONENT(I2C::Ads1115Ptc)
public:
  Ads1115Ptc(const uint8_t address = 0x48,
             const size_t nPolyCoefs = 2,
             const double fixedResistanceOhm = 1.0,
             const size_t nResistanceValues = 5,
             TwoWire* i2c = &Wire,
             Component* parent = nullptr)
    : I2C::Component::Component(
        i2c,
        parent,
        { new Hardware::Component::Measurement(*this, "resistance", "Ohm"),
          new Hardware::Component::Measurement(*this, "temperature", "*C") })
    , _address(address)
    , _fixedResistanceOhm(fixedResistanceOhm)
    , _adc()
    , _resistanceValues(nResistanceValues)
    , calibration(nPolyCoefs)
  {}

  void setAddress(const uint8_t address) { this->_address = address; };
  uint8_t getAddress(void) { return this->_address; };

  virtual char* toString(char* buf, const size_t bufSize) override
  {
    snprintf_P(buf,
               bufSize,
               PSTR("[%s] ADS1115 ADC with PTC thermistor @%#X (%s)"),
               this->name.c_str(),
               this->getAddress(),
               this->error.c_str());
    return buf;
  }

  /**
   * @brief   Checking if the device answers on I2C
   */
  bool isAvailable()
  {
    this->_port->beginTransmission(this->getAddress());
    return this->_port->endTransmission() == 0;
  }

  double measureResistance()
  {
    this->_adc.begin();
    for (auto& value : this->_resistanceValues) {
      this->_adc.setGain(GAIN_ONE);
      const auto Vcc = this->_adc.readADC_Differential_0_1() * 0.000125;
      LOG(Sensor,
          Debug + 1,
          cat(this->name.c_str(), ": Vcc = ", Vcc * 1e3, " mV"));
      this->_adc.setGain(GAIN_SIXTEEN);
      const auto Vout = this->_adc.readADC_Differential_2_3() * 7.8125e-6;
      LOG(Sensor,
          Debug + 1,
          cat(this->name.c_str(), ": Vout = ", Vout * 1e3, " mV"));
      const auto Rv = this->_fixedResistanceOhm * (Vcc / Vout - 1.0);
      LOG(Sensor, Debug + 1, cat(this->name.c_str(), ": R = ", Rv, " Ohm"));
      value = Rv;
    }
    std::sort(this->_resistanceValues.begin(), this->_resistanceValues.end());
    const auto Rmedian =
      this->_resistanceValues[this->_resistanceValues.size() / 2 + 1];
    LOG(Sensor,
        Verbose,
        cat(this->name.c_str(),
            ": R (median of ",
            this->_resistanceValues.size(),
            ") = ",
            Rmedian,
            " Ohm"));
    return Rmedian;
  }

  virtual void measure(Hardware::Component::Measurement& measurement) override
  {
    LOG(Logging::Hardware,
        Debug,
        cat(__PRETTY_FUNCTION__,
            " for ",
            this->name.c_str(),
            " and ",
            measurement.quantity));
    if (measurement == "resistance") {
      measurement = this->measureResistance();
      return;
    }
    if (measurement == "temperature") {
      const auto resistance = this->measurements["resistance"]->as<float>();
      measurement = this->calibration(resistance);
      return;
    }
    LOG(Logging::Hardware,
        Warning,
        cat("unhandled measurement ", measurement.quantity));
  }
  virtual void measure(
    std::initializer_list<const char*> quantities = {}) override
  {
    this->error = this->isAvailable() ? "ok" : "not-found";
    if (this->error)
      return;
    this->_adc.begin();
    Hardware::Component::measure(quantities);
  }

#ifdef ARDUINOJSON_VERSION
  static I2C::Component* from(JsonObjectConst json)
  {
    LOG(
      Logging::Hardware, Debug + 10, cat(__PRETTY_FUNCTION__, "(", json, ")"));
    if (json["type"] != "ads1115ptc")
      return nullptr;
    auto ptr = Component::allocate<Ads1115Ptc>();
    if (not ptr)
      return nullptr;
    auto calib = json["polynom"];
    auto& thermistor =
      *new (ptr) Ads1115Ptc(json["address"] | (uint8_t)0x48,
                            calib.is<JsonArray>() ? calib.size() : 2,
                            json["R"] | 1.0,
                            json["median"] | (size_t)5);
    thermistor.setInterval(json["interval"] | (unsigned long)0);
    if (calib.is<JsonArray>()) {
      size_t i = 0;
      for (const double coef : calib.as<JsonArray>())
        thermistor.calibration.coefficients[i++] = coef;
    } else {
      thermistor.calibration.coefficients[0] = 0;
      thermistor.calibration.coefficients[1] = 1;
    }
    return (&thermistor)->as<I2C::Component*>();
  }
#endif // #ifdef ARDUINOJSON_VERSION

private:
  uint8_t _address;
  double _fixedResistanceOhm;
  Adafruit_ADS1115 _adc;
  std::vector<double> _resistanceValues;
  Polynomial calibration;
};

} // namespace I2C
} // namespace Hardware

#endif // #if WITH_ADS1115PTC
