#pragma once

#if !defined(WITH_BME280) || DOXYGEN_RUN
/**
  @ingroup Bme280Flags
  @brief Whether to enable support for the BME280 environmental sensor
*/
#define WITH_BME280 true
#endif // #ifndef WITH_BME280

#if WITH_BME280

#include "Component.hpp"

#include <Adafruit_BME280.h>
#include <Adafruit_Sensor.h>

#include <algorithm>

namespace Hardware {
namespace I2C {

class Bme280 : public I2C::Component
{
  HARDWARE_COMPONENT(I2C::Bme280)
public:
  Bme280(const uint8_t address = 0x76,
         TwoWire* i2c = &Wire,
         Component* parent = nullptr)
    : I2C::Component::Component(
        i2c,
        parent,
        { new Hardware::Component::Measurement(*this, "humidity", "%"),
          new Hardware::Component::Measurement(*this, "temperature", "*C"),
          new Hardware::Component::Measurement(*this, "pressure", "hPa") })
    , _address(address)
    , _sensor()
  {}

  void setAddress(const uint8_t address) { this->_address = address; };
  uint8_t getAddress(void) { return this->_address; };

  virtual char* toString(char* buf, const size_t bufSize) override
  {
    snprintf_P(buf,
               bufSize,
               PSTR("[%s] Bosch BME280 sensor @%#X (%s)"),
               this->name.c_str(),
               this->getAddress(),
               this->error.c_str());
    return buf;
  }

  virtual void measure(Hardware::Component::Measurement& measurement) override
  {
    LOG(Logging::Hardware,
        Debug,
        cat(__PRETTY_FUNCTION__,
            " for ",
            this->name.c_str(),
            " and ",
            measurement.quantity));
    if (measurement == "humidity") {
      const auto humidity = this->_sensor.readHumidity();
      if (Utils::humidityPercentOkay(humidity)) {
        if (this->error == "bad-humidity" or
            this->error == "bad-temperature+humidity" or
            this->error == "bad-pressure+humidity" or
            this->error == "bad-values" or this->error == "not-found" or
            this->error == "?")
          this->error == "ok";
        measurement = humidity;
      } else {
        if (not this->error or this->error == "not-found" or
            this->error == "?")
          this->error = "bad-humidity";
        else if (this->error == "bad-pressure+temperature")
          this->error = "bad-values";
        else if (this->error == "bad-temperature")
          this->error = "bad-temperature+humidity";
        else if (this->error == "bad-pressure")
          this->error = "bad-pressure+humidity";
      }
      return;
    }
    if (measurement == "temperature") {
      const auto temperature = this->_sensor.readTemperature();
      if (Utils::temperatureCelsiusOkay(temperature)) {
        if (this->error == "bad-temperature" or
            this->error == "bad-temperature+humidity" or
            this->error == "bad-pressure+temperature" or
            this->error == "bad-values" or this->error == "not-found" or
            this->error == "?")
          this->error = "ok";
        measurement = temperature;
      } else {
        if (not this->error or this->error == "not-found" or
            this->error == "?")
          this->error = "bad-temperature";
        else if (this->error == "bad-pressure+humidity")
          this->error = "bad-values";
        else if (this->error == "bad-pressure")
          this->error = "bad-pressure+temperature";
        else if (this->error == "bad-humidity")
          this->error = "bad-temperature+humidity";
      }
      return;
    }
    if (measurement == "pressure") {
      const auto pressure = this->_sensor.readPressure() / 100;
      if (Utils::pressurehPaOkay(pressure)) {
        if (this->error == "bad-pressure" or
            this->error == "bad-pressure+humidity" or
            this->error == "bad-pressure+temperature" or
            this->error == "bad-values" or this->error == "not-found" or
            this->error == "?")
          this->error = "ok";
        measurement = pressure;
      } else {
        if (not this->error or this->error == "not-found" or
            this->error == "?")
          this->error = "bad-pressure";
        else if (this->error == "bad-temperature+humidity")
          this->error = "bad-values";
        else if (this->error == "bad-temperature")
          this->error = "bad-pressure+temperature";
        else if (this->error == "bad-humidity")
          this->error = "bad-pressure+humidity";
      }
      return;
    }
    LOG(Logging::Hardware,
        Warning,
        cat("unhandled measurement ", measurement.quantity));
  }
  virtual void measure(
    std::initializer_list<const char*> quantities = {}) override
  {
    if (this->error) {
      bool found = this->_sensor.begin(this->_address);
      if (not found) {
        this->error = "not-found";
        return;
      }
    }
    Hardware::Component::measure(quantities);
  }

#ifdef ARDUINOJSON_VERSION
  static I2C::Component* from(JsonObjectConst json)
  {
    LOG(
      Logging::Hardware, Debug + 10, cat(__PRETTY_FUNCTION__, "(", json, ")"));
    JsonVariantConst type = json["type"];
    if (type != "bme280")
      return nullptr;
    auto ptr = Component::allocate<Bme280>();
    if (not ptr)
      return nullptr;
    auto& bme = *new (ptr) Bme280;
    if (json["address"].is<uint8_t>())
      bme.setAddress(json["address"].as<uint8_t>());
    return (&bme)->as<I2C::Component*>();
  }
#endif // #ifdef ARDUINOJSON_VERSION

private:
  uint8_t _address;
  Adafruit_BME280 _sensor;
};

} // namespace I2C
} // namespace Hardware

#endif // #if WITH_BME280
