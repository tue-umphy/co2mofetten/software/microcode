#pragma once

#if !defined(WITH_CM1106) || DOXYGEN_RUN
/**
  @ingroup Cm1106Flags
  @brief Whether to enable support for the CM1106 CO2 sensor
*/
#define WITH_CM1106 true
#endif // #ifndef WITH_CM1106

#if WITH_CM1106

#include "Component.hpp"

#include <CM1106.h>

namespace Hardware {
namespace I2C {

class Cm1106 : public I2C::Component
{
  HARDWARE_COMPONENT(I2C::Cm1106)
public:
  Cm1106(TwoWire* i2c = &Wire, Component* parent = nullptr)
    : I2C::Component::Component(
        i2c,
        parent,
        { new Hardware::Component::Measurement(*this, "co2", "ppm") })
    , sensor()
    , _pressureCallback(nullptr)
  {}

  typedef float (*pressureCallback_t)(void);

  void setPressureCallback(pressureCallback_t callback)
  {
    this->_pressureCallback = callback;
  }

  virtual char* toString(char* buf, const size_t bufSize) override
  {
    snprintf_P(buf,
               bufSize,
               PSTR("[%s] Cubic CM1106 sensor (%s)"),
               this->name.c_str(),
               this->error.c_str());
    return buf;
  }

  virtual void measure(Hardware::Component::Measurement& measurement) override
  {
    LOG(Logging::Hardware,
        Debug + 10,
        cat(__PRETTY_FUNCTION__,
            " for ",
            this->name.c_str(),
            " and ",
            measurement.quantity));
    if (measurement == "co2") {
      CM1106_OP_STATUS status;
      uint16_t co2ppm = ~(uint16_t)0;
      this->sensor.getCO2(co2ppm, status);
      if (co2ppm != ~(uint16_t)0 and status == CM1106_OP_STATUS::NORMAL) {
        measurement = co2ppm;
        this->error = "ok";
      } else {
        switch (status) {
          case CM1106_OP_STATUS::PREHEATING:
            this->error = "preheating";
            break;
          case CM1106_OP_STATUS::NORMAL:
            this->error = "ok";
            break;
          case CM1106_OP_STATUS::TROUBLE:
            this->error = "trouble";
            break;
          case CM1106_OP_STATUS::OUT_OF_FS:
            this->error = "out-of-fs";
            break;
          case CM1106_OP_STATUS::NOT_CALIBRATED:
            this->error = "not-calibrated";
            break;
          default:
            this->error = "?";
            break;
        }
      }
      return;
    }
    LOG(Logging::Hardware,
        Warning,
        cat("unhandled measurement ", measurement.quantity));
  }
  virtual void measure(
    std::initializer_list<const char*> quantities = {}) override
  {
    if (this->error and this->error != "preheating") {
      bool found = this->sensor.begin();
      if (not found) {
        this->error = "not-found";
        return;
      }
    }
    Hardware::Component::measure(quantities);
  }

#ifdef ARDUINOJSON_VERSION
  static I2C::Component* from(JsonObjectConst json)
  {
    LOG(
      Logging::Hardware, Debug + 10, cat(__PRETTY_FUNCTION__, "(", json, ")"));
    JsonVariantConst type = json["type"];
    if (type != "cm1106")
      return nullptr;
    auto ptr = Component::allocate<Cm1106>();
    auto& cubic = *new (ptr) Cm1106;
    return (&cubic)->as<I2C::Component*>();
  }
#endif // #ifdef ARDUINOJSON_VERSION

  CM1106 sensor;

private:
  pressureCallback_t _pressureCallback;
};

} // namespace I2C
} // namespace Hardware

#endif // #if WITH_CM1106
