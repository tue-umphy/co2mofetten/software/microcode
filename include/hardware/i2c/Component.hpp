#pragma once

#include "../Component.hpp"

#include <Wire.h>

namespace Hardware {

namespace I2C {

class Component : public Hardware::Component
{
  HARDWARE_COMPONENT(I2C::Component)
public:
  Component(TwoWire* port = &Wire,
            Hardware::Component* parent = nullptr,
            std::initializer_list<Hardware::Component::Measurement*> m = {})
    : Hardware::Component::Component(parent, m)
    , _port(port)
  {
    // if no port was given but a parent, use the parent's port
    if (not port and parent) {
#if HARDWARE_COMPONENT_ENABLE_IS
      if (parent->is<I2C::Component>())
        this->setPort(parent->as<I2C::Component*>()->getPort());
#endif // #if HARDWARE_COMPONENT_ENABLE_IS
    }
  }
  Component(TwoWire& port, Hardware::Component& parent)
    : Component(&port, &parent)
  {}
  Component(Hardware::Component& parent)
    : Component(Wire, parent)
  {}
  Component(TwoWire& port)
    : Component(&port, nullptr)
  {}

  TwoWire* getPort() { return this->_port; }
  void setPort(TwoWire* port) { this->_port = port; }
  void setPort(TwoWire& port) { this->setPort(&port); }

  virtual void traverse(TraverseContext& context,
                        traverseCallback_t callback,
                        void* callbackExtra = nullptr)
  {
    if (context.abort) {
      LOG(Logging::Hardware,
          Debug,
          cat("Skipping ", __PRETTY_FUNCTION__, " for ", this->name.c_str()));
      return;
    }
    callback(*this, context, callbackExtra);
  }
  virtual void traverse(traverseCallback_t callback,
                        void* callbackExtra = nullptr) override
  {
    TraverseContext context;
    this->traverse(context, callback, callbackExtra);
  }

  virtual char* toString(char* buf, const size_t bufSize) = 0;

#ifdef ARDUINOJSON_VERSION
  typedef I2C::Component* (*builderCallback_t)(JsonObjectConst json);
  static std::vector<builderCallback_t>& getBuilders(); // implemented later

  /**
   * @brief Try all builders from getBuilders() to create an object of a given
   * type from a `JsonObject`
   * @returns nullptr on failure
   */
  static Component* from(JsonObjectConst json)
  {
    size_t i = 0;
    for (auto builder : I2C::Component::getBuilders()) {
      LOG(Logging::Hardware,
          Debug,
          cat("Trying ",
#if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
              classString,
              " ",
#endif // #if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
              "builder ",
              i));
      i++;
      Component* component = builder(json);
      if (component) {
        component->setCommonFrom(json);
        return component;
      }
    }
    LOG(Logging::Hardware,
        Logging::Error,
        cat("JSON ",
            json,
            F(" could't be converted to "),
#if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
            classString,
            " ",
#endif // #if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
            "subclass"););
    return nullptr;
  }
#endif // #ifdef ARDUINOJSON_VERSION

protected:
  TwoWire* _port;
};

} // namespace I2C

} // namespace Hardware
