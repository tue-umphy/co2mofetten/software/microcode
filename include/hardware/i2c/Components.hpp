#pragma once

#include "Component.hpp"

namespace Hardware {

namespace I2C {

class Components
  : public std::vector<Component*>
  , public Component
{
  HARDWARE_COMPONENT(I2C::Components)
public:
  Components(TwoWire* port = &Wire, Hardware::Component* parent = nullptr)
    : std::vector<Component*>::vector()
    , Component::Component(port, parent)
  {}
  Components(TwoWire& port, Hardware::Component* parent = nullptr)
    : Components(&port, parent)
  {}
  Components(TwoWire& port, Hardware::Component& parent)
    : Components(&port, &parent)
  {}
  Components(const size_t nComponents,
             TwoWire* port = &Wire,
             Hardware::Component* parent = nullptr)
    : std::vector<Component*>::vector(nComponents, nullptr)
    , Component::Component(port, parent)
  {}
  Components(const size_t nComponents, TwoWire& port)
    : Components(nComponents, &port, nullptr)
  {}
  Components(const size_t nComponents, Hardware::Component& parent)
    : Components(nComponents, &Wire, &parent)
  {}
  Components(std::initializer_list<Component*> components,
             TwoWire& port,
             Hardware::Component& parent)
    : std::vector<Component*>::vector{ components }
    , Component::Component(&port, &parent)
  {
    for (Component* component : *this) {
      if (not component)
        continue;
      component->setParent(this);
    }
  }

  virtual void traverse(TraverseContext& context,
                        traverseCallback_t callback,
                        void* callbackExtra = nullptr) override
  {
    if (context.abort) {
      LOG(Logging::Hardware,
          Debug + 10,
          cat("Skipping ", __PRETTY_FUNCTION__, " for ", this->name.c_str()));
      return;
    }
    callback(*this->as<Component*>(), context, callbackExtra);
    size_t n = 0;
    for (Component* nestedComponent : *this) {
      if (context.abort) {
        LOG(Logging::Hardware,
            Debug + 10,
            cat("Aborting ", __PRETTY_FUNCTION__));
        break;
      }
      n++;
      if (not nestedComponent)
        continue;
      const size_t levelBefore = context.level;
      Component::TraverseContext nestedContext = context; // copy context
      nestedContext.level++;
      nestedComponent->traverse(nestedContext, callback, callbackExtra);
      context = nestedContext; // apply
      context.level = levelBefore;
    }
  }
  virtual void traverse(traverseCallback_t callback,
                        void* callbackExtra = nullptr) override
  {
    TraverseContext context;
    this->traverse(context, callback, callbackExtra);
  }

  virtual char* toString(char* buf, const size_t bufSize) override
  {
    snprintf_P(buf,
               bufSize,
               PSTR("[%s] Collection of max. %u I2C components"),
               this->name.c_str(),
               this->size());
    return buf;
  }

#ifdef ARDUINOJSON_VERSION
  static Component* from(JsonObjectConst json)
  {
    LOG(
      Logging::Hardware, Debug + 10, cat(__PRETTY_FUNCTION__, "(", json, ")"));
    if (json.isNull())
      return nullptr;
    for (JsonPairConst pair : json) {
      if (not pair.value().is<JsonObject>())
        return nullptr;
    }
    auto ptr = Component::allocate<I2C::Components>();
    if (not ptr)
      return nullptr;
    I2C::Components& components = *new (ptr) I2C::Components(json.size());
    size_t iComponent = 0;
    for (JsonPairConst pair : json) {
      JsonObjectConst value = pair.value().as<JsonObject>();
      I2C::Component* component = I2C::Component::from(value);
      if (component) {
        component->name = pair.key().c_str();
        component->setParent(components);
      } else {
        LOG(Logging::Hardware,
            Debug + 10,
            cat(__PRETTY_FUNCTION__,
                ": JSON ",
                value,
                F(" could't be converted")));
      }
      components[iComponent++] = component;
    }
    return (&components)->as<I2C::Component*>();
  }
#endif // #ifdef ARDUINOJSON_VERSION
};

} // namespace I2C

} // namespace Hardware
