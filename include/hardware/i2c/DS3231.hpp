#pragma once

#if !defined(WITH_DS3231) || DOXYGEN_RUN
/**
  @ingroup HardwareFlags
  @brief Whether to enable support for the DS3231 real-time clock
*/
#define WITH_DS3231 true
#endif // #ifndef WITH_DS3231

#if WITH_DS3231

#include "Component.hpp"

#include <RTClib.h>

#include <algorithm>

namespace Hardware {
namespace I2C {

class DS3231 : public I2C::Component
{
  HARDWARE_COMPONENT(I2C::DS3231)
public:
  DS3231(TwoWire* i2c = &Wire, Component* parent = nullptr)
    : I2C::Component::Component(
        i2c,
        parent,
        { new Hardware::Component::Measurement(*this, "time", "unix"),
          new Hardware::Component::Measurement(*this, "temperature", "*C") })
    , device()
  {}

  virtual char* toString(char* buf, const size_t bufSize) override
  {
    snprintf_P(buf,
               bufSize,
               PSTR("[%s] DS3231 RTC (%s)"),
               this->name.c_str(),
               this->error.c_str());
    return buf;
  }

  virtual void measure(Hardware::Component::Measurement& measurement) override
  {
    // As the DS3231 I2C interface only allows querying the unix timestamp in
    // seconds without millisecond precision, we at least want to have that
    // information as precise as possible, so we wait for a second leap
    if (measurement == "time") {
      auto previousUnixtime = this->device.now().unixtime();
      auto currentUnixtime = previousUnixtime;
      const auto timeBefore = millis();
      while (true) {
        if ((millis() - timeBefore) > 2000U) {
          LOG(Logging::Hardware,
              Logging::Error,
              cat(measurement.component().name.c_str(),
                  F(" didn't return any second jump")));
          this->error = "no-second-jump";
          return;
          break;
        }
        if ((currentUnixtime = this->device.now().unixtime()) !=
            previousUnixtime) {
          measurement = currentUnixtime;
          break;
        }
      }
      if (this->error != "bad-temperature")
        this->error = "ok";
      return;
    }
    if (measurement == "temperature") {
      const auto temperature = this->device.getTemperature();
      if (Utils::temperatureCelsiusOkay(temperature)) {
        measurement = temperature;
      } else {
        this->error = "bad-temperature";
      }
      return;
    }
    LOG(Logging::Hardware,
        Warning,
        cat("unhandled ", "measurement ", measurement.quantity));
  }
  virtual void measure(
    std::initializer_list<const char*> quantities = {}) override
  {
    if (this->device.isRunning() && !this->device.lostPower())
      this->error = "ok";
    else if (this->device.isRunning() && this->device.lostPower())
      this->error = "lost-power";
    else if (!this->device.isRunning())
      this->error = "not-found";
    else
      this->error = "error";
    if (this->error)
      return;
    Hardware::Component::measure(quantities);
  }

  bool adjust(const unsigned long unixTime)
  {
    if (not this->device.isRunning()) {
      LOG(Logging::Hardware,
          Logging::Error,
          cat(this->name.c_str(),
              F(" is not running"),
              F(", can't adjust time")));
      this->error = "error";
      return false;
    }
    // unfortunately is void, so no status check possible...
    this->device.adjust(DateTime(unixTime));
    if (not this->device.isRunning()) {
      LOG(Logging::Hardware,
          Logging::Error,
          cat(this->name.c_str(),
              F(" is not running"),
              F(" after setting time?")));
      this->error = "error";
      return false;
    }
    LOG(Logging::Hardware,
        Info,
        cat("Adjusted ", this->name.c_str(), " to unix time ", unixTime));
    this->error = "ok";
    return true;
  }

#ifdef ARDUINOJSON_VERSION
  static I2C::Component* from(JsonObjectConst json)
  {
    LOG(
      Logging::Hardware, Debug + 10, cat(__PRETTY_FUNCTION__, "(", json, ")"));
    JsonVariantConst type = json["type"];
    if (type != "ds3231")
      return nullptr;
    auto ptr = Component::allocate<DS3231>();
    if (not ptr)
      return nullptr;
    auto& rtc = *new (ptr) DS3231;
    return (&rtc)->as<I2C::Component*>();
  }
#endif // #ifdef ARDUINOJSON_VERSION

  RTC_DS3231 device;
};

} // namespace I2C
} // namespace Hardware

#endif // #if WITH_DS3231
