#pragma once

#include "Ads1115Ptc.hpp"
#include "Bme280.hpp"
#include "Cm1106.hpp"
#include "Component.hpp"
#include "Components.hpp"
#include "DS3231.hpp"
#include "GenericComponent.hpp"
#include "Scd30.hpp"
#include "Sht31.hpp"
#include "ShtC3.hpp"
#include "Stc31.hpp"
#include "TCA9548A.hpp"
#include "Theben.hpp"

namespace Hardware {

namespace I2C {

std::vector<I2C::Component::builderCallback_t>&
I2C::Component::getBuilders()
{
  static std::vector<I2C::Component::builderCallback_t> builders
  {
    I2C::Components::from,
#if WITH_I2C_MULTIPLEXER
      I2C::TCA9548A::from,
#endif // #if WITH_I2C_MULTIPLEXER
#if WITH_ADS1115PTC
      I2C::Ads1115Ptc::from,
#endif // #if WITH_ADS1115PTC
#if WITH_BME280
      I2C::Bme280::from,
#endif // #if WITH_BME280
#if WITH_SCD30
      I2C::Scd30::from,
#endif // #if WITH_SCD30
#if WITH_SHT31
      I2C::Sht31::from,
#endif // #if WITH_SHT31
#if WITH_STC31
      I2C::Stc31::from,
#endif // #if WITH_STC31
#if WITH_SHTC3
      I2C::ShtC3::from,
#endif // #if WITH_SHTC3
#if WITH_DS3231
      I2C::DS3231::from,
#endif // #if WITH_DS3231
#if WITH_THEBEN
      I2C::Theben::from,
#endif // #if WITH_THEBEN
#if WITH_CM1106
      I2C::Cm1106::from,
#endif // #if WITH_CM1106
      I2C::GenericComponent::from
  };
  return builders;
}

} // namespace I2C

} // namespace Hardware
