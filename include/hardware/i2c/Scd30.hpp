#pragma once

#if !defined(WITH_SCD30) || DOXYGEN_RUN
/**
  @ingroup Scd30Flags
  @brief Whether to enable support for the SCD30 CO2 sensor
*/
#define WITH_SCD30 true
#endif // #ifndef WITH_SCD30

#if WITH_SCD30

#include "Component.hpp"

#include <SparkFun_SCD30_Arduino_Library.h>

namespace Hardware {
namespace I2C {

class Scd30 : public I2C::Component
{
  HARDWARE_COMPONENT(I2C::Scd30)
public:
  Scd30(TwoWire* i2c = &Wire, Component* parent = nullptr)
    : I2C::Component::Component(
        i2c,
        parent,
        { new Hardware::Component::Measurement(*this, "humidity", "%"),
          new Hardware::Component::Measurement(*this, "temperature", "*C"),
          new Hardware::Component::Measurement(*this, "co2", "ppm") })
    , sensor()
    , _pressureCallback(nullptr)
  {}

  typedef float (*pressureCallback_t)(void);

  void setPressureCallback(pressureCallback_t callback)
  {
    this->_pressureCallback = callback;
  }

  virtual char* toString(char* buf, const size_t bufSize) override
  {
    snprintf_P(buf,
               bufSize,
               PSTR("[%s] Sensiron SCD30 sensor (%s)"),
               this->name.c_str(),
               this->error.c_str());
    return buf;
  }

  virtual void measure(Hardware::Component::Measurement& measurement) override
  {
    LOG(Logging::Hardware,
        Debug + 10,
        cat(__PRETTY_FUNCTION__,
            " for ",
            this->name.c_str(),
            " and ",
            measurement.quantity));
    if (measurement == "humidity") {
      const auto humidity = this->sensor.getHumidity();
      if (Utils::humidityPercentOkay(humidity)) {
        if (this->error == "bad-humidity" or
            this->error == "bad-temperature+humidity" or
            this->error == "bad-co2+humidity" or this->error == "bad-values" or
            this->error == "not-found" or this->error == "?")
          this->error == "ok";
        measurement = humidity;
      } else {
        if (not this->error or this->error == "not-found" or
            this->error == "?")
          this->error = "bad-humidity";
        else if (this->error == "bad-co2+temperature")
          this->error = "bad-values";
        else if (this->error == "bad-temperature")
          this->error = "bad-temperature+humidity";
        else if (this->error == "bad-co2")
          this->error = "bad-co2+humidity";
      }
      return;
    }
    if (measurement == "temperature") {
      const auto temperature = this->sensor.getTemperature();
      if (Utils::temperatureCelsiusOkay(temperature)) {
        if (this->error == "bad-temperature" or
            this->error == "bad-temperature+humidity" or
            this->error == "bad-co2+temperature" or
            this->error == "bad-values" or this->error == "not-found" or
            this->error == "?")
          this->error = "ok";
        measurement = temperature;
      } else {
        if (not this->error or this->error == "not-found" or
            this->error == "?")
          this->error = "bad-temperature";
        else if (this->error == "bad-co2+humidity")
          this->error = "bad-values";
        else if (this->error == "bad-co2")
          this->error = "bad-co2+temperature";
        else if (this->error == "bad-humidity")
          this->error = "bad-temperature+humidity";
      }
      return;
    }
    if (measurement == "co2") {
      if (this->_pressureCallback) {
        const auto pressure = this->_pressureCallback();
        if (Utils::pressurehPaOkay(pressure)) {
          LOG(Logging::Hardware,
              Verbose,
              cat("Setting pressure for ",
#if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
                  this->getClassString(),
                  " ",
#endif // #if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
                  this->name.c_str(),
                  " to ",
                  pressure,
                  " hPa"));
          this->sensor.setAmbientPressure(pressure);
        }
      }
      const auto co2 = this->sensor.getCO2();
      if (Utils::co2ppmOkay(co2)) {
        if (this->error == "bad-co2" or this->error == "bad-co2+humidity" or
            this->error == "bad-co2+temperature" or
            this->error == "bad-values" or this->error == "not-found" or
            this->error == "?")
          this->error = "ok";
        measurement = co2;
      } else {
        if (not this->error or this->error == "not-found" or
            this->error == "?")
          this->error = "bad-co2";
        else if (this->error == "bad-temperature+humidity")
          this->error = "bad-values";
        else if (this->error == "bad-temperature")
          this->error = "bad-co2+temperature";
        else if (this->error == "bad-humidity")
          this->error = "bad-co2+humidity";
      }
      return;
    }
    LOG(Logging::Hardware,
        Warning,
        cat("unhandled measurement ", measurement.quantity));
  }
  virtual void measure(
    std::initializer_list<const char*> quantities = {}) override
  {
    if (this->error) {
      bool found = this->sensor.begin();
      if (not found) {
        this->error = "not-found";
        return;
      }
    }
    Hardware::Component::measure(quantities);
  }

#ifdef ARDUINOJSON_VERSION
  static I2C::Component* from(JsonObjectConst json)
  {
    LOG(
      Logging::Hardware, Debug + 10, cat(__PRETTY_FUNCTION__, "(", json, ")"));
    JsonVariantConst type = json["type"];
    if (type != "scd30")
      return nullptr;
    auto ptr = Component::allocate<Scd30>();
    auto& scd = *new (ptr) Scd30;
    return (&scd)->as<I2C::Component*>();
  }
#endif // #ifdef ARDUINOJSON_VERSION

  SCD30 sensor;

private:
  pressureCallback_t _pressureCallback;
};

} // namespace I2C
} // namespace Hardware

#endif // #if WITH_scd30
