#pragma once

#if !defined(WITH_SHT31) || DOXYGEN_RUN
/**
  @ingroup Sht31Flags
  @brief Whether to enable support for the SHT31 temperature and humidity
*/
#define WITH_SHT31 true
#endif // #ifndef WITH_SHT31

#if WITH_SHT31

#include "Component.hpp"

#include <Adafruit_SHT31.h>
#include <Adafruit_Sensor.h>

#include <algorithm>

namespace Hardware {
namespace I2C {

class Sht31 : public I2C::Component
{
  HARDWARE_COMPONENT(I2C::Sht31)
public:
  Sht31(const uint8_t address = 0x44,
        TwoWire* i2c = &Wire,
        Component* parent = nullptr)
    : I2C::Component::Component(
        i2c,
        parent,
        {
          new Hardware::Component::Measurement(*this, "humidity", "%"),
          new Hardware::Component::Measurement(*this, "temperature", "*C"),
        })
    , _address(address)
    , _sensor()
  {}

  void setAddress(const uint8_t address) { this->_address = address; };
  uint8_t getAddress(void) { return this->_address; };

  virtual char* toString(char* buf, const size_t bufSize) override
  {
    snprintf_P(buf,
               bufSize,
               PSTR("[%s] Sensirion SHT31 sensor @%#X (%s)"),
               this->name.c_str(),
               this->getAddress(),
               this->error.c_str());
    return buf;
  }

  virtual void measure(Hardware::Component::Measurement& measurement) override
  {
    LOG(Logging::Hardware,
        Debug,
        cat(__PRETTY_FUNCTION__,
            " for ",
            this->name.c_str(),
            " and ",
            measurement.quantity));
    if (measurement == "humidity") {
      const auto humidity = this->_sensor.readHumidity();
      if (not Utils::humidityPercentOkay(humidity)) {
        this->error =
          this->error == "bad-temperature" ? "bad-values" : "bad-humidity";
        return;
      }
      measurement = humidity;
      if (this->error != "bad-temperature")
        this->error = "ok";
      return;
    }
    if (measurement == "temperature") {
      const auto temperature = this->_sensor.readTemperature();
      if (not Utils::temperatureCelsiusOkay(temperature)) {
        this->error =
          this->error == "bad-humidity" ? "bad-values" : "bad-temperature";
        return;
      }
      measurement = temperature;
      if (this->error != "bad-humidity")
        this->error = "ok";
      return;
    }
    LOG(Logging::Hardware,
        Warning,
        cat(this->name.c_str(),
            ": ",
            "unhandled measurement ",
            measurement.quantity));
  }

  virtual void measure(
    std::initializer_list<const char*> quantities = {}) override
  {
    if (this->error) {
      bool found = this->_sensor.begin(this->_address);
      if (not found) {
        this->error = "not-found";
        return;
      }
    }
    Hardware::Component::measure(quantities);
  }

#ifdef ARDUINOJSON_VERSION
  static I2C::Component* from(JsonObjectConst json)
  {
    LOG(
      Logging::Hardware, Debug + 10, cat(__PRETTY_FUNCTION__, "(", json, ")"));
    JsonVariantConst type = json["type"];
    if (type != "sht31")
      return nullptr;
    auto ptr = Component::allocate<Sht31>();
    if (not ptr)
      return nullptr;
    auto& sht31 = *new (ptr) Sht31;
    if (json["address"].is<uint8_t>())
      sht31.setAddress(json["address"].as<uint8_t>());
    return (&sht31)->as<I2C::Component*>();
  }
#endif // #ifdef ARDUINOJSON_VERSION

private:
  uint8_t _address;
  Adafruit_SHT31 _sensor;
};

} // namespace I2C
} // namespace Hardware

#endif // #if WITH_SHT31
