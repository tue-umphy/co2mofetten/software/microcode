#pragma once

#if !defined(WITH_SHTC3) || DOXYGEN_RUN
/**
  @ingroup ShtC3Flags
  @brief Whether to enable support for the SHTC3 temperature and humidity
*/
#define WITH_SHTC3 true
#endif // #ifndef WITH_SHTC3

#if WITH_SHTC3

#include "Component.hpp"

#include <Adafruit_SHTC3.h>
#include <Adafruit_Sensor.h>

#include <algorithm>

namespace Hardware {
namespace I2C {

class ShtC3 : public I2C::Component
{
  HARDWARE_COMPONENT(I2C::ShtC3)
public:
  ShtC3(TwoWire* i2c = &Wire, Component* parent = nullptr)
    : I2C::Component::Component(
        i2c,
        parent,
        {
          new Hardware::Component::Measurement(*this, "humidity", "%"),
          new Hardware::Component::Measurement(*this, "temperature", "*C"),
        })
    , _sensor()
  {}

  virtual char* toString(char* buf, const size_t bufSize) override
  {
    snprintf_P(buf,
               bufSize,
               PSTR("[%s] Sensirion SHTC3 sensor (%s)"),
               this->name.c_str(),
               this->error.c_str());
    return buf;
  }

  virtual void measure(Hardware::Component::Measurement& measurement) override
  {
    LOG(Logging::Hardware,
        Debug,
        cat(__PRETTY_FUNCTION__,
            " for ",
            this->name.c_str(),
            " and ",
            measurement.quantity));
    if (measurement == "humidity") {
      sensors_event_t hum, temp;
      this->_sensor.getEvent(&hum, &temp);
      const auto humidity = hum.relative_humidity;
      if (not Utils::humidityPercentOkay(humidity)) {
        this->error =
          this->error == "bad-temperature" ? "bad-values" : "bad-humidity";
        return;
      }
      measurement = humidity;
      if (this->error != "bad-temperature")
        this->error = "ok";
      return;
    }
    if (measurement == "temperature") {
      sensors_event_t humidity, temp;
      this->_sensor.getEvent(&humidity, &temp);
      const auto temperature = temp.temperature;
      if (not Utils::temperatureCelsiusOkay(temperature)) {
        this->error =
          this->error == "bad-humidity" ? "bad-values" : "bad-temperature";
        return;
      }
      measurement = temperature;
      if (this->error != "bad-humidity")
        this->error = "ok";
      return;
    }
    LOG(Logging::Hardware,
        Warning,
        cat(this->name.c_str(),
            ": ",
            "unhandled measurement ",
            measurement.quantity));
  }

  virtual void measure(
    std::initializer_list<const char*> quantities = {}) override
  {
    if (this->error) {
      bool found = this->_sensor.begin();
      if (not found) {
        this->error = "not-found";
        return;
      }
    }
    Hardware::Component::measure(quantities);
  }

#ifdef ARDUINOJSON_VERSION
  static I2C::Component* from(JsonObjectConst json)
  {
    LOG(
      Logging::Hardware, Debug + 10, cat(__PRETTY_FUNCTION__, "(", json, ")"));
    JsonVariantConst type = json["type"];
    if (type != "shtc3")
      return nullptr;
    auto ptr = Component::allocate<ShtC3>();
    if (not ptr)
      return nullptr;
    auto& shtc3 = *new (ptr) ShtC3;
    return (&shtc3)->as<I2C::Component*>();
  }
#endif // #ifdef ARDUINOJSON_VERSION

private:
  Adafruit_SHTC3 _sensor;
};

} // namespace I2C
} // namespace Hardware

#endif // #if WITH_SHTC3
