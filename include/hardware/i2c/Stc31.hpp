#pragma once

#if !defined(WITH_STC31) || DOXYGEN_RUN
/**
  @ingroup Stc31Flags
  @brief Whether to enable support for the STC31 CO2 sensor
*/
#define WITH_STC31 false
#endif // #ifndef WITH_STC31

#if WITH_STC31

#include "Component.hpp"

#include <SensirionI2CStc3x.h>

namespace Hardware {
namespace I2C {

class Stc31 : public I2C::Component
{
  HARDWARE_COMPONENT(I2C::Stc31)
public:
  Stc31(TwoWire* i2c = &Wire, Component* parent = nullptr)
    : I2C::Component::Component(
        i2c,
        parent,
        { new Hardware::Component::Measurement(*this, "co2", "ppm") })
    , sensor()
    , pressureComponentPattern()
    , temperatureComponentPattern()
    , humidityComponentPattern()
    , defaultPressure(FNAN)
    , defaultTemperature(FNAN)
    , defaultHumidity(FNAN)
    , _pressureCallback(nullptr)
    , _pressureMeasurement(nullptr)
    , _temperatureMeasurement(nullptr)
    , _humidityMeasurement(nullptr)
    , _measurementsSearched(false)
  {}

  virtual char* toString(char* buf, const size_t bufSize) override
  {
    snprintf_P(buf,
               bufSize,
               PSTR("[%s] Sensiron STC31 sensor (%s)"),
               this->name.c_str(),
               this->error.c_str());
    return buf;
  }

  typedef float (*pressureCallback_t)(void);

  void setPressureCallback(pressureCallback_t callback)
  {
    this->_pressureCallback = callback;
  }

  static uint16_t gasTicksFromVolumeFraction(float volumeFraction)
  {
    return volumeFraction * ((uint16_t)1 << 15) + ((uint16_t)1 << 14);
  }

  static float volumeFractionFromGasTicks(uint16_t gasTicks)
  {
    // Serial.print("gasTicks = ");
    // Serial.println(gasTicks);
    // Serial.print("(int32_t)gasTicks = ");
    // Serial.println((int32_t)gasTicks);
    // Serial.print("((uint16_t)1 << 14) = ");
    // Serial.println(((uint16_t)1 << 14));
    // Serial.print("((uint16_t)1 << 15) = ");
    // Serial.println(((uint16_t)1 << 15));
    // Serial.print("((int32_t)gasTicks - ((uint16_t)1 << 14)) = ");
    // Serial.println(((int32_t)gasTicks - ((uint16_t)1 << 14)));
    // Serial.print(
    //   "((int32_t)gasTicks - ((uint16_t)1 << 14)) / ((uint16_t)1 << 15) = ");
    // Serial.println(((int32_t)gasTicks - ((uint16_t)1 << 14)) /
    //                ((uint16_t)1 << 15));
    return (float)((int32_t)gasTicks - ((uint16_t)1 << 14)) /
           (float)((uint16_t)1 << 15);
  }

  static uint16_t temperatureTicksFromCelsius(float temperatureCelsius)
  {
    return temperatureCelsius * 200;
  }

  static float celsiusFromTemperatureTicks(uint16_t temperatureTicks)
  {
    return temperatureTicks / 200.0F;
  }

  static uint16_t humidityTicksFromRHFraction(float relativeHumidityFraction)
  {
    return relativeHumidityFraction * (~(uint16_t)0);
  }

  static float fractionFromHumidityTicks(uint16_t humidityTicks)
  {
    return (float)humidityTicks / (~(uint16_t)0);
  }

  void findMeasurements()
  {
    if (this->_measurementsSearched)
      return;
    auto rootParent = this->getRootParent();
    if (not rootParent)
      return;
    if (not this->pressureComponentPattern.empty())
      this->_pressureMeasurement = this->findMeasurement(
        this->pressureComponentPattern.c_str(), "pressure", rootParent);
    if (not this->temperatureComponentPattern.empty())
      this->_temperatureMeasurement = this->findMeasurement(
        this->temperatureComponentPattern.c_str(), "temperature", rootParent);
    if (not this->humidityComponentPattern.empty())
      this->_humidityMeasurement = this->findMeasurement(
        this->humidityComponentPattern.c_str(), "humidity", rootParent);
    this->_measurementsSearched = true;
    if (not(this->_pressureMeasurement and this->_temperatureMeasurement and
            this->_humidityMeasurement)) {
      LOG(Logging::Sensor,
          Logging::Error,
          cat(F("Didn't find all external measurements for "),
              this->name.c_str()));
    }
  }

  virtual void measure(Hardware::Component::Measurement& measurement) override
  {
    LOG(Logging::Hardware,
        Debug + 10,
        cat(__PRETTY_FUNCTION__,
            " for ",
            this->name.c_str(),
            " and ",
            measurement.quantity));
    this->findMeasurements();
    if (this->error != "ok") {
      this->sensor.begin(*(this->_port));
      if (this->sensor.setBinaryGas(0x0001)) {
        this->error = "cant-set-binarygas";
        return;
      }
      if (this->sensor.disableAutomaticSelfCalibration()) {
        this->error = "cant-disable-selfcalib";
        return;
      }
    }
    if (measurement == "co2") {
      float pressurehPa = FNAN;
      if (this->_pressureMeasurement)
        pressurehPa = this->_pressureMeasurement->as<float>();
      if (not Utils::pressurehPaOkay(pressurehPa) and this->_pressureCallback)
        pressurehPa = this->_pressureCallback();
      if (not Utils::pressurehPaOkay(pressurehPa))
        pressurehPa = this->defaultPressure;
      if (Utils::pressurehPaOkay(pressurehPa)) {
        LOG(Logging::Hardware,
            Verbose,
            cat(F("Setting pressure for "),
#if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
                this->getClassString(),
                " ",
#endif // #if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
                this->name.c_str(),
                " to ",
                pressurehPa,
                " hPa"));
        if (this->sensor.setPressure(pressurehPa))
          this->error = "cant-set-pressure";
      }
      if (this->_temperatureMeasurement) {
        float temperatureCelsius = FNAN;
        if (this->_temperatureMeasurement)
          temperatureCelsius = this->_temperatureMeasurement->as<float>();
        if (not Utils::temperatureCelsiusOkay(temperatureCelsius))
          temperatureCelsius = this->defaultTemperature;
        if (Utils::temperatureCelsiusOkay(temperatureCelsius)) {
          LOG(Logging::Hardware,
              Verbose,
              cat(F("Setting temperature for "),
#if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
                  this->getClassString(),
                  " ",
#endif // #if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
                  this->name.c_str(),
                  " to ",
                  temperatureCelsius,
                  " *C"));
          if (this->sensor.setTemperature(
                temperatureTicksFromCelsius(temperatureCelsius)))
            this->error = "cant-set-temperature";
        }
      }
      if (this->_humidityMeasurement) {
        float humidityPercent = FNAN;
        if (not(Utils::humidityPercentOkay(
              humidityPercent = this->_humidityMeasurement->as<float>())))
          humidityPercent = this->defaultHumidity;
        if (Utils::humidityPercentOkay(humidityPercent)) {
          LOG(Logging::Hardware,
              Verbose,
              cat(F("Setting humidity for "),
#if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
                  this->getClassString(),
                  " ",
#endif // #if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
                  this->name.c_str(),
                  " to ",
                  humidityPercent,
                  " %"));
          if (this->sensor.setRelativeHumidity(
                humidityTicksFromRHFraction(humidityPercent / 100.0F)))
            this->error = "cant-set-humidity";
        }
      }
      uint16_t gasTicks;
      uint16_t temperatureTicks;

      const auto error =
        this->sensor.measureGasConcentration(gasTicks, temperatureTicks);
      if (error) {
        this->error = "comm-error";
        return;
      }
      const auto co2ppm = volumeFractionFromGasTicks(gasTicks) * 1000000.0F;
      if (not Utils::co2ppmOkay(co2ppm))
        this->error = "ok";
      else
        this->error = "bad-co2";
      measurement = co2ppm;
      return;
    }
    LOG(Logging::Hardware,
        Warning,
        cat("unhandled measurement ", measurement.quantity));
  }
  virtual void measure(
    std::initializer_list<const char*> quantities = {}) override
  {
    Hardware::Component::measure(quantities);
  }

#ifdef ARDUINOJSON_VERSION
  static I2C::Component* from(JsonObjectConst json)
  {
    LOG(
      Logging::Hardware, Debug + 10, cat(__PRETTY_FUNCTION__, "(", json, ")"));
    JsonVariantConst type = json["type"];
    if (type != "stc31")
      return nullptr;
    auto ptr = Component::allocate<Stc31>();
    if (not ptr)
      return nullptr;
    auto& stc31 = *new (ptr) Stc31;
    if (json["pressure"].is<const char*>())
      stc31.pressureComponentPattern = json["pressure"].as<const char*>();
    if (json["temperature"].is<const char*>())
      stc31.temperatureComponentPattern =
        json["temperature"].as<const char*>();
    if (json["humidity"].is<const char*>())
      stc31.humidityComponentPattern = json["humidity"].as<char*>();
    stc31.defaultPressure = json["defaults"]["pressure"] | FNAN;
    stc31.defaultTemperature = json["defaults"]["temperature"] | FNAN;
    stc31.defaultHumidity = json["defaults"]["humidity"] | FNAN;
    return (&stc31)->as<I2C::Component*>();
  }
#endif // #ifdef ARDUINOJSON_VERSION

  SensirionI2CStc3x sensor;
  std::string pressureComponentPattern;
  std::string temperatureComponentPattern;
  std::string humidityComponentPattern;
  float defaultPressure;
  float defaultTemperature;
  float defaultHumidity;

private:
  pressureCallback_t _pressureCallback;
  Measurement* _pressureMeasurement;
  Measurement* _temperatureMeasurement;
  Measurement* _humidityMeasurement;
  bool _measurementsSearched;
};

} // namespace I2C
} // namespace Hardware

#endif // #if WITH_STC31
