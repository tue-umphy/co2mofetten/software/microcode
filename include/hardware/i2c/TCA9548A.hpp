#pragma once

#include "../../features/Multiplexer.hpp"

#if WITH_I2C_MULTIPLEXER

#include <array>

#include "Component.hpp"
#include "Components.hpp"

namespace Hardware {

namespace I2C {

class TCA9548A : public Component
{
  HARDWARE_COMPONENT(I2C::TCA9548A)
public:
  TCA9548A(std::initializer_list<Components*> initComponents,
           const uint8_t address = 0x70,
           TwoWire* port = &Wire,
           Component* parent = nullptr)
    : Component::Component(port, parent)
  {
    size_t i = 0;
    for (Components* component : initComponents) {
      if (i >= this->ports.max_size())
        break;
      this->ports[i++] = component;
    }
    while (i <= this->ports.max_size())
      this->ports[i++] = nullptr;
    this->device.setAddress(address);
  }
  TCA9548A(const uint8_t address = 0x70,
           TwoWire* port = &Wire,
           Component* parent = nullptr)
    : Component::Component(port, parent)
  {
    this->ports.fill(nullptr);
    this->device.setAddress(address);
  }

  virtual void traverse(TraverseContext& context,
                        traverseCallback_t callback,
                        void* callbackExtra = nullptr)
  {
    LOG(Logging::Hardware,
        Debug + 10,
        cat(__PRETTY_FUNCTION__,
            " for ",
            this->name.c_str(),
            " with context.level ",
            context.level));
    if (context.abort) {
      LOG(Logging::Hardware,
          Debug + 10,
          cat("Skipping ", __PRETTY_FUNCTION__, " for ", this->name.c_str()));
      return;
    }
    callback(*this, context, callbackExtra);
    size_t p = 0;
    struct Container
    {
      TCA9548A& multiplexer;
      size_t port;
      traverseCallback_t callback;
      void* extra;
    } container{ *this, 0, callback, callbackExtra };
    for (Components* components : this->ports) {
      container.port = p++;
      if (context.abort) {
        LOG(Logging::Hardware,
            Debug + 10,
            cat("Aborting ", __PRETTY_FUNCTION__));
        break;
      }
      if (not components)
        continue;
      const size_t levelBefore = context.level;
      const size_t abortBefore = context.abort;
      TraverseContext nestedContext = context; // copy context
      nestedContext.level++;
      if (context.hardwareAccess) {
        traverseCallback_t decoratedCallback =
          [](Hardware::Component& component,
             TraverseContext& context,
             void* extra) {
            Container& container = *reinterpret_cast<Container*>(extra);
            if (context.abort) {
              LOG(Logging::Hardware,
                  Debug + 10,
                  cat("Skipping ",
                      __PRETTY_FUNCTION__,
                      " for ",
                      component.name.c_str()));
              return;
            }
            LOG(Logging::Hardware,
                Verbose,
                cat(F("Before executing callback for "),
                    component.name.c_str(),
                    F(" we ensure the parent "),
#if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
                    container.multiplexer.getClassString(),
                    " ",
#endif // #if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
                    container.multiplexer.name.c_str(),
                    F(" has port "),
                    container.port,
                    " open"));
            if (not container.multiplexer.selectPorts({ container.port })) {
              context.abort = true;
              LOG(Logging::Hardware,
                  Verbose,
                  cat(F("Multiplexer "),
                      container.multiplexer.name.c_str(),
                      F(" error. Not calling callback for "),
                      component.name.c_str()));
              return;
            }
            container.callback(component, context, container.extra);
          };
        components->traverse(nestedContext, decoratedCallback, &container);
        LOG(Logging::Hardware,
            Debug + 20,
            cat("Multiplexer ",
                container.multiplexer.name.c_str(),
                F(": context.abort after traverse is "),
                context.abort ? "true" : "false"));
      } else {
        components->traverse(nestedContext, callback, callbackExtra);
      }
      context = nestedContext; // apply
      context.level = levelBefore;
      context.abort = abortBefore;
    }
    if (context.hardwareAccess) {
      // When we're done with this multiplexer, switch it off
      if (not this->error or this->error == "?") {
        LOG(Logging::Hardware,
            Debug,
            cat(
#if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
              this->getClassString(),
              " ",
#endif // #if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
              this->name.c_str(),
              F(": switch off")));
        this->selectPorts({});
      } else {
        LOG(Logging::Hardware,
            Verbose,
            cat(
#if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
              this->getClassString(),
              " ",
#endif // #if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
              this->name.c_str(),
              F(": NOT switching off because of error")));
      }
    }
  }
  virtual void traverse(traverseCallback_t callback,
                        void* callbackExtra = nullptr)
  {
    TraverseContext context;
    this->traverse(context, callback, callbackExtra);
  }

  virtual char* toString(char* buf, const size_t bufSize) override
  {
    snprintf_P(buf,
               bufSize,
               PSTR("[%s] TCA9548A I2C multiplexer @%#X with %u "
                    "ports (" BYTE_TO_BINARY_PATTERN ") (%s)"),
               this->name.c_str(),
               this->device.getAddress(),
               this->ports.size(),
               BYTE_TO_BINARY(this->device.getSelectedPorts()),
               this->error.c_str());
    return buf;
  }

  bool selectPorts(std::initializer_list<size_t> ports,
                   const bool force = false)
  {
    uint8_t portMask = 0;
    for (auto port : ports) {
      if (port < sizeof(portMask) * 8)
        portMask |= 1 << port;
    }
    if (not force and this->device.getSelectedPorts() == portMask) {
      LOG(Logging::Hardware,
          Debug + 1,
          cat(
#if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
            this->getClassString(),
            " ",
#endif // #if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
            this->name.c_str(),
            ": ",
            F("cached portMask is already "),
            portMask));
      return true;
    }
    LOG(Logging::Hardware,
        Debug,
        cat(
#if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
          this->getClassString(),
          " ",
#endif // #if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
          this->name.c_str(),
          ": ",
          F("selecting portMask "),
          portMask));
    Multiplexer::TCA9548A::Status status =
      this->device.selectVariousPorts(portMask);
    bool previousError = this->error;
    this->error = status == Multiplexer::TCA9548A::Status::Ok ? "ok" : "error";
    if (this->error) {
      LOG(Logging::Hardware,
          Logging::Error,
          cat(
#if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
            this->getClassString(),
            " ",
#endif // #if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
            this->name.c_str(),
            ": ",
            F("error, setting error of children")));
      // set status of all children
      TraverseContext context;
      context.hardwareAccess = false;
      this->traverse(
        context,
        [](Hardware::Component& component,
           Hardware::Component::TraverseContext& context,
           void* extra) {
          if (&component != extra) // only children, not the multiplexer
            component.error = "parent-multiplexer-fail";
        },
        (void*)this);
      return false;
    }
    if (previousError) {
      LOG(Logging::Hardware,
          Verbose,
          cat(
#if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
            this->getClassString(),
            " ",
#endif // #if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
            this->name.c_str(),
            ": ",
            F("error fixed, resetting error of children")));
      // set status of all children
      TraverseContext context;
      context.hardwareAccess = false;
      this->traverse(
        context,
        [](Hardware::Component& component,
           Hardware::Component::TraverseContext& context,
           void* extra) {
          if (&component != extra) { // only children, not the multiplexer
            LOG(Logging::Hardware,
                Verbose,
                cat(F("Resetting error of "), component.name.c_str()));
            component.error = "?";
          }
        },
        (void*)this);
    }
    return true;
  }

  std::array<Components*, 8> ports;

#ifdef ARDUINOJSON_VERSION
  static Component* from(JsonObjectConst json)
  {
    LOG(
      Logging::Hardware, Debug + 10, cat(__PRETTY_FUNCTION__, "(", json, ")"));
    JsonVariantConst type = json["type"];
    if (type != "tca9548A")
      return nullptr;
    auto ptr = Component::allocate<TCA9548A>();
    if (not ptr)
      return nullptr;
    TCA9548A& multiplexer = *new (ptr) TCA9548A;
    if (json["address"].is<uint8_t>())
      multiplexer.device.setAddress(json["address"].as<uint8_t>());
    size_t port = 0;
    for (JsonObjectConst portJson : json["ports"].as<JsonArray>()) {
      if (port >= multiplexer.ports.size()) {
        LOG(Logging::Hardware,
            Warning,
            cat(
#if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
              multiplexer.getClassString(),
              " ",
#endif // #if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
              F("can only hold "),
              multiplexer.ports.size(),
              F(" ports. Ignoring the rest.")));
        break;
      }
      if (portJson.isNull()) {
        multiplexer.ports[port++] = nullptr;
        continue;
      }
      I2C::Component* component = I2C::Components::from(portJson);
      if (component) {
        auto& components = *component->as<I2C::Components*>();
        char tmp[10] = { 0 };
        components.name =
          snprintf2(tmp, Utils::arraySize(tmp), "port/%u", port);
        components.setParent(multiplexer);
        multiplexer.ports[port++] = &components;
      } else {
        LOG(Logging::Hardware,
            Warning,
            cat(F("can't convert port "),
                port,
                F(" JSON "),
                portJson,
                F(" to I2C::Components")));
        multiplexer.ports[port++] = nullptr;
      }
    }
    return (&multiplexer)->as<Component*>();
  }
#endif // #ifdef ARDUINOJSON_VERSION

  Multiplexer::TCA9548A device;

private:
};

} // namespace I2C

} // namespace Hardware

#endif // #if WITH_I2C_MULTIPLEXER
