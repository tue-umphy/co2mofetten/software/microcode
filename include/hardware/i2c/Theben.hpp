#pragma once

#if !defined(WITH_THEBEN) || DOXYGEN_RUN
/**
  @ingroup ThebenFlags
  @brief Whether to enable support for the THEBEN environmental sensor
*/
#define WITH_THEBEN true
#endif // #ifndef WITH_THEBEN

#if WITH_THEBEN

#include "Component.hpp"

#include <ThebenCO2.h>

#include <algorithm>

namespace Hardware {
namespace I2C {

class Theben : public I2C::Component
{
  HARDWARE_COMPONENT(I2C::Theben)
public:
  Theben(TwoWire* i2c = &Wire, Component* parent = nullptr)
    : I2C::Component::Component(
        i2c,
        parent,
        { new Hardware::Component::Measurement(*this, "co2", "ppm"),
          new Hardware::Component::Measurement(*this, "temperature", "*C") })
    , _sensor()
  {}

  virtual char* toString(char* buf, const size_t bufSize) override
  {
    snprintf_P(buf,
               bufSize,
               PSTR("[%s] Theben CO2 sensor (%s)"),
               this->name.c_str(),
               this->error.c_str());
    return buf;
  }

  virtual void measure(Hardware::Component::Measurement& measurement) override
  {
    LOG(Logging::Hardware,
        Debug + 20,
        cat(__PRETTY_FUNCTION__, '(', measurement.quantity, ')'));
    if (measurement == "co2") {
      const auto co2 = this->_sensor.getCO2(THEBENCO2_CO2_READING::RAW);
      if (Utils::co2ppmOkay(co2)) {
        this->error = "ok";
        measurement = co2;
      } else
        this->error = "bad-co2";
      return;
    }
    LOG(Logging::Hardware,
        Warning,
        cat("unhandled measurement ", measurement.quantity));
  }
  virtual void measure(
    std::initializer_list<const char*> quantities = {}) override
  {
    LOG(Logging::Hardware,
        Debug + 20,
        cat(__PRETTY_FUNCTION__, " for ", this->name.c_str()));
    LOG(Logging::Hardware,
        Verbose,
        cat(this->name.c_str(), ' ', F("requesting version")));
    bool found = this->_sensor.requestVersion();
    LOG(Logging::Hardware,
        Debug + 30,
        cat(this->name.c_str(), "done", ' ', F("requesting version")));
    if (not found) {
      this->error = "not-found";
      return;
    }
    Hardware::Component::measure(quantities);
  }

#ifdef ARDUINOJSON_VERSION
  static I2C::Component* from(JsonObjectConst json)
  {
    LOG(
      Logging::Hardware, Debug + 20, cat(__PRETTY_FUNCTION__, "(", json, ")"));
    JsonVariantConst type = json["type"];
    if (type != "theben")
      return nullptr;
    auto ptr = Component::allocate<Theben>();
    if (not ptr)
      return nullptr;
    auto& theben = *new (ptr) Theben;
    return (&theben)->as<I2C::Component*>();
  }
#endif // #ifdef ARDUINOJSON_VERSION

private:
  ThebenCO2 _sensor;
};

} // namespace I2C
} // namespace Hardware

#endif // #if WITH_THEBEN
