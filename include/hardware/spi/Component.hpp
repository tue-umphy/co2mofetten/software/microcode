#pragma once

#include <SPI.h>

auto& defaultSPI = SPI;

#include "../Component.hpp"

namespace Hardware {

namespace SPI {

class Component : public Hardware::Component
{
  HARDWARE_COMPONENT(SPI::Component)
public:
  Component(SPIClass* port = &defaultSPI,
            Hardware::Component* parent = nullptr,
            std::initializer_list<Hardware::Component::Measurement*> m = {})
    : Hardware::Component::Component(parent, m)
    , _port(port)
  {
    // if no port was given but a parent, use the parent's port
    if (not port and parent) {
#if HARDWARE_COMPONENT_ENABLE_IS
      if (parent->is<SPI::Component>())
        this->setPort(parent->as<SPI::Component*>()->getPort());
#endif // #if HARDWARE_COMPONENT_ENABLE_IS
    }
  }
  Component(SPIClass& port, Hardware::Component& parent)
    : Component(&port, &parent)
  {}
  Component(Hardware::Component& parent)
    : Component(defaultSPI, parent)
  {}
  Component(SPIClass& port)
    : Component(&port, nullptr)
  {}

  SPIClass* getPort() { return this->_port; }
  void setPort(SPIClass* port) { this->_port = port; }
  void setPort(SPIClass& port) { this->setPort(&port); }

#ifdef ARDUINOJSON_VERSION
  typedef SPI::Component* (*builderCallback_t)(JsonObjectConst json);
  static std::vector<builderCallback_t>& getBuilders(); // implemented later

  /**
   * @brief Try all builders from getBuilders() to create an object of a given
   * type from a `JsonObject`
   * @returns nullptr on failure
   */
  static Component* from(JsonObjectConst json)
  {
    size_t i = 0;
    for (auto builder : SPI::Component::getBuilders()) {
      LOG(Logging::Hardware,
          Debug,
          cat(F("Trying "),
#if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
              classString,
              " ",
#endif // #if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
              "builder ",
              i,
              F(" (free RAM: "),
              Utils::largestMalloc(),
              F(" bytes) ")));
      i++;
      Component* component = builder(json);
      if (component) {
        component->setCommonFrom(json);
        return component;
      }
    }
    LOG(Logging::Hardware,
        Warning,
        cat("JSON ",
            json,
            F(" could't be converted to "),
#if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
            classString,
            " ",
#endif // #if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
            "subclass"));
    return nullptr;
  }
#endif // #ifdef ARDUINOJSON_VERSION

private:
  SPIClass* _port;
};

} // namespace SPI

} // namespace Hardware
