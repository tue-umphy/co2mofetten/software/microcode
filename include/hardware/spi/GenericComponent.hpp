#pragma once

#include "Component.hpp"

namespace Hardware {

namespace SPI {

class GenericComponent : public Component
{
  HARDWARE_COMPONENT(GenericComponent)
  using Component::Component;

public:
  virtual char* toString(char* buf, const size_t bufSize)
  {
    snprintf_P(
      buf, bufSize, PSTR("[%s] generic SPI component"), this->name.c_str());
    return buf;
  }

#ifdef ARDUINOJSON_VERSION
  static Component* from(JsonObjectConst json)
  {
    LOG(
      Logging::Hardware, Debug + 10, cat(__PRETTY_FUNCTION__, "(", json, ")"));
    auto ptr = Component::allocate<GenericComponent>();
    if (not ptr)
      return nullptr;
    return new (ptr) GenericComponent;
  }
#endif // #ifdef ARDUINOJSON_VERSION
};

} // namespace SPI

} // namespace Hardware
