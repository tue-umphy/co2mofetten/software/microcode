#pragma once

#if !defined(WITH_SD) || DOXYGEN_RUN
/**
  @ingroup SdFlags
  @brief Whether to enable support for the SD cards
*/
#define WITH_SD false
#endif // #ifndef WITH_SD

#ifndef SD_DEFAULT_CHIPSELECT_PIN
/**
  @ingroup SdFlags
  @brief Default Chipselect pin for the SD card
*/
#define SD_DEFAULT_CHIPSELECT_PIN SS
#endif // #ifndef SD_DEFAULT_CHIPSELECT_PIN

#if WITH_SD

#include "Component.hpp"

#include <SdFat.h>

#if ESP8266
#define SDFAT(x) sdfat::x
#else // #if ESP8266
#define SDFAT(x) x
#endif // #if ESP8266

namespace Hardware {
namespace SPI {

class SDCard : public SPI::Component
{
  HARDWARE_COMPONENT(SPI::SDCard)
public:
  SDCard(SPIClass* port = &defaultSPI,
         const int chipselect = SD_DEFAULT_CHIPSELECT_PIN,
         Component* parent = nullptr)
    : SPI::Component::Component(
        port,
        parent,
        { new Hardware::Component::Measurement(*this, "free", "bytes"),
          new Hardware::Component::Measurement(*this, "written", "bytes") })
    , chipselectPin(chipselect)
    , filesystem()
    , retryIntervalMs(0)
    , _bytesWritten(0)
    , _lastInitFailure(0)
  {}

  virtual char* toString(char* buf, const size_t bufSize) override
  {
    snprintf_P(buf,
               bufSize,
               PSTR("[%s] SD card (%s)"),
               this->name.c_str(),
               this->error.c_str());
    return buf;
  }

  /**
    @brief initialize the SD card
    @details Skipped if retryIntervalMs is set and not yet reached
    @return the status of the SD card
  */
  virtual bool init(void)
  {
    if (this->_lastInitFailure and this->retryIntervalMs and
        millis() - this->_lastInitFailure < this->retryIntervalMs)
      return false;
    pinMode(this->chipselectPin, OUTPUT);
    const bool sdWorks = this->filesystem.begin(this->chipselectPin);
    if (not sdWorks) {
      this->_lastInitFailure = millis();
      int errorCode = this->filesystem.card()->errorCode();
      LOG(Storage,
          Logging::Error,
          cat("ERROR: SD card initialization failed (code ", errorCode, ")"));
      LOG(Storage,
          Info,
          cat(F("Run the following command to check the error code: "), "\n");
          cat(F("locate '*SdFat*/SdInfo.h' "
                "| xargs -r grep -B10 -A10 '\\bSD_CARD_ERROR_'")));
      if (0x20 <= errorCode && errorCode < 0x30)
        this->error = "basic-error";
      else if (0x30 <= errorCode && errorCode < 0x60)
        this->error = "rw-error";
      else if (0x60 <= errorCode && errorCode <= 0x65)
        this->error = "misc-error";
      if (this->error)
        return false;
      if (not this->filesystem.vol()->fatType()) {
        LOG(Storage,
            Logging::Error,
            cat("Can't find a valid FAT16/FAT32 partition", "\n"));
        this->error = "invalid-filesystem";
        return false;
      }
      LOG(Storage,
          Logging::Error,
          cat(F("Something is strange with the SD card")));
      this->error = "error";
      return false;
    }
    this->error = "ok";
    return true;
  }

  virtual void measure(Hardware::Component::Measurement& measurement) override
  {
    if (measurement == "free") {
      LOG(Storage, Info, cat(F("Determining free SD size")));
      const uint32_t freeClusters = this->filesystem.vol()->freeClusterCount();
      const uint64_t freeBlocks =
        freeClusters * this->filesystem.vol()->blocksPerCluster();
      const uint64_t freeBytes = freeBlocks * 512;
      measurement = freeBytes;
      return;
    }
    if (measurement == "written") {
      measurement = this->_bytesWritten;
      return;
    }
    LOG(Logging::Hardware,
        Warning,
        cat("unhandled measurement ", measurement.quantity));
  }
  virtual void measure(
    std::initializer_list<const char*> quantities = {}) override
  {
    if (not this->init())
      return;
    Hardware::Component::measure(quantities);
  }

  static bool validFilenameCharacter(const char c)
  {
    return ('0' <= c and c <= '9') or ('A' <= c and c <= 'Z') or
           ('a' <= c and c <= 'z') or c == '-' or c == '_' or c == '.';
  }

  //! Replace characters in a string that are not in another string with a
  // specific character
  template<size_t filenameSize>
  size_t sanitizeFilename(char (&filename)[filenameSize],
                          const char repl = '-')
  {
    size_t replaced = 0;
    for (size_t i = 0; i < filenameSize; i++) {
      char& c = filename[i];
      if (not c)
        break;
      if (not this->validFilenameCharacter(c)) {
        replaced++;
        c = repl;
      }
    }
    return replaced;
  }

  virtual bool chdir(const char* directory)
  {
    if (not this->init())
      return false;
    if (not this->filesystem.chdir(true)) {
      LOG(Storage, Logging::Error, cat(F("Couldn't chdir to root directory")));
      this->error = "error";
      return false;
    }
    // at this point, the path contains the directory
    SDFAT(FatFile) dir = this->filesystem.open(directory, SDFAT(O_RDONLY));
    if (not dir.isDir()) {
      const bool success = this->filesystem.mkdir(directory, true);
      if (success) {
        LOG(Storage, Info, cat(F("Created directory "), directory));
      } else {
        LOG(Storage,
            Logging::Error,
            cat(F("Couldn't create directory "), directory));
        this->error = "error";
        return false;
      }
    }
    if (not this->filesystem.chdir(directory)) {
      LOG(Storage,
          Logging::Error,
          cat(F("Couldn't change directory to "), directory));
      this->error = "error";
      return false;
    }
    return true;
  }

  virtual bool chdir(Hardware::Component& component)
  {
    char directory[265] = { 0 };
    component.toReversePath(directory, Utils::arraySize(directory));
    strrev(directory);
    return this->chdir(directory);
  }

  virtual bool store(const char* directory,
                     const char* filename,
                     const JsonVariantConst json)
  {
    if (not this->chdir(directory))
      return false;
    SDFAT(FatFile)
    file = this->filesystem.open(
      filename, SDFAT(O_RDWR) | SDFAT(O_CREAT) | SDFAT(O_AT_END));
    if (not file.isOpen()) {
      LOG(Storage, Logging::Error, cat(F("Couldn't open file "), filename));
      return false;
    }
    Timestamp time;
    char tmp[64] = { 0 };
    auto bytesWritten = this->_bytesWritten;
    bytesWritten = 0;
    bytesWritten += file.write(time.toString(tmp));
    bytesWritten += file.write(',');
    bytesWritten += serializeJson(json, file);
    bytesWritten += file.write('\n');
    if (file.close()) {
      this->_bytesWritten += bytesWritten;
      LOG(Storage,
          Verbose,
          cat("Wrote ", bytesWritten, F(" bytes to file "), filename));
    } else {
      LOG(Storage, Logging::Error, cat(F("Couldn't close file "), filename));
      this->error = "error";
      return false;
    }
    return true;
  }

  virtual bool store(const Hardware::Component::Error& error)
  {
    if (not this->chdir(error.component()))
      return false;
    LOG(Storage,
        Verbose,
        cat("Storing ",
            error.component().name.c_str(),
            F(" status change to "),
            error.c_str()));
    const char* filename = "status.csv";
    SDFAT(FatFile)
    file = this->filesystem.open(
      filename, SDFAT(O_RDWR) | SDFAT(O_CREAT) | SDFAT(O_AT_END));
    if (not file.isOpen()) {
      LOG(Storage, Logging::Error, cat(F("Couldn't open file "), filename));
      return false;
    }
    char tmp[64] = { 0 };
    auto bytesWritten = this->_bytesWritten;
    bytesWritten = 0;
    bytesWritten += file.write(error.time.toString(tmp));
    bytesWritten += file.write(',');
    bytesWritten += file.write(error.c_str());
    bytesWritten += file.write('\n');
    if (file.close()) {
      this->_bytesWritten += bytesWritten;
      LOG(Storage,
          Verbose,
          cat("Wrote ", bytesWritten, F(" bytes to file "), filename));
    } else {
      LOG(Storage, Logging::Error, cat(F("Couldn't close file "), filename));
      this->error = "error";
      return false;
    }
    return true;
  }

  virtual bool store(const Hardware::Component::Measurement& measurement)
  {
    if (not this->chdir(measurement.component()))
      return false;
    {
      LOG(Storage, Verbose, char measurementString[64] = { 0 };
          cat("Storing ",
              measurement.component().name.c_str(),
              " ",
              measurement.quantity,
              " measurement ",
              measurement.toString(measurementString),
              " ",
              measurement.unit));
    }
    char filename[64] = { 0 };
    snprintf(filename,
             Utils::arraySize(filename),
             "%s-%s.csv",
             measurement.quantity,
             measurement.unit);
    size_t invalidChars = 0;
    invalidChars = this->sanitizeFilename(filename);
    if (invalidChars) {
      LOG(Storage,
          Warning,
          cat("Replaced ", invalidChars, F(" invalid filename characters")));
    }
    SDFAT(FatFile)
    file = this->filesystem.open(
      filename, SDFAT(O_RDWR) | SDFAT(O_CREAT) | SDFAT(O_AT_END));
    if (not file.isOpen()) {
      LOG(Storage, Logging::Error, cat(F("Couldn't open file "), filename));
      return false;
    }
    char tmp[64] = { 0 };
    auto bytesWritten = this->_bytesWritten;
    bytesWritten = 0;
    bytesWritten += file.write(measurement.time.toString(tmp));
    bytesWritten += file.write(',');
    bytesWritten += file.write(measurement.toString(tmp));
    bytesWritten += file.write('\n');
    if (file.close()) {
      this->_bytesWritten += bytesWritten;
      LOG(Storage,
          Verbose,
          cat("Wrote ", bytesWritten, F(" bytes to file "), filename));
    } else {
      LOG(Storage, Logging::Error, cat(F("Couldn't close file "), filename));
      this->error = "error";
      return false;
    }
    return true;
  }

#ifdef ARDUINOJSON_VERSION
  static SPI::Component* from(JsonObjectConst json)
  {
    JsonVariantConst type = json["type"];
    if (type != "sd")
      return nullptr;
    auto ptr = Component::allocate<SDCard>();
    if (not ptr)
      return nullptr;
    auto& sdCard = *(new (ptr) SDCard);
    if (json["chipselect"].is<int>())
      sdCard.chipselectPin = json["chipselect"].as<int>();
    sdCard.retryIntervalMs = json["retry"] | (unsigned long)0;
    return (&sdCard)->as<SPI::Component*>();
  }
#endif // #ifdef ARDUINOJSON_VERSION

  int chipselectPin;

  SDFAT(SdFat) filesystem;
  //! How many milliseconds to let init() return false without even trying to
  //  access the SD card. This is to prevent a broken SD card from slowing down
  //  the whole program as it takes roughly 10x longer even to recognize that
  //  the card is broken.
  unsigned long retryIntervalMs;

private:
  unsigned long _bytesWritten;
  unsigned long _lastInitFailure;
};

} // namespace SPI
} // namespace Hardware

#endif // #if WITH_SD
