#pragma once

#include "Component.hpp"
#include "Components.hpp"
#include "GenericComponent.hpp"
#include "SDCard.hpp"

namespace Hardware {

namespace SPI {

std::vector<SPI::Component::builderCallback_t>&
SPI::Component::getBuilders()
{
  static std::vector<SPI::Component::builderCallback_t> builders
  {
    SPI::Components::from,
#if WITH_SD
      SPI::SDCard::from,
#endif // #if WITH_SD
      SPI::GenericComponent::from
  };
  return builders;
}

} // namespace SPI

} // namespace Hardware
