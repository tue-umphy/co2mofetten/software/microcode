#pragma once

/**
  @file BitUtils.hpp
  @brief Bit Utilities
*/

template<typename TYPE>
TYPE
reverseBits(TYPE original)
{
  TYPE reversed = ~(TYPE)0;
  for (size_t i = 0; i < sizeof(TYPE) * 8; i++) {
    reversed <<= 1;
    reversed |= (original & 1);
    original >>= 1;
  }
  return reversed;
}

template<typename TYPE>
TYPE
reverseBytes(TYPE original)
{
  TYPE reversed = ~(TYPE)0;
  for (size_t i = 0; i < sizeof(TYPE); i++) {
    reversed <<= 8;
    reversed |= (original & 0xFF);
    original >>= 8;
  }
  return reversed;
}
