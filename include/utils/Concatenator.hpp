#pragma once

#include <ArduinoJson.h>

#include <iostream>
#include <limits.h>

#include "StringUtils.hpp"
#include "Utils.hpp"

class Concatenator
{
public:
  Concatenator(
#if NATIVE
    std::ostream
#else  // #if NATIVE
    Print
#endif // #if NATIVE
      & output)
    : _output(output)
  {}

#if NATIVE

  template<typename T>
  void operator()(const T x)
  {
    this->_output << x;
  }

#ifdef ARDUINOJSON_VERSION

  void operator()(const JsonDocument& json)
  {
    serializeJson(json, this->_output);
  }

  void operator()(const JsonObjectConst json)
  {
    serializeJson(json, this->_output);
  }

  void operator()(const JsonArrayConst json)
  {
    serializeJson(json, this->_output);
  }

  void operator()(const JsonVariantConst json)
  {
    serializeJson(json, this->_output);
  }

#endif // #ifdef ARDUINOJSON_VERSION

#else // #if NATIVE

  template<typename T>
  void operator()(const T t)
  {
    this->_output.print(t);
  }

  void operator()(const long long t)
  {
    char tmp[100] = { 0 };
    this->_output.print(sprintf2(tmp, "%lld", t));
  }

  void operator()(const unsigned long long t)
  {
    char tmp[100] = { 0 };
    this->_output.print(t > ULONG_MAX
                          ? sprintf2(tmp, "%llu", t)
                          : sprintf2(tmp, "%lu", (unsigned long)t));
  }

  void operator()(const long double t)
  {
    char tmp[100] = { 0 };
    this->_output.print(sprintf2(tmp, "%lf", t));
  }

  void operator()(const char* s) { this->_output.print(s); }

#ifdef ARDUINOJSON_VERSION
  void operator()(const JsonDocument& json)
  {
    serializeJson(json, this->_output);
  }

  void operator()(const JsonObjectConst json)
  {
    serializeJson(json, this->_output);
  }

  void operator()(const JsonVariantConst json)
  {
    serializeJson(json, this->_output);
  }

  void operator()(const JsonArrayConst json)
  {
    serializeJson(json, this->_output);
  }
#endif // #ifdef ARDUINOJSON_VERSION

#endif // #if NATIVE

  void operator()(void) { this->_output.flush(); }

  template<typename firstType, typename... Args>
  void operator()(firstType firstValue, Args... args)
  {
    this->operator()(firstValue);
    this->operator()(args...);
  }

private:
#if NATIVE
  std::ostream
#else  // #if NATIVE
  Print
#endif // #if NATIVE
    & _output;
};
