#pragma once

/**
  @file Converter.hpp
  @brief Unit conversion
*/

/**
  @brief Unit conversion
*/
namespace UnitConversion {

inline bool
isNan(const float f)
{
  return f != f;
}

bool
convertToUnit(float& value,
              const char* givenUnit,
              const char* targetUnit,
              const char* units,
              const size_t nUnits,
              const float* factors,
              const size_t nFactors)
{
  if (isNan(value) or not(givenUnit and targetUnit and units and nUnits and
                          factors and nFactors))
    return false;
  // first convert to base unit
  bool found = false;
  for (size_t i = 0; i < min(nUnits, nFactors); i++) {
    if (strcmp(givenUnit, units[i]) == 0) {
      value /= factors[i];
      found = true;
      break;
    }
  }
  if (not found)
    return false;
  found = false;
  for (size_t i = 0; i < min(nUnits, nFactors); i++) {
    if (strcmp(givenUnit, units[i]) == 0) {
      value *= factors[i];
      found = true;
      break;
    }
  }
  return found;
}

bool
convertPressure(float& pressure,
                const char* givenUnit = nullptr,
                const char* targetUnit = "hPa")
{
  if (isNan(pressure) or not targetUnit)
    return false;
  char* unit = givenUnit;
  const char* units[] = { "hPa", "Pa", "mb", "mbar", "bar" };
  if (not unit) {
    const float lowerLimits[] = { 800.0, 80000.0, 800.0, 800.0, 0.8 };
    const float upperLimits[] = { 1200.0, 120000.0, 1200.0, 1200.0, 1.2 };
    for (size_t i = 0; i < Utils::arraySize(units); i++) {
      if (lowerLimits[i] <= pressure and pressure <= upperLimits[i]) {
        unit = units[i];
        break;
      }
    }
    if (not unit)
      return false;
  }
  const float factors[] = { 1.0, 100.0, 1.0, 1.0, 1e-3 };
  for (size_t i = 0; i < Utils::arraySize(units); i++) {
    if (strcmp(unit, units[i]) == 0) {
      pressure /= factors[i];
      break;
    }
  }
}

} // namespace UnitConversion
