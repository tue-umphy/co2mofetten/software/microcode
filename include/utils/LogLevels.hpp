#pragma once

namespace Logging {

//! Predefined Levels
enum Level
{
  Error = 0,
  Warning = 100,
  Info = 200,
  Verbose = 300,
  Debug = 400,
};

//! Predefined Channels
enum Channel
{
  General = 0,
  System = 8,
  Hardware = 16,
  Sensor = 24,
  Network = 32,
  Storage = 40,
  Other = 56,
};

}
