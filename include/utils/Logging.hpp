#pragma once

#include "../config/Config.hpp"
#include "Concatenator.hpp"
#include "LogLevels.hpp"

namespace Logging {

#if NATIVE
Concatenator cat(std::cout);
#else  // #if NATIVE
Concatenator cat(LOGPRINT);
#endif // #if NATIVE

const int logLevelMax = LOGLEVEL_MAX;
const int logLevelMin = LOGLEVEL_MIN;
/**
 * @brief Bitmask of compiled-in channels
 * @details There are 64 channels (each of the 64 bits)
 */
const uint64_t channels = LOGGING_CHANNELS;

template<size_t strSize>
char*
levelToString(const int level, char (&str)[strSize])
{
#define LEVELTOSTRING_CASE(s, sS, cL, L)                                      \
  if (cL > L) {                                                               \
    snprintf(s, sS, "%s+%d", #L, cL - L);                                     \
    return s;                                                                 \
  }                                                                           \
  if (cL == L)                                                                \
    return strncpy(s, #L, sS);
  LEVELTOSTRING_CASE(str, strSize, level, Debug)
  LEVELTOSTRING_CASE(str, strSize, level, Verbose)
  LEVELTOSTRING_CASE(str, strSize, level, Info)
  LEVELTOSTRING_CASE(str, strSize, level, Warning)
  LEVELTOSTRING_CASE(str, strSize, level, Error)
  strncpy(str, "?", strSize);
  return str;
#undef LEVELTOSTRING_CASE
}

template<size_t strSize>
char*
channelToString(const int channel, char (&str)[strSize])
{
#define CHANNELTOSTRING_CASE(s, sS, cC, C)                                    \
  if (cC > C) {                                                               \
    snprintf(s, sS, "%s+%d", #C, cC - C);                                     \
    return s;                                                                 \
  }                                                                           \
  if (cC == C)                                                                \
    return strncpy(s, #C, sS);
  CHANNELTOSTRING_CASE(str, strSize, channel, Other)
  CHANNELTOSTRING_CASE(str, strSize, channel, Storage)
  CHANNELTOSTRING_CASE(str, strSize, channel, Network)
  CHANNELTOSTRING_CASE(str, strSize, channel, Sensor)
  CHANNELTOSTRING_CASE(str, strSize, channel, Hardware)
  CHANNELTOSTRING_CASE(str, strSize, channel, System)
  CHANNELTOSTRING_CASE(str, strSize, channel, General)
  strncpy(str, "?", strSize);
  return str;
#undef CHANNELTOSTRING_CASE
}

uint64_t
channelBitMask(const char* str)
{
  size_t topChannel = 63;
  uint64_t bitMask = 0;
#define CHANNELBITMASK_CASE(c)                                                \
  {                                                                           \
    if (strcasecmp(str, #c) == 0) {                                           \
      for (size_t i = c; i <= topChannel; i++)                                \
        bitMask |= (uint64_t)1 << i;                                          \
      return bitMask;                                                         \
    }                                                                         \
    topChannel = c - 1;                                                       \
  }
  CHANNELBITMASK_CASE(Other);
  CHANNELBITMASK_CASE(Storage);
  CHANNELBITMASK_CASE(Network);
  CHANNELBITMASK_CASE(Sensor);
  CHANNELBITMASK_CASE(Hardware);
  CHANNELBITMASK_CASE(System);
  CHANNELBITMASK_CASE(General);
  return bitMask;
#undef CHANNELBITMASK_CASE
}

bool
logLevel(const char* str, int& level)
{
#define LOGLEVEL_CASE(L)                                                      \
  if (strcasecmp(str, #L) == 0) {                                             \
    level = L;                                                                \
    return true;                                                              \
  }
  LOGLEVEL_CASE(Error);
  LOGLEVEL_CASE(Warning);
  LOGLEVEL_CASE(Info);
  LOGLEVEL_CASE(Verbose);
  LOGLEVEL_CASE(Debug);
  return false;
#undef LOGLEVEL_CASE
}

#ifdef ESP8266
#define WHEN_ESP8266_ELSE(x, y) x
#else // #ifdef ESP8266
#define WHEN_ESP8266_ELSE(x, y) y
#endif // #ifdef ESP8266

#if LOGGING_DEBUG_RAM
#define WHEN_LOGGING_DEBUG_RAM(x) x
#else // #if LOGGING_DEBUG_RAM
#define WHEN_LOGGING_DEBUG_RAM(x)
#endif // #if LOGGING_DEBUG_RAM

/**
 *  @brief Macro for conditional execution of logging commands
 *  @param channel the channel for this logging command.
 *  @param level the level for this logging command.
 *  @param cmd the command to execute
 *  @details The given command is only executed (in an own block!), if both
 *  channel and level were compiled in based on #LOGLEVEL_MAX, #LOGLEVEL_MIN
 *  and #LOGGING_CHANNELS **and** the runtime settings #runtimeLogLevelMin,
 *  #runtimeLogLevelMax and #runtimeChannels match.
 */
#define LOG(channel, level, cmd)                                              \
  {                                                                           \
    using namespace Logging;                                                  \
    uint64_t c = (uint64_t)1 << (channel);                                    \
    int l = (level);                                                          \
    if (c & Config::config.runtimeChannels and c & Logging::channels and      \
        Logging::logLevelMin <= l and                                         \
        Config::config.runtimeLogLevelMin <= l and                            \
        l <= Config::config.runtimeLogLevelMax and                            \
        l <= Logging::logLevelMax) {                                          \
      char tmp[17] = { 0 };                                                   \
      cat('[', channelToString(channel, tmp));                                \
      cat(' ', levelToString(level, tmp), ']', ' ');                          \
      WHEN_LOGGING_DEBUG_RAM(if (Config::config.runtimeDebugRam) {            \
        cat('(',                                                              \
            F("free heap: "),                                                 \
            WHEN_ESP8266_ELSE(ESP.getMaxFreeBlockSize(),                      \
                              Utils::largestMalloc()),                        \
            ')',                                                              \
            ' ');                                                             \
      });                                                                     \
      {                                                                       \
        cmd;                                                                  \
      };                                                                      \
      cat('\n');                                                              \
    };                                                                        \
  }

} // namespace Logging

using Logging::cat;
