#pragma once

/**
  @file MultiStream.hpp
  @brief Multiple streams in one
*/

#include <Arduino.h>

/**
 *  @brief Interface multiple streams from a single one
 */
template<size_t nSlots>
class MultiStream : public Stream
{
public:
  MultiStream()
    : _slots{ nullptr } {};

  //! Add a stream
  bool add(Stream& stream)
  {
    uint8_t index;
    const bool anyFree = nextFreeSlot(index);
    if (anyFree)
      this->_slots[index] = &stream;
    return anyFree;
  }

  //! Return the first available byte of any stream in order
  int read()
  {
    int ret = -1;
    for (size_t i = 0; i < nSlots; i++) {
      Stream* stream = this->_slots[i];
      if (not stream)
        continue;
#if ESP8266
      // don't know why (yet) but only this works properly together with our
      // StreamGateway
      ret = stream->read();
      if (ret >= 0)
        return ret;
#else  // #if ESP8266
      char c;
      // don't use stream->read() as it doesn't honor timeout (needed for
      // EEPROM write from cli)
      const size_t n = stream->readBytes(&c, 1);
      if (n > 0)
        return c;
#endif // #if ESP8266
    }
    return ret;
  }

  //! fill given buffer from all streams in order until full or timeout
  size_t readBytes(char* buffer, size_t length)
  {
    size_t count = 0;
    for (size_t i = 0; i < nSlots; i++) {
      char* buf = buffer + count;
      const size_t len = length - count;
      if (not len)
        return count;
      Stream* stream = this->_slots[i];
      if (not stream)
        continue;
      count += stream->readBytes(buf, len);
    }
    return count;
  }

  //! Peek on first stream available in order
  int peek()
  {
    int ret = -1;
    for (size_t i = 0; i < nSlots; i++) {
      Stream* stream = this->_slots[i];
      if (not stream)
        continue;
      ret = stream->peek();
      if (ret >= 0)
        return ret;
    }
    return ret;
  }

  //! Total available bytes on all streams
  int available()
  {
    int total = 0;
    for (size_t i = 0; i < nSlots; i++) {
      Stream* stream = this->_slots[i];
      if (not stream)
        continue;
      const int av = stream->available();
      if (av > 0)
        total += av;
    }
    return total;
  }

  //! Write a byte to all streams
  size_t write(uint8_t b)
  {
    int total = 0;
    for (size_t i = 0; i < nSlots; i++) {
      Stream* stream = this->_slots[i];
      if (not stream)
        continue;
      const int written = stream->write(b);
      if (written > total)
        total = written;
    }
    return total;
  }

  template<typename TYPE>
  size_t write(TYPE b)
  {
    int total = 0;
    for (size_t i = 0; i < nSlots; i++) {
      Stream* stream = this->_slots[i];
      if (not stream)
        continue;
      const int written = stream->write(b);
      if (written > total)
        total = written;
    }
    return total;
  }

#if !defined(ESP8266)
  // For some reason, the ArduinoEPS8266 framework's Stream class doesn't
  // implement getTimeout()...
  //! Determine the maximum timeout of all available streams
  unsigned long getTimeout()
  {
    unsigned long timeout = 0;
    for (size_t i = 0; i < nSlots; i++) {
      Stream* stream = this->_slots[i];
      if (not stream)
        continue;
      const unsigned long streamTimeout = stream->getTimeout();
      if (streamTimeout > timeout)
        timeout = streamTimeout;
    }
    return timeout;
  }
#endif // #if ! defined(ESP8266)

  //! Set timeout of all available streams
  void setTimeout(const unsigned long timeout)
  {
    for (size_t i = 0; i < nSlots; i++) {
      Stream* stream = this->_slots[i];
      if (not stream)
        continue;
      stream->setTimeout(timeout);
    }
  }

  void flush()
  {
    for (size_t i = 0; i < nSlots; i++) {
      Stream* stream = this->_slots[i];
      if (not stream)
        continue;
      stream->flush();
    }
  }

protected:
  Stream* _slots[nSlots] = { nullptr };

  /**
   * @brief determine the next free slot index
   * @param index the next free index
   * @return whether a free slot was available
   */
  bool nextFreeSlot(uint8_t& index)
  {
    for (size_t i = 0; i < nSlots; i++) {
      if (not this->_slots[i]) {
        index = i;
        return true;
      }
    }
    return false;
  };
};
