#pragma once

#if ARDUINO_ARCH_AVR
#include <ArduinoJson.hpp>
using ARDUINOJSON_NAMESPACE::enable_if;
using ARDUINOJSON_NAMESPACE::is_floating_point;
using ARDUINOJSON_NAMESPACE::is_integral;
using ARDUINOJSON_NAMESPACE::is_same;
using ARDUINOJSON_NAMESPACE::is_signed;
using ARDUINOJSON_NAMESPACE::is_unsigned;
#else // #if ARDUINO_ARCH_AVR
#include <type_traits>
using std::enable_if;
using std::is_floating_point;
using std::is_integral;
using std::is_same;
using std::is_signed;
using std::is_unsigned;
#endif // #if ARDUINO_ARCH_AVR

#include <config/Defaults.hpp>
#include <utils/Logging.hpp>

#include <cfloat>
#include <cmath>

// Arduino.h defines min and max as macros which breaks ArduinoSTL
// for some reason we have to delete those macros HERE
#ifdef min
#undef min
#endif // #ifdef min
#ifdef max
#undef max
#endif // #ifdef min
#include <limits>

#define FNAN ((float)0.0F / (float)0.0F)

class Number
{
public:
  template<
    typename T,
    typename enable_if<is_integral<T>::value or is_floating_point<T>::value,
                       int>::type = 0>
  Number(const T value)
  {
    *this = value;
  }
  Number() { *this = 0U; }

  enum Type
  {
    SignedInteger,
    UnsignedInteger,
    FloatingPoint,
  } type;

  template<typename T,
           typename enable_if<is_integral<T>::value, int>::type = 0>
  bool storedLike(void) const
  {
    if (is_signed<T>::value)
      return this->type == Type::SignedInteger;
    if (is_unsigned<T>::value)
      return this->type == Type::UnsignedInteger;
    return false;
  }
  template<typename T,
           typename enable_if<is_floating_point<T>::value, int>::type = 0>
  bool storedLike(void) const
  {
    return this->type == Type::FloatingPoint;
  }

  /**
   * @brief Determine whether the underlying data fits into a specific
   * integer type
   */
  template<typename T,
           typename enable_if<is_integral<T>::value, int>::type = 0>
  bool fits() const
  {
#if ARDUINO_ARCH_AVR
    const auto lower = ARDUINOJSON_NAMESPACE::numeric_limits<T>::lowest();
    const auto upper = ARDUINOJSON_NAMESPACE::numeric_limits<T>::highest();
#else  // #if ARDUINO_ARCH_AVR
    const auto lower = std::numeric_limits<T>::min();
    const auto upper = std::numeric_limits<T>::max();
#endif // #if ARDUINO_ARCH_AVR
    // WHEN_NATIVE(printf("lower = %d, upper = %d\n", lower, upper));
    switch (this->type) {
      case Type::SignedInteger: {
        const auto value = this->memory.s;
        if (is_unsigned<T>::value) {
          if (value < 0)
            return false;
          return (uintmax_t)value <= (uintmax_t)upper;
        } else if (is_signed<T>::value)
          return (intmax_t)lower <= value and value <= (intmax_t)upper;
        else
          return false;
      }
      case Type::UnsignedInteger: {
        const auto value = this->memory.u;
        return value <= (uintmax_t)upper;
      }
      case Type::FloatingPoint: {
        const auto value = this->memory.f;
        if (is_signed<T>::value)
          return lower <= value and value <= upper;
        else if (is_unsigned<T>::value)
          return lower <= (double)value and (double) value <= upper;
        else
          return false;
      }
    }
    return false;
  }

  /**
   * @brief Determine whether the underlying data fits into a specific
   * floating point type
   */
  template<typename T,
           typename enable_if<is_floating_point<T>::value and
                                (is_same<T, float>::value or
                                 is_same<T, double>::value),
                              int>::type = 0>
  bool fits() const
  {
#if ARDUINO_ARCH_AVR
    double lower = 0, upper = 0;
    if (is_same<T, float>::value) {
      lower = -FLT_MAX;
      upper = FLT_MAX;
    }
    if (is_same<T, double>::value) {
      lower = -DBL_MAX;
      upper = DBL_MAX;
    }
#else  // #if ARDUINO_ARCH_AVR
    const T lower = -std::numeric_limits<T>::max();
    const T upper = std::numeric_limits<T>::max();
#endif // #if ARDUINO_ARCH_AVR
    switch (this->type) {
      case Type::SignedInteger: {
        const auto value = this->memory.s;
        return lower <= value and value <= upper;
      }
      case Type::UnsignedInteger: {
        const auto value = this->memory.u;
        return lower <= value and value <= upper;
      }
      case Type::FloatingPoint: {
        const auto value = this->memory.f;
        return lower <= value and value <= upper;
      }
    }
    return false;
  }

  /**
   * @brief Assignment operator for integral types
   */
  template<typename T,
           typename enable_if<is_integral<T>::value, int>::type = 0>
  Number& operator=(const T x)
  {
    if (is_signed<T>::value) {
      this->type = Type::SignedInteger;
      this->memory.s = x;
    }
    if (is_unsigned<T>::value) {
      this->type = Type::UnsignedInteger;
      this->memory.u = x;
    }
    return *this;
  }
  /**
   * @brief Assignment operator for floating point types
   */
  template<typename T,
           typename enable_if<is_floating_point<T>::value, int>::type = 0>
  Number& operator=(const T x)
  {
    this->type = Type::FloatingPoint;
    this->memory.f = x;
    return *this;
  }

  /**
   * @brief Converter function for integral types
   */
  template<
    typename T,
    typename enable_if<is_integral<T>::value or is_floating_point<T>::value,
                       int>::type = 0>
  bool dump(T& x) const
  {
    if (not this->fits<T>())
      return false;
    switch (this->type) {
      case Type::SignedInteger:
        x = this->memory.s;
        return true;
      case Type::UnsignedInteger:
        x = this->memory.u;
        return true;
      case Type::FloatingPoint:
        x = this->memory.f;
        return true;
    }
    return false;
  }

  /**
   * @brief Convert the underlying value to a specific integer type and fall
   * back to a defaulf value of 0 if the data doesn't fit.
   */
  template<typename T,
           typename enable_if<is_integral<T>::value, int>::type = 0>
  T as(void) const
  {
    T x = 0;
    this->dump(x);
    return x;
  }

  /**
   * @brief Convert the underlying value to a specific floating point type and
   * fall back to a defaulf value of NAN if the data doesn't fit.
   */
  template<typename T,
           typename enable_if<is_floating_point<T>::value, int>::type = 0>
  T as(void) const
  {
    T x =
#if ARDUINO_ARCH_AVR
      (T)0 / (T)0
#else  // #if NATIVE
      std::numeric_limits<T>::quiet_NaN()
#endif // #if NATIVE
      ;
    this->dump(x);
    return x;
  }

  char* toString(char* str, const size_t strSize) const
  {
    if (this->type == Type::SignedInteger) {
      long long xll = 0;
#if NATIVE
      if (this->dump(xll)) {
        snprintf(str, strSize, "%lld", xll);
        return str;
      }
#else  // #if NATIVE
      long xl = 0;
      if (this->dump(xl)) {
        snprintf(str, strSize, "%ld", xl);
        return str;
      }
      if (this->dump(xll)) {
        long long factor = 1;
        if (sizeof(long) == 4)
          factor = 1000000000ULL;
        else if (sizeof(long) == 8)
          factor = 1000000000000000000ULL;
        auto lower = (long)(xll % (long)factor);
        snprintf(str,
                 strSize,
                 "%ld%ld",
                 (long)(xll / factor),
                 lower < 0 ? lower * -1 : lower);
        return str;
      }
#endif // #if NATIVE
    }
    if (this->type == Type::UnsignedInteger) {
      unsigned long long xull = 0;
#if NATIVE
      if (this->dump(xull)) {
        snprintf(str, strSize, "%llu", xull);
        return str;
      }
#else  // #if NATIVE
      unsigned long xul = 0;
      if (this->dump(xul)) {
        snprintf(str, strSize, "%lu", xul);
        return str;
      }
      if (this->dump(xull)) {
        uintmax_t factor = 1;
        if (sizeof(unsigned long) == 4)
          factor = 1000000000ULL;
        else if (sizeof(unsigned long) == 8)
          factor = 1000000000000000000ULL;
        snprintf(str,
                 strSize,
                 "%lu%09lu",
                 (unsigned long)(xull / factor),
                 (unsigned long)(xull % factor));
        return str;
      }
#endif // #if NATIVE
    }
    double x = 0;
    if (this->dump(x))
      snprintf(
        str, strSize, this->type == Type::FloatingPoint ? "%g" : "~%f", x);
    else
      snprintf(str, strSize, "%s", "*too-large*");
    return str;
  }
  template<size_t strSize>
  char* toString(char (&str)[strSize]) const
  {
    return this->toString(str, strSize);
  }

  template<
    typename T,
    typename enable_if<is_integral<T>::value or is_floating_point<T>::value,
                       int>::type = 0>
  operator T(void) const
  {
    return this->as<T>();
  }

  template<typename T,
           typename enable_if<is_integral<T>::value, int>::type = 0>
  bool operator==(const T other) const
  {
    switch (this->type) {
      case Type::SignedInteger:
      case Type::UnsignedInteger: {
        T x = 0;
        if (this->dump(x))
          return x == other;
      }
      case Type::FloatingPoint:
        return this->memory.f == other;
    }
    return false;
  }

  template<typename T,
           typename enable_if<is_floating_point<T>::value, int>::type = 0>
  bool operator==(const T other) const
  {
    switch (this->type) {
      case Type::SignedInteger:
        return this->memory.s == other;
      case Type::UnsignedInteger:
        return this->memory.u == other;
      case Type::FloatingPoint:
        return this->memory.f == other;
    }
    return false;
  }

  template<
    typename T,
    typename enable_if<is_integral<T>::value or is_floating_point<T>::value,
                       int>::type = 0>
  bool operator!=(const T other) const
  {
    return not(*this == other);
  }

  template<typename T,
           typename enable_if<is_integral<T>::value, int>::type = 0>
  bool operator<(const T other) const
  {
    T x = 0;
    if (this->dump(x))
      return x < other;
    return false;
  }

  template<typename T,
           typename enable_if<is_floating_point<T>::value, int>::type = 0>
  bool operator<(const T other) const
  {
    switch (this->type) {
      case Type::SignedInteger:
        return this->memory.s < other;
      case Type::UnsignedInteger:
        return this->memory.u < other;
      case Type::FloatingPoint:
        return this->memory.f < other;
    }
    return false;
  }

  template<typename T,
           typename enable_if<is_integral<T>::value, int>::type = 0>
  bool operator>(const T other) const
  {
    T x = 0;
    if (this->dump(x))
      return x > other;
    return false;
  }

  template<typename T,
           typename enable_if<is_floating_point<T>::value, int>::type = 0>
  bool operator>(const T other) const
  {
    switch (this->type) {
      case Type::SignedInteger:
        return this->memory.s > other;
      case Type::UnsignedInteger:
        return this->memory.u > other;
      case Type::FloatingPoint:
        return this->memory.f > other;
    }
    return false;
  }

  template<
    typename T,
    typename enable_if<is_integral<T>::value or is_floating_point<T>::value,
                       int>::type = 0>
  bool operator<=(const T other) const
  {
    return not((*this) > other);
  }
  template<
    typename T,
    typename enable_if<is_integral<T>::value or is_floating_point<T>::value,
                       int>::type = 0>
  bool operator>=(const T other) const
  {
    return not((*this) < other);
  }

  union
  {
    intmax_t s;
    uintmax_t u;
    double f;
  } memory;
};

template<
  typename T,
  typename enable_if<is_integral<T>::value or is_floating_point<T>::value,
                     int>::type = 0>
bool
operator<(const T lhs, const Number& rhs)
{
  return rhs < lhs;
}

template<
  typename T,
  typename enable_if<is_integral<T>::value or is_floating_point<T>::value,
                     int>::type = 0>
bool
operator>(const T lhs, const Number& rhs)
{
  return rhs > lhs;
}

template<
  typename T,
  typename enable_if<is_integral<T>::value or is_floating_point<T>::value,
                     int>::type = 0>
bool
operator==(const T lhs, const Number& rhs)
{
  return rhs == lhs;
}

template<
  typename T,
  typename enable_if<is_integral<T>::value or is_floating_point<T>::value,
                     int>::type = 0>
bool
operator!=(const T lhs, const Number& rhs)
{
  return rhs != lhs;
}

template<
  typename T,
  typename enable_if<is_integral<T>::value or is_floating_point<T>::value,
                     int>::type = 0>
bool
operator<=(const T lhs, const Number& rhs)
{
  return not(rhs > lhs);
}

template<
  typename T,
  typename enable_if<is_integral<T>::value or is_floating_point<T>::value,
                     int>::type = 0>
bool
operator>=(const T lhs, const Number& rhs)
{
  return not(rhs < lhs);
}
