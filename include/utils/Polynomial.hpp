#pragma once

#include "Logging.hpp"

#include <vector>

class Polynomial
{
public:
  Polynomial(const size_t nCoefficients)
    : coefficients(nCoefficients)
  {}
  Polynomial(std::initializer_list<double> coefs)
    : coefficients(coefs)
  {}

  std::vector<double> coefficients;

  double operator()(const double x) const
  {
    LOG(General,
        Debug + 10,
        cat(F("Going to calculate polynomial of "),
            this->coefficients.size(),
            F(" coefs at "),
            x));
    double result = 0;
    size_t exponent = 0;
    for (const double coef : this->coefficients) {
      LOG(General,
          Debug + 10,
          cat(F("result = "),
              result,
              " + ",
              coef,
              " * pow(",
              x,
              ", ",
              (double)exponent,
              ")",
              " = ",
              result + coef * pow(x, (double)exponent)));
      result += coef * pow(x, (double)exponent++);
    }
    LOG(General, Debug + 10, cat(F("result = "), result));
    return result;
  }
};
