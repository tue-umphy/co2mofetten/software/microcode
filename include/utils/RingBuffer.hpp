#pragma once

/**
  @file RingBuffer.hpp
  @brief Simple Ring Buffer functionality
*/

/**
 *  @brief Interface multiple streams from a single one
 *  @details Snake-like order: (shift/unshift) TAIL ======> HEAD (push/pop)
 */
template<typename valueType, size_t bufferSize>
class RingBuffer
{
public:
  RingBuffer()
    : _head(0)
    , _tail(0)
    , _count(0)
  {}

  size_t getHead() { return this->_head; }
  size_t getTail() { return this->_tail; }

  //! Determine occupied size
  size_t count() { return this->_count; }

  void push(const valueType* value, const size_t valueSize)
  {
    for (size_t i = 0; i < valueSize; i++)
      this->push(value[i]);
  }

  template<size_t valueSize>
  void push(const valueType (&value)[valueSize])
  {
    this->push(value, valueSize);
  }

  //! Add value to head
  void push(const valueType value)
  {
    if (this->_count)
      this->_head = (this->_head + 1) % bufferSize;
    this->_buffer[this->_head] = value;
    if (this->_head == this->_tail and this->_count)
      this->_tail = (this->_tail + 1) % bufferSize;
    if (this->_count < bufferSize)
      this->_count++;
  }

  //! Return and drop value from head
  valueType pop()
  {
    valueType value = this->_buffer[this->_head];
    if (this->_count > 1)
      this->_head = (this->_head + bufferSize - 1) % bufferSize;
    if (this->_count > 0)
      this->_count--;
    return value;
  }

  //! Return value from head without dropping
  valueType popPeek() { return this->_buffer[this->_head]; }

  //! Add value to tail
  void unshift(const valueType value)
  {
    if (this->_count)
      this->_tail = (this->_tail + bufferSize - 1) % bufferSize;
    this->_buffer[this->_tail] = value;
    if (this->_head == this->_tail and this->_count)
      this->_head = (this->_head + bufferSize - 1) % bufferSize;
    if (this->_count < bufferSize)
      this->_count++;
  }

  //! Return and remove value from tail
  valueType shift()
  {
    valueType value = this->_buffer[this->_tail];
    if (this->_count > 1)
      this->_tail = (this->_tail + 1) % bufferSize;
    if (this->_count > 0)
      this->_count--;
    return value;
  }

  //! Return value from tail without dropping
  valueType shiftPeek() { return this->_buffer[this->_tail]; }

  //! Determine whether the buffer is completely filled
  bool isFull() { return this->_count == bufferSize; }

  //! Determine whether the buffer is empty
  bool isEmpty() { return this->_count == 0; }

  //! Empty the buffer
  void clear()
  {
    this->_head = 0;
    this->_tail = 0;
    this->_count = 0;
  }

  /**
   * @brief Call a function on the occupied buffer content
   * @details If the buffer is currently wrapped, only one part is processed
   */
  template<typename returnType>
  void shiftCall(returnType (*callback)(const valueType* payload,
                                        const size_t payloadLength))
  {
    if (this->_head >= this->_tail) {
      // process whole buffer
      callback(this->_buffer + this->_tail, this->_count);
      this->clear();
      return;
    } else {
      // only process buffer from tail to buffer end
      callback(this->_buffer + this->_tail, bufferSize - this->_tail);
      this->_tail = 0;
      return;
    }
  }

  valueType operator[](size_t index) const { return this->_buffer[index]; }

protected:
  size_t _head;
  size_t _tail;
  size_t _count;
  valueType _buffer[bufferSize];
};
