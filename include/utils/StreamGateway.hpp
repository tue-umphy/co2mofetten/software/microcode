#pragma once

/**
  @file StreamGateway.hpp
  @brief Generic buffered stream interface
*/

#include <Arduino.h>

#include "RingBuffer.hpp"

/**
 *  @brief Interface multiple streams from a single one
 */
template<size_t inBufferSize = 64, size_t outBufferSize = 64>
class StreamGateway : public Stream
{
public:
  //! typedefs for the output function
  typedef void (*writefunc_t)(const uint8_t* payload,
                              const size_t payloadSize);

  StreamGateway()
    : _writeFunc(nullptr)
  {}

  //! Set the output function
  void setWriteFunc(writefunc_t writeFunc) { this->_writeFunc = writeFunc; }

  //! Return and drop a byte from tail
  int read() { return this->_inBuffer.count() ? this->_inBuffer.shift() : -1; }

  //! Peek on first stream available in order
  int peek()
  {
    return this->_inBuffer.count() ? this->_inBuffer.shiftPeek() : -1;
  }

  //! Total available bytes on all streams
  int available() { return this->_inBuffer.count(); }

  //! Append a byte to head
  size_t write(uint8_t b)
  {
    if (this->_outBuffer.isFull())
      this->flush();
    this->_outBuffer.push(b);
    return 1;
  }

  void flush()
  {
    if (not this->_writeFunc)
      return;
    if (this->_outBuffer.count())
      this->_outBuffer.shiftCall(this->_writeFunc);
  }

  void queue(const uint8_t b) { this->_inBuffer.push(b); }

  void queue(const uint8_t* payload, const size_t payloadSize)
  {
    for (size_t i = 0; i < payloadSize; i++) {
      this->_inBuffer.push(payload[i]);
    }
  }

  template<size_t payloadSize>
  void queue(const uint8_t (&payload)[payloadSize])
  {
    this->queue(payload, payloadSize);
  }

  void queue(const char* payload, const size_t payloadSize)
  {
    this->queue((uint8_t*)payload, payloadSize);
  }

  void queue(const char* payload)
  {
    this->queue((uint8_t*)payload, strlen(payload));
  }

protected:
  RingBuffer<uint8_t, inBufferSize> _inBuffer;
  RingBuffer<uint8_t, outBufferSize> _outBuffer;

  writefunc_t _writeFunc;
};
