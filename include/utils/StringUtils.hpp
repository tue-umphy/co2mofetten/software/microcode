#pragma once

#include "../config/Native.hpp"

#define BYTE_TO_BINARY_PATTERN "%c%c%c%c%c%c%c%c"
#define BYTE_TO_BINARY(byte)                                                  \
  (byte & 0x80 ? '1' : '0'), (byte & 0x40 ? '1' : '0'),                       \
    (byte & 0x20 ? '1' : '0'), (byte & 0x10 ? '1' : '0'),                     \
    (byte & 0x08 ? '1' : '0'), (byte & 0x04 ? '1' : '0'),                     \
    (byte & 0x02 ? '1' : '0'), (byte & 0x01 ? '1' : '0')

#if ESP8266 || NATIVE
#include <cstring>
#endif // #if ESP8266

/**
  @file Utils.hpp
  @brief String Utilities
*/

//! reverse characters of a c-string in-place up to a given length
char*
strrev(char* string, const size_t len)
{
  for (size_t i = 0, j = len - 1; i < j; i++, j--) {
    const char c = string[i];
    string[i] = string[j];
    string[j] = c;
  }
  return string;
}

//! reverse a c-string in-place
char*
strrev(char* string)
{
  const size_t len = strlen(string);
  return strrev(string, len);
}

//! reverse a c-string reference in-place
template<size_t stringSize>
char*
strrev(char (&string)[stringSize])
{
  return strrev(string, stringSize);
}

//! reverse a const c-string to another string
char*
strrev(const char* inputString, char* outputString)
{
  const size_t len = strlen(inputString);
  for (size_t i = 0, j = len - 1; i < len; i++, j--)
    outputString[i] = inputString[j];
  outputString[len] = 0;
  return outputString;
}

bool
strBeginsWith(const char* str, const char* prefix)
{
  return (uintptr_t)strstr(str, prefix) == (uintptr_t)str;
}

/**
 * @brief Determine the amount of characters that can be appended (e.g. with
 * `strcat()`) to a given string
 */
size_t
strnfree(const char* str, const size_t strSize)
{
  const size_t len = strnlen(str, strSize);
  return (len >= strSize - 1) ? 0 : strSize - len - 1;
}

/**
 * @brief Determine the amount of characters that can be appended (e.g. with
 * `strcat()`) to a given string
 */
template<size_t strSize>
size_t
strfree(const char (&str)[strSize])
{
  return strnfree(str, strSize);
}

/**
 * @brief Convenience overload
 */
template<size_t strSize>
size_t
strnlen(const char (&str)[strSize])
{
  return strnlen(str, strSize);
}

//! like Combination of `snprintf` and `strncat`
template<typename... Args>
size_t
snprintfcat(char* str, const size_t strSize, Args... args)
{
  return snprintf(
    str + strnlen(str, strSize), strnfree(str, strSize), args...);
}

//! like Combination of `snprintf` and `strncat`
template<size_t strSize, typename... Args>
size_t
sprintfcat(char (&str)[strSize], Args... args)
{
  return snprintfcat(str, strSize, args...);
}

//! like `snprintf` but returns the string
template<typename... Args>
char*
snprintf2(char* buf, const size_t bufSize, Args... args)
{
  snprintf(buf, bufSize, args...);
  return buf;
}

//! like `sprintf` but returns the string
template<typename... Args>
char*
sprintf2(char* buf, Args... args)
{
  sprintf(buf, args...);
  return buf;
}

/**
 * @brief  Match a glob pattern against a string
 * @details Taken from the Linux kernel
 * (https://github.com/torvalds/linux/blob/master/lib/glob.c) with minor
 * adjustments.
 */
bool
linuxGlobMatch(char const* pat, char const* str)
{
  /*
   * Backtrack to previous * on mismatch and retry starting one
   * character later in the string.  Because * matches all characters
   * (no exception for /), it can be easily proved that there's
   * never a need to backtrack multiple levels.
   */
  char const *back_pat = NULL, *back_str = back_str;

  /*
   * Loop over each token (character or class) in pat, matching
   * it against the remaining unmatched tail of str.  Return false
   * on mismatch, or true after matching the trailing nul bytes.
   */
  for (;;) {
    unsigned char c = *str++;
    unsigned char d = *pat++;

    switch (d) {
      case '?': /* Wildcard: anything but nul */
        if (c == '\0')
          return false;
        break;
      case '*':           /* Any-length wildcard */
        if (*pat == '\0') /* Optimize trailing * case */
          return true;
        back_pat = pat;
        back_str = --str; /* Allow zero-length match */
        break;
      case '[': { /* Character class */
        bool match = false, inverted = (*pat == '!');
        char const* cls = pat + inverted;
        unsigned char a = *cls++;

        /*
         * Iterate over each span in the character cls.
         * A span is either a single character a, or a
         * range a-b.  The first span may begin with ']'.
         */
        do {
          unsigned char b = a;

          if (a == '\0') /* Malformed */
            goto literal;

          if (cls[0] == '-' && cls[1] != ']') {
            b = cls[1];

            if (b == '\0')
              goto literal;

            cls += 2;
            /* Any special action if a > b? */
          }
          match |= (a <= c && c <= b);
        } while ((a = *cls++) != ']');

        if (match == inverted)
          goto backtrack;
        pat = cls;
      } break;
      case '\\':
        d = *pat++;
        /*FALLTHROUGH*/
      default: /* Literal character */
      literal:
        if (c == d) {
          if (d == '\0')
            return true;
          break;
        }
      backtrack:
        if (c == '\0' || !back_pat)
          return false; /* No point continuing */
        /* Try again from last *, one character later in str. */
        pat = back_pat;
        str = ++back_str;
        break;
    }
  }
}
