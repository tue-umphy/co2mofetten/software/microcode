#pragma once

#include <unity.h>

#include "../config/Defaults.hpp"

#ifndef VERBOSE
#define VERBOSE false
#endif // #ifndef VERBOSE

#if VERBOSE
#define WHEN_VERBOSE(x) x
#else // #if VERBOSE
#define WHEN_VERBOSE(x)
#endif // #if VERBOSE

#define TEST_MESSAGE_(fmt, n, ...)                                            \
  {                                                                           \
    char tmp[n];                                                              \
  }

namespace Test {

template<size_t N, typename... Args>
void
message(const char* fmt, Args... args)
{
  char tmp[N];
  snprintf(tmp, N, fmt, args...);
  TEST_MESSAGE(tmp);
}

void
assertBool(const bool shouldBe, const bool is, const char* message = nullptr)
{
  if (shouldBe) {
    if (message) {
      TEST_ASSERT_TRUE_MESSAGE(is, message);
    } else {
      TEST_ASSERT_TRUE(is);
    }
  } else {
    if (message) {
      TEST_ASSERT_FALSE_MESSAGE(is, message);
    } else {
      TEST_ASSERT_FALSE(is);
    }
  }
}

} // namespace Test
