#pragma once

/**
  @file TimeUtils.hpp
  @brief Time Utilities
*/

#include <Time.h>
#include <TimeLib.h>

/**
  @brief Time Utilities
*/
namespace TimeUtils {

void
printDateTime(Print& output, time_t time = 0)
{
  if (not time)
    time = now();
  output.print(year(time));
  output.print("-");
  output.print(month(time));
  output.print("-");
  output.print(day(time));
  output.print(" ");
  output.print(hour(time));
  output.print(":");
  output.print(minute(time));
  output.print(":");
  output.print(second(time));
}

} // namespace TimeUtils
