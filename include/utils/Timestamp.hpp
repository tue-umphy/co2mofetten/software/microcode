#pragma once

#if NATIVE
#include <chrono>
#include <cstdio>
#else // #if NATIVE
#include <Time.h>
#endif // #if NATIVE

#include "Number.hpp"

struct Timestamp : public Number
{
  Timestamp(const unsigned long s, const unsigned int m = 0)
    : Number::Number((uintmax_t)s * 1000 + m)
  {}
  Timestamp() { this->refresh(); }
  void refresh(void) { *this = this->epochMillisecondsNow(); }
  static uintmax_t epochMillisecondsNow()
  {
    uintmax_t seconds = 0;
    unsigned int milliseconds = 0;
#ifdef _Time_h
#ifdef TIMELIB_ENABLE_MILLIS // from jamesmyatt's Pull Request
    uint32_t ms = 0;
    seconds = now(ms);
    milliseconds = ms; // (ms / 1000) * 1000;
#else  // #ifdef TIMELIB_ENABLE_MILLIS // from jamesmyatt's Pull Request
    seconds = now();
    milliseconds = 0;
#endif // #ifdef TIMELIB_ENABLE_MILLIS // from jamesmyatt's Pull Request
#else  // #ifdef _Time_h
#if NATIVE
    auto timeNow = std::chrono::system_clock::now();
    auto timeSinceEpoch = timeNow.time_since_epoch();
    auto secondsSinceEpoch =
      std::chrono::duration_cast<std::chrono::seconds>(timeSinceEpoch);
    seconds = secondsSinceEpoch.count();
    milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(
                     timeSinceEpoch - secondsSinceEpoch)
                     .count();
#else // #if NATIVE
#pragma warning("Timestamp can't determine time!")
#endif // #if NATIVE
#endif // #ifdef _Time_h
    ;
    return seconds * 1000 + milliseconds % 1000;
  }
  using Number::operator=;
  const uintmax_t epochMilliseconds(void) const
  {
    return this->as<uintmax_t>();
  }
  const uintmax_t epochSeconds(void) const
  {
    return this->epochMilliseconds() / 1000;
  }
  const unsigned int epochMillisecondsFraction(void) const
  {
    return this->epochMilliseconds() % 1000;
  }
};
