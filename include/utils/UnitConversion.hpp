#pragma once

/**
  @file Converter.hpp
  @brief Unit conversion
*/

#if ARDUINO_ARCH_AVR
#include <ArduinoSTL.h>
#endif // #if ARDUINO_ARCH_AVR

#include <algorithm>
#include <cmath>

#include "../config/Native.hpp"
#include "StringUtils.hpp"
#include "Utils.hpp"

#ifndef UNITCONVERSION_MAX_PATH_LENGTH
//! Maximum path length used in ConverterCollection.convert()
#define UNITCONVERSION_MAX_PATH_LENGTH 16
#endif // #ifndef UNITCONVERSION_MAX_PATH_LENGTH

/**
  @brief Unit conversion
*/
namespace UnitConversion {

typedef double (*converterCallback_t)(const double);

const char* siPrefixes[] = { "u", "m", "c", "d", "", "h", "k", "M", "G" };
const int16_t siExponents[] = { -6, -3, -2, -1, 0, 2, 3, 6, 9 };

/**
 * @brief Determine the decimal exponent for a conversion between two units
 * @param [out] exponent the decimal exponent which when raising 10 to the
 * power of this number yields the factor to multiply a value in the given unit
 * to obtain the value in the target unit
 * @returns whether an exponent could be determined
 */
bool
siConversionExponent(const char* givenUnit,
                     const char* targetUnit,
                     int16_t& exponent)
{
  char givenUnitRev[16], targetUnitRev[16];
  strrev(givenUnit, givenUnitRev);
  strrev(targetUnit, targetUnitRev);
#if NATIVE && VERBOSE
  printf("Given unit \"%s\" reversed is \"%s\", Target unit \"%s\" reversed "
         "is \"%s\"\n",
         givenUnit,
         givenUnitRev,
         targetUnit,
         targetUnitRev);
#endif // #if NATIVE && VERBOSE
  const size_t len = std::min(strlen(givenUnitRev), strlen(targetUnitRev));
  size_t nTrailing = 0;
  for (size_t i = 0; i < len; i++, nTrailing++) {
    if (givenUnitRev[nTrailing] != targetUnitRev[nTrailing])
      break;
  }
#if NATIVE && VERBOSE
  printf("\"%s\" and \"%s\" share %u trailing chars\n",
         givenUnit,
         targetUnit,
         nTrailing);
#endif // #if NATIVE && VERBOSE
  if (not nTrailing)
    return false;
  char givenUnitPrefix[16], targetUnitPrefix[16];
  strrev(givenUnitRev + nTrailing, givenUnitPrefix);
  strrev(targetUnitRev + nTrailing, targetUnitPrefix);
#if NATIVE && VERBOSE
  printf("Given unit \"%s\" has prefix \"%s\", Target unit \"%s\" has prefix "
         "\"%s\".\n",
         givenUnit,
         givenUnitPrefix,
         targetUnit,
         targetUnitPrefix);
#endif // #if NATIVE && VERBOSE
  const int givenUnitPrefixIndex =
    Utils::indexInArray(givenUnitPrefix, siPrefixes);
  if (givenUnitPrefixIndex < 0)
    return false;
  const int targetUnitPrefixIndex =
    Utils::indexInArray(targetUnitPrefix, siPrefixes);
  if (targetUnitPrefixIndex < 0)
    return false;
  const int givenUnitExponent = siExponents[(size_t)givenUnitPrefixIndex];
  const int targetUnitExponent = siExponents[(size_t)targetUnitPrefixIndex];
  exponent = givenUnitExponent - targetUnitExponent;
#if NATIVE && VERBOSE
  printf("Given  unit \"%s\" means exponent %d, "
         "target unit \"%s\" means exponent %d, "
         "meaning that 1 %s = 10^%d %s\n",
         givenUnit,
         givenUnitExponent,
         targetUnit,
         targetUnitExponent,
         givenUnit,
         exponent,
         targetUnit);
#endif // #if NATIVE && VERBOSE
  return true;
}

template<size_t nSlots>
class ConverterCollection
{
public:
  ConverterCollection()
    : _fromUnits{ nullptr }
    , _toUnits{ nullptr }
    , _converters{ nullptr }
  {}

  /**
   * @brief find a converter to convert between a given and a target unit
   * @returns the converter function, nullptr if none was found
   */
  converterCallback_t findConverter(const char* givenUnit,
                                    const char* targetUnit)
  {
    if (givenUnit == targetUnit or strcmp(givenUnit, targetUnit) == 0)
      return [](const double x) { return x; };
    for (size_t i = 0; i < nSlots; i++) {
      converterCallback_t converter = this->_converters[i];
      const char* fromUnit = this->_fromUnits[i];
      const char* toUnit = this->_toUnits[i];
      if (not(converter and toUnit and fromUnit))
        continue;
      if ((givenUnit == fromUnit or strcmp(givenUnit, fromUnit) == 0) and
          (targetUnit == toUnit or strcmp(targetUnit, toUnit) == 0)) {
        return converter;
      }
    }
    return nullptr;
  }

  /**
   * @brief Determine a path of conversions to convert a given unit to a target
   * unit
   * @todo Put the converter callbacks directly into the path?
   */
  bool convert(double& value,
               const char* givenUnit,
               const char* targetUnit,
               const char** path,
               const size_t pathLength,
               const size_t pathStep = 0)
  {
#if NATIVE && VERBOSE
    double valueBackup = value;
#endif // #if NATIVE && VERBOSE
    if (pathStep + 1 >= pathLength) {
#if NATIVE && VERBOSE
      for (size_t i = 0; i < pathStep; i++)
        printf(" ");
      printf("ERROR: Path is getting too long. Stopping.\n");
#endif // #if NATIVE && VERBOSE
      return false;
    }
    path[pathStep] = givenUnit;
#if NATIVE && VERBOSE
    for (size_t i = 0; i < pathStep; i++)
      printf(" ");
    printf("Searching a way to convert %f %s to \"%s\".\n",
           value,
           givenUnit,
           targetUnit);
    for (size_t i = 0; i < pathStep; i++)
      printf(" ");
    printf("Current path: ");
    for (size_t i = 0; i < pathLength; i++) {
      const char* pathElement = path[i];
      if (not pathElement)
        break;
      if (i)
        printf(" -> ");
      printf("%s", pathElement);
    }
    printf("\n");
#endif // #if NATIVE && VERBOSE
    // first search the given converters
    converterCallback_t directConverter =
      this->findConverter(givenUnit, targetUnit);
    if (directConverter) {
      path[pathStep + 1] = targetUnit;
#if NATIVE && VERBOSE
      valueBackup = value;
#endif // #if NATIVE && VERBOSE
      value = directConverter(value);
#if NATIVE && VERBOSE
      for (size_t i = 0; i < pathStep; i++)
        printf(" ");
      printf("Found direct converter from %f %s to %f %s. That's it!\n",
             valueBackup,
             givenUnit,
             value,
             targetUnit);
#endif // #if NATIVE && VERBOSE
      return true;
    }
    // if no given converter matches, try SI prefixes
    int16_t exponent;
    if (siConversionExponent(givenUnit, targetUnit, exponent)) {
      path[pathStep + 1] = targetUnit;
      const double factor = std::pow(10.0F, exponent);
#if NATIVE && VERBOSE
      valueBackup = value;
#endif // #if NATIVE && VERBOSE
      value = value * factor;
#if NATIVE && VERBOSE
      for (size_t i = 0; i < pathStep; i++)
        printf(" ");
      printf("Converted %f %s to %f %s by multiplying with %f due to SI "
             "prefixes.\n",
             valueBackup,
             givenUnit,
             value,
             targetUnit,
             factor);
#endif // #if NATIVE && VERBOSE
      return true;
    }
    // if neither a given converter, nor SI prefixes match directly, try all
    // given converters
    for (size_t i = 0; i < nSlots; i++) {
      double valueTmp = value;
      const char* fromUnit = this->_fromUnits[i];
      const char* toUnit = this->_toUnits[i];
      if (not(toUnit and fromUnit))
        continue;
      if (not(givenUnit == fromUnit or strcmp(givenUnit, fromUnit) == 0)) {
        // if this known converter's input is no exact match for our given
        // unit, at least try to convert our given unit to its input unit using
        // SI prefixes
        int16_t exponent;
        if (not siConversionExponent(givenUnit, fromUnit, exponent))
          continue;
#if NATIVE && VERBOSE
        valueBackup = valueTmp;
#endif // #if NATIVE && VERBOSE
        valueTmp *= std::pow(10.0F, exponent);
#if NATIVE && VERBOSE
        for (size_t i = 0; i < pathStep; i++)
          printf(" ");
        printf("We can use our \"%s\" to \"%s\" converter if we adjust our "
               "given input %f %s to %f %s based on the SI prefixes.\n",
               fromUnit,
               toUnit,
               valueBackup,
               givenUnit,
               valueTmp,
               fromUnit);
#endif // #if NATIVE && VERBOSE
      }
      // This given converter at least takes our given unit as input. Let's
      // give this a closer look
      converterCallback_t converter = this->_converters[i];
#if NATIVE && VERBOSE
      for (size_t i = 0; i < pathStep; i++)
        printf(" ");
      printf("We have a converter from \"%s\" to \"%s\". ", fromUnit, toUnit);
#endif // #if NATIVE && VERBOSE
       // stop if cyclic
      if (Utils::indexInArray(toUnit, path, pathStep) >= 0) {
#if NATIVE && VERBOSE
        printf("But \"%s\" is already in the path. No luck here...\n", toUnit);
#endif // #if NATIVE && VERBOSE
        continue;
      }
#if NATIVE && VERBOSE
      valueBackup = valueTmp;
#endif // #if NATIVE && VERBOSE
      const double valueConverted = converter(valueTmp);
#if NATIVE && VERBOSE
      printf(
        "Let's see if we can somehow convert %f %s (= %f %s) to \"%s\".\n",
        valueBackup,
        fromUnit,
        valueConverted,
        toUnit,
        targetUnit);
      valueBackup = value;
#endif // #if NATIVE && VERBOSE
       // RECURSION!
      double valueTmpTmp = valueConverted;
      bool converted = convert(
        valueTmpTmp, toUnit, targetUnit, path, pathLength, pathStep + 1);
      if (converted) {
        value = valueTmpTmp;
#if NATIVE && VERBOSE
        for (size_t i = 0; i < pathStep; i++)
          printf(" ");
        printf("Successfully converted %f %s to %f %s.\n",
               valueBackup,
               toUnit,
               value,
               targetUnit);
#endif // #if NATIVE && VERBOSE
        return true;
      }
    }
    path[pathStep] = nullptr;
    return false;
  }

  template<size_t pathLength>
  bool convert(double& value,
               const char* givenUnit,
               const char* targetUnit,
               const char* (&path)[pathLength],
               const size_t pathStep = 0)
  {
    return this->convert(value, givenUnit, targetUnit, path, pathLength);
  }

  bool convert(double& value, const char* givenUnit, const char* targetUnit)
  {
    if (not(givenUnit and targetUnit))
      return false;
    if (givenUnit == targetUnit or strcmp(givenUnit, targetUnit) == 0)
      return true;
    const char* path[UNITCONVERSION_MAX_PATH_LENGTH] = { nullptr };
#if NATIVE && VERBOSE
    const double valueBackup = value;
#endif // #if NATIVE && VERBOSE
    double valueTmp = value;
    const bool converted =
      this->convert(valueTmp, givenUnit, targetUnit, path);
    if (converted) {
      value = valueTmp;
#if NATIVE && VERBOSE
      printf("The conversion path from %f %s to %f %s was: ",
             valueBackup,
             givenUnit,
             value,
             targetUnit);
      for (size_t i = 0; i < Utils::arraySize(path); i++) {
        const char* nextConversionTo = path[i];
        if (not path[i])
          break;
        if (i)
          printf(" -> ");
        printf("%s", nextConversionTo);
      }
      printf("\n");
#endif // #if NATIVE && VERBOSE
      return true;
    }
#if NATIVE && VERBOSE
    printf(
      "Couldn't find a path from \"%s\" to \"%s\"!\n", givenUnit, targetUnit);
#endif // #if NATIVE && VERBOSE
    return false;
  }

  /**
   *  @brief Add a converter function
   *  @param fromUnit the unit to convert from
   *  @param toUnit the unit to convert to
   *  @param converter the function to queue
   *  @return whether the converter was successfully added
   */
  bool add(const char* fromUnit,
           const char* toUnit,
           converterCallback_t converter)
  {
    uint8_t freeSlot;
    if (not this->nextFreeSlot(freeSlot))
      return false;
    this->_fromUnits[freeSlot] = fromUnit;
    this->_toUnits[freeSlot] = toUnit;
    this->_converters[freeSlot] = converter;
#if NATIVE && VERBOSE
    this->printConverters();
#endif // #if NATIVE && VERBOSE
    return true;
  }

#if NATIVE && VERBOSE
  void printConverters()
  {
    for (size_t i = 0; i < nSlots; i++) {
      const char* fromUnit = this->_fromUnits[i];
      const char* toUnit = this->_toUnits[i];
      converterCallback_t converter = this->_converters[i];
      if (not(fromUnit and toUnit and converter))
        continue;
      printf("Converter Nr. %u at address %u converts "
             "from from \"%s\" to \"%s\"\n",
             i,
             (uintptr_t)converter,
             fromUnit,
             toUnit);
    }
  }
#endif // #if NATIVE && VERBOSE

protected:
  /**
   * @brief determine the next free slot index
   * @param index the index variable to change
   * @return whether a free slot was found
   */
  bool nextFreeSlot(uint8_t& index)
  {
    for (size_t i = 0; i < nSlots; i++) {
      if (not(this->_toUnits[i] or this->_fromUnits[i] or
              this->_converters[i])) {
        index = i;
        return true;
      }
    }
    return false;
  };

  const char* _fromUnits[nSlots];
  const char* _toUnits[nSlots];
  converterCallback_t _converters[nSlots];
};

} // namespace UnitConversion
