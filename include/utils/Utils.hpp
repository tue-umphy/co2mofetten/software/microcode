#pragma once

/**
  @file Utils.hpp
  @brief Utilities
*/

#include "../config/Native.hpp"

#if ESP8266 || NATIVE
// #include <cmath>
// #include <cstdlib>
#include <cstring>
#endif // #if ESP8266

#if ARDUINO
#include <Arduino.h>
#endif // #if ARDUINO

/**
  @brief Utilities
*/
namespace Utils {

template<size_t SIZE, class T>
inline size_t
arraySize(T (&arr)[SIZE])
{
  return SIZE;
}

template<typename T>
inline size_t
sizeOf(T x)
{
  return sizeof(x);
}

template<typename TYPE>
int
indexInArray(TYPE value, TYPE* array, const size_t arraySize)
{
  for (size_t i = 0; i < arraySize; i++) {
    TYPE arrayValue = array[i];
    if (value == arrayValue)
      return i;
  }
  return -1;
}

template<typename TYPE, size_t arraySize>
int
indexInArray(TYPE value, TYPE (&array)[arraySize])
{
  return indexInArray(value, array, arraySize);
}

int
indexInArray(const char* string, const char** array, const size_t arraySize)
{
  for (size_t i = 0; i < arraySize; i++) {
    const char* arrayString = array[i];
    if (string == arrayString or strcmp(string, arrayString) == 0)
      return i;
  }
  return -1;
}

template<size_t arraySize>
int
indexInArray(const char* string, const char* (&array)[arraySize])
{
  return indexInArray(string, array, arraySize);
}

const float
round_places(const float number, const size_t places)
{
  const unsigned int factor = pow(10, places);
  return round(number * factor) / factor;
}

//! variable holding the start of the stack. Set by initStackWatch() and used
// by stackSize()
char* stackStart = 0;

/**
 *  @brief Initialize watching the stack size
 *  @details This function needs to be called as soon as possible in the code
 *  (i.e. as the very first statement in setup()) to make stackSize() return
 *  sensible values.
 */
void
initStackWatch(void)
{
  char tmp;
  stackStart = &tmp;
}

ptrdiff_t
stackSize(void)
{
  char tmp;
  return stackStart - &tmp;
}

/**
 * @brief Determine the largest possible call to `malloc()`
 * @details Using a binary search to optimize performance. Results may be
 * inaccurate by a couple of bytes. The return type should actually be
 * something like `uintptr_t` or `ptrdiff_t`, but it only works on Arduinos and
 * ESP8266 with a 16-bit integer.
 */
uint16_t
largestMalloc(void)
{
  uint16_t lower = 1;
  uint16_t upper = ~(uint16_t)0;
  bool ok = false;
  void* buf;
  while (true) {
    uint16_t center = lower + (upper - lower) / 2;
    ok = (buf = malloc(center));
    free(buf);
    if (lower == center)
      return ok ? center : 0;
    if (buf)
      lower = center;
    else
      upper = center;
  }
  return 0;
}

inline float
heapFragmentation(const size_t maxFreeBlock, const size_t freeHeap)
{
  return 1.0 - (float)maxFreeBlock / (float)freeHeap;
}

#if ESP8266
inline float
heapFragmentation()
{
  return 1.0 - (float)ESP.getMaxFreeBlockSize() / (float)ESP.getFreeHeap();
}
#endif //  #if ESP8266

inline bool
isNan(const float f)
{
  return f != f;
}

inline bool
notNan(const float f)
{
  return f == f;
}

inline bool
pressurehPaOkay(const float p)
{
  return notNan(p) and 800 <= p and p <= 1200;
}

inline bool
temperatureCelsiusOkay(const float t)
{
  return notNan(t) and -50 <= t and t <= 60;
}

inline bool
humidityPercentOkay(const float h)
{
  return notNan(h) and 0 <= h and h <= 100;
}

inline bool
co2ppmOkay(const float c)
{
  return notNan(c) and 0 < c and c < 1000000;
}

} // namespace Utils
