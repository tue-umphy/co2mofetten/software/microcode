import shlex
import json
import warnings
import re

Import("projenv")

cppstring = projenv["_cppstring"]


def force_two_elements(x):
    try:
        it = iter((x,) if isinstance(x, str) else x)
    except TypeError:
        warnings.warn("Strange CPPDEFINES value: {}".format(repr(x)))
        return (str(x), True)
    first = next(it, str(x))
    second = next(it, True)
    rest = tuple(it)
    if rest:
        warnings.warn(
            "Too many CPPDEFINES values for {}: {}".format(first, rest)
        )
    return (first, second)


projenv.Append(
    CPPDEFINES=(
        "BUILD_FLAGS",
        shlex.quote(
            cppstring(
                json.dumps(
                    {
                        k: (
                            re.sub(r"\s+", "_", v) if isinstance(v, str) else v
                        )
                        for k, v in map(
                            force_two_elements, projenv["CPPDEFINES"],
                        )
                    },
                    separators=(",", ":"),
                )
            )
        ),
    )
)
