import subprocess
import shlex
import datetime
import os
import json

Import("env")


def cppstring(s):
    return json.dumps(s, separators=(",", ":"))


env["_cppstring"] = cppstring


git_revision = (
    subprocess.check_output(["git", "rev-parse", "--short", "HEAD"])
    .decode()
    .strip()
)

try:
    subprocess.check_call(["git", "diff", "--quiet"])
    git_dirty = ""
except subprocess.CalledProcessError:
    git_dirty = "-dirty"

env.Append(
    CPPDEFINES={"GIT_REV": shlex.quote(cppstring(git_revision + git_dirty))}
)

hostname = subprocess.check_output(["hostname"]).decode().strip()

env.Append(
    CPPDEFINES={
        "SHELL_USER": shlex.quote(cppstring(os.environ.get("USER", "?")))
    }
)
env.Append(CPPDEFINES={"SHELL_HOST": shlex.quote(cppstring(hostname))})

wifi_networks_json_environ = os.environ.get("DEFAULT_NETWORKS_JSON")
if wifi_networks_json_environ:
    if os.path.exists(wifi_networks_json_environ):
        with open(wifi_networks_json_environ) as fh:
            try:
                wifi_networks_json = json.load(fh)
            except BaseException as e:
                print(
                    "Could not parse file {} "
                    "(from environment variable DEFAULT_NETWORKS_JSON) "
                    "value as JSON: {}".format(
                        repr(wifi_networks_json_environ), e
                    )
                )
                Exit(1)
    else:
        try:
            wifi_networks_json = json.loads(wifi_networks_json_environ)
        except BaseException as e:
            print(
                "Could not parse environment variable DEFAULT_NETWORKS_JSON"
                " value (»»{}««) as JSON: {}".format(
                    repr(wifi_networks_json_environ), e
                )
            )
            Exit(1)
    env.Append(
        CPPDEFINES={
            "DEFAULT_NETWORKS_JSON": shlex.quote(
                cppstring(
                    json.dumps(wifi_networks_json, separators=(",", ":"))
                )
            )
        }
    )

# Note:
#
# This flag varies with every run. This causes SCons to rebuild EACH AND
# EVERY source code file, making a build terribly slow. The benefit of (easily)
# knowing the compilation timestamp is not that significant, so we'll just
# ditch it for now. There is still the __DATE__ and __TIME__ macros provided by
# gcc itself, though in an inconvenient human-readable format that we could
# parse...
#
# env.Append(
#     CPPDEFINES={
#         "COMPILATION_TIMESTAMP_UTC": int(
#             datetime.datetime.utcnow().strftime("%s")
#         )
#     }
# )
