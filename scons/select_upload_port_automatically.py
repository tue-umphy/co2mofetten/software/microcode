import operator
import warnings
from functools import partial
from serial.tools import list_ports
import re

Import("env")


def port_fulfills_condition(port, condition):
    # print(
    #     "Checking condition {} against port {}".format(repr(condition), port)
    # )
    if hasattr(condition, "search"):
        return any(
            map(
                condition.search,
                filter(
                    bool,
                    map(
                        lambda a: getattr(port, a, None),
                        ("description", "manufacturer", "product"),
                    ),
                ),
            )
        )
    if isinstance(condition, list):
        return any(map(partial(port_fulfills_condition, port), condition))
    if hasattr(condition, "items"):
        return all(
            (v.search if hasattr(v, "search") else lambda x: x == v)(
                getattr(port, k, "")
            )
            for k, v in condition.items()
        )
    try:
        iterable = iter(condition)
    except:
        iterable = False
    if iterable:
        return all(map(partial(port_fulfills_condition, port), condition))
    else:
        warnings.warn(
            "Don't know how to check condition »{}« against port {}".format(
                repr(condition), port
            )
        )
    return False


def selector(*conditions):
    """
    Return a function that when called returns the first serial device object
    that matches a given set of conditions. Internally,
    :any:`port_fulfills_condition` is used. If multiple conditions are given as
    arguments, all conditions must be met for a port to be considered as
    matching. Conditions can be nested. Iterable types are considered as groups
    of conditions to be **all** matched, however a :any:`list` of conditions
    means that **any** of these conditions may match for a port to be
    considered matching. A :any:`re.compile`d regular expression object is
    matched against all string properties of the port object , ``(i.e.
    ``description``, ``product``, ``manufacturer``). A :any:`dict`'s keys are
    interpreted as attributes of the port object and compared against the value
    (also matching :any:`re.compile`d regular expressions)
    """

    def wrapper():
        return filter(
            lambda p: port_fulfills_condition(p, conditions),
            list_ports.comports(),
        )

    return wrapper


board_selectors = {
    "megaatmega2560":
    # original Arduino Megas provide a text in their metadata
    selector(
        [
            re.compile(r"arduino.*mega", re.IGNORECASE),
            {"pid": 0x0042, "vid": 0x2341},
        ]
    ),
    "nodemcu": selector(
        {"vid": 0x1A86, "pid": 0x7523},  # the standard FTDI USB interface...
    ),
    "d1_mini": selector(
        [
            re.compile(
                r"esp(8266|ressif)", re.IGNORECASE
            ),  # either an ESP-like string matches
            {
                "vid": 0x1A86,
                "pid": 0x7523,
            },  # or the AzDelivery D1 Mini USB signature
            {
                "vid": 0x10C4,
                "pid": 0xEA60,
            },  # or the AzDelivery D1 Mini Pro USB signature
        ]
    ),
}


def select_upload_port_automatically(source, target, env):
    if not env.get("UPLOAD_PORT"):
        board = env["BOARD"]
        board_selector = board_selectors.get(board)
        if board_selector:
            matching_ports = tuple(board_selector())
            if len(matching_ports) == 1:
                upload_port = next(iter(matching_ports)).device
                print(
                    "Choosing {} automatically as upload port".format(
                        upload_port
                    )
                )
                env["UPLOAD_PORT"] = upload_port
            elif len(matching_ports) > 1:
                warnings.warn(
                    "Don't know which of the {} matching ports "
                    "to choose as upload port for {}-board: {}".format(
                        len(matching_ports),
                        board,
                        ", ".join(
                            map(operator.attrgetter("device"), matching_ports)
                        ),
                    )
                )
            else:
                warnings.warn(
                    "Didn't find any matching port for {}-board".format(board)
                )
        else:
            warnings.warn(
                "Don't know how to distinguish "
                "{}-board port from others".format(board)
            )
    else:
        print(
            "An UPLOAD_PORT was already specified ({}). "
            "No need to determine one automatically".format(
                env.get("UPLOAD_PORT")
            )
        )


env.AddPreAction("upload", select_upload_port_automatically)
env.AddPreAction("uploadfs", select_upload_port_automatically)
