#!/bin/sh
### This script can be executed on the CENTRAL STATION in the field to update the configs in this repo from the actual stations
scriptlocation="`dirname "$0"`"
gitroot="`cd $scriptlocation;git rev-parse --show-toplevel`"
if test -z "$gitroot";then
    echo "Where am I? Not in the microcode repository?"
    exit 1
fi

if test -z "$@";then
    stations=$(timeout -sINT --preserve-status 1 avahi-browse -a | cut -d' ' -f5 | sort -u)
    echo "Will query all stations stations: $stations"
else
    stations=$@
fi

for station in $stations;do
    echo "station: $station"
    configdir="$gitroot/configs/$station"
    (set -x;mkdir -p "$configdir")
    function sendcmd() { 
        echo $@ | sed 's|^| INPUT>>> |g' >&2
        output="`echo $@ | mqtt-cli "$station" | tail -n+2 | grep -v 'device>' | grep -v '^\s*$' | tr -d '\r'`"
        echo "$output" | sed 's|^|OUTPUT>>> |g' >&2
        echo "$output"
    }
    sendcmd -e "\r\n" # one empty command
    fsfiles=`sendcmd fs ls`
    echo files: $fsfiles
    for filename in $fsfiles;do
        sendcmd fs cat "$filename" | (jq -r 2>/dev/null || true) | (set -x;tee "$configdir/$filename")
    done
done
