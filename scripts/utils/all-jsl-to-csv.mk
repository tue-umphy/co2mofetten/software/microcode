#!/usr/bin/make -f
thisMakefile = $(realpath $(lastword $(MAKEFILE_LIST)))
thisMakefileDir = $(dir $(thisMakefile))

jslParser = $(thisMakefileDir)/parse-jsonlines.py

jslFiles = $(wildcard *.jsl *.JSL)

.PHONY: all
all: $(patsubst %.jsl,%.csv,$(patsubst %.JSL,%.jsl,$(jslFiles)))

%.csv: %.jsl
	$(jslParser) -i $< -o $@

%.csv: %.JSL
	$(jslParser) -i $< -o $@
