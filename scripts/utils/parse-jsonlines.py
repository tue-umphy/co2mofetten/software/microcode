#!/usr/bin/env python3
import argparse
import sys
import itertools
import json
import logging
import csv

parser = argparse.ArgumentParser(
    description="Parse jsonlines format and output CSV",
    epilog="The header is written out each time new keys are introduced.",
)
parser.add_argument(
    "-v", "--verbose", help="Verbose output", action="store_true"
)
parser.add_argument("-q", "--quiet", help="less output", action="store_true")
parser.add_argument(
    "-i",
    "--input",
    type=argparse.FileType("r"),
    default=sys.stdin,
    help="input file",
)
parser.add_argument(
    "-o",
    "--output",
    type=argparse.FileType("w"),
    default=sys.stdout,
    help="output file",
)
args = parser.parse_args()

logging.basicConfig(
    level={-1: logging.ERROR, 0: logging.INFO, 1: logging.DEBUG}[
        args.verbose - args.quiet
    ]
)

logger = logging.getLogger(__name__)

writer = csv.DictWriter(args.output, fieldnames=tuple())

def to_iterable(x):
    return (x,) if isinstance(x, str) or not hasattr(x, "__iter__") else x

def listdict_to_dictlist(d):
    for t in itertools.zip_longest(*map(to_iterable, d.values())):
        yield dict(zip(d, t))

def unicode_lines(f):
    while True:
        try:
            yield next(f)
        except StopIteration:
            break
        except UnicodeDecodeError:
            continue

for n, line in enumerate(unicode_lines(args.input)):
    try:
        data = json.loads(line)
    except json.JSONDecodeError as e:
        logger.warning(
            "Error interpreting line {n} as JSON: "
            "{error}\n>>>\n{line}>>>\n".format(n=n+1, line=line, error=e)
        )
        continue
    logger.debug("read data from file: {}".format(data))
    for i, row in enumerate(listdict_to_dictlist(data)):
        logger.debug("Row Nr. {}: {}".format(i, row))
        fieldnames = set(writer.fieldnames)
        new_fieldnames = set(data) - set(writer.fieldnames)
        if new_fieldnames:
            logger.info(
                "Input line {}: Adding fieldnames {}".format(n+1, new_fieldnames)
            )
            for d in data:
                fieldnames.add(d)
            writer.fieldnames = sorted(fieldnames)
            args.output.write(
                "# new fieldnames: {}\n".format(
                    ", ".join(map(repr, new_fieldnames))
                )
            )
            writer.writeheader()
        writer.writerow(row)
