#!/usr/bin/expect --
# Launch PlatformIO Device Monitor with cli arguments
spawn -noecho pio device monitor {*}$argv
# Hide communication
log_user 0
# Send the current UNIX time if requested
expect -re "time:\s*" { send [timestamp -gmt] }
# Show communication
log_user 1
# Run the rest interactively
interact
