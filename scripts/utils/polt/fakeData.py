#!/usr/bin/env python3

import json
import random
import time
import sys

box_names = ("central", "station1", "station2")

while True:
    # n = random.randint(1, 10)
    n = 1
    d = {
        "box": random.choice(box_names),
        "bme_temp_c": tuple(random.random() for i in range(n)),
        "scd_co2_ppm": tuple(random.random() for i in range(n)),
    }
    sys.stdout.write(json.dumps(d))
    sys.stdout.write("\r\n")
    sys.stdout.flush()
    time.sleep(0.1)
