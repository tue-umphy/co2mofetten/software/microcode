#!/usr/bin/env python3
# system modules
import json
import logging

# external modules
import polt
from polt.parser.csvparser import CsvParser

logger = logging.getLogger(__name__)


class JsonLinesParser(CsvParser):
    # This is required for some reason
    HEADER_REGEX_KEY_QUANTITY_UNIT = CsvParser.HEADER_REGEX_KEY_QUANTITY_UNIT

    @property
    def data(self):
        for line in self.f:
            try:
                d = json.loads(line)
            except json.JSONDecodeError:
                continue
            box = str(next(iter(d.get("box", [])),"unknown"))
            yield {
                self.parse_header(box + str(k)): self.str2num(v)
                for k, v in d.items()
                if not k in self.not_columns
            }
