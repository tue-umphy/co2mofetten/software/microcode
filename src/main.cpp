/** @file src.ino
  @brief Main source code file
  @details
    This file contains routines managing the high-level workflow.
*/

#include <Arduino.h>

#include "config/Config.hpp"
#include "utils/Logging.hpp"

#include "config/Globals.hpp"

#include "features/Cli.hpp"
#include "features/Hardware.hpp"
#include "features/Mqtt.hpp"
#include "features/OTA.hpp"
#include "features/Scheduler.hpp"
#include "features/WatchDog.hpp"
#include "features/WiFi.hpp"

#include <ArduinoJson.h>

/**
  @brief Main task scheduler object
*/
Scheduler::Scheduler<20> scheduler;

/**
  @brief Initially Set up Resources
  @details
    This function is called once at startup. According to the compiler flags
    (@ref ConfigurationFlags) it initializes the following components:
    - Automatic emergency resets (#WatchDog) if #WITH_WATCHDOG is true
    - set up #IDLE_BLINK_PIN if #BLINK_WHEN_IDLE is true
    - EnergyLogging::energyLoggingInit() if #WITH_ENERGYLOGGER is true
    - I²C interface
    - Serial interface if #WITH_SERIAL is true
    - Cli::setupCli() if #WITH_CLI is true
    - Gps::initGps() if #WITH_GPS is true
    - Scheduler::Scheduler::add()s these routines:
      - #WatchDog disabling during pause if #WITH_WATCHDOG is true
      - Cli shell_task() if #WITH_CLI is true
    - Scheduler::Scheduler::repeat()s these routines:
      - performing measurements
      - Gps::parseGps() if #WITH_GPS is true
      - EnergyLogging::energyLogging() if #WITH_ENERGYLOGGER is true

*/
void
setup(void)
{
  Utils::initStackWatch();
#if WITH_WATCHDOG
  WatchDog.disable();
#endif // #if WITH_WATCHDOG
  Wire.begin();
#if BLINK_WHEN_IDLE
  pinMode(IDLE_BLINK_PIN, OUTPUT);
#endif // #if BLINK_WHEN_IDLE
#if WITH_ENERGYLOGGER
  EnergyLogging::energyLoggingInit();
#endif // #if WITH_ENERGYLOGGER
#if WITH_SERIAL
  Serial.begin(SERIAL_BAUDRATE);
  if (not Config::config.read()) {
    LOG(System, Error, cat(F("Couldn't read configuration")));
  }
#if WITH_CLI
  Cli::setupCli();
#endif // #if WITH_CLI
#endif // #if WITH_SERIAL
#if WITH_GPS
  Gps::initGps();
#endif // #if WITH_GPS
#if BLINK_WHEN_IDLE
  scheduler.repeat(IDLE_BLINK_INTERVAL_MS, []() {
    digitalWrite(IDLE_BLINK_PIN, not digitalRead(IDLE_BLINK_PIN));
  });
#endif // #if BLINK_WHEN_IDLE
#if WITH_CLI
  scheduler.add([]() { Cli::scheduler.loop(); });
#endif // #if WITH_CLI
#if WITH_ENERGYLOGGER
  scheduler.repeat(MEASUREMENT_INTERVAL_MS,
                   []() { EnergyLogging::EnergyLogger.log_energy(); });
#endif // #if WITH_ENERGYLOGGER
#if WITH_WIFI
  WiFiNetwork::setup();
  scheduler.add([]() { WiFiNetwork::scheduler.loop(); });
#if WITH_OTA
  OTA::initOTA();
  scheduler.add([]() { OTA::handleOTA(); });
#endif // #if WITH_OTA
#endif // #if WITH_WIFI
  Hardware::setup();
  scheduler.add([]() {
    if (not Globals::paused)
      Hardware::scheduler.loop();
  });
#if WITH_ENERGYLOGGER
  scheduler.repeat(MEASUREMENT_INTERVAL_MS, []() {
    float supplyVoltageV = FNAN;
    EnergyLogging::energyLogging(supplyVoltageV);
    // JsonArray supplyVoltageArray =
    //   sensorjson.createNestedArray("ina219_voltage_v");
    // supplyVoltageArray.add(supplyVoltageV);
  });
#endif // #if WITH_ENERGYLOGGER
#if WITH_GPS
  scheduler.repeat(MEASUREMENT_INTERVAL_MS, []() {
    JsonVariant fakeJson;
    Gps::parseGps(fakeJson);
  });
#endif // #if WITH_GPS
#if WITH_WATCHDOG
  const Scheduler::msInterval_t maxInterval = scheduler.maxInterval();
  //! Making sure the WatchDog is always on
  scheduler.repeat(maxInterval > 5e3 ? maxInterval : 5e3, []() {
    if (not Globals::paused) {
      const Scheduler::msInterval_t maxInterval = scheduler.maxInterval();
      WatchDog.device_reset_after_ms((maxInterval > 5e3 ? maxInterval : 5e3) *
                                     5);
    }
  });
  //! Disabling the WatchDog during pause
  scheduler.add([]() {
    if (Globals::paused != Globals::previouslyPaused) {
      if (Globals::paused) {
        LOG(System, Info, cat(F("Disabling watchdog during pause")));
        WatchDog.disable();
      } else {
        LOG(System, Info, cat(F("Reenabling watchdog after pause")));
        WATCHDOG_ENABLE();
      }
      Globals::previouslyPaused = Globals::paused;
    }
  });
#endif // #if WITH_WATCHDOG // #if WITH_WATCHDOG
}

//!  @brief Perform scheduled tasks
void
loop()
{
  scheduler.loop();
}
