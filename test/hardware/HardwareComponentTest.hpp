#include <iostream>

#include "hardware/Hardware.hpp"
#include "utils/Test.hpp"
#include "utils/Utils.hpp"

#include <unity.h>

#if ARDUINO
#include <Time.h>
#else // #if ARDUINO
#include <chrono>
#include <thread>
#endif // #if ARDUINO

namespace HardwareComponentTest {

using namespace Hardware;

bool
isEqual(Component* component1, Component* component2)
{
  const bool equal = component1 == component2;
  WHEN_VERBOSE(Test::message<100>("Component* at %#X %s Component* at %#X",
                                  (uintptr_t)component1,
                                  equal ? "equals" : "differs from",
                                  (uintptr_t)component2));
  return equal;
}

bool
isEqual(Component& component1, Component& component2)
{
  return isEqual(&component1, &component2);
}

bool
isEqual(Component* component1, Component& component2)
{
  return isEqual(component1, &component2);
}

bool
isEqual(Component& component1, Component* component2)
{
  return isEqual(&component1, component2);
}

void
testComponentParent()
{
  GenericComponent component1;
  WHEN_VERBOSE(Test::message<100>("component1 (%#X)", (uintptr_t)&component1));
  TEST_ASSERT(isEqual(component1.getParent(), nullptr));
  TEST_ASSERT(isEqual(component1.getRootParent(), nullptr));
  GenericComponent component2(component1);
  WHEN_VERBOSE(Test::message<100>("component2 (%#X)", (uintptr_t)&component2));
  TEST_ASSERT(isEqual(component2.getParent(), component1));
  TEST_ASSERT(isEqual(component2.getRootParent(), component1));
  GenericComponent component3;
  WHEN_VERBOSE(Test::message<100>("component3 (%#X)", (uintptr_t)&component3));
  component3.setParent(component2);
  TEST_ASSERT(isEqual(component3.getParent(), component2));
  TEST_ASSERT(isEqual(component3.getRootParent(), component1));
  GenericComponent* component4 = new GenericComponent(&component2);
  WHEN_VERBOSE(Test::message<100>("component4 (%#X)", (uintptr_t)component4));
  TEST_ASSERT(isEqual(component4->getParent(), component2));
  TEST_ASSERT(isEqual(component4->getRootParent(), component1));
  // Components
  Components components1(3, component1);
  WHEN_VERBOSE(
    Test::message<100>("components1 (%#X)", (uintptr_t)&component1));
  TEST_ASSERT(isEqual(components1.getParent(), component1));
  TEST_ASSERT(isEqual(components1.getRootParent(), component1));
  components1.add(component2);
  TEST_ASSERT(isEqual(component2.getParent(), components1));
  Components components2(
    { &component1, &components1, &component3, &component3 });
  TEST_ASSERT_EQUAL(4, components2.size());
  for (Component* component : components2) {
    TEST_ASSERT_NOT_NULL(component);
    TEST_ASSERT_EQUAL(component->getParent(), &components2);
  }
}

void
testToString()
{
  GenericComponent component1;
  char tmp[100] = { 0 };
  TEST_ASSERT(strnlen(component1.toString(tmp), Utils::arraySize(tmp)) > 0);
  TEST_ASSERT(strnlen(tmp, Utils::arraySize(tmp)) > 0);
  WHEN_VERBOSE(Test::message<100>("%s", tmp));
  Components components1;
  TEST_ASSERT(strnlen(components1.toString(tmp), Utils::arraySize(tmp)) > 0);
  TEST_ASSERT(strnlen(tmp, Utils::arraySize(tmp)) > 0);
  WHEN_VERBOSE(Test::message<100>("%s", tmp));
}

void
testTraverse()
{
  GenericComponent component1;
  GenericComponent component2;
  Components components;
  components.add(component1);
  components.add(component2);
  WHEN_VERBOSE(Test::message<100>(
    "components by intitializer list: size() = %u, capacity()) = %u, "
    "max_size() = %u",
    components.size(),
    components.capacity(),
    components.max_size()));
  GenericComponent component3;
  GenericComponent component4;
  Components components2({ &component3, &component4 });
  components2.add(component3);
  components2.add(component4);
  WHEN_VERBOSE(Test::message<100>(
    "components plain: size() = %u, capacity() = %u, max_size()) = %u",
    components2.size(),
    components2.capacity(),
    components2.max_size()));
  WHEN_VERBOSE(Test::message<100>(
    "components plain after adding: size() = %u, capacity()) = %u, "
    "max_size() = %u",
    components2.size(),
    components2.capacity(),
    components2.max_size()));
  components.push_back(nullptr);
  components.push_back(&components2);
  GenericComponent component5;
  components.push_back(&component5);
#ifdef TwoWire_h
  I2C::GenericComponent i2cComponent1();
  I2C::GenericComponent i2cComponent2();
#if WITH_I2C_MULTIPLEXER
  I2C::TCA9548A i2cMultiplexer;
  i2cMultiplexer.ports[3] = new I2C::Components();
  i2cMultiplexer.ports[3]->push_back(new I2C::GenericComponent());
  i2cMultiplexer.ports[7] = new I2C::Components();
  i2cMultiplexer.ports[7]->push_back(new I2C::GenericComponent());
  i2cMultiplexer.ports[7]->push_back(new I2C::GenericComponent());
  components.push_back(&i2cMultiplexer);
#endif // #if WITH_I2C_MULTIPLEXER
#endif // #ifdef TwoWire_h
  components.traverse([](Component& component,
                         Component::TraverseContext& context,
                         void* extra) {
    char str[256] = { 0 };
    component.toString(str);
    TEST_ASSERT(strnlen(str, Utils::arraySize(str)) > 0);
#if ARDUINO
#if VERBOSE
    // I really don't know why %*s stopped working in snprintf, but that's
    // how it is...
    char indent[100] = { 0 };
    for (size_t i = 0; i < context.level; i++)
      indent[i] = ' ';
    Test::message<256>(
      "%s `-->  (%#X) %s", indent, (uintptr_t)&component, str);
#endif // #if VERBOSE
#else  // #if ARDUINO
    WHEN_VERBOSE(Test::message<256>(
      "%*s `--> (%#X) %s", context.level, "", (uintptr_t)&component, str));
#endif // #if ARDUINO
  });
}

#if HARDWARE_COMPONENT_ENABLE_IS
void
testIs()
{
  GenericComponent component1;
  WHEN_VERBOSE(Test::message<100>("component1 (%#X)", (uintptr_t)&component1));
  TEST_ASSERT(component1.is<GenericComponent>());
  UNLESS_NATIVE(TEST_ASSERT_UNLESS(component1.is<I2C::Component>()));
  Components components1;
  WHEN_VERBOSE(
    Test::message<100>("components1 (%#X)", (uintptr_t)&components1));
  TEST_ASSERT(components1.is<Components>());
  TEST_ASSERT_UNLESS(components1.is<Component>());
  UNLESS_NATIVE(TEST_ASSERT_UNLESS(components1.is<I2C::Component>()));
}
#endif // #if HARDWARE_COMPONENT_ENABLE_IS

#ifdef ARDUINOJSON_VERSION
void
testFromJsonObject()
{
  WHEN_VERBOSE(cat(__PRETTY_FUNCTION__, "\n"));
  DynamicJsonDocument doc(
#if ESP8266
    ESP.getMaxFreeBlockSize() - sizeof(DynamicJsonDocument)
#else // #if ESP8266
#if NATIVE
    4000
#else  // #if NATIVE
    1000
#endif // #if NATIVE
#endif // #if ESP8266
  );
  // For some reason, zero-copy mode (using writable char json[]) messes
  // everything up... So we stick with copying.
  const char* json =
    "{\"i2c\":{\"type\":\"i2c\",\"bus\":{\"rtc\":{\"type\":\"ds3231\","
    "\"interval\":60000},\"multiplexer-1\":{\"type\":\"tca9548A\",\"address\":"
    "112,\"ports\":[{\"bme-1\":{\"type\":\"bme280\",\"address\":119,"
    "\"interval\":5},\"scd-1\":{\"type\":\"scd30\",\"pressure\":\"bme-1\","
    "\"interval\":5},\"theben-1\":{\"type\":\"theben\",\"interval\":5000}},"
    "{\"bme-2\":{\"type\":\"bme280\",\"address\":119,\"interval\":5000},\"scd-"
    "2\":{\"type\":\"scd30\",\"pressure\":\"bme-2\",\"interval\":5000},"
    "\"theben-2\":{\"type\":\"theben\",\"interval\":5000}}]}}},\"spi\":"
    "{\"type\":\"spi\",\"bus\":{}}}";
  WHEN_VERBOSE(cat("Deserializing ", json, "\n"));
  DeserializationError err = deserializeJson(doc, json);
  WHEN_VERBOSE(cat("after deserializeJson", "\n"));
  doc.shrinkToFit();
  WHEN_VERBOSE(
    Test::message<100>("doc.memoryUsage() = %u, doc.capacity() = %u",
                       doc.memoryUsage(),
                       doc.capacity()));
  {
    char errorMsg[256];
    snprintf_P(errorMsg,
               Utils::arraySize(errorMsg),
               PSTR("Couldn't deserialize Json: %s"),
               err.c_str());
    TEST_ASSERT_UNLESS_MESSAGE(err, errorMsg);
  }
#if VERBOSE
#if ARDUINO
  serializeJson(doc, Serial);
  Serial.println();
#else  // #if ARDUINO
  serializeJson(doc, std::cout);
  std::cout << std::endl;
#endif // #if ARDUINO
#endif // #if VERBOSE
  Component* c = Component::from(doc.as<JsonObject>());
  TEST_ASSERT_NOT_NULL(c);
#if HARDWARE_COMPONENT_ENABLE_IS
  {
#if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
    char tmp[100];
    snprintf(tmp,
             Utils::arraySize(tmp),
             "Should be %s, but is %s",
             Components::classString,
             c->getClassString());
    TEST_ASSERT_TRUE_MESSAGE(c->is<Components>(), tmp);
#else  // #if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
    TEST_ASSERT_TRUE(c->is<Components>());
#endif // #if HARDWARE_COMPONENT_ENABLE_CLASS_STRING
  }
#endif // #if HARDWARE_COMPONENT_ENABLE_IS
  Components& components = *c->as<Components*>();
#if HARDWARE_COMPONENT_ENABLE_IS
  TEST_ASSERT_FALSE(components.is<Component>());
#endif // #if HARDWARE_COMPONENT_ENABLE_IS
  TEST_ASSERT_EQUAL(2, components.size());
#ifdef TwoWire_h
  TEST_ASSERT_NOT_NULL(components[0]);
#if HARDWARE_COMPONENT_ENABLE_IS
  TEST_ASSERT_TRUE(components[0]->is<I2C::Components>());
#endif // #if HARDWARE_COMPONENT_ENABLE_IS
#endif // #ifdef TwoWire_h
}
#endif // #ifdef ARDUINOJSON_VERSION

void
testMeasurementCopy(void)
{
  GenericComponent component1;
  Component::Measurement measurement1(component1);
  measurement1 = 1234U;
  TEST_ASSERT_TRUE_MESSAGE(measurement1.time.epochMilliseconds() > 0,
                           "ms < 0");
  Component::Measurement measurement2 = measurement1;
  TEST_ASSERT_EQUAL_MESSAGE(
    measurement1.time, measurement2.time, "copied time differs");
  TEST_ASSERT_EQUAL_MESSAGE(measurement1, measurement2, "doesn't equal");
  TEST_ASSERT_EQUAL_UINT_MESSAGE((uintptr_t)&measurement1.component(),
                                 (uintptr_t)&measurement2.component(),
                                 "component pointers differ");
#if NATIVE
  std::this_thread::sleep_for(std::chrono::milliseconds(100));
#else  // #if NATIVE
  delay(101);
#endif // #if NATIVE
  measurement2 = 4321U;
  TEST_ASSERT_EQUAL_MESSAGE(
    measurement1.as<unsigned int>(), 1234U, "first value wrong");
  TEST_ASSERT_EQUAL_MESSAGE(
    measurement2.as<unsigned int>(), 4321U, "other value wrong");
  TEST_ASSERT_TRUE_MESSAGE(measurement2.time.epochMilliseconds() -
                               measurement1.time.epochMilliseconds() >=
                             100,
                           "time diff wrong");
}

void
test()
{
  RUN_TEST(testComponentParent);
  RUN_TEST(testTraverse);
  RUN_TEST(testToString);
#if HARDWARE_COMPONENT_ENABLE_IS
  RUN_TEST(testIs);
#endif // #if HARDWARE_COMPONENT_ENABLE_IS
#ifdef ARDUINOJSON_VERSION
  RUN_TEST(testFromJsonObject);
#endif // #ifdef ARDUINOJSON_VERSION
  RUN_TEST(testMeasurementCopy);
}

} // namespace HardwareComponentTest
