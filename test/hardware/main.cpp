#include <unity.h>

#include "HardwareComponentTest.hpp"

void
test()
{
  UNITY_BEGIN();
  HardwareComponentTest::test();
  UNITY_END();
}

#if ARDUINO

#include <Arduino.h>

void
setup()
{
  test();
}

void
loop()
{}

#else // #if ARDUINO

int
main(int argc, char** argv)
{
  test();
}

#endif // #if ARDUINO
