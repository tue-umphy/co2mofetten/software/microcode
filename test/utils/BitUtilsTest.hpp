#include <unity.h>

#include "config/Defaults.hpp"
#include "utils/BitUtils.hpp"
#include "utils/Utils.hpp"

namespace BitUtilsTest {

void
reverseBitsTest()
{
  const uint32_t originals[] = {
    0x0, 0b111, 0b10, 0b1, 0b11110000111100000000000000000000
  };
  const uint32_t reversedOriginals[] = { 0x0,
                                         0b11100000000000000000000000000000,
                                         0b01000000000000000000000000000000,
                                         0b10000000000000000000000000000000,
                                         0b00000000000000000000111100001111 };
  for (size_t i = 0; i < Utils::arraySize(originals); i++) {
    const uint32_t original = originals[i];
    const uint32_t reversedOriginal = reversedOriginals[i];
    const uint32_t reversed = reverseBits(original);
    TEST_ASSERT_EQUAL_HEX32(reversed, reversedOriginal);
  }
}

void
reverseBytesTest()
{
  const uint32_t originals[] = { 0, 0x0000ABCD, 0xDEADBEEF };
  const uint32_t reversedOriginals[] = { 0x0, 0xCDAB0000, 0xEFBEADDE };
  for (size_t i = 0; i < Utils::arraySize(originals); i++) {
    const uint32_t original = originals[i];
    const uint32_t reversedOriginal = reversedOriginals[i];
    const uint32_t reversed = reverseBytes(original);
    TEST_ASSERT_EQUAL_HEX32(reversed, reversedOriginal);
  }
}

void
test()
{
  RUN_TEST(reverseBytesTest);
  RUN_TEST(reverseBitsTest);
}

} // namespace BitUtilsTest
