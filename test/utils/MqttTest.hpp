#include <unity.h>

#include "features/Mqtt.hpp"
#include "utils/Utils.hpp"

#if WITH_MQTT
namespace MqttTest {

using namespace MQTT;

void
testMqttTopics()
{
  const char* topics[] = {
    "first/second/third", "first/second",       "first",        "first/second",
    "first/second",       "first/second/third", "first/second", "first",
    "first/second",       "first/second",
  };
  const char* patterns[] = { "+/second/#", "first/+/#", "#", "second/+", "+/+",
                             "#",          "#",         "#", "#",        "#" };
  const bool matches[] = { true, false, true, false, true,
                           true, true,  true, true,  true };
  for (size_t i = 0; i < Utils::arraySize(matches); i++) {
    const char* topic = topics[i];
    const char* pattern = patterns[i];
    const bool match = matches[i];
    const bool matched = topicMatchesPattern(topic, pattern);
    TEST_ASSERT(matched == match);
  }
}

void
test()
{
  RUN_TEST(testMqttTopics);
}

} // namespace StringUtilsTest

#endif // #if WITH_MQTT
