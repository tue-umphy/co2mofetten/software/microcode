#include <unity.h>

#include "utils/Number.hpp"
#include "utils/Polynomial.hpp"
#include "utils/Test.hpp"

namespace NumberTest {

void
TEST_ASSERT_NUMBER_STORED_LIKE_INT(Number& number)
{
  TEST_ASSERT_EQUAL(Number::Type::SignedInteger, number.type);
  // signed int types
  TEST_ASSERT_TRUE(number.storedLike<int>());
  TEST_ASSERT_TRUE(number.storedLike<long>());
  TEST_ASSERT_TRUE(number.storedLike<long long>());
  TEST_ASSERT_TRUE(number.storedLike<int8_t>());
  TEST_ASSERT_TRUE(number.storedLike<int16_t>());
  TEST_ASSERT_TRUE(number.storedLike<int32_t>());
  TEST_ASSERT_TRUE(number.storedLike<int64_t>());
  // unsigned int types
  TEST_ASSERT_FALSE(number.storedLike<unsigned int>());
  TEST_ASSERT_FALSE(number.storedLike<unsigned long>());
  TEST_ASSERT_FALSE(number.storedLike<unsigned long long>());
  TEST_ASSERT_FALSE(number.storedLike<uint8_t>());
  TEST_ASSERT_FALSE(number.storedLike<uint16_t>());
  TEST_ASSERT_FALSE(number.storedLike<uint32_t>());
  TEST_ASSERT_FALSE(number.storedLike<uint64_t>());
  // floating point types
  TEST_ASSERT_FALSE(number.storedLike<float>());
  TEST_ASSERT_FALSE(number.storedLike<double>());
}

void
TEST_ASSERT_NUMBER_STORED_LIKE_UINT(Number& number)
{
  TEST_ASSERT_EQUAL(Number::Type::UnsignedInteger, number.type);
  // signed int types
  TEST_ASSERT_FALSE(number.storedLike<int>());
  TEST_ASSERT_FALSE(number.storedLike<long>());
  TEST_ASSERT_FALSE(number.storedLike<long long>());
  TEST_ASSERT_FALSE(number.storedLike<int8_t>());
  TEST_ASSERT_FALSE(number.storedLike<int16_t>());
  TEST_ASSERT_FALSE(number.storedLike<int32_t>());
  TEST_ASSERT_FALSE(number.storedLike<int64_t>());
  // unsigned int types
  TEST_ASSERT_TRUE(number.storedLike<unsigned int>());
  TEST_ASSERT_TRUE(number.storedLike<unsigned long>());
  TEST_ASSERT_TRUE(number.storedLike<unsigned long long>());
  TEST_ASSERT_TRUE(number.storedLike<uint8_t>());
  TEST_ASSERT_TRUE(number.storedLike<uint16_t>());
  TEST_ASSERT_TRUE(number.storedLike<uint32_t>());
  TEST_ASSERT_TRUE(number.storedLike<uint64_t>());
  // floating point types
  TEST_ASSERT_FALSE(number.storedLike<float>());
  TEST_ASSERT_FALSE(number.storedLike<double>());
}

void
TEST_ASSERT_NUMBER_STORED_LIKE_FLOAT(Number& number)
{
  TEST_ASSERT_EQUAL(Number::Type::FloatingPoint, number.type);
  // signed int types
  TEST_ASSERT_FALSE(number.storedLike<int>());
  TEST_ASSERT_FALSE(number.storedLike<long>());
  TEST_ASSERT_FALSE(number.storedLike<long long>());
  TEST_ASSERT_FALSE(number.storedLike<int8_t>());
  TEST_ASSERT_FALSE(number.storedLike<int16_t>());
  TEST_ASSERT_FALSE(number.storedLike<int32_t>());
  TEST_ASSERT_FALSE(number.storedLike<int64_t>());
  // unsigned int types
  TEST_ASSERT_FALSE(number.storedLike<unsigned int>());
  TEST_ASSERT_FALSE(number.storedLike<unsigned long>());
  TEST_ASSERT_FALSE(number.storedLike<unsigned long long>());
  TEST_ASSERT_FALSE(number.storedLike<uint8_t>());
  TEST_ASSERT_FALSE(number.storedLike<uint16_t>());
  TEST_ASSERT_FALSE(number.storedLike<uint32_t>());
  TEST_ASSERT_FALSE(number.storedLike<uint64_t>());
  // floating point types
  TEST_ASSERT_TRUE(number.storedLike<float>());
  TEST_ASSERT_TRUE(number.storedLike<double>());
}

typedef unsigned int uint;
typedef unsigned long ulong;
typedef unsigned long long ulonglong;

template<typename SHOULD_BE_TYPE,
         typename IS_TYPE,
         typename enable_if<is_integral<SHOULD_BE_TYPE>::value or
                              is_floating_point<SHOULD_BE_TYPE>::value,
                            int>::type = 0>
void
ASSERT_EQUAL(const SHOULD_BE_TYPE shouldBe,
             const IS_TYPE is,
             const char* message = nullptr)
{
  if (not is_same<SHOULD_BE_TYPE, IS_TYPE>::value) {
    if (message) {
      TEST_ASSERT_EQUAL_MESSAGE(shouldBe, is, message);
    } else {
      TEST_ASSERT_EQUAL(shouldBe, is);
    }
  }
  if (is_integral<SHOULD_BE_TYPE>::value) {
    if (is_signed<SHOULD_BE_TYPE>::value) {
      if (message) {
        TEST_ASSERT_EQUAL_INT64_MESSAGE(shouldBe, is, message);
      } else {
        TEST_ASSERT_EQUAL_INT64(shouldBe, is);
      }
    } else {
      if (message) {
        TEST_ASSERT_EQUAL_UINT64_MESSAGE(shouldBe, is, message);
      } else {
        TEST_ASSERT_EQUAL_UINT64(shouldBe, is);
      }
    }
  } else if (is_floating_point<SHOULD_BE_TYPE>::value) {
    if (is_same<SHOULD_BE_TYPE, float>::value) {
      if (message) {
        if (isnan(shouldBe))
          TEST_ASSERT_FLOAT_IS_NAN_MESSAGE(shouldBe, message);
        else
          TEST_ASSERT_EQUAL_FLOAT_MESSAGE(shouldBe, is, message);
      } else {
        if (isnan(shouldBe))
          TEST_ASSERT_FLOAT_IS_NAN(shouldBe);
        else
          TEST_ASSERT_EQUAL_FLOAT(shouldBe, is);
      }
    } else if (is_same<SHOULD_BE_TYPE, double>::value) {
      if (message) {
        if (isnan(shouldBe))
          TEST_ASSERT_DOUBLE_IS_NAN(shouldBe);
        else
          TEST_ASSERT_EQUAL_DOUBLE(shouldBe, is);
      } else {
        if (isnan(shouldBe))
          TEST_ASSERT_DOUBLE_IS_NAN(shouldBe);
        else
          TEST_ASSERT_EQUAL_DOUBLE(shouldBe, is);
      }
    } else {
      TEST_FAIL_MESSAGE("Strange floating-point type given");
    }
  } else {
    TEST_FAIL_MESSAGE("Strange type given");
  }
}

template<
  typename T,
  typename enable_if<is_integral<T>::value or is_floating_point<T>::value,
                     int>::type = 0>
void
TEST_ASSERT_NUMBER_CONVERSION_YIELDS(Number& number,
                                     const bool success,
                                     T value,
                                     const char* message = nullptr)
{
  T x = 0;
  const bool dumpSuccess = number.dump<T>(x);
  Test::assertBool(
    success, dumpSuccess, message ? message : "wrong dump return value");
  ASSERT_EQUAL(value, x, message ? message : "wrong dump()-result");
  ASSERT_EQUAL(
    value, number.as<T>(), message ? message : "wrong as() return value");
}

void
testNumberPositiveInt(void)
{
  Number number;
  number = 1234;
  TEST_ASSERT_NUMBER_STORED_LIKE_INT(number);
  //
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<int8_t>(number, false, 0, "int8_t");
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<int16_t>(number, true, 1234);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<int32_t>(number, true, 1234);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<int64_t>(number, true, 1234);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<int>(number, true, 1234);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<long>(number, true, 1234);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<long long>(number, true, 1234);
  //
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<uint8_t>(number, false, 0);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<uint16_t>(number, true, 1234);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<uint32_t>(number, true, 1234);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<uint64_t>(number, true, 1234);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<unsigned int>(number, true, 1234);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<unsigned long>(number, true, 1234);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<unsigned long long>(number, true, 1234);
  //
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<float>(number, true, 1234.0F);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<double>(number, true, 1234.0);
}

void
testNumberNegativeInt(void)
{
  Number number;
  number = -1234;
  TEST_ASSERT_NUMBER_STORED_LIKE_INT(number);
  //
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<int8_t>(number, false, 0);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<int16_t>(number, true, -1234);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<int32_t>(number, true, -1234);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<int64_t>(number, true, -1234);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<int>(number, true, -1234);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<long>(number, true, -1234);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<long long>(number, true, -1234);
  //
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<uint8_t>(number, false, 0);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<uint16_t>(number, false, 0);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<uint32_t>(number, false, 0);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<uint64_t>(number, false, 0);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<unsigned int>(number, false, 0);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<unsigned long>(number, false, 0);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<unsigned long long>(number, false, 0);
  //
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<float>(number, true, -1234.0F);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<double>(number, true, -1234.0);
}

void
testNumberUint(void)
{
  const auto value = 72345U;
  Number number;
  number = value;
  TEST_ASSERT_NUMBER_STORED_LIKE_UINT(number);
  //
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<int8_t>(number, false, 0);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<int16_t>(number, false, 0);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<int32_t>(number, true, value);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<int64_t>(number, true, value);
#if ARDUINO_ARCH_AVR
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<int>(number, false, 0, "int");
#else  // #if ARDUINO_ARCH_AVR
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<int>(number, true, value, "int");
#endif // #if ARDUINO_ARCH_AVR
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<long>(number, true, value);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<long long>(number, true, value);
  //
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<uint8_t>(number, false, 0);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<uint16_t>(number, false, 0);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<uint32_t>(number, true, value);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<uint64_t>(number, true, value);
#if ARDUINO_ARCH_AVR
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<unsigned int>(number, false, 0);
#else  // #if ARDUINO_ARCH_AVR
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<unsigned int>(number, true, value);
#endif // #if ARDUINO_ARCH_AVR
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<unsigned long>(number, true, value);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<unsigned long long>(
    number, true, value);
  //
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<float>(number, true, value);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<double>(number, true, value);
}

void
testNumberPositiveDouble(void)
{
  const auto value = 7253421.12423;
  Number number;
  number = value;
  TEST_ASSERT_NUMBER_STORED_LIKE_FLOAT(number);
  //
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<int8_t>(number, false, 0);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<int16_t>(number, false, 0);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<int32_t>(number, true, value);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<int64_t>(number, true, value);
#if ARDUINO_ARCH_AVR
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<int>(number, false, 0);
#else  // #if ARDUINO_ARCH_AVR
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<int>(number, true, value);
#endif // #if ARDUINO_ARCH_AVR
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<long>(number, true, value);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<long long>(number, true, value);
  //
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<uint8_t>(number, false, 0);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<uint16_t>(number, false, 0);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<uint32_t>(number, true, value);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<uint64_t>(number, true, value);
#if ARDUINO_ARCH_AVR
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<unsigned int>(number, false, 0);
#else  // #if ARDUINO_ARCH_AVR
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<unsigned int>(number, true, value);
#endif // #if ARDUINO_ARCH_AVR
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<unsigned long>(number, true, value);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<unsigned long long>(
    number, true, value);
  //
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<float>(number, true, value);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<double>(number, true, value);
}

void
testNumberNegativeFloat(void)
{
  const auto value = -7253421.12423F;
  Number number;
  number = value;
  TEST_ASSERT_NUMBER_STORED_LIKE_FLOAT(number);
  //
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<int8_t>(number, false, 0);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<int16_t>(number, false, 0);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<int32_t>(number, true, value);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<int64_t>(number, true, value);
#if ARDUINO_ARCH_AVR
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<int>(number, false, 0);
#else  // #if ARDUINO_ARCH_AVR
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<int>(number, true, value);
#endif // #if ARDUINO_ARCH_AVR
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<long>(number, true, value);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<long long>(number, true, value);
  //
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<uint8_t>(number, false, 0);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<uint16_t>(number, false, 0);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<uint32_t>(number, false, 0);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<uint64_t>(number, false, 0);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<unsigned int>(number, false, 0);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<unsigned long>(number, false, 0);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<unsigned long long>(number, false, 0);
  //
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<float>(number, true, value);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<double>(number, true, value);
}

#ifndef ARDUINO_ARCH_AVR
void
testNumberLargePositiveDouble(void)
{
  const auto value = 1.234e40;
  Number number;
  number = value;
  TEST_ASSERT_NUMBER_STORED_LIKE_FLOAT(number);
  //
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<int8_t>(number, false, 0);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<int16_t>(number, false, 0);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<int32_t>(number, false, 0);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<int64_t>(number, false, 0);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<int>(number, false, 0);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<long>(number, false, 0);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<long long>(number, false, 0);
  //
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<uint8_t>(number, false, 0);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<uint16_t>(number, false, 0);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<uint32_t>(number, false, 0);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<uint64_t>(number, false, 0);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<unsigned int>(number, false, 0);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<unsigned long>(number, false, 0);
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<unsigned long long>(number, false, 0);
  //
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<float>(
    number,
    false,
#if NATIVE
    std::numeric_limits<float>::quiet_NaN()
#else  // #if NATIVE
    NAN
#endif // #if NATIVE
  );
  TEST_ASSERT_NUMBER_CONVERSION_YIELDS<double>(number, true, value);
}
#endif // #ifndef ARDUINO_ARCH_AVR

void
testNumberComparisonsDouble()
{
  Number number(1234.45);
  TEST_ASSERT_TRUE(number < 1235);
  TEST_ASSERT_TRUE(number <= 1234.45);
  TEST_ASSERT_TRUE(number <= 1234.5);
  TEST_ASSERT_TRUE(number == 1234.45);
  TEST_ASSERT_TRUE(number != 1234);
  TEST_ASSERT_TRUE(number >= 1234.4);
  TEST_ASSERT_TRUE(number >= 1234.45);
  TEST_ASSERT_TRUE(number > 1234.4);
  // other way round
  TEST_ASSERT_TRUE(1235 < number);
  TEST_ASSERT_TRUE(1234.45 <= number);
  TEST_ASSERT_TRUE(1234.5 <= number);
  TEST_ASSERT_TRUE(1234.45 == number);
  TEST_ASSERT_TRUE(1234 != number);
  TEST_ASSERT_TRUE(1234.4 >= number);
  TEST_ASSERT_TRUE(1234.45 >= number);
  TEST_ASSERT_TRUE(1234.4 > number);
}

void
testNumberComparisonsPositiveInt()
{
  Number number(1235);
  TEST_ASSERT_TRUE(number < 1236);
  TEST_ASSERT_TRUE(number <= 1235.45);
  TEST_ASSERT_TRUE(number <= 1235);
  TEST_ASSERT_TRUE(number == 1235);
  TEST_ASSERT_TRUE(number != 1235.34);
  TEST_ASSERT_TRUE(number >= 1235);
  TEST_ASSERT_TRUE(number >= 1234.45);
  TEST_ASSERT_TRUE(number > 1234.4);
  // other way round
  TEST_ASSERT_TRUE(1236 < number);
  TEST_ASSERT_TRUE(1235.45 <= number);
  TEST_ASSERT_TRUE(1235 <= number);
  TEST_ASSERT_TRUE(1235 == number);
  TEST_ASSERT_TRUE(1235.34 != number);
  TEST_ASSERT_TRUE(1235 >= number);
  TEST_ASSERT_TRUE(1234.45 >= number);
  TEST_ASSERT_TRUE(1234.4 > number);
}

void
testNumberComparisonsNegativeInt()
{
  Number number(-1235);
  TEST_ASSERT_TRUE(number > -1236);
  TEST_ASSERT_TRUE(number >= -1235.45);
  TEST_ASSERT_TRUE(number >= -1235);
  TEST_ASSERT_TRUE(number == -1235);
  TEST_ASSERT_TRUE(number != -1235.34);
  TEST_ASSERT_TRUE(number <= -1235);
  TEST_ASSERT_TRUE(number <= -1234.45);
  TEST_ASSERT_TRUE(number < -1234.4);
  // other way round
  TEST_ASSERT_TRUE(-1236 > number);
  TEST_ASSERT_TRUE(-1235.45 >= number);
  TEST_ASSERT_TRUE(-1235 >= number);
  TEST_ASSERT_TRUE(-1235 == number);
  TEST_ASSERT_TRUE(-1235.34 != number);
  TEST_ASSERT_TRUE(-1235 <= number);
  TEST_ASSERT_TRUE(-1234.45 <= number);
  TEST_ASSERT_TRUE(-1234.4 < number);
}

void
testNumberCopy(void)
{
  Number n1(1.234F);
  Number n2 = n1;
  TEST_ASSERT_EQUAL(n1, n2);
  n2 = 4312U;
  TEST_ASSERT_TRUE(n2.storedLike<unsigned int>());
  TEST_ASSERT_FALSE(n1.storedLike<unsigned int>());
  TEST_ASSERT_TRUE(n1.storedLike<float>());
  TEST_ASSERT_EQUAL(n1, 1.234F);
}

void
testNumberToString(void)
{
  Number n;
  char tmp[32] = { 0 };
  TEST_ASSERT_EQUAL_STRING("1.234", (n = 1.234F, n.toString(tmp)));
  TEST_ASSERT_EQUAL_STRING("1234", (n = 1234, n.toString(tmp)));
  TEST_ASSERT_EQUAL_STRING("1234", (n = 1234U, n.toString(tmp)));
  TEST_ASSERT_EQUAL_STRING("-1234", (n = -1234, n.toString(tmp)));
  TEST_ASSERT_EQUAL_STRING("1586962531120",
                           (n = 1586962531120ULL, n.toString(tmp)));
  TEST_ASSERT_EQUAL_STRING("-1586962531120",
                           (n = -1586962531120LL, n.toString(tmp)));
}

void
testPolynomial(void)
{
  {
    Polynomial polynom{ 0.0, 1.0, 2.0, 3.0, 4.0 };
    TEST_ASSERT_EQUAL_FLOAT(0.0, polynom.coefficients[0]);
    TEST_ASSERT_EQUAL_FLOAT(1.0, polynom.coefficients[1]);
    TEST_ASSERT_EQUAL_FLOAT(2.0, polynom.coefficients[2]);
    TEST_ASSERT_EQUAL_FLOAT(3.0, polynom.coefficients[3]);
    TEST_ASSERT_EQUAL_FLOAT(4.0, polynom.coefficients[4]);
    TEST_ASSERT_EQUAL_FLOAT(0.0, polynom(0.0));
    TEST_ASSERT_EQUAL_FLOAT(10.0, polynom(1.0));
    TEST_ASSERT_EQUAL_FLOAT(98.0, polynom(2.0));
  }
  {
    Polynomial polynom(3);
    polynom.coefficients[0] = 0.0;
    polynom.coefficients[1] = 1.0;
    polynom.coefficients[2] = 2.0;
    TEST_ASSERT_EQUAL_FLOAT(0.0, polynom(0.0));
    TEST_ASSERT_EQUAL_FLOAT(3.0, polynom(1.0));
  }
}

void
test(void)
{
  RUN_TEST(testNumberPositiveInt);
  RUN_TEST(testNumberNegativeInt);
  RUN_TEST(testNumberUint);
  RUN_TEST(testNumberPositiveDouble);
  RUN_TEST(testNumberNegativeFloat);
  RUN_TEST(testNumberComparisonsDouble);
  RUN_TEST(testNumberComparisonsPositiveInt);
  RUN_TEST(testNumberComparisonsNegativeInt);
  RUN_TEST(testNumberCopy);
  RUN_TEST(testNumberToString);
  RUN_TEST(testPolynomial);
#ifndef ARDUINO_ARCH_AVR
  RUN_TEST(testNumberLargePositiveDouble);
#endif // #ifndef ARDUINO_ARCH_AVR
}

} // namespace BitUtilsTest
