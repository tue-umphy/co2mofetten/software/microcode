#ifndef ARDUINO_ARCH_AVR
#include <cstddef>
#include <cstdint>
#include <cstdio>
#include <cstring>
#endif // #ifndef ARDUINO_ARCH_AVR

#include <unity.h>

#include "config/Defaults.hpp"
#include "utils/RingBuffer.hpp"
#include "utils/StringUtils.hpp"
#include "utils/Test.hpp"

//! Unit tests for the RingBuffer class
namespace RingBufferTest {

RingBuffer<char, 16> ringBuffer;

#if VERBOSE
template<typename valueType, size_t bufferSize>
void
dump(RingBuffer<valueType, bufferSize>& ringBuffer)
{
  char buf1[256] = { 0 };
  char buf2[256] = { 0 };
  strncat(buf1, "[Size: ", strfree(buf1));
  sprintfcat(buf1, "%u] ", ringBuffer.count());
  sprintfcat(buf2, "%*s", strnlen(buf1), "");
  for (size_t i = 0; i < bufferSize; i++) {
    sprintfcat(buf1,
               i == ringBuffer.getHead()   ? " %02x]"
               : i == ringBuffer.getTail() ? "{%02x "
                                           : " %02x ",
               ringBuffer[i]);
    sprintfcat(buf2,
               i == ringBuffer.getHead()   ? " %2c]"
               : i == ringBuffer.getTail() ? "{%2c "
                                           : " %2c ",
               ringBuffer[i]);
  }
  TEST_MESSAGE(buf1);
  TEST_MESSAGE(buf2);
}
#endif // #if VERBOSE

void
assertContent1(const char* payload, const size_t payloadLength)
{
  TEST_ASSERT(strncmp(payload, "DFQWER", payloadLength) == 0);
}

void
assertContentEmpty(const char* payload, const size_t payloadLength)
{
  TEST_ASSERT(payloadLength == 0);
}

void
test_RingBuffer()
{
  for (size_t n = 0; n < 3; n++) {
    WHEN_VERBOSE(Test::message<100>("Empty:"));
    WHEN_VERBOSE(dump(ringBuffer));
    TEST_ASSERT(ringBuffer.count() == 0);
    TEST_ASSERT(ringBuffer.isEmpty());
    TEST_ASSERT(!ringBuffer.isFull());

    WHEN_VERBOSE(Test::message<100>("Unshifting A:"));
    ringBuffer.unshift('A');
    WHEN_VERBOSE(dump(ringBuffer));
    TEST_ASSERT(ringBuffer.count() == 1);
    TEST_ASSERT(!ringBuffer.isEmpty());
    TEST_ASSERT(!ringBuffer.isFull());

    const char data[] = "ASDFQWER";
    WHEN_VERBOSE(Test::message<100>("Adding '%s'", data));
    ringBuffer.push(data, strlen(data));
    WHEN_VERBOSE(dump(ringBuffer));
    TEST_ASSERT(ringBuffer.count() == 9);
    TEST_ASSERT(!ringBuffer.isEmpty());
    TEST_ASSERT(!ringBuffer.isFull());

    WHEN_VERBOSE(Test::message<100>("Shifting 3 times:"));
    for (size_t i = 0; i < 3; i++) {
#if VERBOSE
      char c =
#endif // #if VERBOSE
        ringBuffer.shift();
      WHEN_VERBOSE(Test::message<100>(" %2c", c));
    }
    WHEN_VERBOSE(Test::message<100>("After shifting 3 times:"));
    WHEN_VERBOSE(dump(ringBuffer));
    TEST_ASSERT(ringBuffer.count() == 6);
    TEST_ASSERT(!ringBuffer.isEmpty());
    TEST_ASSERT(!ringBuffer.isFull());
    ringBuffer.shiftCall(assertContent1);

    const char data2[] = "asdfqwerDFQWER";
    WHEN_VERBOSE(Test::message<100>("Pushing '%s'", data2));
    ringBuffer.push(data2, strlen(data2));
    WHEN_VERBOSE(dump(ringBuffer));
    TEST_ASSERT(ringBuffer.count() == 14);
    TEST_ASSERT(!ringBuffer.isEmpty());
    TEST_ASSERT(!ringBuffer.isFull());

    const char data3[] = "tziuop";
    WHEN_VERBOSE(Test::message<100>("Pushing '%s'", data3));
    ringBuffer.push(data2, strlen(data3));
    WHEN_VERBOSE(dump(ringBuffer));
    TEST_ASSERT(ringBuffer.count() == 16);
    TEST_ASSERT(!ringBuffer.isEmpty());
    TEST_ASSERT(ringBuffer.isFull());

    WHEN_VERBOSE(Test::message<100>("Poping 9 times"));
    for (size_t i = 0; i < 9; i++) {
#if VERBOSE
      char c =
#endif // #if VERBOSE
        ringBuffer.shift();
      WHEN_VERBOSE(Test::message<100>(" %2c", c));
    }
    WHEN_VERBOSE(Test::message<100>(""));
    WHEN_VERBOSE(dump(ringBuffer));
    TEST_ASSERT(ringBuffer.count() == 7);
    TEST_ASSERT(!ringBuffer.isEmpty());
    TEST_ASSERT(!ringBuffer.isFull());

    WHEN_VERBOSE(Test::message<100>("Clearing"));
    ringBuffer.clear();
    WHEN_VERBOSE(dump(ringBuffer));
    TEST_ASSERT(ringBuffer.count() == 0);
    TEST_ASSERT(ringBuffer.isEmpty());
    TEST_ASSERT(!ringBuffer.isFull());
  }
}

void
test()
{
  RUN_TEST(test_RingBuffer);
}

} // namespace RingBufferTest
