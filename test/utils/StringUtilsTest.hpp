#include <unity.h>

#include "utils/Logging.hpp"
#include "utils/StringUtils.hpp"
#include "utils/Utils.hpp"

namespace StringUtilsTest {

void
testStrrev()
{
  const char* inputStrings[] = { "hello", "dafuq", "asdf" };
  const char* outputStrings[] = { "olleh", "qufad", "fdsa" };
  for (size_t i = 0; i < Utils::arraySize(inputStrings); i++) {
    const char* inputString = inputStrings[i];
    const char* outputString = outputStrings[i];
    {
      char output[16];
      strrev(inputString, output);
      TEST_ASSERT_EQUAL_STRING(outputString, output);
    }
    {
      char tmp[16] = { 0 };
      strcpy(tmp, inputString);
      TEST_ASSERT_EQUAL_STRING(outputString, strrev(tmp));
    }
  }
}

void
testStrfree()
{
  {
    char str[64] = "This is a string with 36 characters.";
    TEST_ASSERT_EQUAL_UINT8(strfree(str), 64 - 36 - 1);
  }
  {
    char str[4] = "asd";
    TEST_ASSERT_EQUAL_UINT8(strfree(str), 0);
  }
  {
    char str[10] = "12345678";
    TEST_ASSERT_EQUAL_UINT8(strfree(str), 1);
  }
  {
    char str[10] = { 0 };
    TEST_ASSERT_EQUAL_UINT8(strfree(str), Utils::arraySize(str) - 1);
  }
}

void
testLinuxGlobMatch(void)
{
  const char* strings[] = { "asdf", "asdf", "asdf", "asdf",
                            "asdf", "asdf", "asdf", "asdf" };
  const char* patterns[] = { "*",    "?sdf", "a?df", "aadf",
                             "??d*", "*sdf", "*sd*", "*shit" };
  const bool shouldMatch[] = {
    true, true, true, false, true, true, true, false
  };
  for (size_t i = 0; i < Utils::arraySize(strings); i++) {
    const bool matches = linuxGlobMatch(patterns[i], strings[i]);
    LOG(General,
        Debug,
        cat("linuxGlobMatch(\"",
            patterns[i],
            "\",\"",
            strings[i],
            "\") = ",
            matches ? "true" : "false",
            " should be ",
            shouldMatch[i] ? "true" : "false"));
    if (shouldMatch[i])
      TEST_ASSERT_TRUE(matches)
    else
      TEST_ASSERT_FALSE(matches)
  }
}

void
test()
{
  RUN_TEST(testStrrev);
  RUN_TEST(testStrfree);
  RUN_TEST(testLinuxGlobMatch);
}

} // namespace StringUtilsTest
