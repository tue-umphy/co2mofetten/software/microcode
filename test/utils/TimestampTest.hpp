#include "utils/Timestamp.hpp"

#if ARDUINO
#include <Time.h>
#else // #if ARDUINO
#include <chrono>
#include <thread>
#endif // #if ARDUINO

namespace TimestampTest {

void
testTimestamp(void)
{
#if ARDUINO
  setTime(1586504465);
#if TIMELIB_ENABLE_MILLIS
  uint32_t milliSecondsNow = 0;
  unsigned long secondsNow = now(milliSecondsNow);
#else  // #if TIMELIB_ENABLE_MILLIS
  unsigned int milliSecondsNow = 0;
  unsigned long secondsNow = now();
#endif // #if TIMELIB_ENABLE_MILLIS
#else  // #if ARDUINO
  auto timeNow = std::chrono::system_clock::now();
  auto timeSinceEpoch = timeNow.time_since_epoch();
  auto secondsSinceEpoch =
    std::chrono::duration_cast<std::chrono::seconds>(timeSinceEpoch);
  unsigned long secondsNow =
    std::chrono::duration_cast<std::chrono::seconds>(timeSinceEpoch).count();
  unsigned int milliSecondsNow =
    std::chrono::duration_cast<std::chrono::milliseconds>(timeSinceEpoch -
                                                          secondsSinceEpoch)
      .count();
#endif // #if ARDUINO
  Timestamp timestamps[] = {
    Timestamp(),
    { 1, 3 },
    Timestamp(1, 1234),
    { 5, 65050 },
    Timestamp(25, 100),
    Timestamp(1592564413, 123),
    Timestamp(1591999999, 999),
    Timestamp(1592000008, 876),
    Timestamp(1592008008, 123),
  };
  const unsigned long timestampSeconds[] = {
    secondsNow, 1, 2, 70, 25, 1592564413, 1591999999, 1592000008, 1592008008
  };
  const unsigned int timestampMilliSeconds[] = {
    (unsigned int)milliSecondsNow, 3, 234, 50, 100, 123, 999, 876, 123
  };
  const char* timestampToString[] = {
    nullptr,         "1003",          "2234",          "70050",        "25100",
    "1592564413123", "1591999999999", "1592000008876", "1592008008123"
  };
  for (size_t i = 0; i < Utils::arraySize(timestamps); i++) {
    auto& timestamp = timestamps[i];
    TEST_ASSERT_TRUE(timestamp.storedLike<unsigned int>());
    auto seconds = timestampSeconds[i];
    auto milliSeconds = timestampMilliSeconds[i];
    TEST_ASSERT_EQUAL(seconds, timestamp.epochSeconds());
    TEST_ASSERT_EQUAL(milliSeconds, timestamp.epochMillisecondsFraction());
    if (timestampToString[i]) {
      char str[16] = { 0 };
      TEST_ASSERT_EQUAL_STRING(timestampToString[i], timestamp.toString(str));
    }
  }
}

void
test(void)
{
  RUN_TEST(testTimestamp);
}

}; // namespace TimestampTest
