#include <cmath>
#include <unity.h>

#include "utils/Test.hpp"
#include "utils/UnitConversion.hpp"
#include "utils/Utils.hpp"

namespace UnitConversionTest {

void
testConverterCollection()
{
  const size_t nConverters = 5;
  UnitConversion::ConverterCollection<nConverters> converterCollection;
  converterCollection.add(
    "Pa", "bar", [](const double x) { return x * 0.00001; });
  converterCollection.add(
    "bar", "psi", [](const double x) { return x * 14.50377; });

  {
    const char* testGivenUnits[] = { "Pa", "Pa",  "hPa", "bullshit",
                                     "Pa", "hPa", "kPa" };
    const char* testTargetUnits[] = { "hPa", "Pa",   "Pa", "asdf",
                                      "bar", "cbar", "psi" };
    const double testInputValues[] = { 100000.0, 1.0,    1000.0, 1.0,
                                       100000.0, 1000.0, 100.0 };
    const double testOutputValues[] = { 1000.0, 1.0,   100000.0, 1.0,
                                        1.0,    100.0, 14.50377 };
    const bool testReturn[] = { true, true, true, false, true, true, true };
    for (size_t i = 0; i < Utils::arraySize(testGivenUnits); i++) {
      double value = testInputValues[i];
      const bool ret = converterCollection.convert(
        value, testGivenUnits[i], testTargetUnits[i]);
      WHEN_VERBOSE(
        Test::message<512>("ConverterCollection.convert(%f,\"%s\",\"%s\") "
                           "= %s (should be %s) and "
                           "changes first argument to %f (should be %f)",
                           testInputValues[i],
                           testGivenUnits[i],
                           testTargetUnits[i],
                           ret ? "true" : "false",
                           testReturn[i] ? "true" : "false",
                           value,
                           testOutputValues[i]));
      TEST_ASSERT_EQUAL(ret, testReturn[i]);
      TEST_ASSERT_FLOAT_WITHIN(
        testOutputValues[i] * 1e-4, value, testOutputValues[i]);
    }
  }
}

void
testSiConversionExponent()
{
  const char* givenUnits[] = { "Pa", "m", "dl", "kg", "cl", "bar" };
  const char* targetUnits[] = { "hPa", "mm", "l", "g", "ml", "hPa" };
  const bool returnResults[] = { true, true, true, true, true, false };
  const int16_t exponentResults[] = { -2, 3, -1, 3, 1, 0 };
  for (size_t i = 0; i < Utils::arraySize(givenUnits); i++) {
    int16_t exponent = 0;
    const char* givenUnit = givenUnits[i];
    const char* targetUnit = targetUnits[i];
    const int16_t exponentResult = exponentResults[i];
    const bool returnResult = returnResults[i];
    const bool ret =
      UnitConversion::siConversionExponent(givenUnit, targetUnit, exponent);
    WHEN_VERBOSE(Test::message<512>(
      "siConversionExponent(\"%s\", \"%s\", exponent) returns %s "
      "(should be %s) and sets exponent to %d (should be %d), "
      "which means a conversion factor of %f (should be %f)",
      givenUnit,
      targetUnit,
      ret ? "true" : "false",
      returnResult ? "true" : "false",
      exponent,
      exponentResult,
      std::pow(10.0, exponent),
      std::pow(10.0, exponentResult)));
    TEST_ASSERT_EQUAL(ret, returnResult);
    TEST_ASSERT_EQUAL(exponent, exponentResult);
  }
}

void
test()
{
  RUN_TEST(testSiConversionExponent);
  RUN_TEST(testConverterCollection);
}

} // namespace UnitConversionTest
