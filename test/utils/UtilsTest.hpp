#include <unity.h>

#include "utils/Utils.hpp"

namespace UtilsTest {

void
test_indexInArray_int()
{
  int array[] = { 5, 1, 6, -5 };
  TEST_ASSERT_EQUAL(Utils::indexInArray(5, array), 0);
  TEST_ASSERT_EQUAL(Utils::indexInArray(5, array), 0);
  for (size_t i = 0; i < Utils::arraySize(array); i++)
    TEST_ASSERT_EQUAL(Utils::indexInArray(array[i], array), i);
  TEST_ASSERT_EQUAL(Utils::indexInArray(-8, array), -1);
}

void
test_indexInArray_string()
{
  const char* stringArray[] = { "first", "second", "third" };
  for (size_t i = 0; i < Utils::arraySize(stringArray); i++)
    TEST_ASSERT_EQUAL(Utils::indexInArray(stringArray[i], stringArray),
                      (int)i);
  TEST_ASSERT_EQUAL(Utils::indexInArray("not there", stringArray), -1);
  char dummyString[] = "secondSubstitute";
  stringArray[1] = (const char*)dummyString;
  TEST_ASSERT_EQUAL(Utils::indexInArray("second", stringArray), -1);
  TEST_ASSERT_EQUAL(Utils::indexInArray("secondSubstitute", stringArray), 1);
  char secondDummyString[20] = "second";
  strcat(secondDummyString, "Substitute");
  TEST_ASSERT_EQUAL(Utils::indexInArray(secondDummyString, stringArray), 1);
  char thirdDummyString[20] = "second"; // not long enough
  strcat(thirdDummyString, "Bullshit");
  TEST_ASSERT_EQUAL(Utils::indexInArray(thirdDummyString, stringArray), -1);
}

void
test()
{
  RUN_TEST(test_indexInArray_int);
  RUN_TEST(test_indexInArray_string);
}

} // namespace UtilsTest
