#include <unity.h>

#include "BitUtilsTest.hpp"
#include "MqttTest.hpp"
#include "NumberTest.hpp"
#include "RingBufferTest.hpp"
#include "StringUtilsTest.hpp"
#include "TimestampTest.hpp"
#include "UnitConversionTest.hpp"
#include "UtilsTest.hpp"

void
test()
{
  UNITY_BEGIN();
  UtilsTest::test();
  StringUtilsTest::test();
  BitUtilsTest::test();
  RingBufferTest::test();
  UnitConversionTest::test();
  TimestampTest::test();
  NumberTest::test();
#if WITH_MQTT
  MqttTest::test();
#endif // #if WITH_MQTT
  UNITY_END();
}

#if ARDUINO

#include <Arduino.h>

void
setup()
{
  test();
}

void
loop()
{}

#else // #if ARDUINO

int
main(int argc, char** argv)
{
  test();
}

#endif // #if ARDUINO
